/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.engines;

import com.github.bhlangonijr.chesslib.CastleRight;
import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Rank;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import com.github.bhlangonijr.chesslib.move.MoveGenerator;
import com.github.bhlangonijr.chesslib.move.MoveGeneratorException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.director.AbstractGameEvents;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.controls.GameResultPopup;
import org.pidome.games.chess.utils.controls.PromotionPopup;

/**
 * Abstract class for engine implementations.
 *
 * @author John
 */
public abstract class AbstractGameEngine extends AbstractGameEvents {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(AbstractGameEngine.class);

    /**
     * The game engine type.
     */
    public enum GameEngineType {
        /**
         * Spectating a game.
         */
        SPECTATOR,
        /**
         * Human versus human.
         */
        HUMAN_VS_HUMAN,
        /**
         * Human versus a CPU.
         */
        HUMAN_VS_CPU,
        /**
         * The game editor used by the scene editor.
         */
        EDITOR,
        /**
         * Game engine that doesn't do a thing.
         */
        VOID;
    }

    /**
     * Allowed moves in this game.
     */
    protected List<Move> allowedMoves = new ArrayList<>();

    /**
     * The game options.
     */
    private AbstractGenericGameOptions gameOptions;

    /**
     * Registers for events.
     */
    public final void registerforEvents() {
        GameEventsPublisher.registerForEvents(this.gameOptions.getGameId(), this);
    }

    /**
     * Removes events registration.
     */
    public final void unregisterForEvents() {
        GameEventsPublisher.unRegisterForEvents(this.gameOptions.getGameId(), this);
    }

    /**
     * Starts a game.
     */
    protected final void startGame() {
        this.getGameBoard().setEnableEvents(true);
    }

    /**
     * Ends a game.
     */
    public final void endGame() {
        this.getGameBoard().setEnableEvents(false);
        this.getGameBoard().clear();
        this.destroy();
    }

    /**
     * Return the game options.
     *
     * @return The game options.
     */
    public AbstractGenericGameOptions getGameOptions() {
        return this.gameOptions;
    }

    /**
     * Set the game options.
     *
     * @param gameOptions The game options.
     */
    public void setGameOptions(final AbstractGenericGameOptions gameOptions) {
        this.gameOptions = gameOptions;
    }

    /**
     * Called when an engine is requested to destroy itself with any references.
     */
    protected abstract void destroy();

    /**
     * Checks the status of the king after a move.
     *
     * @param event The move event.
     */
    protected void kingAttackStatus(final Move event, final boolean interactive) {
        if (this.getGameBoard().isStaleMate()) {
            this.getGameContainer().highlightSquare(getCurrentSideKingSquare(), HighlightType.STALE_MATE);
            if (interactive) {
                GameResultPopup winPopup = new GameResultPopup(this.getGameContainer(), null);
                winPopup.display();
            }
        } else if (this.getGameBoard().isKingAttacked()) {
            if (this.getGameBoard().isMated()) {
                this.getGameContainer().highlightSquare(getCurrentSideKingSquare(), HighlightType.CHECK_MATE);
                if (interactive) {
                    GameResultPopup winPopup = new GameResultPopup(this.getGameContainer(), getOppositeSide());
                    winPopup.display();
                }
            } else {
                this.getGameContainer().highlightSquare(getCurrentSideKingSquare(), HighlightType.CHECK);
            }
        } else {
            this.getGameContainer().highlightSquare(event.getFrom(), HighlightType.NONE);
            this.getGameContainer().highlightSquare(getOpponentSideKingSquare(), HighlightType.NONE);
        }
    }

    /**
     * Generates legal moves.
     */
    protected void generateLegalMoves() {
        try {
            allowedMoves = MoveGenerator.generateLegalMoves(this.getGameBoard());
            LOG.debug("Generated moves: {}", allowedMoves);
        } catch (MoveGeneratorException ex) {
            LOG.error("Could not generate legal moves: [{}]", ex.getMessage(), ex);
        }
    }

    /**
     * Gets the king square of the current side to move.
     *
     * @return square with king of current side to move.
     */
    protected Square getCurrentSideKingSquare() {
        return this.getGameBoard().getKingSquare(this.getGameBoard().getSideToMove());
    }

    /**
     * Gets the king square of the current side to move.
     *
     * @return square with king of current side to move.
     */
    protected Square getOpponentSideKingSquare() {
        return this.getGameBoard().getKingSquare(this.getGameBoard().getSideToMove().equals(Side.WHITE) ? Side.BLACK : Side.WHITE);
    }

    /**
     * If the square equals the current player turn side.
     *
     * @param square The square to check the turn side for.
     * @return true when the square contains a piece of the current turn of the
     * player.
     */
    protected boolean isCurrentTurn(final Square square) {
        return this.getGameBoard().getSideToMove().equals(this.getGameBoard().getPiece(square).getPieceSide());
    }

    /**
     * Returns the opposite side in respect to who is in current turn at method
     * request.
     *
     * @return The opposite side's color.
     */
    protected Side getOppositeSide() {
        return this.getGameBoard().getSideToMove().equals(Side.WHITE) ? Side.BLACK : Side.WHITE;
    }

    /**
     * Handle a move to perform.
     *
     * This is default handling based on a default rule set and is interactive.
     *
     * @param move The move to perform.
     */
    protected void performMove(final Move move) {
        performMove(move, true);
    }

    /**
     * Handle a move to perform.
     *
     * This is default handling based on a default rule set.
     *
     *
     * @param move The move to perform.
     * @param interactive If the move is interactive, if so, display popup when
     * needed.
     */
    protected void performMove(final Move move, boolean interactive) {
        if (this.getGameBoard().getContext().isCastleMove(move)) {
            LOG.debug("Performing castling at side [{}] of type [{}]", this.getGameBoard().getSideToMove(), this.getGameBoard().getContext().isKingSideCastle(move) ? CastleRight.KING_SIDE : CastleRight.QUEEN_SIDE);
            this.getGameContainer().moveCastling(
                    this.getGameBoard().getSideToMove(),
                    this.getGameBoard().getContext().isKingSideCastle(move) ? CastleRight.KING_SIDE : CastleRight.QUEEN_SIDE);
        } else {
            if (isPromotionMove(move)) {
                if (interactive) {
                    FXUtils.runOnFXThread(() -> {
                        final PromotionPopup popup = new PromotionPopup();
                        popup.requestPromotion(getPromotionMoves(move));
                        popup.setOnAction(() -> {
                            LOG.debug("Selected move for promotion [{}]", popup.getSelectedPromotion());
                            performPromotion(popup.getSelectedPromotion());
                        });
                        popup.setOnCancel(() -> {
                            popup.close();
                        });
                        popup.display();
                    });
                } else {
                    performPromotion(move);
                }
            } else {
                performIfEnpassantMove(move);
                performNormalMove(move);
            }
        }
    }

    /**
     * Check if the move implied or done is legal.
     *
     * @return true when legal, false otherwise.
     */
    protected boolean isLegalStep(final Move move) {
        boolean allowedMove = this.allowedMoves != null ? this.allowedMoves.contains(move) : false;
        return allowedMove ? allowedMove : isPromotionMove(move);
    }

    /**
     * Check if an intended move is a piece promotion move.
     *
     * @return true when move is an intended promotion move.
     */
    protected boolean isPromotionMove(final Move move) {
        if (move.getFrom().getRank().equals(Rank.RANK_7) || move.getFrom().getRank().equals(Rank.RANK_2)) {
            for (Move knownMove : this.allowedMoves) {
                if (move.getFrom().equals(knownMove.getFrom())
                        && move.getTo().equals(knownMove.getTo())
                        && !knownMove.getPromotion().equals(Piece.NONE)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the promotion moves eligible for the current move.
     *
     * @param move The move for which the promotion moves should be.
     * @return The promotion moves.
     */
    protected List<Move> getPromotionMoves(final Move move) {
        List<Move> moves = new ArrayList<>();
        for (Move knownMove : this.allowedMoves) {
            if (move.getFrom().equals(knownMove.getFrom())
                    && move.getTo().equals(knownMove.getTo())
                    && !knownMove.getPromotion().equals(Piece.NONE)) {
                moves.add(knownMove);
            }
        }
        return moves;
    }

    /**
     * Performs a promotion move.
     *
     * @param move The promotion move to perform.
     */
    protected void performPromotion(final Move move) {
        this.getGameContainer().selectPiece(move.getFrom(), false);
        this.getGameContainer().promotionMove(move);
    }

    /**
     * Performs a normal move.
     *
     * @param move The move to perform.
     */
    protected void performNormalMove(final Move move) {
        this.getGameContainer().selectPiece(move.getFrom(), false);
        this.getGameContainer().movePiece(move);
    }

    /**
     * Checks if a move is an enpassant move.
     *
     * @param move The move to check.
     */
    protected void performIfEnpassantMove(final Move move) {
        // get the piece type moved
        final Piece piece = this.getGameBoard().getPiece(move.getFrom());
        if (piece.equals(Piece.BLACK_PAWN) || piece.equals(Piece.WHITE_PAWN)) {
            /// The engine already has created legal moves, we now than only have to check if the rank is different (diagonal move) and the target
            /// square has no piece
            if (!move.getFrom().getFile().equals(move.getTo().getFile()) && this.getGameBoard().getPiece(move.getTo()).equals(Piece.NONE)) {
                final Square targetSquare;
                if (piece.equals(Piece.BLACK_PAWN)) {
                    targetSquare = Square.fromValue(
                            move.getTo().getFile().getNotation()
                            + String.valueOf(Integer.valueOf(move.getTo().getRank().getNotation()) + 1)
                    );
                } else {
                    targetSquare = Square.fromValue(
                            move.getTo().getFile().getNotation()
                            + String.valueOf(Integer.valueOf(move.getTo().getRank().getNotation()) - 1)
                    );
                }
                LOG.debug("Performed en passant move [{}], taking piece from square [{}]", move, targetSquare);
                this.getGameContainer().takeEnpassantTarget(targetSquare);
            }
        }
    }

}
