/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.engines;

/**
 * Enum for highlighting squares.
 *
 * @author johns
 */
public enum HighlightType {

    /**
     * Removes any highlighting on a square.
     */
    NONE(false),
    /**
     * When a square is selected.
     */
    SELECT(false),
    /**
     * Move highlight.
     */
    MOVE(false),
    /**
     * A check.
     */
    CHECK(true),
    /**
     * Mate highlight.
     */
    CHECK_MATE(true),
    /**
     * Stalemate highlight.
     */
    STALE_MATE(true);

    /**
     * If the mark is of attacking type.
     */
    private final boolean attackMark;

    /**
     * Enum constructor.
     *
     * @param isAttackMark If the mark is applied on a piece which is attacked.
     */
    private HighlightType(final boolean isAttackMark) {
        this.attackMark = isAttackMark;
    }

    /**
     * If the mark is of attacking type.
     *
     * @return True when mark is attacking type.
     */
    public final boolean isAttackMark() {
        return this.attackMark;
    }

}
