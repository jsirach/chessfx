/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.engines;

import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.events.SquareIntentEvent;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.resources.GameEngine;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * A default game of chess engine.
 *
 * The game object decides when a game is ended. If an engine decides this the
 * result is unpredictable.
 *
 * @author John
 */
@GameEngine(
        type = AbstractGameEngine.GameEngineType.HUMAN_VS_HUMAN,
        i18nName = I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_LOCAL,
        mode = GameOptions.GamePlayMode.VERSUS
)
public class LocalPlayModeEngine extends AbstractGameEngine {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(LocalPlayModeEngine.class);

    /**
     * The currently selected piece.
     */
    private final ObjectProperty<Square> currentSquare = new SimpleObjectProperty<>(Square.NONE);

    /**
     * When a move is intended.
     *
     * This method should finish the move intent. Minimal requirement is the
     * move type MOVE is included.
     *
     * @param move The move.
     * @param moveTypes The move types included.
     */
    @Override
    public void onPieceMoveIntent(final Move move, final List<MoveEvent.MoveType> moveTypes) {
        this.handlePieceMoveIntent(move, moveTypes, true);
    }

    /**
     * Handles piece move intents.
     *
     * @param move The move intended to make.
     * @param moveTypes The move types.
     * @param interactive when required performs frontend interaction if set to
     * true.
     */
    protected void handlePieceMoveIntent(final Move move, final List<MoveEvent.MoveType> moveTypes, boolean interactive) {
        if (moveTypes.contains(MoveEvent.MoveType.MOVE)) {
            if (isLegalStep(move)) {
                performMove(move, true);
            }
        }
    }

    /**
     * When a piece has moved on the board.This event will never have a capture
     * event.
     *
     * @param event The move event of the piece.
     * @param moveTypes The move types done in this single move.
     */
    @Override
    public void onPieceMoved(final Move event, final List<MoveEvent.MoveType> moveTypes) {
        if (moveTypes.contains(MoveEvent.MoveType.MOVE)) {
            this.kingAttackStatus(event, true);
            generateLegalMoves();
        }
    }

    /**
     * When a move is undone.
     *
     * @param move The move that is the undo move.
     */
    @Override
    public void onUndoMove(final Move move) {
        /// Not used now.
    }

    /**
     * When a piece is selected on the board.
     *
     * @param square The square selected.
     */
    @Override
    public void onSquareSelected(final Square square) {
        boolean correctTurn = isCurrentTurn(square);
        LOG.debug("Selected square: [{}], is turn [{}], selected turn square [{}]", square, this.getGameBoard().getSideToMove(), correctTurn);
        if (correctTurn) {
            if (this.currentSquare.getValue() != null) {
                this.getGameContainer().selectPiece(this.currentSquare.getValue(), false);
            }
            this.currentSquare.setValue(square);
            this.getGameContainer().selectPiece(square, true);
        }
    }

    /**
     * When a piece is deselected on the board.
     *
     * @param square The square that has been deselected.
     */
    @Override
    public void onSquareDeSelected(final Square square) {
        LOG.debug("Deselected square: [{}]", square);
        this.currentSquare.setValue(Square.NONE);
        this.getGameContainer().selectPiece(square, false);
    }

    /**
     * Changes listening for events in game cycles.
     *
     * @param event The event dispatched.
     */
    @Override
    public void onGameStateChangeEvent(final GameStateEvent event) {
        switch (event.getSource()) {
            case RESET:
                this.getGameContainer().prepareGame();
                break;
            case READY:
                generateLegalMoves();
                break;
        }
    }

    /**
     * An event indication an intended action with a square.
     *
     * @param squareIntentEvent The intent event.
     */
    @Override
    public void onSquareIntentEvent(final SquareIntentEvent squareIntentEvent) {
        switch (squareIntentEvent.getSquareIntent()) {
            case OVER:
                if (this.allowedMoves != null && !this.currentSquare.getValue().equals(Square.NONE)) {
                    final Move move = new Move(this.currentSquare.getValue(), squareIntentEvent.getSource());
                    if (isLegalStep(move)) {
                        this.getGameContainer().highlightSquare(squareIntentEvent.getSource(), HighlightType.MOVE);
                    }
                }
                break;
            case OUT:
                if (squareIntentEvent.getCurrentHighlightType().equals(HighlightType.MOVE)) {
                    this.getGameContainer().highlightSquare(squareIntentEvent.getSource(), HighlightType.NONE);
                }
                break;
            case SELECT:

                break;
            case DE_SELECT:

                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void destroy() {
        this.allowedMoves = new ArrayList<>();
    }

}
