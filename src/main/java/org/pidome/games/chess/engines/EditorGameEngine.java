/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.engines;

import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import java.util.List;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.events.SquareIntentEvent;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.resources.GameEngine;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * Game engine used in the editor.
 *
 * It swallows all events, in the editor the game engine is there for
 * compatibility reasons. Pieces and boards used are only there for positioning
 * references.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
@GameEngine(
        type = AbstractGameEngine.GameEngineType.EDITOR,
        i18nName = I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_LOCAL,
        mode = GameOptions.GamePlayMode.EDITOR
)
public class EditorGameEngine extends AbstractGameEngine {

    @Override
    protected void destroy() {
        //Not needed
    }

    @Override
    public void onPieceMoveIntent(Move move, List<MoveEvent.MoveType> moveTypes) {
        //Nope
    }

    @Override
    public void onPieceMoved(Move move, List<MoveEvent.MoveType> moveTypes) {
        // It's fine in this mode not to do anything.
    }

    @Override
    public void onUndoMove(Move move) {
        // It's fine in this mode not to do anything.
    }

    @Override
    public void onSquareSelected(Square square) {
        // also not used.
    }

    @Override
    public void onSquareDeSelected(Square square) {
        /// nope.
    }

    @Override
    public void onGameStateChangeEvent(GameStateEvent gameState) {
        // Not used.
    }

    @Override
    public void onSquareIntentEvent(SquareIntentEvent squareIntentEvent) {
        // nope.
    }

}
