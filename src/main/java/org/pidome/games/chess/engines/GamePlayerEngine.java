/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.engines;

import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.game.Game;
import com.github.bhlangonijr.chesslib.game.GameResult;
import com.github.bhlangonijr.chesslib.move.Move;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.resources.GameEngine;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.controls.GameResultPopup;

/**
 * An engine which is capable of re-playing a game.
 *
 * @author John
 */
@GameEngine(
        type = AbstractGameEngine.GameEngineType.SPECTATOR,
        i18nName = I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_PLAYER,
        mode = GameOptions.GamePlayMode.PLAYER
)
public class GamePlayerEngine extends LocalPlayModeEngine {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(GamePlayerEngine.class);

    /**
     * Which game to play.
     */
    private int gameIndex = 0;

    /**
     * The current game being played.
     */
    private ChessGameModel chessGame;

    /**
     * List of moves to play.
     */
    private LinkedList<Move> playMoves = new LinkedList<>();

    /**
     * The index in the playlist.
     */
    private int playIndex = 0;

    /**
     * the delay of playing the next piece.
     */
    private final int piecePlaysecondsDelay = 5;

    /**
     * If playing or not.
     */
    private boolean playing = Boolean.FALSE;

    /**
     * for delayed executions of moves.
     */
    private ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();

    /**
     * When a piece has moved on the board.
     *
     * This event will never have a capture event.
     *
     * @param event The move event of the piece.
     */
    @Override
    public void onPieceMoved(final Move event, final List<MoveEvent.MoveType> moveTypes) {
        if (moveTypes.contains(MoveEvent.MoveType.MOVE)) {
            this.kingAttackStatus(event, false);
            scheduleItem(() -> {
                if (playing) {
                    moveNext();
                }
            }, piecePlaysecondsDelay);
        }
    }

    /**
     * Changes listening for events in game cycles.
     *
     * @param event The event dispatched.
     */
    @Override
    public void onGameStateChangeEvent(final GameStateEvent event) {
        switch (event.getSource()) {
            case LOADED:
                prepareNextGame();
                break;
            case RESET:
                this.getGameContainer().prepareGame();
                break;
            case READY:
                play();
                break;
            case ENDED:
                if (playing) {
                    final GameResultPopup winPopup = new GameResultPopup(this.getGameContainer(),
                            chessGame.getResult().equals(GameResult.BLACK_WON) ? Side.BLACK
                            : chessGame.getResult().equals(GameResult.WHITE_WON) ? Side.WHITE : null);
                    winPopup.display();
                    if (hasNextGame()) {
                        scheduleItem(() -> {
                            winPopup.close();
                            goNextGame();
                        }, 5);
                    }
                } else {
                    goNextGame();
                }
                break;
            case UNLOADING:
                destroy();
                break;
            case GAME_PLAYER_PLAY:
                play();
                break;
            case GAME_PLAYER_PAUSE:
                pause();
                break;
            case GAME_PLAYER_NEXT:
                playNext();
                break;
            case GAME_PLAYER_PREVIOUS:
                playPrevious();
                break;
        }
    }

    /**
     * Pause playing.
     */
    private void pause() {
        playing = false;
    }

    /**
     * Play.
     */
    private void play() {
        playing = Boolean.TRUE;
        moveNext();
    }

    /**
     * Play next in line.
     */
    private void playNext() {
        if (hasNextGame()) {
            pause();
            GameEventsPublisher.publishGameState(
                    new GameStateEvent(this.getGameOptions().getGameId(), GameStateEvent.GameState.ENDED),
                    0);
        }
    }

    /**
     * Play the previous game.
     */
    private void playPrevious() {
        if (hasPreviousGame()) {
            pause();
            GameEventsPublisher.publishGameState(
                    new GameStateEvent(this.getGameOptions().getGameId(), GameStateEvent.GameState.ENDED),
                    0);
        }
    }

    /**
     * Navigate to the next game.
     */
    private void goNextGame() {
        gameIndex++;
        prepareNextGame();
        this.getGameContainer().resetGame(true);
    }

    /**
     * {@inheritDoc}
     */
    protected void destroy() {
        super.destroy();
        if (scheduledExecutor != null) {
            scheduledExecutor.shutdownNow();
            scheduledExecutor = null;
        }
    }

    /**
     * If there is a next game available.
     *
     * @return true if a new game is available.
     */
    private boolean hasNextGame() {
        return gameIndex < this.getGameContainer().getPreloadedGames().size();
    }

    /**
     * Checks if there is a game before this one.
     *
     * @return If there is a game before this one.
     */
    private boolean hasPreviousGame() {
        return gameIndex > 0;
    }

    /**
     * Prepare the next game to be played.
     */
    private void prepareNextGame() {
        if (gameIndex < this.getGameContainer().getPreloadedGames().size()) {
            playIndex = 0;
            try {
                playMoves.clear();
                chessGame = this.getGameContainer().getPreloadedGames().get(gameIndex);

                Game game = chessGame.getGame();
                game.loadMoveText();
                this.playMoves = game.getHalfMoves();

                System.out.println("Engine: " + chessGame.getPlayerWhite().nameProperty());

                this.getGameOptions().currentGameProperty().setValue(chessGame);

            } catch (NullPointerException ex) {
                LOG.error("Could not load play loaded game", ex);
            } catch (Exception ex) {
                LOG.error("Could not load play loaded game", ex);
            }
        }
    }

    /**
     * When a piece is selected on the board.
     *
     * @param piece The piece selected.
     */
    @Override
    public void onSquareSelected(final Square piece) {
        /// Not allowed in spectator/player mode.
    }

    /**
     * Plays the next item in the list.
     */
    private void moveNext() {
        if (playIndex < playMoves.size() && this.scheduledExecutor != null) {
            final Move move = playMoves.get(playIndex);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Performing move [{}]", move);
            }
            performMove(move, false);
            playIndex++;
        } else {
            GameEventsPublisher.publishGameState(
                    new GameStateEvent(this.getGameOptions().getGameId(), GameStateEvent.GameState.ENDED),
                    0);
        }
    }

    /**
     * Schedule an item to be executed.
     *
     * @param runnable The runnable.
     * @param secondsDelay The delay to execute.
     */
    private void scheduleItem(final Runnable runnable, final int secondsDelay) {
        if (scheduledExecutor != null) {
            scheduledExecutor.schedule(runnable, secondsDelay, TimeUnit.SECONDS);
        }
    }

}
