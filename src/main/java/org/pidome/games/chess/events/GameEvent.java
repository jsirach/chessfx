/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.events;

import java.util.EventObject;

/**
 * Base game event class.It is important that the event gets consumed at the end
 * of the iteration chain to release any source object.
 *
 * It is important that the event gets consumed as early as possible when
 * required by any listener. Otherwise any calls to <code>getSource</code> can
 * be undefined for subsequent listeners.
 *
 * @author John
 * @param <T> The type of object in the event object's source.
 */
public class GameEvent<T> extends EventObject {

    /**
     * Game event types.
     */
    public enum GameEventType {
        /**
         * A game state event.
         *
         * The game state indicates loading, ready, started, ended.
         */
        STATE,
        /**
         * A square is active under mouse focus.
         */
        SQUARE_OVER,
        /**
         * A square select event type.
         */
        SQUARE_SELECT,
        /**
         * A move intent is done.
         */
        MOVE_INTENT,
        /**
         * A move has been done.
         */
        MOVE,
        /**
         * A piece is select event type.
         */
        PIECE_SELECT;
    }

    /**
     * If the event has been consumed or not.
     */
    private boolean consumed = false;

    /**
     * The event type.
     */
    private final GameEventType eventType;

    /**
     * The game id of this event.
     */
    private final String gameId;

    /**
     * Game event constructor.
     *
     * @param gameId The game id of this event.
     * @param eventType The event type.
     * @param source The event object source.
     */
    public GameEvent(final String gameId, final GameEventType eventType, final T source) {
        super(source);
        this.eventType = eventType;
        this.gameId = gameId;
    }

    /**
     * Returns the event source.
     *
     * @return The source of the event.
     */
    @Override
    @SuppressWarnings("unchecked")
    public final T getSource() {
        return (T) super.source;
    }

    /**
     * Returns the source of the event type.
     *
     * @return The event type.
     */
    public final GameEventType getEventType() {
        return this.eventType;
    }

    /**
     * consume an event.
     *
     * When an event is consumed it should not pass on to any other listeners.
     * When an event is marked consumed the sources will be released. Any follow
     * up will not be able to get to the event source.
     *
     * @param consumed true to consume.
     */
    public final void setConsumed(final boolean consumed) {
        if (consumed != true) {
            this.source = null;
            this.consumed = true;
        }
    }

    /**
     * Return if the event has been consumed.
     *
     * If the event has been consumed, the source is not be available.
     *
     * @return If the event has been consumed.
     */
    public final boolean isConsumed() {
        return this.consumed;
    }

    /**
     * Return the game id of the game the event originates from.
     *
     * @return The id of the game.
     */
    public final String getGameId() {
        return this.gameId;
    }

}
