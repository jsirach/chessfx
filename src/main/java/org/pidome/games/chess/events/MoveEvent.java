/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.events;

import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.move.Move;
import java.util.ArrayList;
import java.util.List;

/**
 * A special object only used internally by event composers.
 *
 * Not to be used by any normal implementation. If events are required in your
 * engine implementation extend the <code>AbstractGameEvents</code> which will
 * nicely supply you with all the events.
 *
 * @author John
 */
public final class MoveEvent extends GameEvent<Move> {

    /**
     * The move types.
     */
    public enum MoveType {
        /**
         * Normal move.
         */
        MOVE,
        /**
         * A move with a capture.
         */
        MOVE_CAPTURE,
        /**
         * A move resulting in a check.
         */
        MOVE_CHECK,
        /**
         * A move resulting in a checkmate.
         */
        MOVE_CHECKMATE,
        /**
         * A move resulting in a stalemate.
         */
        MOVE_STALEMATE
    }

    /**
     * The movetypes included in the move.
     */
    private final List<MoveType> moveTypes = new ArrayList<>();

    /**
     * the piece involved in the move.
     */
    private final Piece piece;

    /**
     * Event constructor.
     *
     * @param gameId the if of the game originating this event.
     * @param type The game event type.
     * @param move The move to include.
     * @param piece The initial piece the event is about.
     */
    public MoveEvent(final String gameId, final GameEventType type, final Move move, final Piece piece) {
        super(gameId, type, move);
        this.piece = piece;
        if (move == null) {
            moveTypes.remove(MoveType.MOVE);
        } else {
            moveTypes.add(MoveType.MOVE);
        }
    }

    /**
     * The piece involved in the move.
     *
     * @return The piece.
     */
    public final Piece getPiece() {
        return this.piece;
    }

    /**
     * @return the moveTypes
     */
    public List<MoveType> getMoveTypes() {
        return moveTypes;
    }

    /**
     * @param moveType the moveTypes to add.
     */
    public void addMoveType(MoveType moveType) {
        if (!moveTypes.contains(moveType)) {
            moveTypes.add(moveType);
        }
    }

    /**
     * @param moveTypes the moveTypes to set
     */
    public void setMoveTypes(List<MoveType> moveTypes) {
        this.moveTypes.retainAll(moveTypes);
    }

    /**
     * String version of the object.
     *
     * @return Returns this event object as string.
     */
    @Override
    public String toString() {
        return new StringBuilder("[Move: ").append(this.getSource()).append(", MoveType:").append(this.moveTypes).append("]").toString();
    }

}
