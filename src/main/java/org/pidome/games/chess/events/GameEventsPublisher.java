/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.events;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.events.GameEvent.GameEventType;

/**
 * publisher for game events.
 *
 * @author John
 */
public class GameEventsPublisher {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(GameEventsPublisher.class);

    /**
     * Base listeners map.
     *
     * Please use the synchronized one.
     */
    private static final Map<String, List<GameEventListener>> _LISTENERS = new HashMap<>();

    /**
     * Map for listening to events, multiple games compatible.
     */
    private static final Map<String, List<GameEventListener>> LISTENERS = Collections.synchronizedMap(_LISTENERS);

    /**
     * Executor service to fire service events.
     */
    private static final ExecutorService SERVICE = Executors.newCachedThreadPool();

    /**
     * In case of scheduled send notifications.
     */
    private static final ScheduledExecutorService SCHEDULED_GAME_EVENTS_EXECUTOR = Executors.newSingleThreadScheduledExecutor();

    /**
     * Register for events on a specific board.
     *
     * @param gameHash The hash to identify a game.
     * @param listener The listener.
     */
    public static void registerForEvents(final String gameHash, final GameEventListener listener) {
        if (!LISTENERS.containsKey(gameHash)) {
            LISTENERS.put(gameHash, Collections.synchronizedList(new ArrayList<GameEventListener>()));
        }
        if (!LISTENERS.get(gameHash).contains(listener)) {
            LISTENERS.get(gameHash).add(listener);
        }
    }

    /**
     * De-registers a game for publishing events.
     *
     * This method detaches all listeners for a given game.
     *
     * @param gameHash The hash to remove.
     */
    public static void unregisterGame(final String gameHash) {
        LISTENERS.remove(gameHash);
    }

    /**
     * Register for events on a specific board.
     *
     * @param gameHash The hash to identify a game.
     * @param listener The listener.
     */
    public static void unRegisterForEvents(final String gameHash, final GameEventListener listener) {
        if (LISTENERS.containsKey(gameHash)) {
            LISTENERS.get(gameHash).remove(listener);
        }
    }

    /**
     * The game is loaded and ready to start.
     *
     * @param event The game ready event object.
     * @param sendDelay The amount of seconds to send delayed.
     */
    public static void publishGameState(final GameStateEvent event, final int sendDelay) {
        SCHEDULED_GAME_EVENTS_EXECUTOR.schedule(() -> {
            publishEvent(GameEventType.STATE, event);
        }, sendDelay, TimeUnit.SECONDS);
    }

    /**
     * Publishes a move event.
     *
     * @param event The move event to publish
     */
    public static void publishSquareOverEvent(final SquareIntentEvent event) {
        publishEvent(GameEventType.SQUARE_OVER, event);
    }

    /**
     * Publishes a move event.
     *
     * @param event The move event to publish
     */
    public static void publishMoveIntentEvent(final MoveEvent event) {
        publishEvent(GameEventType.MOVE_INTENT, event);
    }

    /**
     * Publishes a move event.
     *
     * @param event The move event to publish
     */
    public static void publishMovedEvent(final MoveEvent event) {
        publishEvent(GameEventType.MOVE, event);
    }

    /**
     * Publishes a move event.
     *
     * @param event The move event to publish
     */
    public static void publishPieceSelectedEvent(final SquareSelectEvent event) {
        publishEvent(GameEventType.PIECE_SELECT, event);
    }

    /**
     * Publishes an event.
     *
     * It would be wise to use a convenience method to publish as this method is
     * an aggregator and the convenience methods guarantees event types.
     *
     * @param gameHash The hash to identify a game.
     * @param type The event type to publish.
     * @param moveEvent The move event to publish.
     */
    private static <T extends GameEvent> void publishEvent(final GameEventType type, final T event) {
        LOG.trace("Hash: [{}], type: [{}], event: [{}]", event.getGameId(), type, event);
        if (LISTENERS.containsKey(event.getGameId())) {
            SERVICE.execute(() -> {
                final List<GameEventListener> eventListeners = LISTENERS.get(event.getGameId());
                synchronized (eventListeners) {
                    final Iterator<GameEventListener> iter = eventListeners.iterator();
                    while (iter.hasNext() && !event.isConsumed()) {
                        switch (type) {
                            case STATE:
                                iter.next().onGameStateChangeEvent((GameStateEvent) event);
                                break;
                            case MOVE_INTENT:
                                iter.next().onPieceMoveIntentEvent((MoveEvent) event);
                                break;
                            case MOVE:
                                iter.next().onPieceMovedEvent((MoveEvent) event);
                                break;
                            case PIECE_SELECT:
                                iter.next().onSquareSelectedEvent((SquareSelectEvent) event);
                                break;
                            case SQUARE_OVER:
                                iter.next().onSquareIntentEvent((SquareIntentEvent) event);
                                break;
                        }
                    }
                    event.setConsumed(true);
                }
            });
        }
    }

}
