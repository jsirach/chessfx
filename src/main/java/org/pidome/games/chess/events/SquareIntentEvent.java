/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.events;

import com.github.bhlangonijr.chesslib.Square;
import org.pidome.games.chess.engines.HighlightType;

/**
 * An event used in conjunction with a board square.
 *
 * @author johns
 */
public final class SquareIntentEvent extends GameEvent<Square> {

    /**
     * If a square is entered or left.
     */
    private final SquareIntent squareIntent;

    /**
     * The intent in the event for the given square.
     */
    public enum SquareIntent {
        /**
         * When a square is in focus (mouse)
         */
        OVER,
        /**
         * When a square is out of focus (mouse)
         */
        OUT,
        /**
         * When a square is clicked/touched to select.
         */
        SELECT,
        /**
         * When a square is clicked/touched to de-select.
         */
        DE_SELECT;
    }

    /**
     * The highlight currently on the square.
     */
    private final HighlightType highlightType;

    /**
     * Constructor for square selection.
     *
     * @param gameId the if of the game originating this event.
     * @param source The square as source.
     * @param squareIntent The intent performed on the square.
     * @param highlightType The highlight type to apply.
     */
    public SquareIntentEvent(final String gameId, final Square source, final SquareIntent squareIntent, final HighlightType highlightType) {
        super(gameId, GameEventType.SQUARE_OVER, source);
        this.squareIntent = squareIntent;
        this.highlightType = highlightType;
    }

    /**
     * Returns the intent on the square.
     *
     * @return The intent.
     */
    public final SquareIntent getSquareIntent() {
        return this.squareIntent;
    }

    /**
     * Returns the current highlighting on the square.
     *
     * @return The current highlighting.
     */
    public final HighlightType getCurrentHighlightType() {
        return this.highlightType;
    }

    /**
     * This event as string.
     *
     * @return The event as a string.
     */
    @Override
    public String toString() {
        return new StringBuilder("[Square: ").append(this.getSource())
                .append(", squareIntent: ").append(this.squareIntent)
                .append(", highlightType: ").append(this.highlightType).append("]").toString();
    }

}
