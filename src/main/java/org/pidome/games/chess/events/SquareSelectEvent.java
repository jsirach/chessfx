/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.events;

import com.github.bhlangonijr.chesslib.Square;

/**
 * The event used when a piece is selected.
 *
 * @author John
 */
public final class SquareSelectEvent extends GameEvent<Square> {

    /**
     * If the event publishes a select or a de-select action.
     */
    private boolean selectAction = false;

    /**
     * Constructor for square selection.
     *
     * @param gameId the if of the game originating this event.
     * @param source The square as source.
     * @param isSelectedAction If the action is meant to select or de-select a
     * piece.
     */
    public SquareSelectEvent(final String gameId, final Square source, final boolean isSelectedAction) {
        super(gameId, GameEventType.PIECE_SELECT, source);
        selectAction = isSelectedAction;
    }

    /**
     * Indicates if the event is a select or deselect action.
     *
     * When the action is a select action it means the piece has not been
     * selected yet in the game.
     *
     * @return true when the action is a select action.
     */
    public boolean isSelectAction() {
        return this.selectAction;
    }

    /**
     * This event as string.
     *
     * @return The event as a string.
     */
    @Override
    public String toString() {
        return new StringBuilder("[VisualPiece: ").append(this.getSource()).append(", selectAction:").append(this.selectAction).append("]").toString();
    }

}
