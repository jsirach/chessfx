/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.events;

/**
 * Interface for game events.
 *
 * @author John
 */
public interface GameEventListener {

    /**
     * Event fired when there is a change in a game state.
     *
     * @param gameState The game state change event.
     */
    public void onGameStateChangeEvent(GameStateEvent gameState);

    /**
     * An event sent from the game but not available in the board.
     *
     * @param moveEvent The move event when event type ON_MOVE occurred,
     * otherwise null.
     */
    public void onPieceMoveIntentEvent(MoveEvent moveEvent);

    /**
     * When a piece is moved.
     *
     * @param moveEvent The move event.
     */
    public void onPieceMovedEvent(MoveEvent moveEvent);

    /**
     * An event published when a piece is selected.
     *
     * @param selectEvent The piece select event.
     */
    public void onSquareSelectedEvent(SquareSelectEvent selectEvent);

    /**
     * Any intent with a square.
     *
     * These events are from a square that has been hovered to a square that has
     * been clicked to move a piece to.
     *
     * @param squareIntentEvent The square intent event.
     */
    public void onSquareIntentEvent(SquareIntentEvent squareIntentEvent);

}
