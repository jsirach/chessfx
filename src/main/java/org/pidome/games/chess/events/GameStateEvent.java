/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.events;

/**
 * Event describing a state of the game.
 *
 * @author John
 */
public final class GameStateEvent extends GameEvent<GameStateEvent.GameState> {

    /**
     * Indicates a state of a game.
     */
    public enum GameState {
        /**
         * A game view is initializing and loading it's assets.
         */
        LOADING,
        /**
         * A game view with all the assets is loaded.
         */
        LOADED,
        /**
         * A game is fully reset and pieces are placed next to the board.
         */
        RESET,
        /**
         * The game is ready to be started.
         *
         * Ready is when the pieces are placed on the board.
         */
        READY,
        /**
         * The game is started.
         */
        STARTED,
        /**
         * The game is ended.
         */
        ENDED,
        /**
         * A game is unloading.
         */
        UNLOADING,
        /**
         * A game is unloaded.
         */
        UNLOADED,
        /**
         * Play action from player.
         */
        GAME_PLAYER_PLAY,
        /**
         * Pause action from player
         */
        GAME_PLAYER_PAUSE,
        /**
         * Next from player.
         */
        GAME_PLAYER_NEXT,
        /**
         * Previous from player.
         */
        GAME_PLAYER_PREVIOUS

    }

    /**
     * Game state constructor.
     *
     * @param gameId the if of the game originating this event.
     * @param state The state of the game.
     */
    public GameStateEvent(final String gameId, final GameState state) {
        super(gameId, GameEventType.STATE, state);
    }

}
