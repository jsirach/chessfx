/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import javafx.beans.binding.DoubleBinding;
import javafx.scene.shape.Line;

/**
 * A line
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class HR extends Line {

    public HR(final DoubleBinding widthBindingProperty) {
        this.getStyleClass().add("hr");
        this.setStartX(0);
        this.setStartY(0);
        this.setEndY(1);
        this.endXProperty().bind(widthBindingProperty);
    }

}
