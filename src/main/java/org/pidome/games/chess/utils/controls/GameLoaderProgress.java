/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import java.util.concurrent.atomic.AtomicInteger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import static javafx.scene.layout.Region.USE_PREF_SIZE;
import javafx.stage.Stage;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.resources.ResourcesLoaderListener;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.gui.desktop.dialog.BasePopup;

/**
 * Game load progress.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class GameLoaderProgress extends BasePopup implements ResourcesLoaderListener {

    /**
     * Total items to load.
     */
    private double total = 0;

    /**
     * Amount of loaded items.
     */
    private AtomicInteger loaded = new AtomicInteger(0);

    /**
     * The progress value.
     */
    private DoubleProperty progressProperty = new SimpleDoubleProperty(0.0);

    /**
     * The progress bar.
     */
    private ProgressBar progressBar = new ProgressBar();

    /**
     * Constructor.
     *
     * @param owner The stage owning this popup.
     * @param resourcesLoader the resource loader.
     */
    public GameLoaderProgress(final Stage owner, final ResourcesLoader resourcesLoader) {
        super(owner);
        resourcesLoader.bind(this);
        progressBar.progressProperty().bind(progressProperty);
    }

    /**
     * Shows the indicator.
     */
    @Override
    public final void display() {
        super.display(true);
    }

    /**
     * Builds the view.
     */
    @Override
    protected final void buildSkeleton() {
        final GridPane progressContainer = new GridPane();
        final double width = 100 / 3;
        progressContainer.getColumnConstraints().addAll(
                new ColumnConstraints() {
            {
                setPercentWidth(width);
            }
        },
                new ColumnConstraints() {
            {
                setPercentWidth(width);
            }
        },
                new ColumnConstraints() {
            {
                setPercentWidth(width);
            }
        }
        );

        progressContainer.setVgap(10);
        progressContainer.getStyleClass().addAll("spinner-loading");
        final Label loading = new Label(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_LOADING).get());
        BorderPane.setAlignment(loading, Pos.CENTER);

        progressBar.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        progressBar.setPrefSize(300, 40);

        progressContainer.add(progressBar, 0, 0, 3, 1);

        Label all = new Label("100%");
        Label none = new Label("0%");
        GridPane.setHalignment(none, HPos.LEFT);
        GridPane.setHalignment(loading, HPos.CENTER);
        GridPane.setHalignment(all, HPos.RIGHT);
        progressContainer.addRow(1, none, loading, all);

        BorderPane.setAlignment(progressContainer, Pos.CENTER);
        progressContainer.setPadding(new Insets(20));
        this.setCenter(progressContainer);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void toLoad(double amount) {
        this.total = amount;
    }

    /**
     * @inheritDoc
     */
    @Override
    public void update() {
        progressProperty.setValue(
                (1.0 / total) * loaded.incrementAndGet()
        );
    }

    /**
     * Action to perform when an wait spinner is cancelled.
     *
     * The wait spinner can not be purposely be cancelled.
     */
    @Override
    protected void runOnCancelAction() {
        /// The wait spinner can not purposely be cancelled.
    }
}
