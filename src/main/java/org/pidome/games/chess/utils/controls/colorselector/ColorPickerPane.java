/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls.colorselector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Stop;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.controls.FractionPercentageSlider;

/**
 * A color picker pane with additional features.
 *
 * @author johns
 */
public final class ColorPickerPane extends GridPane {

    /**
     * The color selector object.
     */
    private final ColorSelector colorSelector = new ColorSelector(
            new Stop(0.000, Color.rgb(255, 255, 255)),
            new Stop(0.142, Color.rgb(255, 0, 0)),
            new Stop(0.285, Color.rgb(255, 255, 0)),
            new Stop(0.428, Color.rgb(0, 255, 0)),
            new Stop(0.571, Color.rgb(0, 255, 255)),
            new Stop(0.714, Color.rgb(0, 0, 255)),
            new Stop(0.856, Color.rgb(255, 0, 255)),
            new Stop(1.000, Color.rgb(255, 255, 255))
    );

    /**
     * The opacity slider. This is only available when conditional
     * <code>ColorPickerFeature.SELECT_OPACITY</code> is given in the
     * constructor.
     */
    private FractionPercentageSlider opacitySlider;

    /**
     * Brightness selection
     */
    private FractionPercentageSlider brightnessSlider = new FractionPercentageSlider();

    /**
     * The initial color which is set when a colorProperty is bound.
     */
    private Color initialColor;

    /**
     * The bound property.
     */
    private ObjectProperty<Color> boundProperty;

    /**
     * The opacity property. Only valid with
     * <code>ColorPickerFeature.SELECT_OPACITY</code> given and opacity bound.
     */
    private DoubleProperty opacityProperty;

    /**
     * List of additional features to enable.
     */
    private final List<ColorPickerFeature> additionalFeatures = new ArrayList<>();

    /**
     * Constructor.
     *
     * When <code>ColorPickerFeature.SELECT_OPACITY</code> is given you need to
     * bind a <code>DoubleProperty</code> using
     *
     * @param features The additional features to set, may be omitted.
     */
    public ColorPickerPane(ColorPickerFeature... features) {
        if (features != null) {
            additionalFeatures.addAll(Arrays.asList(features));
        }
        colorSelector.setMaxSize(150, 150);
        this.add(colorSelector, 0, 0);
        if (containsFeature(ColorPickerFeature.SELECT_OPACITY)) {
            opacitySlider = new FractionPercentageSlider();
            opacitySlider.setOrientation(Orientation.VERTICAL);
            this.add(opacitySlider, 1, 0);
        }
        this.add(new Label(I18nProxy.getInstance().get(I18nApplicationKey.COLOR_BRIGHTNESS).get()), 0, 1);
        colorSelector.selectedColorProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                brightnessSlider.setValue(newValue.getBrightness());
            }
        });
        brightnessSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                Color origiginal = colorSelector.selectedColorProperty().getValue();
                if (newValue.doubleValue() > 0.01) {
                    colorSelector.selectedColorProperty().setValue(Color.hsb(origiginal.getHue(), origiginal.getSaturation(), newValue.doubleValue()));
                }
            }
        });
        this.add(brightnessSlider, 0, 2);
    }

    /**
     * Returns if a given feature is enabled or not.
     *
     * @param feature The feature to check.
     * @return True when the given feature is enabled.
     */
    public final boolean containsFeature(ColorPickerFeature feature) {
        return this.additionalFeatures.contains(feature);
    }

    /**
     * Binds a color property to the color picker control.
     *
     * Bindings set are bidirectional.
     *
     * @param colorProperty Object property with <code>Color</code> object.
     */
    public void bindColor(ObjectProperty<Color> colorProperty) {
        initialColor = colorProperty.get();
        boundProperty = colorProperty;
        colorSelector.selectedColorProperty().bindBidirectional(boundProperty);
    }

    /**
     * Binds a double property to the opacity slider.
     *
     * When <code>ColorPickerFeature.SELECT_OPACITY</code> is missing this
     * method does nothing.
     *
     * @param opacityProperty The double property to bind to the opacity slider.
     */
    public void bindOpacity(DoubleProperty opacityProperty) {
        if (containsFeature(ColorPickerFeature.SELECT_OPACITY)) {
            this.opacityProperty = opacityProperty;
            this.opacityProperty.bind(opacitySlider.valueProperty());
        }
    }

    /**
     * Sets a color as selected.
     *
     * @param color The color to select.
     */
    public void setSelectedColor(Color color) {
        colorSelector.setSelectedColor(color);
    }

    /**
     * Return the initial color set when a color property is bound.
     *
     * @return The initial color, can be null.
     */
    public Color getInitialColor() {
        return this.initialColor;
    }

    /**
     * Unbinds all known bindings.
     */
    public void unbind() {
        if (boundProperty != null) {
            colorSelector.selectedColorProperty().unbindBidirectional(boundProperty);
            boundProperty = null;
        }
        if (this.opacityProperty != null) {
            this.opacityProperty.unbind();
            this.opacityProperty = null;
        }
    }

}
