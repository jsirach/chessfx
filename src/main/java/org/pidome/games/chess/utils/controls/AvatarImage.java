/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;

/**
 * Display an image in an avatar style.
 *
 * @author johns
 */
public final class AvatarImage extends StackPane {

    /**
     * The profile picture imageView.
     */
    private final ImageView picture = new ImageView();

    /**
     * Constructor.
     */
    public AvatarImage() {
        this.setMinSize(0, 0);
        picture.setPreserveRatio(true);
        final Rectangle clip = new Rectangle();
        clip.getStyleClass().add("avatar");
        clip.widthProperty().bind(this.widthProperty());
        clip.heightProperty().bind(this.heightProperty());
        clip.setArcWidth(3);
        clip.setArcHeight(3);
        this.setClip(clip);
        this.heightProperty().addListener((observable, oldVal, newVal) -> {
            picture.prefWidth(newVal.doubleValue());
        });
        this.getChildren().add(picture);
    }

    /**
     * Updates the avatar with the given player.
     *
     * @param person The person.
     */
    public void setPlayerAvatar(final ChessGamePlayerModel person) {
        picture.setImage(
                new Image(this.getClass().getResourceAsStream(person.imageLocationProperty().get()), 200, 200, true, true)
        );
    }

}
