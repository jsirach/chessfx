/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils;

import javafx.application.Platform;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.paint.Color;

/**
 * Tool methods specific for FX.
 *
 * @author johns
 */
public class FXUtils {

    /**
     * Run a runnable on the FX thread.
     *
     * This methods checks if the calling thread is performing the call on the
     * FX thread, if not it will be added to the FX thread runnable queue.
     *
     * @param runnable The runnable to execute.
     */
    public static void runOnFXThread(final Runnable runnable) {
        if (!Platform.isFxApplicationThread()) {
            try {
                Platform.runLater(runnable);
            } catch (java.lang.IllegalStateException ex) {
                runnable.run();
            }
        } else {
            runnable.run();
        }
    }

    /**
     * Generic method to set cache hints for a node.
     *
     * @param node The node to apply the cache to.
     * @param cacheHint The cache hint.
     */
    public static void setCache(final Node node, final CacheHint cacheHint) {
        node.setCache(true);
        node.setCacheHint(cacheHint);
    }

    /**
     * Returns a color to it's RGB string representation.
     *
     * @param c The color.
     * @return The rgb string.
     */
    public static final String colorToRgb(final Color c) {
        return String.format("#%02X%02X%02X%02X",
                (int) (c.getRed() * 255),
                (int) (c.getGreen() * 255),
                (int) (c.getBlue() * 255),
                (int) (c.getOpacity() * 255));
    }

}
