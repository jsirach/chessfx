/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nKey;
import org.pidome.games.chess.utils.Actionable;
import org.pidome.games.chess.utils.Disposable;

/**
 * A tab to be used in the game tab pane.
 *
 * @author johns
 */
public abstract class GameTab extends Tab implements Actionable, Disposable {

    /**
     * If the tabs content is valid or not.
     */
    private final BooleanProperty contentValid = new SimpleBooleanProperty(Boolean.TRUE);

    /**
     * constructor with I18N compatibility.
     *
     * @param title The title.
     */
    public GameTab(final I18nKey title) {
        this.textProperty().bind(I18nProxy.getInstance().get(title));
        this.setClosable(false);
    }

    /**
     * Closes a tab.
     */
    public final void closeTab() {
        if (this.textProperty().isBound()) {
            this.textProperty().unbind();
        }
        dispose();
    }

    /**
     * Returns available fieldssets on this pane.
     *
     * @return
     */
    public List<FieldSet> getFieldSets() {
        if (getContent() instanceof Pane) {
            return ((Pane) getContent()).getChildren().stream()
                    .filter(pane -> (pane instanceof FieldSet))
                    .map(pane -> (FieldSet) pane)
                    .collect(Collectors.toList());
        }
        return Collections.EMPTY_LIST;
    }

    /**
     * A game tab to return a valid tab content.
     *
     * Initial usage is when a new game is to start but not all required info
     * has been supplied.
     *
     * @return If the tab content is valid.
     */
    public BooleanProperty contentValid() {
        return contentValid;
    }

    /**
     * A game tab to return a valid tab content.
     *
     * Initial usage is when a new game is to start but not all required info
     * has been supplied.
     *
     * @return If the tab content is valid.
     */
    public boolean isContentValid() {
        return contentValid.getValue();
    }

}
