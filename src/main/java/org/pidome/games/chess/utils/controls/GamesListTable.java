/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * Table view containing a list of games.
 *
 * @author johns
 */
public class GamesListTable extends TableView<ChessGameModel> {

    /**
     * Constructor.
     */
    public GamesListTable() {
        createColumns();
        this.setEditable(true);
        this.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.getSelectionModel().setCellSelectionEnabled(false);
    }

    /**
     * Creates the columns to show.
     */
    private void createColumns() {

        final DateTimeFormatter dateFormat = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);

        final TableColumn<ChessGameModel, String> round = new TableColumn<>("#");
        round.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(cellData.getValue().getChessEventRound().getRoundNumber())));

        final TableColumn<ChessGameModel, String> eventWhen = new TableColumn<>(I18nProxy.getString(I18nApplicationKey.DATE));
        eventWhen.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(
                cellData.getValue().getChessEventRound().getChessEvent().getEventDate().format(dateFormat)
        ));

        final TableColumn<ChessGameModel, String> white = new TableColumn<>(I18nProxy.getString(I18nApplicationKey.PLAYER_WHITE));
        white.setCellValueFactory(cellData -> cellData.getValue().getPlayerWhite().nameProperty());

        final TableColumn<ChessGameModel, String> black = new TableColumn<>(I18nProxy.getString(I18nApplicationKey.PLAYER_BLACK));
        black.setCellValueFactory(cellData -> cellData.getValue().getPlayerBlack().nameProperty());

        final TableColumn<ChessGameModel, String> result = new TableColumn<>(I18nProxy.getString(I18nApplicationKey.GAME_RESULT));
        result.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(
                I18nProxy.getString(I18nApplicationKey.valueOf(cellData.getValue().getResult().toString()))
        ));

        this.getColumns().addAll(round, eventWhen, white, black, result);

    }

}
