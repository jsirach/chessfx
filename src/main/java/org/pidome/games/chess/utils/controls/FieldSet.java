/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.utils.Actionable;
import org.pidome.games.chess.utils.Disposable;

/**
 * A fieldset emulator for JavaFX.
 *
 * Really, really.... really simplistic.
 *
 * @author johns
 */
public class FieldSet extends StackPane implements Actionable, Disposable {

    /**
     * Title label
     */
    private final Label title = new Label();

    /**
     * the content holder.
     */
    private final StackPane fieldsetContent = new StackPane();

    /**
     * If the fieldset has valid content.
     *
     * Example for invalidation would be a form in a fieldset which contains
     * invalid values and implementation wants to check fieldset validation.
     */
    private final BooleanProperty validProperty = new SimpleBooleanProperty(Boolean.TRUE);

    /**
     * Constructor setting the title.
     *
     * @param title The title for the fieldSet.
     */
    public FieldSet(final I18nKey title) {
        this.title.getStyleClass().add("field-set-label");
        this.getStyleClass().add("field-set");
        this.setAlignment(Pos.TOP_LEFT);
        fieldsetContent.getStyleClass().add("field-set-content");
        this.title.translateYProperty().bind(this.title.heightProperty().divide(2).negate());
        this.title.textProperty().bind(I18nProxy.getInstance().get(title));
    }

    /**
     * Set the content for the fieldset.
     *
     * @param content The content to set.
     */
    public final void setContent(final Node content) {
        this.getChildren().addAll(this.title, fieldsetContent);
        fieldsetContent.getChildren().add(content);
    }

    /**
     * If the form fields are valid or not.
     *
     * @return if valid or not, defaults to true.
     */
    public BooleanProperty validProperty() {
        return this.validProperty;
    }

    /**
     * Sets some defaults on labels
     *
     * @param label The label.
     * @param key The label text key.
     */
    public static void setSettingsLabelProperties(final Label label, final I18nApplicationKey key) {
        label.textProperty().bind(I18nProxy.getInstance().get(key));
        label.setMinWidth(125);
        label.setMaxWidth(125);
    }

    /**
     * To be implemented by a class extending this fieldset.
     *
     * This method does nothing, but when special actions are needed this class
     * can be extended and onAction is used when an action is required.
     *
     * Always refer to the parent to see if this method is overwritten if it
     * would be needed.
     */
    @Override
    public void onAction() {
        // Not used by default.
    }

    /**
     * To be implemented by a class extending this fieldset.
     *
     * This method does nothing, but when special actions are needed this class
     * can be extended and dispose is used when a dispose is required.
     *
     * Always refer to the parent to see if this method is overwritten if it
     * would be needed.
     */
    @Override
    public void dispose() {
        // Not used by default.
    }

}
