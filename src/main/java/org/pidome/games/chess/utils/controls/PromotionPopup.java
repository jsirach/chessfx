/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import com.github.bhlangonijr.chesslib.move.Move;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nInGameKey;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;

/**
 * Popup to request a promotion.
 *
 * @author John
 */
public class PromotionPopup extends GamePopup {

    /**
     * List of promotable pieces.
     */
    private final ObservableList<Move> promotionMoves = FXCollections.observableArrayList();

    /**
     * The select for the pieces.
     */
    private final ComboBox<Move> promotionSelect = new ComboBox<>(promotionMoves);

    /**
     * Constructor.
     */
    public PromotionPopup() {
        super(I18nProxy.getInstance().get(I18nInGameKey.PROMOTION_SELECT_PIECE_TITLE));
    }

    /**
     * The promotion request.
     *
     * @param moves The possible promotion moves.
     */
    public final void requestPromotion(final List<Move> moves) {
        final HBox requestPane = new HBox(5);
        requestPane.setAlignment(Pos.CENTER_LEFT);
        final Label request = new Label(I18nProxy.getInstance().get(I18nInGameKey.PROMOTION_SELECT_PIECE).get());

        promotionSelect.setCellFactory((ListView<Move> l) -> new ListCell<Move>() {
            @Override
            protected void updateItem(final Move move, final boolean empty) {
                super.updateItem(move, empty);
                if (move == null || empty) {
                    setGraphic(null);
                } else {
                    setText(selectTranslation(move));
                }
            }
        });

        promotionSelect.setConverter(new StringConverter<Move>() {

            @Override
            public Move fromString(final String pieceName) {
                for (Move move : promotionMoves) {
                    if (selectTranslation(move).equals(pieceName)) {
                        return move;
                    }
                }
                return null;
            }

            @Override
            public String toString(final Move move) {
                if (move == null) {
                    return null;
                }
                return selectTranslation(move);
            }
        });

        request.setLabelFor(this.promotionSelect);
        requestPane.getChildren().addAll(request, this.promotionSelect);
        this.setCenter(requestPane);
        moves.forEach(move -> {
            promotionMoves.add(move);
        });
        if (!promotionMoves.isEmpty()) {
            promotionSelect.setValue(promotionMoves.get(0));
        }
    }

    private String selectTranslation(final Move move) {
        String moveString = move.toString();
        switch (moveString.substring(moveString.length() - 1).toLowerCase()) {
            case "k":
                return I18nProxy.getInstance().get(I18nInGameKey.PIECE_KING).get();
            case "q":
                return I18nProxy.getInstance().get(I18nInGameKey.PIECE_QUEEN).get();
            case "b":
                return I18nProxy.getInstance().get(I18nInGameKey.PIECE_BISHOP).get();
            case "n":
                return I18nProxy.getInstance().get(I18nInGameKey.PIECE_KNIGHT).get();
            case "r":
                return I18nProxy.getInstance().get(I18nInGameKey.PIECE_ROOK).get();
            default:
                return "";
        }
    }

    /**
     * Returns the selected promotion piece.
     *
     * @return The promotion piece.
     */
    public final Move getSelectedPromotion() {
        return promotionSelect.getValue();
    }

}
