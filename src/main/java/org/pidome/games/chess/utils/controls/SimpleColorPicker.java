/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import javafx.beans.property.ObjectProperty;
import javafx.scene.paint.Color;
import org.pidome.games.chess.resources.i18n.I18nPlural;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nPluralKey;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.utils.controls.colorselector.ColorPickerFeature;
import org.pidome.games.chess.utils.controls.colorselector.ColorPickerPane;

/**
 * A color picker popup.
 *
 * This popup features when canceling the original color is set to a bound color
 * property. if this is undesired behavior set the <code>onCancel</code> method
 * to overwrite the popups default behavior.
 *
 * @author johns
 */
public class SimpleColorPicker extends GamePopup {

    /**
     * The color picker pane.
     */
    private final ColorPickerPane colorPickerPane;

    /**
     * Constructor for color picker.
     *
     * @param features Additional features to enable, may be omitted.
     */
    public SimpleColorPicker(final ColorPickerFeature... features) {
        super(I18nProxy.getInstance()
                .getNonPersistentPlural(I18nApplicationKey.COLOR_PICKER_POPUP_TITLE,
                        new I18nPlural(I18nPluralKey.count, 1)));
        colorPickerPane = new ColorPickerPane(features);
        createContent();
    }

    /**
     * Binds a color property to the color picker control.
     *
     * @param colorProperty Object property with <code>Color</code> object.
     */
    public void bindColor(final ObjectProperty<Color> colorProperty) {
        colorPickerPane.bindColor(colorProperty);
    }

    /**
     * Creates the content.
     */
    private void createContent() {
        this.setCenter(colorPickerPane);
        this.setOnCancel(() -> {
            if (colorPickerPane.getInitialColor() != null) {
                colorPickerPane.setSelectedColor(colorPickerPane.getInitialColor());
            }
        });
    }

    /**
     * Closes the popup and unbinds.
     */
    @Override
    public final void close() {
        super.close();
        colorPickerPane.unbind();
    }

}
