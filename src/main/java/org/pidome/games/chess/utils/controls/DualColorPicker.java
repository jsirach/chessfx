/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import org.pidome.games.chess.resources.i18n.I18nPlural;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nKey;
import org.pidome.games.chess.resources.i18n.I18nPluralKey;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.utils.controls.colorselector.ColorPickerFeature;
import org.pidome.games.chess.utils.controls.colorselector.ColorPickerPane;

/**
 * Color picker with dual selectors.
 *
 * @author johns
 */
public class DualColorPicker extends GamePopup {

    /**
     * The color picker pane.
     */
    private final ColorPickerPane colorPickerPaneLeft;

    /**
     * Title for the left pane.
     */
    private final Label leftPanetitle = new Label();

    /**
     * The color picker pane.
     */
    private final ColorPickerPane colorPickerPaneRight;

    /**
     * Title for the right pane.
     */
    private final Label rightPanetitle = new Label();

    /**
     * List of features.
     */
    private final List<ColorPickerFeature> features = new ArrayList<>();

    /**
     * the opacity slider. This is only available when conditional
     * <code>ColorPickerFeature.SELECT_OPACITY_DUAL_SINGLE</code> and only in
     * this specific class is given in the constructor.
     */
    private FractionPercentageSlider genericOpacitySlider;

    /**
     * Property used for the <code>genericOpacitySlider</code>.
     *
     * Only available when a genericOpacitySlider is used.
     */
    private DoubleProperty genericOpacity;

    /**
     * The original opacity value when the genericOpacity is set.
     */
    private double originalOpacity;

    /**
     * Color picker dual content.
     */
    private final GridPane contentPane = new GridPane();

    /**
     * Constructor for dual color picker popup.
     *
     * @param features The feature to add to the picker.
     */
    public DualColorPicker(final ColorPickerFeature... features) {
        super(I18nProxy.getInstance()
                .getNonPersistentPlural(I18nApplicationKey.COLOR_PICKER_POPUP_TITLE,
                        new I18nPlural(I18nPluralKey.count, 2)));
        if (features != null) {
            this.features.addAll(Arrays.asList(features));
        }
        colorPickerPaneLeft = new ColorPickerPane(
                (this.features.contains(ColorPickerFeature.SELECT_OPACITY) || this.features.contains(ColorPickerFeature.SELECT_OPACITY_LEFT)) ? ColorPickerFeature.SELECT_OPACITY : null
        );
        colorPickerPaneRight = new ColorPickerPane(
                (this.features.contains(ColorPickerFeature.SELECT_OPACITY) || this.features.contains(ColorPickerFeature.SELECT_OPACITY_RIGHT)) ? ColorPickerFeature.SELECT_OPACITY : null
        );
        createContent();
    }

    /**
     * Creates the content.
     */
    private void createContent() {
        contentPane.setHgap(15);
        contentPane.setVgap(5);
        GridPane.setHalignment(leftPanetitle, HPos.CENTER);
        contentPane.add(leftPanetitle, 0, 0);
        contentPane.add(colorPickerPaneLeft, 0, 1);

        GridPane.setHalignment(rightPanetitle, HPos.CENTER);
        contentPane.add(rightPanetitle, 1, 0);
        contentPane.add(colorPickerPaneRight, 1, 1);
        this.setOnCancel(() -> {
            if (colorPickerPaneLeft.getInitialColor() != null) {
                colorPickerPaneLeft.setSelectedColor(colorPickerPaneLeft.getInitialColor());
            }
            if (colorPickerPaneRight.getInitialColor() != null) {
                colorPickerPaneRight.setSelectedColor(colorPickerPaneRight.getInitialColor());
            }
            if (this.genericOpacity != null) {
                Platform.runLater(() -> {
                    this.genericOpacity.setValue(this.originalOpacity);
                    this.genericOpacity = null;
                });
            }
        });
        if (this.features.contains(ColorPickerFeature.SELECT_OPACITY_DUAL_SINGLE)) {
            genericOpacitySlider = new FractionPercentageSlider();
            final Label valueLabel = new Label();
            valueLabel.textProperty().bind(
                    Bindings.concat(
                            I18nProxy.getInstance().get(I18nApplicationKey.COLOR_OPACITY),
                            " ",
                            genericOpacitySlider.valueProperty().multiply(100).asString("%.0f"),
                            "%"
                    )
            );
            GridPane.setHalignment(valueLabel, HPos.LEFT);
            contentPane.add(valueLabel, 0, 2, 2, 1);
            contentPane.add(genericOpacitySlider, 0, 3, 2, 1);
        }
        this.setCenter(contentPane);
    }

    /**
     * Returns the color picker dual content pane.
     *
     * This is the actual pane containing the color pickers, if you need to add
     * to this pane check the source code for the locations of the current
     * controls.
     *
     * @return The color picker content pane.
     */
    protected final GridPane getContentPane() {
        return this.contentPane;
    }

    /**
     * Sets the title for the left pane.
     *
     * @param leftTitle The i18n title.
     */
    public final void setLeftPanetitle(final I18nKey leftTitle) {
        leftPanetitle.setText(I18nProxy.getInstance().get(leftTitle).get());
    }

    /**
     * Sets the title for the right pane.
     *
     * @param rightTitle The i18n title.
     */
    public final void setRightPanetitle(final I18nKey rightTitle) {
        rightPanetitle.setText(I18nProxy.getInstance().get(rightTitle).get());
    }

    /**
     * Binds a color property to the color picker control.
     *
     * Only applicable when <code>ColorPickerFeature.SELECT_OPACITY</code> or
     * <code>ColorPickerFeature.SELECT_OPACITY_LEFT</code> is given in the
     * constructor.
     *
     * @param colorProperty Object property with <code>Color</code> object.
     */
    public final void bindLeftColorPicker(final ObjectProperty<Color> colorProperty) {
        colorPickerPaneLeft.bindColor(colorProperty);
    }

    /**
     * Binds an opacity property to the left color picker control.
     *
     * @param opacityProperty Double property binding to opacity slider.
     */
    public final void bindLeftOpacity(final DoubleProperty opacityProperty) {
        colorPickerPaneLeft.bindOpacity(opacityProperty);
    }

    /**
     * Binds a color property to the color picker control.
     *
     * @param colorProperty Object property with <code>Color</code> object.
     */
    public final void bindRightColorPicker(final ObjectProperty<Color> colorProperty) {
        colorPickerPaneRight.bindColor(colorProperty);
    }

    /**
     * Binds an opacity property to the right color picker control.
     *
     * Only applicable when <code>ColorPickerFeature.SELECT_OPACITY</code> or
     * <code>ColorPickerFeature.SELECT_OPACITY_LEFT</code> is given in the
     * constructor.
     *
     * @param opacityProperty Double property binding to opacity slider.
     */
    public final void bindRightOpacity(final DoubleProperty opacityProperty) {
        colorPickerPaneRight.bindOpacity(opacityProperty);
    }

    /**
     * Binds an opacity property to the generic opacity slider.
     *
     * Only applicable when
     * <code>ColorPickerFeature.SELECT_OPACITY_DUAL_SINGLE</code> is given in
     * the constructor.
     *
     * @param opacityProperty Double property binding to opacity slider.
     */
    public final void bindSingleOpacity(final DoubleProperty opacityProperty) {
        this.originalOpacity = opacityProperty.getValue();
        this.genericOpacity = opacityProperty;
        this.genericOpacitySlider.valueProperty().bindBidirectional(this.genericOpacity);
    }

    /**
     * Closes the popup and unbinds.
     */
    @Override
    public void close() {
        super.close();
        colorPickerPaneLeft.unbind();
        colorPickerPaneRight.unbind();
        genericOpacitySlider.valueProperty().unbindBidirectional(this.genericOpacity);
    }

}
