/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import static javafx.scene.layout.Region.USE_PREF_SIZE;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.gui.desktop.dialog.BasePopup;

/**
 * A popup while waiting for actions to complete.
 *
 * @author John
 */
public class WaitSpinner extends BasePopup {

    /**
     * Progress indicator.
     */
    final ProgressIndicator pi = new ProgressIndicator();

    /**
     * Constructor.
     *
     * @param owner The owner window.
     */
    public WaitSpinner(final Stage owner) {
        super(owner);
        this.setDecoration(PopupDecoration.TRANSPARENT);
    }

    /**
     * Shows the indicator.
     */
    @Override
    public final void display() {
        super.display(true);
    }

    /**
     * Builds the view.
     */
    @Override
    protected final void buildSkeleton() {
        final StackPane piContainer = new StackPane();
        piContainer.getStyleClass().addAll("spinner-loading");
        final Label loading = new Label(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_LOADING).get());
        BorderPane.setAlignment(loading, Pos.CENTER);

        pi.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        pi.setPrefSize(150, 150);

        piContainer.setPrefSize(180, 180);
        piContainer.getChildren().addAll(pi, loading);
        BorderPane.setAlignment(piContainer, Pos.CENTER);
        piContainer.setPadding(new Insets(20));
        this.setCenter(piContainer);
    }

    /**
     * Action to perform when an wait spinner is cancelled.
     *
     * The wait spinner can not be purposely be cancelled.
     */
    @Override
    protected void runOnCancelAction() {
        /// The wait spinner can not purposely be cancelled.
    }

}
