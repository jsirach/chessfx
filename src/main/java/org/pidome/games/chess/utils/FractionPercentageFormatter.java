/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils;

import java.text.NumberFormat;
import javafx.util.StringConverter;

/**
 * Percentage formatter used with the double range 0.0 to 1.0.
 *
 * @author johns
 */
public final class FractionPercentageFormatter extends StringConverter<Double> {

    /**
     * Number formatter used.
     */
    private final NumberFormat percentageFormatter = NumberFormat.getNumberInstance();

    /**
     * Constructor.
     */
    public FractionPercentageFormatter() {
        percentageFormatter.setMaximumFractionDigits(0);
    }

    /**
     * Convert to percentage.
     *
     * @param doubleValue the double value to convert.
     * @return String in percentage.
     */
    @Override
    public String toString(final Double doubleValue) {
        return percentageFormatter.format(doubleValue * 100) + "%";
    }

    /**
     * Convert percentage String to double.
     *
     * @param stringValue the string value to convert.
     * @return double.
     */
    @Override
    public Double fromString(final String stringValue) {
        return Double.valueOf(stringValue.replace("%", "").trim()) / 100.0;
    }
}
