/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;
import org.pidome.games.chess.model.game.ChessEventModel;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.utils.FXUtils;

/**
 * Popup showing a games list.
 *
 * @author johns
 */
public class GamesListPopup extends GamePopup {

    /**
     * Holds the visuals.
     */
    private final GridPane container = new UiGridPane();

    /**
     * Checkbox used to select or deselect all items in the table.
     */
    private final CheckBox selectDeselectAll = new CheckBox();

    /**
     * List of games.
     */
    private final ObservableList<ChessGameModel> gamesList = FXCollections.observableArrayList();

    /**
     * A filtered list to be used with the tableview with a predicate linked to
     * the combobox..
     */
    private final FilteredList<ChessGameModel> filteredGamesList = new FilteredList<>(gamesList);

    /**
     * Games list sorted.
     */
    private final SortedList<ChessGameModel> sortedTableList = new SortedList<>(filteredGamesList, (ChessGameModel cgm1, ChessGameModel cgm2) -> {
        int compare = cgm1.getChessEventRound().getChessEvent().getEventDate().compareTo(cgm2.getChessEventRound().getChessEvent().getEventDate());
        if (compare == 0) {
            compare = Integer.compare(cgm1.getChessEventRound().getRoundNumber(), cgm2.getChessEventRound().getRoundNumber());
            if (compare == 0) {
                compare = cgm1.getDate().compareTo(cgm2.getDate());
            }
        }
        return compare;
    });

    /**
     * Table for showing a list of games.
     */
    private final GamesListTable gamesTable = new GamesListTable();

    /**
     * List of events for the combobox.
     */
    private final ObservableList<ChessEventModel> eventsList = FXCollections.observableArrayList();

    /**
     * Combobox for the events table to be filterd with events.
     */
    private final ComboBox<ChessEventModel> eventsDropDown = new ComboBox<>();

    /**
     * Constructor.
     *
     * @param events Object containing game events.
     */
    public GamesListPopup(final ObservableList<ChessEventModel> events) {
        super(I18nProxy.getStringProperty(I18nApplicationKey.GAMES));
        construct(events);
        this.gamesTable.setPrefWidth(850);
    }

    /**
     * Creates the popup.
     */
    private void construct(final ObservableList<ChessEventModel> events) {
        container.add(this.selectDeselectAll, 0, 0);
        container.add(this.eventsDropDown, 1, 0);

        container.add(this.gamesTable, 0, 1, 2, 1);

        this.selectDeselectAll.setOnAction(event -> {
            for (int i = 0; i < filteredGamesList.size(); i++) {
                filteredGamesList.get(i).playSelectedProperty().setValue(this.selectDeselectAll.isSelected());
            }
        });
        this.setCenter(container);
        events.forEach((model) -> {
            eventsList.add(model);
            model.getRounds().forEach((round) -> {
                gamesList.addAll(round.getGames());
            });
        });
        this.gamesTable.setItems(sortedTableList);
        eventsDropDown.valueProperty().addListener(event -> {
            filteredGamesList.setPredicate(gameModel -> {
                if (Objects.isNull(gameModel) && Objects.isNull(eventsDropDown.getValue())
                        || !gameModel.getChessEventRound().getChessEvent().equals(eventsDropDown.getValue())) {
                    return false;
                }
                return true;
            });
            setAllSelectedIndicator();
        });
        eventsDropDown.setCellFactory((ListView<ChessEventModel> l) -> new ListCell<ChessEventModel>() {
            @Override
            protected void updateItem(final ChessEventModel event, final boolean empty) {
                super.updateItem(event, empty);
                if (event == null || empty) {
                    setGraphic(null);
                } else {
                    setText(event.getName());
                }
            }
        });

        eventsDropDown.setConverter(new StringConverter<ChessEventModel>() {

            @Override
            public ChessEventModel fromString(final String eventName) {
                for (ChessEventModel event : eventsList) {
                    if (event.getName().equals(eventName)) {
                        return event;
                    }
                }
                return null;
            }

            @Override
            public String toString(final ChessEventModel event) {
                if (event == null) {
                    return null;
                }
                return event.getName();
            }
        });
        eventsDropDown.setItems(eventsList);
        FXUtils.runOnFXThread(() -> {
            eventsDropDown.setValue(eventsList.get(0));
        });

        addSelectionColumn();

    }

    /**
     * Determines if the current filtered selection should be undetermined.
     *
     * @return If undetermined or not.
     */
    private void setAllSelectedIndicator() {
        long selected = filteredGamesList.stream()
                .filter(model -> model.playSelectedProperty().get())
                .count();
        if (selected == 0) {
            selectDeselectAll.setIndeterminate(false);
            selectDeselectAll.setSelected(false);
        } else if (selected == filteredGamesList.size()) {
            selectDeselectAll.setIndeterminate(false);
            selectDeselectAll.setSelected(true);
        } else {
            selectDeselectAll.setIndeterminate(true);
        }
    }

    /**
     * Adds a selection column.
     */
    private void addSelectionColumn() {
        final TableColumn<ChessGameModel, Boolean> selectColumn = new TableColumn<>("");
        selectColumn.setCellValueFactory(new PropertyValueFactory<>("playSelected"));
        selectColumn.setCellFactory(CheckBoxTableCell.forTableColumn(index -> {
            setAllSelectedIndicator();
            return this.filteredGamesList.get(index).playSelectedProperty();
        }));
        this.gamesTable.getColumns().add(0, selectColumn);
    }

    /**
     * Returns a list of the selected games in the table view.
     *
     * @return The list of selected games.
     */
    public final List<ChessGameModel> getSelectedGames() {
        return this.gamesList.stream()
                .filter(item -> item.playSelectedProperty().getValue())
                .collect(Collectors.toList());
    }

}
