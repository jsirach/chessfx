/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils.controls;

import com.github.bhlangonijr.chesslib.Side;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nReplacePair;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nInGameKey;
import org.pidome.games.chess.resources.i18n.I18nReplaceKey;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.director.GameProxy;

/**
 * A popup showing who has won the game.
 *
 * @author johns
 */
public final class GameResultPopup extends GamePopup {

    /**
     * Text to be shown.
     */
    private final Label wonText = new Label();

    /**
     * Constructor.
     *
     * @param game The game that is being played.
     * @param side The side which has won.
     */
    public GameResultPopup(final GameProxy game, final Side side) {
        this(game, side, true);
    }

    /**
     * Constructor.
     *
     * @param game The game that is being played.
     * @param side The side which has won.
     * @param interactive If ok and or cancel buttons should be enabled.
     */
    public GameResultPopup(final GameProxy game, final Side side, boolean interactive) {
        super(I18nProxy.getInstance().get(
                side != null ? I18nApplicationKey.GAME_WIN_POPUP_TITLE_WIN : I18nApplicationKey.GAME_WIN_POPUP_TITLE_DRAW,
                getSideReplaceKey(Side.BLACK),
                getSideReplaceKey(Side.WHITE)
        ));
        wonText.setPadding(new Insets(0, 10, 0, 10));
        wonText.getStyleClass().add("text");
        if (side != null) {
            wonText.textProperty().bind(
                    I18nProxy.getInstance().get(I18nApplicationKey.GAME_WIN_POPUP_TEXT,
                            getSideReplaceKey(side),
                            new I18nReplacePair(I18nReplaceKey.playername,
                                    game.getPlayerBySide(side).nameProperty().getValueSafe())
                    )
            );
        } else {
            wonText.textProperty().bind(
                    I18nProxy.getInstance().get(I18nApplicationKey.GAME_WIN_POPUP_TEXT_DRAW)
            );
        }
        this.setCenter(wonText);
        if (interactive) {
            this.setOnAction(PopupAction.EMPTY);
        }
    }

    /**
     * Utility method for returning the value for the given side.
     *
     * @param side The side to get the value for.
     * @return The value found.
     */
    private static I18nReplacePair getSideReplaceKey(final Side side) {
        return new I18nReplacePair(I18nReplaceKey.side,
                I18nProxy.getInstance().get(side.equals(Side.BLACK) ? I18nInGameKey.PIECE_COLOR_BLACK : I18nInGameKey.PIECE_COLOR_WHITE).get());
    }

}
