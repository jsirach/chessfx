/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Some threading tools.
 *
 * @author johns
 */
public class ThreadTools {

    /**
     * Thread pool with two threads.
     */
    private static final ExecutorService DUAL_THREAD_POOL = Executors.newFixedThreadPool(2);

    /**
     * Runs a <code>Runnable</code> in a thread pool with two threads.
     *
     * Be aware in what you run, as only two threads run in the pool as absolute
     * maximum.
     *
     * There is no error checking whatsoever. If this is needed take care of
     * this yourself. Yes, you need to work for it.
     *
     * @param runnable The runnable to be executed in the thread pool.
     */
    public static void threadExecuted(final Runnable runnable) {
        DUAL_THREAD_POOL.submit(runnable);
    }

}
