package org.pidome.games.chess;

import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import static javafx.scene.layout.Region.USE_PREF_SIZE;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.Style;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.gui.desktop.DesktopView;
import org.pidome.games.chess.gui.desktop.dialog.ExitAppPopup;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.settings.GenericSettings;
import org.pidome.games.chess.settings.GraphicsSettings;
import org.pidome.games.chess.settings.Settings;

/**
 * Entry class.
 *
 * @author John Sirach
 */
public class Chess extends Application {

    /**
     * The application master window.
     */
    private static Stage applicationMasterStage;

    /**
     * When the stage is shown this is true.
     */
    private static final BooleanProperty stageShown = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(Chess.class);

    /**
     * JavaFX start entry.
     *
     * @param primaryStage The start stage.
     */
    @Override
    public void start(final Stage primaryStage) {
        System.setProperty("log4j.skipJansi", "true");
        GameResourcesHelper.preFetch();
        primaryStage.fullScreenExitKeyProperty().set(KeyCombination.NO_MATCH);
        primaryStage.setFullScreenExitHint("");
        primaryStage.setFullScreen(true);

        final StackPane masterRoot = new StackPane();
        masterRoot.getStyleClass().add("no-border");

        final DesktopView desktop = new DesktopView();
        masterRoot.getChildren().add(desktop);
        final Scene desktopScene = new Scene(masterRoot);

        // Keep as is currently, future impl of dark to light switching.
        final JMetro metro = new JMetro(desktopScene, Style.DARK);

        desktopScene.getStylesheets().add("/org/pidome/games/chess/styling/dark.css");
        desktopScene.getStylesheets().add("/org/pidome/games/chess/styling/skeleton.css");

        desktop.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
        desktop.prefHeightProperty().bind(primaryStage.heightProperty());
        desktop.prefWidthProperty().bind(primaryStage.widthProperty());

        primaryStage.setScene(desktopScene);

        primaryStage.getScene().getWindow().addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, (event) -> {
            System.exit(0);
        });

        applicationMasterStage = primaryStage;

        primaryStage.setOnShown((windowEvent) -> {
            stageShown.setValue(Boolean.TRUE);
            desktop.postActions();
        });

        // Use in settings, keeping as reminder.
        primaryStage.renderScaleXProperty().bind(Settings.get(GraphicsSettings.class).globalQualityAsDoubleProperty());
        primaryStage.renderScaleYProperty().bind(Settings.get(GraphicsSettings.class).globalQualityAsDoubleProperty());

        primaryStage.show();
        primaryStage.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode().equals(KeyCode.ESCAPE)) {
                event.consume();
                final ExitAppPopup popup = new ExitAppPopup();
                popup.display();
            }
        });

    }

    /**
     * Returns a read only boolean property for listening when the stage is
     * shown.
     *
     * @return Boolean property for when stage is shown.
     */
    public static ReadOnlyBooleanProperty getStageShown() {
        return ReadOnlyBooleanProperty.readOnlyBooleanProperty(stageShown);
    }

    /**
     * Returns the application window.
     *
     * The application window is the top most window. If a new stage/window is
     * requesting this window as an owner, keep in mind that if there is
     * <code>showAndWait</code> used, it will block all mouse events from
     * propagating below the popup.
     *
     * @return
     */
    public static final Stage getApplicationStage() {
        return applicationMasterStage;
    }

    /**
     * Default java start entry.
     *
     * @param args arguments
     */
    public static void main(final String[] args) {
        Settings.load(GenericSettings.class);
        Settings.load(GraphicsSettings.class);
        launch();
    }

}
