/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Base class for settings loading.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public abstract class Settings {

    /**
     * Base path for configurations.
     */
    private static final String CONFIG_BASE_PATH = "./resources/settings/";

    /**
     * Configuration file.
     */
    private final File configFile;

    /**
     * The properties holding the configuration.
     */
    protected Properties configuration;

    /**
     * If bindings are already created.
     */
    private boolean bindingsCreated = false;

    /**
     * Map containing the settings.
     */
    private static final Map<Class<? extends Settings>, Settings> SETTINGS_MAP = new HashMap<>();

    /**
     * Constructor.
     *
     * @param configFile The config file.
     */
    protected Settings(final String configFile) {
        this.configFile = new File(CONFIG_BASE_PATH + configFile);
    }

    public static <T extends Settings> T get(final Class<T> settingsClass) {
        load(settingsClass);
        if (SETTINGS_MAP.containsKey(settingsClass)) {
            return (T) SETTINGS_MAP.get(settingsClass);
        }
        return null;
    }

    public static <T extends Settings> void load(final Class<T> settingsClass) {
        try {
            if (!SETTINGS_MAP.containsKey(settingsClass)) {
                T settings = settingsClass.getConstructor().newInstance();
                settings.load();
                SETTINGS_MAP.put(settingsClass, settings);
            }
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Return instance config file.
     *
     * @return The config file.
     */
    private File getSettingFile() {
        return this.configFile;
    }

    /**
     * Loads settings from a properties file.
     */
    protected void load() {
        if (!configFile.exists()) {
            if (!configFile.getParentFile().exists()) {
                configFile.getParentFile().mkdir();
            }
            try {
                configFile.createNewFile();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to create default configuration file");
            }
        }
        configuration = new Properties();
        try ( InputStream stream = new FileInputStream(configFile)) {
            configuration.load(stream);
            if (!bindingsCreated) {
                bindingsCreated = true;
                createBindings();
            }
            setValues();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Saves the current values.
     */
    public static void save() {
        SETTINGS_MAP.forEach((key, settings) -> {
            try ( OutputStream stream = new FileOutputStream(settings.getSettingFile())) {
                settings.configuration.store(stream, "Settings save");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    public static void revert() {
        SETTINGS_MAP.forEach((key, settings) -> {
            settings.load();
        });
    }

    public void setBindingsCreated() {
        this.bindingsCreated = true;
    }

    protected abstract void createBindings();

    protected abstract void setValues();

}
