/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.settings;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;

/**
 * Graphics settings.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class GraphicsSettings extends Settings {

    /**
     * The configuration file.
     */
    private static final String CONFIG_FILE = "graphics.config";

    /**
     * Global quality.
     */
    private static final ObjectProperty<RenderQuality> globalQuality = new SimpleObjectProperty();

    /**
     * Global quality.
     */
    private static final BooleanProperty motion3DPieces = new SimpleBooleanProperty();

    /**
     * Global quality.
     */
    private static final BooleanProperty motion2DPieces = new SimpleBooleanProperty();

    /**
     * Protected constructor.
     */
    public GraphicsSettings() {
        super(CONFIG_FILE);
    }

    /**
     * the global graphical quality.
     *
     * @return The quality as decimal number.
     */
    public ObjectProperty<RenderQuality> globalQuality() {
        return globalQuality;
    }

    /**
     * 3D pieces motion enabled.
     *
     * @return 3D pieces motion enabled property.
     */
    public BooleanProperty motion3DPieces() {
        return motion3DPieces;
    }

    /**
     * 2D pieces motion enabled.
     *
     * @return 2D pieces motion enabled property.
     */
    public BooleanProperty motion2DPieces() {
        return motion2DPieces;
    }

    /**
     * the global quality render quality as double value.
     *
     * @return double binding.
     */
    public DoubleBinding globalQualityAsDoubleProperty() {
        return Bindings.createDoubleBinding(() -> globalQuality.getValue().getQualityLevel(), globalQuality);
    }

    /**
     * Sets any default values.
     */
    @Override
    protected void setValues() {
        globalQuality.setValue(
                RenderQuality.valueOf(
                        configuration.getProperty(Setting.GLOBAL_QUALITY.name(), RenderQuality.NORMAL.name())
                )
        );
        motion3DPieces.setValue(
                Boolean.valueOf(
                        configuration.getProperty(Setting.PIECE3D_MOVE_MOTION.name(), Boolean.TRUE.toString())
                )
        );
        motion2DPieces.setValue(
                Boolean.valueOf(
                        configuration.getProperty(Setting.PIECE2D_MOVE_MOTION.name(), Boolean.TRUE.toString())
                )
        );
    }

    /**
     * Creates bindings to mutate setting changes to the properties.
     */
    @Override
    protected void createBindings() {
        globalQuality.addListener((o, ov, nv) -> configuration.setProperty(Setting.GLOBAL_QUALITY.name(), nv.name()));
        motion3DPieces.addListener((o, ov, nv) -> configuration.setProperty(Setting.PIECE3D_MOVE_MOTION.name(), nv.toString()));
        motion2DPieces.addListener((o, ov, nv) -> configuration.setProperty(Setting.PIECE2D_MOVE_MOTION.name(), nv.toString()));
    }

    /**
     * Render quality option possibilities.
     */
    public enum RenderQuality {
        /**
         * Low quality.
         */
        //LOW(0.8, I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL_VALUE_LOW),
        /**
         * Normal quality.
         */
        NORMAL(1.0, I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL_VALUE_NORMAL),
        /**
         * High quality.
         */
        QUALITY_HIGH(2.0, I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL_VALUE_HIGH);

        /**
         * The level.
         */
        private final double qualityLevel;

        /**
         * The label property.
         */
        private final StringProperty label;

        /**
         * Constructor.
         *
         * @param qualityLevel The quality level.
         */
        RenderQuality(final double qualityLevel, final I18nApplicationKey labelKey) {
            this.qualityLevel = qualityLevel;
            label = I18nProxy.getStringProperty(labelKey);
        }

        /**
         * Return the level.
         *
         * @return
         */
        public double getQualityLevel() {
            return this.qualityLevel;
        }

        /**
         * Return the label of the value.
         *
         * @return The label.
         */
        public String getLabel() {
            return this.label.getValue();
        }

    }

    /**
     * The setting as in the properties file.
     */
    private enum Setting {
        /**
         * the global quality setting.
         */
        GLOBAL_QUALITY,
        /**
         * Animate motion from square to square.
         */
        PIECE3D_MOVE_MOTION,
        /**
         * Animate motion from square to square.
         */
        PIECE2D_MOVE_MOTION,
    }

}
