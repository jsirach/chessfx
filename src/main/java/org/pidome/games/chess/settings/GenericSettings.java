/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.settings;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.pidome.games.chess.resources.GameLanguages;

/**
 * Base class for user settings.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class GenericSettings extends Settings {

    /**
     * The configuration file.
     */
    private static final String CONFIG_FILE = "generic.config";

    /**
     * User locale.
     */
    private final StringProperty locale = new SimpleStringProperty();

    /**
     * Protected constructor.
     */
    public GenericSettings() {
        super(CONFIG_FILE);
    }

    /**
     * The locale setting property.
     *
     * @return The locale as string.
     */
    public StringProperty locale() {
        return this.locale;
    }

    /**
     * Creates bindings to mutate setting changes to the properties.
     */
    protected void createBindings() {
        this.locale.addListener((o, ov, nv) -> configuration.setProperty(Setting.LOCALE.name(), nv));
    }

    /**
     * Sets any default values.
     */
    protected void setValues() {
        locale.setValue(
                configuration.getProperty(Setting.LOCALE.name(), GameLanguages.DEFAULT_LOCALE_KEY_JAVA_NOTATION)
        );
    }

    /**
     * The setting as in the properties file.
     */
    private enum Setting {
        /**
         * The locale setting.
         */
        LOCALE
    }

}
