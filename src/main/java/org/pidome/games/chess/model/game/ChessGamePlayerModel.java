/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.game;

import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.game.PlayerType;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * A person in the game.
 *
 * @author johns
 */
public final class ChessGamePlayerModel {

    /**
     * The full name.
     */
    private final StringProperty name = new SimpleStringProperty();

    /**
     * the description.
     */
    private final StringProperty description = new SimpleStringProperty();

    /**
     * the long description.
     */
    private final StringProperty longDescription = new SimpleStringProperty();

    /**
     * Elo rating property.
     */
    private final IntegerProperty elo = new SimpleIntegerProperty();

    /**
     * the id.
     */
    private final StringProperty id = new SimpleStringProperty();

    /**
     * the player type.
     */
    private final ObjectProperty<PlayerType> playerType = new SimpleObjectProperty();

    /**
     * The player's play side.
     */
    private final ObjectProperty<Side> playerSide = new SimpleObjectProperty<>();

    /**
     * Image representation of the person.
     */
    private final StringProperty imageLocation = new SimpleStringProperty("/org/pidome/games/chess/images/profile/anonymous.png");

    /**
     * @return the name
     */
    public final StringProperty nameProperty() {
        return name;
    }

    /**
     * The description.
     *
     * @return The description string property.
     */
    public final StringProperty descriptionProperty() {
        return this.description;
    }

    /**
     * The long description.
     *
     * @return The long description string property.
     */
    public final StringProperty longDescriptionProperty() {
        return this.longDescription;
    }

    /**
     * The elo rating.
     *
     * @return The elo string property.
     */
    public final IntegerProperty eloProperty() {
        return this.elo;
    }

    /**
     * The id.
     *
     * @return The id string property.
     */
    public final StringProperty idProperty() {
        return this.id;
    }

    /**
     * The player type.
     *
     * @return The player type property.
     */
    public final ObjectProperty<PlayerType> playerTypeProperty() {
        return this.playerType;
    }

    /**
     * Returns the player side.
     *
     * This property's contents can be null.
     *
     * @return The player side object property.
     */
    public final ObjectProperty<Side> sideProperty() {
        return playerSide;
    }

    /**
     * @return the image
     */
    public final StringProperty imageLocationProperty() {
        return imageLocation;
    }

}
