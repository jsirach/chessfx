/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.options.boards;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;
import org.pidome.games.chess.model.options.GameOptions.GameViewType;

/**
 * Options for a custom flat board.
 *
 * @author johns
 */
public class CustomFlatBoardOptions extends BoardOptions {

    /**
     * Color of the white pieces.
     */
    private final ObjectProperty<Color> whiteColor = new SimpleObjectProperty<>();

    /**
     * color of the field for black pieces.
     */
    private final ObjectProperty<Color> blackColor = new SimpleObjectProperty<>();

    /**
     * Show rank and files.
     */
    private final BooleanProperty showBoardNotations = new SimpleBooleanProperty();

    /**
     * Color of the board markups.
     */
    private final ObjectProperty<Color> boardMarkupColor = new SimpleObjectProperty<>();

    /**
     * Custom flat board constructor.
     */
    public CustomFlatBoardOptions() {
        super(GameViewType.FLAT);
    }

}
