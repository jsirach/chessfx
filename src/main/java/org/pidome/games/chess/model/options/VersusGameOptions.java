/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.options;

import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * Options for a versus game.
 *
 * @author johns
 */
public final class VersusGameOptions extends AbstractInteractiveGameOptions {

    /**
     * Versus game types.
     */
    public enum VersusGameType {
        /**
         * A local game on a single pc.
         */
        LOCAL,
        /**
         * A game inside the same network.
         */
        NETWORK,
        /**
         * A game played over the internet.
         */
        INTERNET
    }

    /**
     * Constructor setting game play type.
     */
    public VersusGameOptions() {
        super(GamePlayMode.VERSUS, I18nApplicationKey.GAME_PLAY_TYPE_VERSUS);
    }

}
