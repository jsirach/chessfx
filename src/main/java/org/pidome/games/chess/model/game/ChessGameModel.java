/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.game;

import com.github.bhlangonijr.chesslib.game.Game;
import com.github.bhlangonijr.chesslib.game.GameResult;
import com.github.bhlangonijr.chesslib.game.Termination;
import java.time.LocalDate;
import java.time.LocalTime;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * Model for defining a chess game.
 *
 * @author johns
 */
public class ChessGameModel {

    /**
     * the id of the game.
     */
    private String gameId;

    /**
     * White player.
     */
    private ChessGamePlayerModel playerWhite = new ChessGamePlayerModel();

    /**
     * Black player.
     */
    private ChessGamePlayerModel playerBlack = new ChessGamePlayerModel();

    /**
     * The annotator.
     */
    private String annotator;

    /**
     * The game's date.
     */
    private LocalDate date;

    /**
     * Encyclopedia of Chess Openings notation.
     */
    private String eco;

    /**
     * If the game starts with an initial pieces setup.
     */
    private boolean initialPosition;

    /**
     * Localized representation of the opening played.
     */
    private String opening;

    /**
     * the amount of moves in the game.
     */
    private int plyCount;

    /**
     * The result of the game.
     */
    private GameResult result;

    /**
     * Reason of the game has ending.
     */
    private Termination termination;

    /**
     * The time a game took.
     */
    private LocalTime time;

    /**
     * The opening variation refined.
     */
    private String variation;

    /**
     * The variation refined.
     */
    private String subVariation;

    /**
     * A game's start position.
     *
     * When set also set <code>initialPosition</code> to true.
     */
    private String fen;

    /**
     * The chess game round.
     */
    private ChessEventRoundModel chessEventRound;

    /**
     * The raw original game being played object.
     */
    private Game game;

    /**
     * Helper for when selecting a game to be played in a player or other
     * interfaces.
     */
    private final BooleanProperty playSelectedProperty = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * Helper property for selecting if a game should be played.
     *
     * @return the boolean property containing true or false wether a game
     * should be selected.
     */
    public final BooleanProperty playSelectedProperty() {
        return this.playSelectedProperty;
    }

    /**
     * @return the playerWhite
     */
    public ChessGamePlayerModel getPlayerWhite() {
        return playerWhite;
    }

    /**
     * @param playerWhite the playerWhite to set
     */
    public void setPlayerWhite(ChessGamePlayerModel playerWhite) {
        this.playerWhite = playerWhite;
    }

    /**
     * @return the playerBlack
     */
    public ChessGamePlayerModel getPlayerBlack() {
        return playerBlack;
    }

    /**
     * @param playerBlack the playerBlack to set
     */
    public void setPlayerBlack(ChessGamePlayerModel playerBlack) {
        this.playerBlack = playerBlack;
    }

    /**
     * The game annotator.
     *
     * @return the annotator.
     */
    public final String getAnnotator() {
        return this.annotator;
    }

    /**
     * Sets the annotator.
     *
     * @param annotator The annotator to set.
     */
    public final void setAnnotator(String annotator) {
        this.annotator = annotator;
    }

    /**
     * The game's date.
     *
     * @return The date of the game start.
     */
    public final LocalDate getDate() {
        return this.date;
    }

    /**
     * The game's date.
     *
     * @param date The game dat to set.
     */
    public final void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * This is used for an opening designation from the five volume Encyclopedia
     * of Chess Openings. This tag pair is associated with the use of the EPD
     * opcode "eco"
     *
     * @returnChess opening tag pair.
     */
    public final String getEco() {
        return this.eco;
    }

    /**
     * Sets the opening tag designation from the five volume Encyclopedia of
     * Chess Openings. This tag pair is associated with the use of the EPD
     * opcode "eco"
     *
     * @param eco The tag to set.
     */
    public final void setEco(String eco) {
        this.eco = eco;
    }

    /**
     * The id of the game.
     *
     * @return String property with game id.
     */
    public final String getGameId() {
        return this.gameId;
    }

    /**
     * Sets the game id.
     *
     * @param gameId The game id to set.
     */
    public final void setGameId(String gameId) {
        this.gameId = gameId;
    }

    /**
     * If an initial game position has been set.
     *
     * When true the game must be configured with a FEN start notation.
     *
     * @return If the game must be started from an initial position.
     */
    public final boolean getInitialPosition() {
        return this.initialPosition;
    }

    /**
     * Sets the initial position.
     *
     * When set it requires a FEN to be set.
     *
     * @param initialPosition The initial position value.
     */
    public final void setInitialPosition(boolean initialPosition) {
        this.initialPosition = initialPosition;
    }

    /**
     * Returns the opening move.
     *
     * @return The opening move as text.
     */
    public final String getOpening() {
        return this.opening;
    }

    /**
     * Sets the opening move.
     *
     * @param opening The opening move to set.
     */
    public final void setOpening(String opening) {
        this.opening = opening;
    }

    /**
     * Amount of moves in the game.
     *
     * @return The amount of moves.
     */
    public final int getPlyCount() {
        return this.plyCount;
    }

    /**
     * Sets the moves play count.
     *
     * @param plyCount The moves play count to set.
     */
    public final void setPlyCount(int plyCount) {
        this.plyCount = plyCount;
    }

    /**
     * The result of the game.
     *
     * @return The game's result.
     */
    public final GameResult getResult() {
        return this.result;
    }

    /**
     * Sets the game result.
     *
     * @param result The result to set.
     */
    public final void setResult(GameResult result) {
        this.result = result;
    }

    /**
     * Game completion reason.
     *
     * @return The reason why a game has ended.
     */
    public final Termination getTermination() {
        return this.termination;
    }

    /**
     * Sets the game termination.
     *
     * @param termination The termination to set.
     */
    public final void setTermination(Termination termination) {
        this.termination = termination;

    }

    /**
     * The time a game lasted.
     *
     * @return local time of the length of a game.
     */
    public final LocalTime getTime() {
        return this.time;
    }

    /**
     * Sets the time a game took.
     *
     * @param time The time to set.
     */
    public final void setTime(LocalTime time) {
        this.time = time;
    }

    /**
     * Refining of the opening tag.
     *
     * @return String property with the refining of the opening tag.
     */
    public final String getVariation() {
        return this.variation;
    }

    /**
     * Sets the opening refinement.
     *
     * @param variation The opening refinement to set.
     */
    public final void setVariation(String variation) {
        this.variation = variation;
    }

    /**
     * Further refinement of the variation tag.
     *
     * @return String property with the refining of the variation tag.
     */
    public final String subVariationProperty() {
        return this.subVariation;
    }

    /**
     * Sets the variation refinement
     *
     * @param subVariation The variation refinement to set.
     */
    public final void setSubVariation(String subVariation) {
        this.subVariation = subVariation;
    }

    /**
     * Returns the games start position.
     *
     * @return The start position as string set.
     */
    public final String getFen() {
        return this.fen;
    }

    /**
     * Set the game's start position.
     *
     * When set, also set the <code>initialPosition</code> property.
     *
     * @param fen The start position to set.
     */
    public final void setFen(String fen) {
        this.fen = fen;
    }

    /**
     * @return the chessEvent
     */
    public ChessEventRoundModel getChessEventRound() {
        return chessEventRound;
    }

    /**
     * @param chessEventRound the chessEvent round to set
     */
    public void setChessEventRound(ChessEventRoundModel chessEventRound) {
        this.chessEventRound = chessEventRound;
    }

    /**
     * @return the game
     */
    public Game getGame() {
        return game;
    }

    /**
     * @param game the game to set
     */
    public void setGame(Game game) {
        this.game = game;
    }

}
