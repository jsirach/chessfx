/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.game;

import java.util.ArrayList;

/**
 * List of events.
 *
 * @author johns
 */
public class ChessEvents extends ArrayList<ChessEventModel> {

    /**
     * Check if an event exists in the list.
     *
     * @param eventName The name of the event.
     * @return True if an event exists.
     */
    public final boolean hasEventModel(final String eventName) {
        return stream().anyMatch(event -> event.getName().equals(eventName));
    }

    /**
     * Returns the model based on the given name.
     *
     * @param eventName The event name.
     * @return the event model.
     */
    public final ChessEventModel getEventModel(final String eventName) {
        return stream().filter(event -> event.getName().equals(eventName))
                .findAny().get();
    }

}
