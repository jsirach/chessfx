/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.options;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.pidome.games.chess.model.options.boards.FlatBoardOptions;
import org.pidome.games.chess.model.options.boards.PerspectiveBoardOptions;
import org.pidome.games.chess.resources.SceneResource;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.world.views.AbstractGameView;
import org.pidome.games.chess.world.views.DefaultGameView;
import org.pidome.games.chess.world.views.FlatGameView;
import org.pidome.games.chess.world.views.PerspectiveGameView;

/**
 * Default base options for playing any game.
 *
 * @author johns
 */
public abstract class GameOptions {

    /**
     * The visual view type.
     */
    public enum GamePlayMode {
        /**
         * Default view type.
         */
        VERSUS,
        /**
         * Spectator view type.
         */
        SPECTATOR,
        /**
         * The player view type.
         */
        PLAYER,
        /**
         * Game type with freeform playfield and a window containing a game
         * field.
         */
        BROADCASTER,
        /**
         * Game type for the scene editor.
         */
        EDITOR;
    }

    /**
     * View type when spectating.
     */
    public enum GameViewType {
        /**
         * If only perspective is viewed.
         */
        PERSPECTIVE(TypeConstant.PERSPECTIVE, PerspectiveGameView.class,
                "/org/pidome/games/chess/images/views/3D.png"),
        /**
         * If only flat is viewed.
         */
        FLAT(TypeConstant.FLAT, FlatGameView.class,
                "/org/pidome/games/chess/images/views/2D.png"),
        /**
         * When both are viewed.
         */
        BOTH(TypeConstant.BOTH, DefaultGameView.class,
                "/org/pidome/games/chess/images/views/3D_2D.png");

        /**
         * Constant value reference.
         */
        private final int constantTypeId;

        /**
         * The preview image.
         */
        private final String previewImage;

        /**
         * the gameview type.
         */
        private final Class<? extends AbstractGameView> gameViewClass;

        /**
         * Constructor.
         *
         * @param constantTypeId identifiable int.
         */
        private GameViewType(final int constantTypeId, final Class<? extends AbstractGameView> gameViewClass,
                final String previewImage) {
            this.constantTypeId = constantTypeId;
            this.gameViewClass = gameViewClass;
            this.previewImage = previewImage;
        }

        /**
         * Returns the enum by given constant type id.
         *
         * Advised to use <code>GameViewType.TypeConstant</code> constants to
         * make sure no <code>IllegalArgumentException</code> is thrown and it's
         * clear which enum will be returned.
         *
         * @param constantTypeId The int representation of the enum which must
         * exactly match <code>GameViewType.constantTypeId;</code>
         * @return The enum found. If no enum is found which exactly matches
         * <code>GameViewType.constantTypeId;</code>
         */
        public static final GameViewType byConstant(final int constantTypeId) {
            for (GameViewType type : GameViewType.values()) {
                if (type.constantTypeId == constantTypeId) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Unknown type id " + constantTypeId);
        }

        /**
         * Returns the class associated with the view.
         *
         * @return The game view class.
         */
        public Class<? extends AbstractGameView> getGameViewClass() {
            return this.gameViewClass;
        }

        /**
         * Returns the descrption in the correct language for the enum.
         *
         * @return
         */
        public String getDescription() {
            switch (this.constantTypeId) {
                case TypeConstant.PERSPECTIVE:
                    return getFromKey(I18nApplicationKey.NEW_GAME_OPTIONS_VIEW_3D);
                case TypeConstant.FLAT:
                    return getFromKey(I18nApplicationKey.NEW_GAME_OPTIONS_VIEW_2D);
                default:
                    return getFromKey(I18nApplicationKey.NEW_GAME_OPTIONS_VIEW_2D3D);
            }
        }

        /**
         * Return the preview image.
         *
         * @return The preview image.
         */
        public String getPreviewImage() {
            return this.previewImage;
        }

        /**
         * Returns the text from the given key.
         *
         * @param key The key to get for.
         * @return The String belonging to the key.
         */
        private String getFromKey(final I18nApplicationKey key) {
            return I18nProxy.getInstance().get(key).get();
        }

        /**
         * Constants to be used for correctly string identify the enum.
         */
        public static class TypeConstant {

            /**
             * Constant for perspective.
             */
            public static final int PERSPECTIVE = 1;
            /**
             * Constant for flat.
             */
            public static final int FLAT = 2;
            /**
             * Constant for both.
             */
            public static final int BOTH = 3;
        }

    }

    /**
     * The scene type.
     */
    public enum SceneViewType {
        /**
         * If only perspective is viewed.
         */
        PERSPECTIVE(GameViewType.TypeConstant.PERSPECTIVE),
        /**
         * Flat scene view.
         */
        FLAT(GameViewType.TypeConstant.FLAT);

        /**
         * Constant value reference.
         */
        private final int constantTypeId;

        /**
         * Constructor.
         *
         * @param constantTypeId identifiable int.
         */
        private SceneViewType(final int constantTypeId) {
            this.constantTypeId = constantTypeId;
        }

        /**
         * The constant type id.
         *
         * @return The id.
         */
        public final int getConstantTypeId() {
            return this.constantTypeId;
        }

        /**
         * Checks if the scene type is compatible with the given game view type.
         *
         * @param viewType The view type to compare to.
         * @return true when compatible.
         */
        public boolean compatibleWithGameViewType(final GameViewType viewType) {
            return viewType.constantTypeId == this.constantTypeId;
        }

    }

    /**
     * The view type of the game.
     */
    private final ObjectProperty<GameViewType> gameViewType = new SimpleObjectProperty<>(GameViewType.BOTH);

    /**
     * The play type of the game.
     */
    private final ObjectProperty<GamePlayMode> gamePlayMode = new SimpleObjectProperty<>();

    /**
     * 3D board options.
     */
    private final ObjectProperty<PerspectiveBoardOptions> board3DOptions = new SimpleObjectProperty<>();

    /**
     * The perspective scene resource options.
     */
    private final ObjectProperty<SceneResource> scene3DResource = new SimpleObjectProperty<>();

    /**
     * 2D board options.
     */
    private final ObjectProperty<FlatBoardOptions> board2DOptions = new SimpleObjectProperty<>();

    /**
     * The perspective scene resource options.
     */
    private final ObjectProperty<SceneResource> scene2DResource = new SimpleObjectProperty<>();

    /**
     * The game scene.
     */
    private final ObjectProperty<Class<? extends AbstractGameView>> gameView = new SimpleObjectProperty<>();

    /**
     * The game type naming in natural language.
     */
    private final StringProperty gameTypeName = new SimpleStringProperty("");

    /**
     * Constructor.
     *
     * @param gamePlayType The view type of the game.
     */
    public GameOptions(final GamePlayMode gamePlayType) {
        this.gamePlayMode.setValue(gamePlayType);
    }

    /**
     * Sets the naming of the game type.
     *
     * @param namingKey The I18n key for the type naming.
     */
    protected final void setNaming(final I18nApplicationKey namingKey) {
        gameTypeName.setValue(I18nProxy.getInstance().get(namingKey).get());
    }

    /**
     * Returns the game type naming property.
     *
     * @return Property with game type naming.
     */
    public final StringProperty getGameTypeNameProperty() {
        return gameTypeName;
    }

    /**
     * Returns the game scene.
     *
     * @return The game scene extending <code>AbstractGameScene</code>.
     */
    public final ObjectProperty<Class<? extends AbstractGameView>> getGameView() {
        return this.gameView;
    }

    /**
     * Returns the game's play type.
     *
     * @return The read only play type.
     */
    public final ObjectProperty<GamePlayMode> gamePlayModeProperty() {
        return this.gamePlayMode;
    }

    /**
     * Returns the game's play type.
     *
     * @return The play type.
     */
    public final ReadOnlyObjectProperty<GamePlayMode> gameReadOnlyPlayModeProperty() {
        return new ReadOnlyObjectWrapper(gamePlayModeProperty().get()).getReadOnlyProperty();
    }

    /**
     * Returns the view type.
     *
     * @return The view type.
     */
    public final ObjectProperty<GameViewType> gameViewTypeProperty() {
        return this.gameViewType;
    }

    /**
     * Returns the view type.
     *
     * @return The view type.
     */
    public final ReadOnlyObjectProperty<GameViewType> gameReadonlyViewTypeProperty() {
        return new ReadOnlyObjectWrapper(gameViewTypeProperty().get()).getReadOnlyProperty();
    }

    /**
     * Get the 3d scene.
     *
     * @return The 3d scene.
     */
    public final ObjectProperty<SceneResource> getScene3DResource() {
        return this.scene3DResource;
    }

    /**
     * 3D Options board.
     *
     * @return board options.
     */
    public final ObjectProperty<PerspectiveBoardOptions> perspectiveBoardOptions() {
        return this.board3DOptions;
    }

    /**
     * 3D Options board.
     *
     * @return board options.
     */
    public final ReadOnlyObjectProperty<PerspectiveBoardOptions> getPerspectiveBoardOptions() {
        return new ReadOnlyObjectWrapper(perspectiveBoardOptions().get()).getReadOnlyProperty();
    }

    /**
     * Get the 2D scene.
     *
     * @return The 2D scene.
     */
    public final ObjectProperty<SceneResource> getScene2DResource() {
        return this.scene2DResource;
    }

    /**
     * 2D Options board.
     *
     * @return board options.
     */
    public final ObjectProperty<FlatBoardOptions> flatBoardOptions() {
        return this.board2DOptions;
    }

    /**
     * 2D Options board.
     *
     * @return board options.
     */
    public final ReadOnlyObjectProperty<FlatBoardOptions> getFlatBoardOptions() {
        return new ReadOnlyObjectWrapper(flatBoardOptions().get()).getReadOnlyProperty();
    }

}
