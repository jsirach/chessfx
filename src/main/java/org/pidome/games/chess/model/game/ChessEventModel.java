/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.game;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * A single game event.
 *
 * @author johns
 */
public class ChessEventModel {

    /**
     * The event id.
     */
    private String id;

    /**
     * The event name.
     */
    private String name;

    /**
     * Event location.
     */
    private String site;

    /**
     * The date of the event.
     */
    private LocalDate eventDate;

    /**
     * Rounds in the event.
     */
    private List<ChessEventRoundModel> rounds = new ArrayList<>();

    /**
     * If the event is an artifical event.
     *
     * Artificial events are automatically created events to be conform model
     * contract.
     *
     */
    private boolean artificial = false;

    /**
     * Sets if an event is artificial or not.
     *
     * @param artificial true to set artificial.
     */
    public final void setIsArtificial(boolean artificial) {
        this.artificial = artificial;
    }

    /**
     * Returns if the game is artificial or not.
     *
     * @return
     */
    public boolean isArtificial() {
        return this.artificial;
    }

    /**
     * Returns true if the event contains a single game.
     *
     * @return If the event contains a single game.
     */
    public final boolean isSingleGame() {
        return this.rounds.size() == 1 && this.rounds.get(0).getGames().size() == 1;
    }

    /**
     * Returns the id of the event.
     *
     * @return The event id.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the id of the event.
     *
     * @param id The event id to set.
     */
    public final void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * @param site the site to set
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * @return the eventDate
     */
    public LocalDate getEventDate() {
        return eventDate;
    }

    /**
     * @param eventDate the eventDate to set
     */
    public void setEventDate(LocalDate eventDate) {
        this.eventDate = eventDate;
    }

    /**
     * @return the rounds
     */
    public List<ChessEventRoundModel> getRounds() {
        return rounds;
    }

    /**
     * @param rounds the rounds to set
     */
    public void setRounds(List<ChessEventRoundModel> rounds) {
        this.rounds = rounds;
    }

    /**
     * Convenience method to add a round to the model.
     *
     * @param round The round to add to the model.
     */
    public final void addRound(ChessEventRoundModel round) {
        getRounds().add(round);
    }

    /**
     * Checks if a round exists with the given notation.
     *
     * @param roundNotation The round notation.
     * @return If found or not.
     */
    public boolean hasRound(final String roundNotation) {
        return getRounds().stream().anyMatch(round -> round.getNotation().equals(roundNotation));
    }

    /**
     * Returns the round model.
     *
     * @return The model
     */
    public ChessEventRoundModel getChessEventRoundModel(final String roundNotation) {
        return getRounds().stream().filter(round -> round.getNotation().equals(roundNotation))
                .findAny().get();
    }

}
