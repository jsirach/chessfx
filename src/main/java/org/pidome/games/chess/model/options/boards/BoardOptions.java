/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.options.boards;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.pidome.games.chess.model.options.GameOptions.GameViewType;
import org.pidome.games.chess.resources.BoardResource;
import org.pidome.games.chess.resources.PiecesResource;

/**
 * The generic board options options container.
 *
 * @author johns
 */
public abstract class BoardOptions {

    /**
     * If the board is a perspective board.
     */
    private final ObjectProperty<GameViewType> viewType = new SimpleObjectProperty();

    /**
     * Which game board to use.
     */
    private final ObjectProperty<BoardResource> gameBoard = new SimpleObjectProperty<>();

    /**
     * Which game board to use.
     */
    private final ObjectProperty<PiecesResource> gamePieces = new SimpleObjectProperty<>();

    /**
     * Board options constructor.
     *
     * @param viewType The view type for these board options.
     */
    public BoardOptions(GameViewType viewType) {
        this.viewType.setValue(viewType);
    }

    /**
     * Returns if the board is perspective or not.
     *
     * @return if the board is a perspective one.
     */
    public final ObjectProperty<GameViewType> viewTypeProperty() {
        return this.viewType;
    }

    /**
     * Which game board to use.
     *
     * @return property for the game board.
     */
    public final ObjectProperty<BoardResource> gameBoardProperty() {
        return this.gameBoard;
    }

    /**
     * Which pieces to use.
     *
     * @return property for the pieces.
     */
    public final ObjectProperty<PiecesResource> gamePiecesProperty() {
        return this.gamePieces;
    }

    /**
     * String representation of this class.
     *
     * @return This class as string.
     */
    @Override
    public final String toString() {
        return new StringBuilder("BoardOptions: [")
                .append("perspective: ").append(viewType)
                .append("gameBoard: ").append(gameBoard)
                .append("gamePieces: ").append(gamePieces)
                .append("]").toString();
    }

}
