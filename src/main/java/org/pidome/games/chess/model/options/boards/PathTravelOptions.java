/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.options.boards;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

/**
 * Options for travel paths for pieces.
 *
 * @author johns
 */
public final class PathTravelOptions extends SimpleBooleanProperty {

    /**
     * The default thickness of the traveled path of a piece.
     */
    public static final double DEFAULT_TRAVEL_PATH_THICKNESS = 11.25;

    /**
     * White pieces travel path color property.
     */
    private final ObjectProperty<Color> whitePathColorProperty = new SimpleObjectProperty(Color.AQUA);

    /**
     * Black pieces travel path color property.
     */
    private final ObjectProperty<Color> blackPathColorProperty = new SimpleObjectProperty(Color.DARKSLATEBLUE);

    /**
     * The thickness of the follow path line.
     */
    private final DoubleProperty lineThicknessProperty = new SimpleDoubleProperty(PathTravelOptions.DEFAULT_TRAVEL_PATH_THICKNESS / 2);

    /**
     * The paths opacity.
     *
     * This is a supplemental value and not directly bound to the colors. It
     * must be set manually on the objects which apply the colors.
     */
    private final DoubleProperty pathOpacityProperty = new SimpleDoubleProperty(.5);

    /**
     * Constructor setting initial value.
     *
     * @param initialValue The initial boolean value.
     */
    public PathTravelOptions(final boolean initialValue) {
        super(initialValue);
    }

    /**
     * White pieces travel path color property.
     *
     * @return Paint object property.
     */
    public final ObjectProperty<Color> whitePathColorProperty() {
        return this.whitePathColorProperty;
    }

    /**
     * Black pieces travel path color property.
     *
     * @return Paint object property.
     */
    public final ObjectProperty<Color> blackPathColorProperty() {
        return this.blackPathColorProperty;
    }

    /**
     * Returns the property containing the thickness of the path line.
     *
     * @return Double property.
     */
    public final DoubleProperty lineThicknessProperty() {
        return this.lineThicknessProperty;
    }

    /**
     * Returns the opacity of the paths.
     *
     * This is a supplemental value and not directly bound to the colors. It
     * must be set manually on the objects which apply the colors.
     *
     * @return The paths opacity.
     */
    public final DoubleProperty pathOpacityProperty() {
        return this.pathOpacityProperty;
    }

}
