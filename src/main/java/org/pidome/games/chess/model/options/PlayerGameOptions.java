/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.options;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * Options for replaying a recorded game.
 *
 * @author johns
 */
public final class PlayerGameOptions extends AbstractInteractiveGameOptions {

    /**
     * The games to be played.
     */
    private final ObservableList<ChessGameModel> games = FXCollections.observableArrayList();

    /**
     * Enable demo mode.
     *
     * In the perspective view it is possible to disable navigation, so when
     * demo mode is enabled, you will not be allowed to move pieces. and the
     * camera will be floating around the board.
     *
     * Depending on the chosen scenery you could theoretically play on the 2d
     * board while the 3D board follows the moves with the moving camera. When
     * this would be implemented that is.
     */
    private final BooleanProperty demoMode = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * Constructor setting PLAYER type.
     */
    public PlayerGameOptions() {
        super(GamePlayMode.PLAYER, I18nApplicationKey.GAME_PLAY_TYPE_PLAYER);
    }

    /**
     * Returns an observable list with games to be played.
     *
     * @return List of playable games.
     */
    public ObservableList<ChessGameModel> games() {
        return this.games;
    }

    /**
     * Demo enabled or disabled.
     *
     * @return Boolean.TRUE when enabled.
     */
    public BooleanProperty demoMode() {
        return this.demoMode;
    }

}
