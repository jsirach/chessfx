/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.options;

import com.github.bhlangonijr.chesslib.Side;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.pidome.games.chess.engines.AbstractGameEngine;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;
import org.pidome.games.chess.model.options.boards.PathTravelOptions;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * A set of options for generic games.
 *
 * @author johns
 */
public abstract class AbstractGenericGameOptions extends GameOptions {

    /**
     * Show the path a piece has traveled after a move.
     */
    private final PathTravelOptions showTraveledPathProperty = new PathTravelOptions(Boolean.TRUE);

    /**
     * Shows an indicator about the kings situation when in check of mate
     * position.
     */
    private final BooleanProperty showKingCheckIndicatorProperty = new SimpleBooleanProperty(Boolean.TRUE);

    /**
     * Which game engine should be used to play a game.
     */
    private final ObjectProperty<Class<? extends AbstractGameEngine>> gameEngineProperty = new SimpleObjectProperty<>();

    /**
     * If legal steps should be shown when a piece is selected.
     */
    private final BooleanProperty showLegalStepsIndicatorProperty = new SimpleBooleanProperty(Boolean.TRUE);

    /**
     * Object property containing the current game model.
     */
    private ObjectProperty<ChessGameModel> currentGame = new SimpleObjectProperty(new ChessGameModel());

    /**
     * Returns the game id.
     */
    private String gameId;

    private boolean editMode = Boolean.FALSE;

    /**
     * Constructor.
     *
     * @param gamePlayType The gameplay type.
     * @param gameTypeNamingKey The game type naming key.
     */
    public AbstractGenericGameOptions(final GamePlayMode gamePlayType, final I18nApplicationKey gameTypeNamingKey) {
        super(gamePlayType);
        this.setNaming(gameTypeNamingKey);
    }

    /**
     * If in edit mode or not.
     *
     * @return true when in edit mode.
     */
    public boolean isEditMode() {
        return this.editMode;
    }

    /**
     * Set edit mode.
     *
     * This has no use if a normal game mode is started.
     *
     * @param editMode true to enabled edit mode.
     */
    public final void setEditMode(final boolean editMode) {
        this.editMode = editMode;
    }

    /**
     * Set the game id.
     *
     * @param gameId The game id.
     */
    public void setGameId(final String gameId) {
        this.gameId = gameId;
    }

    /**
     * Returns the game id.
     *
     * @return The game id.
     */
    public final String getGameId() {
        return this.gameId;
    }

    /**
     * Returns a game player by the given side.
     *
     * @param side The side to get the player for.
     * @return A game player.
     */
    public final ChessGamePlayerModel playerbySide(final Side side) {
        switch (side) {
            case WHITE:
                return this.currentGame.getValue().getPlayerWhite();
            default:
                return this.currentGame.getValue().getPlayerBlack();
        }
    }

    /**
     * The current game being played.
     *
     * @return The object property to bind to for determining the current game
     * being played.
     */
    public final ObjectProperty<ChessGameModel> currentGameProperty() {
        return this.currentGame;
    }

    /**
     * Returns The property containing the game engine.
     *
     * @return The game engine property.
     */
    public final ObjectProperty<Class<? extends AbstractGameEngine>> gameEngineProperty() {
        return gameEngineProperty;
    }

    /**
     * Returns the property containing the indicator if a king check should be
     * highlighted or not.
     *
     * @return The king highlight property.
     */
    public final BooleanProperty showKingCheckProperty() {
        return showKingCheckIndicatorProperty;
    }

    /**
     * Returns the property containing the indicator if the path traveled by a
     * moved piece should be shown or not.
     *
     * @return The property with the piece travel path indicator.
     */
    public final PathTravelOptions showTraveledPathProperty() {
        return showTraveledPathProperty;
    }

    /**
     * Returns the property containing the indicator if legal steps should be
     * shown or not.
     *
     * @return The property with the legal steps indicator.
     */
    public final BooleanProperty showLegalStepsProperty() {
        return showLegalStepsIndicatorProperty;
    }

}
