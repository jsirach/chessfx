/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.move;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.pidome.games.chess.events.GameEventListener;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.events.SquareIntentEvent;
import org.pidome.games.chess.events.SquareSelectEvent;

/**
 * Base class to record moves during a game.
 *
 * @author John
 */
public final class Moves implements GameEventListener {

    /**
     * The id of the game.
     */
    private final String gameId;

    /**
     * The observable list of moves.
     */
    private final ObservableList<MoveSet> moves = FXCollections.observableArrayList();

    /**
     * Constructor.
     *
     * @param gameId The string of the game id.
     */
    public Moves(final String gameId) {
        this.gameId = gameId;
    }

    /**
     * Registers the moves object to start listening for game events.
     */
    public final void registerForEvents() {
        GameEventsPublisher.registerForEvents(gameId, this);
    }

    /**
     * Returns the moves observable list.
     *
     * @return the list of moves.
     */
    public ObservableList<MoveSet> getMovesObservable() {
        return this.moves;
    }

    /**
     * USed to reset the view.
     *
     * @param gameState The game state event.
     */
    @Override
    public void onGameStateChangeEvent(final GameStateEvent gameState) {
        switch (gameState.getSource()) {
            case RESET:
                moves.clear();
                break;

        }
    }

    /**
     * Note used.
     *
     * @param moveEvent The move intent event.
     */
    @Override
    public void onPieceMoveIntentEvent(final MoveEvent moveEvent) {
        /// Not used.
    }

    /**
     * To update the list with the move done.
     *
     * @param moveEvent The move done event.
     */
    @Override
    public void onPieceMovedEvent(final MoveEvent moveEvent) {
        switch (moveEvent.getEventType()) {
            case MOVE:
                switch (moveEvent.getPiece().getPieceSide()) {
                    case WHITE:
                        final MoveSet moveSet = new MoveSet();
                        moveSet.setTurn(moves.size() + 1);
                        moveSet.addMoveEvent(moveEvent);
                        moves.add(moveSet);
                        break;
                    case BLACK:
                        moves.get(moves.size() - 1).addMoveEvent(moveEvent);
                        break;
                }
                break;
        }
    }

    /**
     * Not used.
     *
     * This could be used in the future to act as a ghost move in the list.
     *
     * @param selectEvent The piece selection event.
     */
    @Override
    public void onSquareSelectedEvent(final SquareSelectEvent selectEvent) {
        /// Also, not used.
    }

    /**
     * Not used.
     *
     * @param squareIntentEvent The event indicating an event on an square.
     */
    @Override
    public void onSquareIntentEvent(final SquareIntentEvent squareIntentEvent) {
        // Not used
    }

}
