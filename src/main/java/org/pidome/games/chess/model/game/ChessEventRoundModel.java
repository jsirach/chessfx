/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.game;

import java.util.ArrayList;
import java.util.List;

/**
 * A single round in an event.
 *
 * @author johns
 */
public class ChessEventRoundModel {

    /**
     * The round in the event.
     */
    private int roundNumber;

    /**
     * The chess event in this model.
     */
    private ChessEventModel chessEvent;

    /**
     * The games in this round.
     */
    private List<ChessGameModel> games = new ArrayList<>();

    /**
     * The round notation.
     */
    private String notation;

    /**
     * Sets the round notation.
     *
     * @param roundNotation The round notation.
     */
    public void setNotation(final String roundNotation) {
        this.notation = roundNotation;
    }

    /**
     * Returns the round notation.
     *
     * @return The notation.
     */
    public String getNotation() {
        return this.notation;
    }

    /**
     * @return the roundNumber
     */
    public int getRoundNumber() {
        return roundNumber;
    }

    /**
     * @param roundNumber the roundNumber to set
     */
    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    /**
     * @return the games
     */
    public List<ChessGameModel> getGames() {
        return games;
    }

    /**
     * @param games the games to set
     */
    public void setGames(List<ChessGameModel> games) {
        this.games = games;
    }

    /**
     * Convenience method to add a game to the list.
     *
     * @param game The game to add to the list.
     */
    public final void addGame(ChessGameModel game) {
        this.games.add(game);
    }

    /**
     * @return the chessEvent
     */
    public ChessEventModel getChessEvent() {
        return chessEvent;
    }

    /**
     * @param chessEvent the chessEvent to set
     */
    public void setChessEvent(ChessEventModel chessEvent) {
        this.chessEvent = chessEvent;
    }

    /**
     * Checks if a game exists in the round.
     *
     * @param gameId The game id.
     * @return true when it exists.
     */
    public boolean hasGame(final String gameId) {
        return getGames().stream().anyMatch(game -> game.getGameId().equals(gameId));
    }

    /**
     * Returns the the game of the given id.
     *
     * @param gameId The game id.
     * @return The found game.
     */
    public ChessGameModel getGame(final String gameId) {
        return getGames().stream().filter(game -> game.getGameId().equals(gameId))
                .findAny().get();
    }

}
