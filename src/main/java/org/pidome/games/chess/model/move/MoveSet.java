/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.model.move;

import com.github.bhlangonijr.chesslib.PieceType;
import com.github.bhlangonijr.chesslib.move.Move;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.world.pieces.PieceInterface;

/**
 * A single move set.
 *
 * @author John
 */
public class MoveSet {

    /**
     * Turn number.
     */
    private int turn;

    /**
     * The move made by white.
     */
    private final ObjectProperty<Move> moveWhite = new SimpleObjectProperty();

    /**
     * The move made by black.
     */
    private final ObjectProperty<Move> moveBlack = new SimpleObjectProperty();

    /**
     * The string output for white.
     */
    private final StringProperty moveWhiteString = new SimpleStringProperty();

    /**
     * The string output for black.
     */
    private final StringProperty moveBlackString = new SimpleStringProperty();

    /**
     * @return the turn
     */
    public final int getTurn() {
        return turn;
    }

    /**
     * @param turn the turn to set
     */
    public final void setTurn(final int turn) {
        this.turn = turn;
    }

    /**
     * @return the moveWhite
     */
    public final ObjectProperty<Move> getMoveWhiteProperty() {
        return moveWhite;
    }

    /**
     * @param moveWhite the moveWhite to set
     */
    public final void setMoveWhite(final Move moveWhite) {
        this.moveWhite.setValue(moveWhite);
        if (moveWhite == null) {
            this.moveWhiteString.setValue("");
        }
    }

    /**
     * @return the moveBlack
     */
    public final ObjectProperty<Move> getMoveBlackProperty() {
        return moveBlack;
    }

    /**
     * @param moveBlack the moveBlack to set
     */
    public final void setMoveBlack(final Move moveBlack) {
        this.moveBlack.setValue(moveBlack);
        if (moveWhite == null) {
            this.moveBlackString.setValue("");
        }
    }

    /**
     * @return the moveWhiteString
     */
    public final StringProperty getMoveWhiteStringProperty() {
        return moveWhiteString;
    }

    /**
     * @param moveWhiteString the moveWhiteString to set
     */
    private void setMoveWhiteString(final String moveWhiteString) {
        this.moveWhiteString.setValue(moveWhiteString);
    }

    /**
     * @return the moveBlackString
     */
    public final StringProperty getMoveBlackStringProperty() {
        return moveBlackString;
    }

    /**
     * @param moveBlackString the moveBlackString to set
     */
    private void setMoveBlackString(final String moveBlackString) {
        this.moveBlackString.setValue(moveBlackString);
    }

    /**
     * Adds a move event.
     *
     * @param moveEvent
     */
    public final void addMoveEvent(final MoveEvent moveEvent) {
        final String publicize = getNotation(moveEvent);
        switch (moveEvent.getPiece().getPieceSide()) {
            case BLACK:
                this.setMoveBlack(moveEvent.getSource());
                this.setMoveBlackString(publicize);
                break;
            case WHITE:
                this.setMoveWhite(moveEvent.getSource());
                this.setMoveWhiteString(publicize);
                break;
        }
    }

    /**
     * Returns the notation string for a move.
     *
     * @param moveEvent The move event.
     * @return The notation string.
     */
    private static String getNotation(final MoveEvent moveEvent) {
        return getPieceTranslation(moveEvent.getPiece().getPieceType())
                + isCaptureMove(moveEvent)
                + moveEvent.getSource().getTo().name().toLowerCase();
    }

    /**
     * If the move is a capture move.
     *
     * @param move The move
     * @return x when it is a capture move.
     */
    private static String isCaptureMove(final MoveEvent event) {
        return constainsMoveType(event, MoveEvent.MoveType.MOVE_CAPTURE) ? "x" : "";
    }

    /**
     * Returns true if a specific move type is included.
     *
     * @param event The event.
     * @param type The type to search for.
     * @return true if it contains.
     */
    private static boolean constainsMoveType(final MoveEvent event, final MoveEvent.MoveType type) {
        if (!event.getMoveTypes().isEmpty()) {
            return event.getMoveTypes().contains(type);
        }
        return false;
    }

    /**
     * Returns the short notation of a piece type.
     *
     * @param type The type to get the piece for.
     * @return String representation of the type.
     */
    private static String getPieceTranslation(final PieceType type) {
        final PieceInterface.Variant variant = PieceInterface.Variant.getByType(type.name());
        if (variant != null) {
            return variant.name();
        } else {
            return "";
        }
    }

}
