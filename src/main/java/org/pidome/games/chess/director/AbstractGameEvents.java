/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.director;

import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.pidome.games.chess.events.GameEventListener;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.events.SquareSelectEvent;

/**
 * Base game events class suitable to be integrated by engines and remote
 * connectors.
 *
 * The events framework is build up as such it is able to gather information
 * before and after a move. Before a move we check if a piece is being captured,
 * after the move we evaluate if the move has caused situations like mate,
 * checkmate or stalemate.
 *
 * @author John
 */
public abstract class AbstractGameEvents implements GameEventListener {

    /**
     * Executor for sending events.
     */
    private final ScheduledExecutorService eventsExecutor = Executors.newSingleThreadScheduledExecutor();

    /**
     * The game that's being played.
     */
    private GameDirector game;

    /**
     * Sets the game for this engine.
     *
     * @param game The game played.
     */
    protected final void setGameContainer(final GameDirector game) {
        this.game = game;
    }

    /**
     * Returns the internal game reference.
     *
     * @return The game played.
     */
    protected final GameDirector getGameContainer() {
        return this.game;
    }

    /**
     * Returns the game for this engine.
     *
     * @return The game played.
     */
    public final GameProxy getGameInteraction() {
        return this.game;
    }

    /**
     * Returns the board of the game.
     *
     * shorthand for <code>getGame().getGameBoard()</code>
     *
     * @return The game board with the game contents.
     */
    public final Board getGameBoard() {
        return this.getGameContainer().getGameBoard();
    }

    /**
     * For handling non registered board game events.
     *
     * These events are custom events created by the application and not the
     * board object game.
     *
     * @param moveEvent The move event object.
     */
    @Override
    public final void onPieceMoveIntentEvent(final MoveEvent moveEvent) {
        onPieceMoveIntent(moveEvent.getSource(), moveEvent.getMoveTypes());
    }

    /**
     * the piece moved event.
     *
     * @param moveEvent The move event of the move.
     */
    @Override
    public final void onPieceMovedEvent(final MoveEvent moveEvent) {
        switch (this.getGameBoard().getContext().getGameMode()) {
            case HUMAN_VS_MACHINE:
            case MACHINE_VS_MACHINE:
                delayedEventSend(() -> {
                    onPieceMoved(moveEvent.getSource(), moveEvent.getMoveTypes());
                }, 1000);
                break;
            default:
                onPieceMoved(moveEvent.getSource(), moveEvent.getMoveTypes());
                break;
        }
    }

    /**
     * When a square is selected or de-selected.
     *
     * @param squareSelectEvent The event to pass.
     */
    @Override
    public final void onSquareSelectedEvent(final SquareSelectEvent squareSelectEvent) {
        if (squareSelectEvent.isSelectAction()) {
            onSquareSelected(squareSelectEvent.getSource());
        } else {
            onSquareDeSelected(squareSelectEvent.getSource());
        }
    }

    /**
     * When an event needs to be send delayed.
     *
     * Have a setting for animated moves, we do not want to pass on a move
     * earlier then when an animation has been done. An animation takes between
     * 1 and 3 seconds. We only want to immediately send a move when the engine
     * player is human.
     *
     * @param runnable The runnable to execute.
     */
    private void delayedEventSend(final Runnable runnable, final long delay) {
        eventsExecutor.schedule(runnable, delay, TimeUnit.MILLISECONDS);
    }

    /**
     * Called when a move is intended by a player.
     *
     * @param move The move that is intended.
     * @param moveTypes The move types in the intention.
     */
    public abstract void onPieceMoveIntent(final Move move, final List<MoveEvent.MoveType> moveTypes);

    /**
     * Called when a move is done.
     *
     * @param move The move that has been done.
     * @param moveTypes The move types.
     */
    public abstract void onPieceMoved(final Move move, final List<MoveEvent.MoveType> moveTypes);

    /**
     * Called when a move is undone.
     *
     * @param move The undo mode that has been done.
     */
    public abstract void onUndoMove(final Move move);

    /**
     * When a piece is selected.
     *
     * @param square The selected piece.
     */
    public abstract void onSquareSelected(final Square square);

    /**
     * When the piece gets deselected.
     *
     * @param square The deselected piece
     */
    public abstract void onSquareDeSelected(final Square square);

}
