/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.director;

import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.CastleRight;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import java.util.List;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;
import org.pidome.games.chess.engines.HighlightType;

/**
 * Interface supplied to perform actions on a game.
 *
 * @author John
 */
public interface GameProxy {

    /**
     * Returns the game information board being played on.
     *
     * @return The board containing the game information.
     */
    public Board getGameBoard();

    /**
     * Resets a game.
     *
     * Fully resets a game and puts pieces next to the board.
     *
     * @param animated
     */
    public void resetGame(final boolean animated);

    /**
     * Initializes a game to be started.
     */
    public void prepareGame();

    /**
     * Ends a game.
     */
    public void endGame();

    /**
     * Returns a preloaded game.
     *
     * Only available when a game has been preloaded, returns null if no
     * preloaded game is available.
     *
     * @return A preloaded game.
     */
    public List<ChessGameModel> getPreloadedGames();

    /**
     * Returns a game player by the give side.
     *
     * @param side The side to get the player for.
     * @return A game player.
     */
    public ChessGamePlayerModel getPlayerBySide(Side side);

    /**
     * Select a piece in the game.
     *
     * @param square The piece to select.
     * @param select Set to true to select, false to deselect.
     */
    public void selectPiece(Square square, boolean select);

    /**
     * Performs a promotion move.
     *
     * @param move The promotion move to execute.
     */
    public void promotionMove(final Move move);

    /**
     * To highlight a square for selection.
     *
     * @param square The square to perform highlighting actions on.
     * @param type The type of highlighting to apply use
     * <code>HighlightType.NONE</code> to remove any highlighting.
     */
    public void highlightSquare(Square square, HighlightType type);

    /**
     * Performs a move on in the game.
     *
     * @param move The move to make.
     */
    public void movePiece(Move move);

    /**
     * Performs a castling move
     *
     * @param side The side color.
     * @param castleRight What kind of castling.
     */
    public void moveCastling(Side side, CastleRight castleRight);

    /**
     * Takes a target en passant pawn from the board.
     *
     * @param square The square to take the piece from.
     */
    public void takeEnpassantTarget(Square square);

}
