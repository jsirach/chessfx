/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.director;

import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.CastleRight;
import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.game.GameContext;
import com.github.bhlangonijr.chesslib.game.GameMode;
import com.github.bhlangonijr.chesslib.move.Move;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.engines.AbstractGameEngine;
import org.pidome.games.chess.engines.HighlightType;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.model.options.PlayerGameOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.world.views.AbstractGameView;

/**
 * A game on a board.
 *
 * @author John
 */
public final class GameDirector implements GameProxy {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(GameDirector.class);

    /**
     * The game board.
     */
    private Board board;

    /**
     * The view(s) containing the game board(s).
     */
    private AbstractGameView gameView;

    /**
     * The engine used in this game.
     */
    private AbstractGameEngine engine;

    /**
     * The game's options.
     */
    private AbstractGenericGameOptions options;

    /**
     * Game constructor.
     *
     * @param options The options for the game.
     */
    public GameDirector(final AbstractGenericGameOptions options) {
        this.options = options;
        this.options.setGameId(UUID.randomUUID().toString());
    }

    /**
     * Returns a list preloaded games.
     *
     * @return The preloaded games.
     */
    @Override
    public final List<ChessGameModel> getPreloadedGames() {
        if (this.options instanceof PlayerGameOptions) {
            return ((PlayerGameOptions) this.options).games();
        } else {
            return null;
        }
    }

    /**
     * Loads the assets required for the game to display.
     *
     * @param resourcesLoader The resource loader scheduler.
     * @return true when all assets are loaded.
     */
    public final BooleanProperty initAndLoadAssets(final ResourcesLoader resourcesLoader) {
        try {
            this.gameView = GameResourcesHelper.createViewInstance(this.options.getGameView().get(), this.options);
            this.gameView.scheduleAssetsLoading(resourcesLoader);
        } catch (Exception ex) {
            LOG.error("Unable to load game", ex);
            return new SimpleBooleanProperty(Boolean.FALSE);
        }
        return new SimpleBooleanProperty(Boolean.TRUE);
    }

    /**
     * Loads the game routines.
     *
     * @return true when loaded.
     */
    public final Boolean loadGame() {
        try {
            CompletableFuture<Boolean> engineFuture = CompletableFuture.supplyAsync(() -> {
                try {
                    LOG.debug("Instantiating a new engine");
                    this.engine = GameResourcesHelper.createEngineInstance(this.options.gameEngineProperty().get());
                    this.engine.setGameOptions(options);
                    this.engine.setGameContainer(this);
                    this.engine.registerforEvents();
                    return Boolean.TRUE;
                } catch (Exception ex) {
                    LOG.error("Could not initialize engine", ex);
                    return Boolean.FALSE;
                }
            });
            return engineFuture.get();
        } catch (InterruptedException | ExecutionException ex) {
            LOG.error("Loading game routines failed", ex);
        }
        return Boolean.FALSE;
    }

    /**
     * Returns the engine.
     *
     * @return The engine.
     */
    protected final AbstractGameEngine getEngine() {
        return this.engine;
    }

    /**
     * Resets a game.
     */
    @Override
    public final void resetGame(final boolean animated) {
        LOG.debug("Instantiating a new play board");
        final GameContext context = new GameContext();
        context.setGameMode(GameMode.HUMAN_VS_HUMAN);
        this.board = new Board(context, true);
        this.gameView.resetBoard(animated);
    }

    /**
     * Initializes a game making it ready to start.
     */
    @Override
    public final void prepareGame() {
        this.gameView.prepareBoard();
    }

    /**
     * Selects a piece in the game.
     *
     * @param square The square to select.
     * @param select true to select a piece, false to deselect a piece.
     */
    @Override
    public final void selectPiece(final Square square, final boolean select) {
        this.gameView.selectPiece(square, select);
    }

    /**
     * Highlight a square or not.
     *
     * @param square The square to highlight or not.
     * @param type type of highlighting, <code>HighlightType.NONE</code> to
     * remove.
     */
    @Override
    public void highlightSquare(final Square square, final HighlightType type) {
        LOG.debug("Highlighting square [{}] as [{}]", square, type);
        this.gameView.highlightSquare(square, type);
    }

    /**
     * Moves a piece.
     *
     * This method checks if there is a piece on the target square and if this
     * is the case will take the piece if it's from the opposite color.
     *
     * @param move The move to make.
     */
    @Override
    public final void movePiece(final Move move) {
        removeOpponentPieceIfPresent(move);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Moving piece [{}] from [{}] to [{}]", this.getGameBoard().getPiece(move.getFrom()), move.getFrom(), move.getTo());
        }
        this.gameView.performMove(move, this.getGameBoard().getPiece(move.getFrom()));
        this.board.doMove(move);
    }

    /**
     * Performs a promotion move.
     *
     * @param move The promotion move to perform.
     */
    @Override
    public final void promotionMove(final Move move) {
        removeOpponentPieceIfPresent(move);
        this.gameView.performMove(move, this.getGameBoard().getPiece(move.getFrom()));
        this.board.doMove(move);
    }

    /**
     * Removes a piece from the opponent on the target field if present.
     *
     * @param move The move being performed.
     */
    private void removeOpponentPieceIfPresent(final Move move) {
        if (!this.getGameBoard().getPiece(move.getTo()).equals(Piece.NONE)) {
            LOG.debug("Attacking piece [{}] on [{}]", this.getGameBoard().getPiece(move.getTo()), move.getTo());
            takePiece(move.getTo());
        }
    }

    /**
     * Performs a castling move with the rook.
     *
     * @param side The side castling.
     * @param castleRight King or queen side.
     */
    @Override
    public final void moveCastling(final Side side, final CastleRight castleRight) {
        Move castlingRookMove = this.getGameBoard().getContext().getRookCastleMove(side, castleRight);
        Move castlingKingMove = this.getGameBoard().getContext().getKingCastleMove(side, castleRight);
        this.gameView.performMove(castlingRookMove, this.getGameBoard().getPiece(castlingRookMove.getFrom()), false);
        selectPiece(castlingKingMove.getFrom(), false);
        this.gameView.performMove(castlingKingMove, this.getGameBoard().getPiece(castlingKingMove.getFrom()));
        this.board.doMove(castlingKingMove);
    }

    /**
     * Takes an en passant target.
     *
     * @param square The target square.
     */
    @Override
    public void takeEnpassantTarget(final Square square) {
        takePiece(square);
    }

    /**
     * Ends a game.
     */
    @Override
    public final void endGame() {
        final String gameId = this.options.getGameId();
        GameEventsPublisher.publishGameState(
                new GameStateEvent(gameId, GameStateEvent.GameState.ENDED), 0);
        this.options = null;
        this.engine.unregisterForEvents();
        this.engine.endGame();
        this.gameView.destroy();
        GameEventsPublisher.unregisterGame(gameId);
    }

    /**
     * Takes a piece from the board from the given square.
     *
     * @param square The square to take the piece from.
     */
    public final void takePiece(final Square square) {
        final Piece piece = this.board.getPiece(square);
        if (piece != null && !piece.equals(Piece.NONE)) {
            LOG.debug("Taking piece [{}] on [{}]", piece, square);
            this.gameView.takePiece(square);
        }
    }

    /**
     * Returns the game's view.
     *
     * @return The game view.
     */
    public final AbstractGameView getGameView() {
        return this.gameView;
    }

    /**
     * The board we are playing on.
     *
     * @return The non graphical board we are playing on.
     */
    @Override
    public final Board getGameBoard() {
        return this.board;
    }

    /**
     * The game hash code.
     *
     * @return The hash code.
     */
    @Override
    public final int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.options.getGameId());
        return hash;
    }

    /**
     * Game equality check.
     *
     * @param obj The object to check for equality.
     * @return if the game equals the given game.
     */
    @Override
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equals(this.options.getGameId(), ((GameDirector) obj).options.getGameId());
    }

    /**
     * Returns a player for a given side.
     *
     * @param side The side to get the player for.
     * @return A game player.
     */
    @Override
    public final ChessGamePlayerModel getPlayerBySide(final Side side) {
        return this.options.playerbySide(side);
    }

}
