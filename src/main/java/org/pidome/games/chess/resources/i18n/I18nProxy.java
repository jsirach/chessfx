/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources.i18n;

import com.github.resource4j.objects.providers.ResourceObjectProviders;
import static com.github.resource4j.objects.providers.resolvers.ResourceObjectProviderPredicates.name;
import com.github.resource4j.resources.RefreshableResources;
import com.github.resource4j.resources.Resources;
import static com.github.resource4j.resources.ResourcesConfigurationBuilder.configure;
import com.github.resource4j.resources.processors.BasicValuePostProcessor;
import java.io.File;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import org.pidome.games.chess.settings.GenericSettings;
import org.pidome.games.chess.settings.Settings;

/**
 * Proxy for internationalization.
 *
 * All internationalization properties must pass through this proxy as the proxy
 * binds the resources to the properties.
 *
 * @author johns
 */
public class I18nProxy {

    /**
     * The main resources access.
     */
    protected static Resources RESOURCES;

    /**
     * The default bundle name.
     */
    protected static final String DEFAULT_BUNDLE = "language";

    /**
     * Collection of observables with the ability to refresh during language
     * change.
     */
    private final Map<I18nKey, I18nStringProperty> keyRegistration = new HashMap<>();

    /**
     * The locale to be used.
     */
    protected static ObjectProperty<Locale> bundleLocale = new SimpleObjectProperty<>(
            Locale.getDefault()
    );

    /**
     * Static initialization block.
     */
    static {
        RESOURCES = new RefreshableResources(
                configure()
                        .defaultBundle(DEFAULT_BUNDLE)
                        .sources(ResourceObjectProviders.filesIn(
                                new File(
                                        "resources/i18n"
                                )
                        )
                                .objectsLike(name(".\\.properties$")))
                        .postProcessingBy(new BasicValuePostProcessor())
                        .get()
        );
        Settings.get(GenericSettings.class).locale().addListener((observable, oldLocaleString, newLocaleString) -> {
            setNewLocaleFromString(newLocaleString);
        });
        setNewLocaleFromString(Settings.get(GenericSettings.class).locale().get());
    }

    /**
     * Set a new locale by configuration.
     *
     * @param localeByString The locale as string.
     */
    private static void setNewLocaleFromString(final String localeByString) {
        Locale newLocale = Locale.forLanguageTag(localeByString);
        bundleLocale.setValue(newLocale);
        Locale.setDefault(newLocale);
    }

    /**
     * Static for the proxy self.
     */
    private static I18nProxy proxy;

    /**
     * Private constructor.
     */
    private I18nProxy() {
    }

    /**
     * Returns a translated string.
     *
     * @param key The key for the string to return.
     * @return The string, or an empty string when not found.
     */
    public static String getString(final I18nKey key) {
        return getStringProperty(key).getValueSafe();
    }

    /**
     * Returns the translated String property.
     *
     * @param key The key to return for.
     * @return The string property.
     */
    public static StringProperty getStringProperty(final I18nKey key) {
        return getInstance().get(key);
    }

    /**
     * Returns the proxy instance.
     *
     * @return The proxy.
     */
    public static final I18nProxy getInstance() {
        if (proxy == null) {
            proxy = new I18nProxy();
        }
        return proxy;
    }

    /**
     * Returns a simple string from a resource.
     *
     * These properties are persistent and cached. Requested properties which
     * already exist will be returned and not overwritten.
     *
     * @param key The key of the simple string to return.
     * @return A string
     */
    public final I18nStringProperty get(final I18nKey key) {
        if (!keyExists(key)) {
            I18nStringProperty property = new I18nStringProperty(key);
            registerProperty(key, property);
        }
        return getProperty(key);
    }

    /**
     * Returns a simple string from a resource.
     *
     * These properties are not persistent and not cached.
     *
     * @param key The key of the complex with replacements string to return.
     * @param replacePairs The pairs which contain the values to be replaced in
     * the string.
     * @return A string
     */
    public final I18nStringProperty get(final I18nKey key, final I18nReplacePair... replacePairs) {
        return new I18nStringProperty(key, replacePairs);
    }

    /**
     * Registers a string with one or more plurals.
     *
     * This method returns a string property which is not persistent so keys are
     * not re-used. With duplicate keys when plurals are used it is not always
     * safe to say that even when the same key is used, the plurals are also the
     * same.
     *
     * An example would be that if there is a list of users you would use the
     * same key to provide "{USER} won {NUMGAMES} {Game/s|PLURAL}". If the
     * property was cached all user would have won an even amount and all would
     * show the text "Game/s" instead of "Game" or Games.
     *
     * @param key The key of the main string to return.
     * @param plurals The plurals key.
     * @return A property which automatically updates on plural count.
     */
    public final I18nStringProperty getNonPersistentPlural(final I18nKey key, final I18nPlural... plurals) {
        return new I18nStringProperty(key, plurals);
    }

    /**
     * Returns the current locale.
     *
     * @return The current locale.
     */
    public final ObjectProperty<Locale> getLocale() {
        return bundleLocale;
    }

    /**
     * Checks if a key exists.
     *
     * @param key The key to check.
     * @return true when key exists, otherwise false.
     */
    private boolean keyExists(final I18nKey key) {
        return this.keyRegistration.containsKey(key);
    }

    /**
     * Returns the property belonging to the key.
     *
     * @param key The key to supply to get the property for.
     */
    private I18nStringProperty getProperty(final I18nKey key) {
        return this.keyRegistration.get(key);
    }

    /**
     * Registers an <code>I18nStringProperty</code>.
     *
     * This method is duplicate safe.
     *
     * @param key The key to register.
     * @param property The property to register with the given key.
     */
    private void registerProperty(final I18nKey key, final I18nStringProperty property) {
        if (!keyExists(key)) {
            this.keyRegistration.put(key, property);
        }
    }

}
