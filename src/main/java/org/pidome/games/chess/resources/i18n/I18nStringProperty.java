/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources.i18n;

import static com.github.resource4j.ResourceKey.key;
import static com.github.resource4j.resources.context.ResourceResolutionContext.in;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import static org.pidome.games.chess.resources.i18n.I18nProxy.DEFAULT_BUNDLE;
import org.pidome.games.chess.utils.FXUtils;

/**
 * An i18N String property.
 *
 * @author johns
 */
public class I18nStringProperty extends SimpleStringProperty {

    /**
     * the base type for this string property.
     */
    private enum BaseType {
        /**
         * A simple text.
         */
        SIMPLE,
        /**
         * A text with plurals included.
         */
        PLURAL,
        /**
         * A text with macro's to be replaced.
         */
        REPLACE;
    }

    /**
     * The string property type.
     */
    private final BaseType type;

    /**
     * Hash set of plurals if given.
     */
    private List<I18nPlural> plurals;

    /**
     * List with replacements if given.
     */
    private List<I18nReplacePair> replacements;

    /**
     * The base key used.
     */
    private final I18nKey baseKey;

    /**
     * Simple constructor.
     *
     * @param baseKey The base key.
     */
    public I18nStringProperty(final I18nKey baseKey) {
        this.baseKey = baseKey;
        type = BaseType.SIMPLE;
        refresh();
    }

    /**
     * Simple constructor with plurals.
     *
     * @param baseKey The base key.
     * @param plurals The plurals to assign to the base key.
     */
    public I18nStringProperty(final I18nKey baseKey, final I18nPlural... plurals) {
        this.baseKey = baseKey;
        this.plurals = new ArrayList<>();
        this.plurals.addAll(Arrays.asList(plurals));
        type = BaseType.PLURAL;
        refresh();
    }

    /**
     * Constructor with replacement keys.
     *
     * @param baseKey The key of the string to get from the language file.
     * @param replacePairs The replace keys to replace the values in the string
     * with.
     */
    public I18nStringProperty(final I18nKey baseKey, final I18nReplacePair... replacePairs) {
        this.baseKey = baseKey;
        this.replacements = new ArrayList<>();
        this.replacements.addAll(Arrays.asList(replacePairs));
        type = BaseType.REPLACE;
        refresh();
    }

    /**
     * Returns the key used in this property.
     *
     * @return The property used I18nKey.
     */
    public final I18nKey getKey() {
        return this.baseKey;
    }

    /**
     * Returns the listeral key name as string following
     * <code>Enum.name()</code> rules.
     *
     * @return The key as string.
     */
    public final String getKeyName() {
        return this.baseKey.name();
    }

    /**
     * Refreshes the string property's value.
     *
     * This method should only be used when there is a language change or during
     * initialization of the property.
     */
    protected final void refresh() {
        switch (type) {
            case SIMPLE:
                refreshSimple(this);
                break;
            case PLURAL:
                refreshPlural(this, plurals);
                break;
            case REPLACE:
                refreshReplacements(this, replacements);
        }
    }

    /**
     * Refreshes the property value.
     *
     * @param property The property to update.
     */
    private static void refreshSimple(final I18nStringProperty property) {
        I18nProxy.bundleLocale.addListener((obsValue, oldValue, newValue) -> {
            updateSimple(property);
        });
        updateSimple(property);
    }

    /**
     * Updates a simple property.
     *
     * @param property The property to update.
     */
    private static void updateSimple(final I18nStringProperty property) {
        FXUtils.runOnFXThread(() -> {
            property.setValue(I18nProxy.RESOURCES.get(key(DEFAULT_BUNDLE, property.getKeyName()), in(I18nProxy.bundleLocale.get()))
                    .orDefault(property.getKeyName()));
        });
    }

    /**
     * Refreshes a plural string property.
     *
     * @param property The property to refresh.
     * @param plurals The plurals to refresh in the found string.
     */
    private static void refreshPlural(final I18nStringProperty property, final List<I18nPlural> plurals) {
        plurals.forEach(plural -> {
            plural.addListener((obsevableValue, oldValue, newValue) -> {
                updatePlural(property, plurals);
            });
        });
        I18nProxy.bundleLocale.addListener((obsValue, oldValue, newValue) -> {
            updatePlural(property, plurals);
        });
        updatePlural(property, plurals);
    }

    /**
     * Updates a property with the given plural values.
     *
     * @param property The property to update.
     * @param pluralKey The plural parameter.
     * @param pluralCountValue The plural count value.
     */
    private static void updatePlural(final I18nStringProperty property, final List<I18nPlural> plurals) {
        FXUtils.runOnFXThread(() -> {
            property.setValue(I18nProxy.RESOURCES.get(key(DEFAULT_BUNDLE, property.getKeyName()), in(I18nProxy.bundleLocale.get()).with(
                    new HashMap<String, Object>() {
                {
                    plurals.forEach(plural -> {
                        put(plural.getKey().name(), plural.get());
                    });
                }
            }))
                    .orDefault(property.getKeyName())
            );
        });
    }

    /**
     * Refreshes a plural string property.
     *
     * @param property The property to refresh.
     * @param plurals The plurals to refresh in the found string.
     */
    private static void refreshReplacements(final I18nStringProperty property, final List<I18nReplacePair> replacements) {
        I18nProxy.bundleLocale.addListener((obsValue, oldValue, newValue) -> {
            updateReplacement(property, replacements);
        });
        updateReplacement(property, replacements);
    }

    /**
     * Updates a replacement string.
     *
     * @param property The string property.
     * @param replacements The replacements to perform.
     */
    private static void updateReplacement(final I18nStringProperty property, final List<I18nReplacePair> replacements) {
        property.setValue(I18nProxy.RESOURCES.get(key(DEFAULT_BUNDLE, property.getKeyName()), in(I18nProxy.bundleLocale.get()).with(
                new HashMap<String, Object>() {
            {
                replacements.forEach(pair -> {
                    put(pair.getKey().name(), pair.getValue());
                });
            }
        }))
                .orDefault(property.getKeyName())
        );
    }

}
