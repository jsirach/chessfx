/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

/**
 * A resource load result.
 *
 * @author John Sirach <john.sirach@pidome.org>
 * @param <T> The loaded object to be returned.
 */
public class ResourceLoadResult {

    /**
     * A cause when completion fails.
     */
    private Throwable cause;

    /**
     * If completed or not.
     */
    private boolean completed;

    /**
     * The object when completed.
     */
    private Runnable callback;

    /**
     * Complete without additional runnable action.
     *
     * @return this.
     */
    public ResourceLoadResult complete() {
        this.completed = true;
        return this;
    }

    /**
     * Complete the load result.
     *
     * @param callback The callback to be executed.
     * @return this.
     */
    public ResourceLoadResult complete(final Runnable callback) {
        this.completed = true;
        this.callback = callback;
        return this;
    }

    /**
     * Fail a completion.
     *
     * @param cause The cause of the fail.
     * @return this.
     */
    public ResourceLoadResult complete(final Throwable cause) {
        this.completed = false;
        this.cause = cause;
        return this;
    }

    /**
     * Runs the callback.
     */
    public void callback() {
        if (this.callback != null) {
            this.callback.run();
        }
    }

    /**
     * If loading has been completed.
     *
     * @return if completed or not.
     */
    public boolean isCompleted() {
        return this.completed;
    }

    /**
     * The cause when completed is false.
     *
     * @return the cause of failing completion.
     */
    protected Throwable getCause() {
        return this.cause;
    }

}
