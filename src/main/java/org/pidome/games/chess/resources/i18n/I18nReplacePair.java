/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources.i18n;

import javafx.beans.property.SimpleStringProperty;

/**
 * A key value pair which replaces values in a text string.
 *
 * There are two replace constructor. One with a simple string and one with a
 * I18nKey which represents a normal key in the language files.
 *
 * Both constructors only use the current literal value and real time language
 * updates on screen are not supported by this class.
 *
 * @author johns
 */
public class I18nReplacePair extends SimpleStringProperty {

    /**
     * The key to be replaced.
     */
    private final I18nReplaceKey key;

    /**
     * Constructor.
     *
     * @param key The key to be replaced.
     * @param i18nValue The string value to replace with.
     */
    public I18nReplacePair(final I18nReplaceKey key, final I18nKey i18nValue) {
        this(key, I18nProxy.getInstance().get(i18nValue).get());
    }

    /**
     * Constructor.
     *
     * @param key The key to be replaced.
     * @param stringValue The string value to replace with.
     */
    public I18nReplacePair(final I18nReplaceKey key, final String stringValue) {
        super(stringValue);
        this.key = key;
    }

    /**
     * Returns the key to replace.
     *
     * @return The key.
     */
    protected final I18nReplaceKey getKey() {
        return this.key;
    }

}
