/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.pidome.games.chess.engines.AbstractGameEngine.GameEngineType;
import org.pidome.games.chess.model.options.GameOptions.GamePlayMode;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * Annotation to define game engines.
 *
 * @author johns
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface GameEngine {

    /**
     * The game engine type.
     *
     * @return The type.
     */
    GameEngineType type() default GameEngineType.HUMAN_VS_HUMAN;

    /**
     * The game engine name.
     *
     * @return The name.
     */
    I18nApplicationKey i18nName();

    /**
     * Fallback name if no i18nName is available.
     *
     * Should only be english.
     *
     * @return Fallback name as string.
     */
    String fallbackName() default "";

    /**
     * The supported game modes.
     *
     * @return
     */
    GamePlayMode mode();
}
