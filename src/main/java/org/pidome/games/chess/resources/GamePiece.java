/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.pidome.games.chess.model.options.GameOptions.GameViewType;

/**
 * Game piece annotation.
 *
 * @author johns
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface GamePiece {

    /**
     * the piece type.
     */
    public enum GamepieceType {
        /**
         * A two dimensional image type board.
         */
        PIECE_IMAGE(GameViewType.FLAT),
        /**
         * The custom board type.
         */
        PIECE_CUSTOM(GameViewType.FLAT),
        /**
         * A 3D object board.
         */
        PIECE_3D(GameViewType.PERSPECTIVE);

        /**
         * The registered view type.
         */
        private final GameViewType viewType;

        /**
         * Private constructor setting the view type.
         *
         * @param view The view type to set.
         */
        private GamepieceType(final GameViewType viewType) {
            this.viewType = viewType;
        }

        /**
         * Returns the view how the board is/should be displayed.
         *
         * @return The view to display.
         */
        public final GameViewType getViewType() {
            return this.viewType;
        }

    }

    /**
     * The game piece type.
     *
     * @return The piece type.
     */
    GamepieceType type();

    /**
     * The board name.
     *
     * @return The name.
     */
    String name();

    /**
     * The preview image to show in the interface.
     *
     * @return The path to the preview image.
     */
    String preview() default "/org/pidome/games/chess/assets/game/images/no_preview.jpg";

}
