/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources.i18n;

/**
 * Language keys used in game.
 *
 * @author johns
 */
public enum I18nInGameKey implements I18nKey {

    /**
     * Select promotion piece title.
     */
    PROMOTION_SELECT_PIECE_TITLE,
    /**
     * Select a promotion piece.
     */
    PROMOTION_SELECT_PIECE,
    /**
     * King.
     */
    PIECE_KING,
    /**
     * Queen.
     */
    PIECE_QUEEN,
    /**
     * Bishop.
     */
    PIECE_BISHOP,
    /**
     * Knight.
     */
    PIECE_KNIGHT,
    /**
     * rook.
     */
    PIECE_ROOK,
    /**
     * Pawn.
     */
    PIECE_PAWN,
    /**
     * Piece black color.
     */
    PIECE_COLOR_BLACK,
    /**
     * First character black piece color uppercase
     */
    PIECE_COLOR_BLACK_UPPER,
    /**
     * Piece white color.
     */
    PIECE_COLOR_WHITE,
    /**
     * First character white piece color uppercase
     */
    PIECE_COLOR_WHITE_UPPER;

}
