/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources.i18n;

/**
 * Generic application language keys.
 *
 * @author johns
 */
public enum I18nApplicationKey implements I18nKey {

    /**
     * Text for default.
     */
    DEFAULT,
    /**
     * Example.
     */
    EXAMPLE,
    /**
     * Lighter setting.
     */
    LIGHTER,
    /**
     * Darker setting.
     */
    DARKER,
    /**
     * Random, used for example in random selections.
     */
    RANDOM,
    /**
     * Literal thickness.
     */
    THICKNESS,
    /**
     * Literal thin.
     */
    THIN,
    /**
     * Literal thick.
     */
    THICK,
    /**
     * Load from internet.
     */
    FILE_INTERNET,
    /**
     * Load from local device.
     */
    FILE_LOCAL,
    /**
     * Load form internet description.
     */
    FILE_INTERNET_DESCRIPTION,
    /**
     * Load from local device description.
     */
    FILE_LOCAL_DESCRIPTION,
    /**
     * the file menu key.
     */
    MENU_FILE,
    /**
     * Extra menu.
     */
    MENU_EXTRA,
    /**
     * The exit application key
     */
    MENU_FILE_EXIT,
    /**
     * Exit application title.
     */
    EXIT_APP_ARE_YOU_SURE_TITLE,
    /**
     * Exit application question.
     */
    EXIT_APP_ARE_YOU_SURE,
    /**
     * The generic game menu.
     */
    MENU_GAME,
    /**
     * Start a new game.
     */
    MENU_GAME_NEW,
    /**
     * Asking of someone wants to leave the game title.
     */
    QUIT_CURRENT_GAME_ARE_YOU_SURE_TITLE,
    /**
     * Asking of someone wants to leave the game.
     */
    QUIT_CURRENT_GAME_ARE_YOU_SURE,
    /**
     * New game title header
     */
    NEW_GAME_TITLE,
    /**
     * Games.
     */
    GAMES,
    /**
     * Load a new game.
     */
    NEW_GAME_LOADING,
    /**
     * New / Edit a game scene options.
     */
    GAME_PLAY_TYPE_EDITOR,
    /**
     * Play a versus game.
     */
    GAME_PLAY_TYPE_VERSUS,
    /**
     * Versus description.
     */
    GAME_PLAY_TYPE_VERSUS_DESCRIPTION,
    /**
     * Watch a game being played.
     */
    GAME_PLAY_TYPE_SPECTATE,
    /**
     * Spectate description.
     */
    GAME_PLAY_TYPE_SPECTATE_DESCRIPTION,
    /**
     * Replay a game.
     */
    GAME_PLAY_TYPE_PLAYER,
    /**
     * Game replay description.
     */
    GAME_PLAY_TYPE_PLAYER_DESCRIPTION,
    /**
     * Game broadcast.
     */
    GAME_PLAY_TYPE_OBSERVE_BROADCAST,
    /**
     * Game broadcast description.
     */
    GAME_PLAY_TYPE_OBSERVE_BROADCAST_DESCRIPTION,
    /**
     * Game won popup title.
     */
    GAME_WIN_POPUP_TITLE_WIN,
    /**
     * Game lost popup title.
     */
    GAME_WIN_POPUP_TITLE_LOSE,
    /**
     * A draw game.
     */
    GAME_WIN_POPUP_TITLE_DRAW,
    /**
     * Game won popup text.
     */
    GAME_WIN_POPUP_TEXT,
    /**
     * Game draw popup text.
     */
    GAME_WIN_POPUP_TEXT_DRAW,
    /**
     * View category label.
     */
    NEW_GAME_OPTIONS_CATEGORY_VIEW,
    /**
     * View and layout select label.
     */
    NEW_GAME_OPTIONS_VIEW_SELECT,
    /**
     * Select a 2D view.
     */
    NEW_GAME_OPTIONS_VIEW_2D,
    /**
     * Select a 3D view.
     */
    NEW_GAME_OPTIONS_VIEW_3D,
    /**
     * Select a combined 2D and 3D view.
     */
    NEW_GAME_OPTIONS_VIEW_2D3D,
    /**
     * Category for game type and options.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME,
    /**
     * Scenery title.
     */
    NEW_GAME_OPTIONS_SCENERY_SELECT,
    /**
     * Board select title.
     */
    NEW_GAME_OPTIONS_BOARD_SELECT,
    /**
     * 3D board
     */
    NEW_GAME_OPTIONS_SELECT_BOARD_3D,
    /**
     * 2D board.
     */
    NEW_GAME_OPTIONS_SELECT_BOARD_2D,
    /**
     * Human player versus human player.
     */
    PERSON_VS_PERSON,
    /**
     * Human player versus computer player.
     */
    PERSON_VS_COMPUTER,
    /**
     * Engine selection in new versus game.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_SELECT_VERSUS_ENGINE,
    /**
     * Players new game fieldset title.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_PLAYERS_TITLE,
    /**
     * Title for entering players names.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_PLAYERS_NAMES,
    /**
     * Player one label.
     */
    PLAYER_WHITE,
    /**
     * Player two label.
     */
    PLAYER_BLACK,
    /**
     * Title for randomizing player colors.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_PLAYER_COLOR_RANDOM,
    /**
     * New game with local vs engine.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_LOCAL,
    /**
     * Description of local game on same device versus game.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_LOCAL_DESCRIPTION,
    /**
     * New game with local network engine.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_NETWORK,
    /**
     * Description of versus on local network play.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_NETWORK_DESCRIPTION,
    /**
     * New game with online play via the internet.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_INTERNET,
    /**
     * Description of versus over internet game.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_INTERNET_DESCRIPTION,
    /**
     * Engine supporting the playing of a recorded game.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_PLAYER,
    /**
     * Playlist options in player type game.
     */
    NEW_GAME_OPTIONS_CATEGORY_PLAYER_PLAYLIST_OPTIONS,
    /**
     * Options shown for visual game help.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS,
    /**
     * Show when a king is put in check position.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS_SHOW_CHECK,
    /**
     * Show the legal steps a piece can perform after selection.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS_SHOW_LEGAL_STEPS,
    /**
     * Show the path a piece has traveled after it moved.
     */
    NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS_SHOW_PIECE_PATH,
    /**
     * Settings menu item.
     */
    MENU_SETTINGS,
    /**
     * Application settings menu item.
     */
    MENU_SETTINGS_APPLICATION,
    /**
     * Application settings title.
     */
    SETTINGS_APPLICATION_TITLE,
    /**
     * Select language fieldset label in settings.
     */
    SETTINGS_APPLICATION_LANGUAGE_LABEL,
    /**
     * The title of the color picker popup.
     */
    COLOR_PICKER_POPUP_TITLE,
    /**
     * Select path color for white.
     */
    SELECT_COLOR_PIECE_PATH_WHITE,
    /**
     * Select path color for black.
     */
    SELECT_COLOR_PIECE_PATH_BLACK,
    /**
     * Lighter color.
     */
    COLOR_LIGHTER,
    /**
     * Darker color.
     */
    COLOR_DARKER,
    /**
     * Color opacity.
     */
    COLOR_OPACITY,
    /**
     * Color brightness.
     */
    COLOR_BRIGHTNESS,
    /**
     * Application settings UI tab.
     */
    SETTINGS_APPLICATION_DEFAULT_UI_TAB,
    /**
     * Application settings game quality tab.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_TITLE,
    /**
     * Quick and generic quality settings.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL,
    /**
     * Low quality.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL_VALUE_LOW,
    /**
     * Normal quality.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL_VALUE_NORMAL,
    /**
     * High quality.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL_VALUE_HIGH,
    /**
     * Enable pieces motion animation.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_MOTION,
    /**
     * High quality pieces.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_PIECES,
    /**
     * High quality boards.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_BOARDS,
    /**
     * 3D quality settings.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_3D,
    /**
     * Individual pieces animation when available.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_3D_ANIMATION,
    /**
     * 2D quality settings.
     */
    SETTINGS_APPLICATION_GAME_QUALITY_2D,
    /**
     * Unable to load a file.
     */
    LOAD_FILE_UNABLE,
    /**
     * Paste a link from the internet.
     */
    PASTE_LINK_TO_ONLINE_FILE,
    /**
     * Open one or multiple games.
     */
    GAMES_OPEN,
    /**
     * Select a file.
     */
    SELECT_FILE,
    /**
     * PGN files.
     */
    PGN_FILES,
    /**
     * Loading PGN but no games found.
     */
    LOAD_PGN_NO_GAMES,
    /**
     * PGN game contains no events.
     */
    LOAD_PGN_NO_EVENTS,
    /**
     * No games found in an event.
     */
    LOAD_PGN_NO_GAMES_IN_EVENT,
    /**
     * Name of a game event is unknown.
     */
    GAME_UNKNOWN_EVENT,
    /**
     * Name of a game location is unknown.
     */
    GAME_UNKNOWN_LOCATION,
    /**
     * Game result.
     */
    GAME_RESULT,
    /**
     * White won,
     */
    WHITE_WON,
    /**
     * Black won,
     */
    BLACK_WON,
    /**
     * Draw,
     */
    DRAW,
    /**
     * Undecided / Ongoing.
     */
    ONGOING,
    /**
     * Date
     */
    DATE,
    /**
     * Enable demo view.
     */
    NEW_GAME_OPTIONS_ENABLE_DEMO_VIEW,
    /**
     * Explaining demo view.
     */
    NEW_GAME_OPTIONS_ENABLE_DEMO_VIEW_TOOLTIP,
    /**
     * Player play.
     */
    GAME_PLAYER_ACTION_PLAY,
    /**
     * Player pause.
     */
    GAME_PLAYER_ACTION_PAUSE,
    /**
     * Player next.
     */
    GAME_PLAYER_ACTION_PLAY_NEXT,
    /**
     * Player previous.
     */
    GAME_PLAYER_ACTION_PLAY_PREVIOUS,
    // Items used in the scene editor //
    /**
     * Editor name.
     */
    SCENE_EDITOR,
    /**
     * New scene.
     */
    SCENE_EDITOR_NEW_SCENE,
    /**
     * Edit scene.
     */
    SCENE_EDITOR_EDIT_SCENE,
}
