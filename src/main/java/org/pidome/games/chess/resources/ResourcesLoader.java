/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for handling resource loading to start a game.
 *
 * The purpose is to allow multi threaded loading of resources and we are not
 * making this too difficult.
 * <p>
 * <ul>
 * Good order of method using would be:
 * <li>Construct
 * <li>schedule
 * <li>run
 * </ul></p>
 *
 * Only bind if you want to record progress, and yes, that's what we want. But
 * that's another story. Use wisely. Oh, bind before run.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class ResourcesLoader {

    /**
     * The maximum amount of threads used for loading.
     */
    private static final int MAX_THREADS = 4;

    /**
     * The amount of threads to differ from the available "processors".
     *
     * Don't consume all.
     */
    private static final int CPU_THREADS_DIFF = 2;

    /**
     * The resource list of items to be loaded.
     */
    private final List<Callable<ResourceLoadResult>> resourceList = new ArrayList<>();

    /**
     * Load service.
     */
    private final ExecutorService loadService;

    /**
     * Completion service for loaded items.
     */
    private final CompletionService<ResourceLoadResult> completionService;

    /**
     * Progress listener.
     */
    private ResourcesLoaderListener progressListener;

    /**
     * Constructor.
     *
     * Constructs
     */
    public ResourcesLoader() {
        /// Read the Runtime.getRuntime().availableProcessors() javaDoc.
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        int availableThreads = availableProcessors >= MAX_THREADS + CPU_THREADS_DIFF
                ? MAX_THREADS : availableProcessors - CPU_THREADS_DIFF;
        loadService = Executors.newFixedThreadPool(availableThreads);
        completionService = new ExecutorCompletionService<>(loadService);
    }

    /**
     * Bind a listener for progress update.
     *
     * @param progressListener The listener to bind.
     */
    public void bind(final ResourcesLoaderListener progressListener) {
        this.progressListener = progressListener;
    }

    /**
     * Schedule item to be loaded.
     *
     * The Callable requires a <code>ResourceLoadResult</code> to be returned.
     * if there are any actions to perform while loading, this should be handled
     * within the class sending to the scheduler.
     *
     * @param callable The item to be loaded within a callable returning a
     * <code>ResourceLoadResult</code> type.
     */
    public void schedule(final Callable<ResourceLoadResult> callable) {
        resourceList.add(callable);
    }

    /**
     * Run the resource loader.
     *
     * If there is any binding done, it will be informed about amount of items
     * to be loaded and every time an item is loaded, it will call
     * ResourcesLoaderListener.update()
     *
     * @throws ResourceLoaderException When a resource fails to load.
     */
    public void run() throws ResourceLoaderException {
        AtomicInteger taskListSize = new AtomicInteger(this.resourceList.size());
        if (progressListener != null) {
            progressListener.toLoad(taskListSize.doubleValue());
        }
        final Iterator<Callable<ResourceLoadResult>> iterator = this.resourceList.iterator();
        while (iterator.hasNext()) {
            this.completionService.submit(iterator.next());
            iterator.remove();
        }
        while (taskListSize.intValue() > 0) {
            try {
                ResourceLoadResult loadResult = this.completionService.take()
                        .get();
                if (!loadResult.isCompleted()) {
                    ImDone();
                    Logger.getLogger(ResourcesLoader.class.getName()).log(Level.SEVERE, null, loadResult.getCause());
                    throw new ResourceLoaderException("Unable to load game", loadResult.getCause());
                } else {
                    loadResult.callback();
                    if (this.progressListener != null) {
                        this.progressListener.update();
                    }
                }
            } catch (InterruptedException | ExecutionException ex) {
                ImDone();
                Logger.getLogger(ResourcesLoader.class.getName()).log(Level.SEVERE, null, ex);
                throw new ResourceLoaderException("Unable to load game", ex);
            }
            taskListSize.getAndDecrement();
        }
    }

    /**
     * When done with it.
     */
    private void ImDone() {
        this.loadService.shutdown();
        this.progressListener = null;
    }

}
