/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources.i18n;

/**
 * Period based keys.
 *
 * @author johns
 */
public enum I18nPeriodKey implements I18nKey {
    /**
     * One second.
     */
    SECOND_ONE,
    /**
     * Multiple seconds.
     */
    SECOND_OTHER,
    /**
     * One hour.
     */
    HOUR_ONE,
    /**
     * Multiple hours.
     */
    HOUR_OTHER,
    /**
     * One minute.
     */
    MINUTE_ONE,
    /**
     * Multiple minutes.
     */
    MINUTE_OTHER,
    /**
     * Plural seconds selector.
     */
    DURATION_SECONDS,
    /**
     * Plural hours selector.
     */
    DURATION_HOURS,
    /**
     * Plural minutes selector.
     */
    DURATION_MINUTES;
}
