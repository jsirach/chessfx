/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import com.interactivemesh.jfx.importer.obj.ObjImportOption;
import com.interactivemesh.jfx.importer.obj.ObjModelImporter;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.application.ConditionalFeature;
import static javafx.application.Platform.isSupported;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.MeshView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.engines.AbstractGameEngine;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.GameOptions.GameViewType;
import org.pidome.games.chess.model.options.GameOptions.SceneViewType;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.world.board.AbstractBoard;
import org.pidome.games.chess.world.board.CustomFlatBoard;
import org.pidome.games.chess.world.board.PerspectiveBoard;
import org.pidome.games.chess.world.pieces.AbstractPieces;
import org.pidome.games.chess.world.pieces.FlatPieces;
import org.pidome.games.chess.world.pieces.PerspectivePieces;
import org.pidome.games.chess.world.views.AbstractGameView;
import org.pidome.games.chess.world.views.GameView;

/**
 * Helper for the game resources annotations.
 *
 * @author johns
 */
public class GameResourcesHelper {

    /**
     * The resource location to get the files from.
     */
    public enum ResourceLocation {
        /**
         * Internal default provided resources.
         */
        INTERNAL("internal/"),
        /**
         * Community/custom provided resources.
         */
        COMMUNITY("community/");

        /**
         * The path.
         */
        private String path;

        /**
         * Enum constructor.
         *
         * @param path Path linked to resource.
         */
        ResourceLocation(final String path) {
            this.path = path;
        }

        /**
         * Returns the resource path.
         *
         * @return The path.
         */
        public String getPath() {
            return this.path;
        }

    }

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(GameResourcesHelper.class);

    /**
     * Available scenes.
     */
    private static Map<ResourceLocation, List<SceneResource>> availableScenes = new HashMap<>();

    /**
     * Available scenes.
     */
    private static List<BoardResource> availableBoards = new ArrayList<>();

    /**
     * Available scenes.
     */
    private static List<PiecesResource> availablePieces = new ArrayList<>();

    /**
     * Pre-fetch all knowns.
     */
    public static final void preFetch() {
        getAvailableGameEngines();
        getAvailableGameScenes(ResourceLocation.INTERNAL);
        getAvailableGameScenes(ResourceLocation.COMMUNITY);
        preFetchAvailableBoards();
        preFetchAvailablePieces();
    }

    /**
     * Returns a list of game engines.
     *
     * @param playMode The playmode eligible for the return list.
     * @return The list of engines.
     */
    public static List<Class<AbstractGameEngine>> getPlaymodeTypeGameEngines(final GameOptions.GamePlayMode playMode) {
        return getAvailableGameEngines().stream()
                .filter(b -> gameEngineSupportsPlaymode(b.getAnnotationsByType(GameEngine.class)[0], playMode))
                .collect(Collectors.toList());
    }

    /**
     * Returns the play modes available for the given engine.
     *
     * @param engine The engine to check.
     * @return The available play modes.
     */
    public static GameOptions.GamePlayMode getGameEnginePlaymodes(final GameEngine engine) {
        return engine.mode();
    }

    /**
     * If the given game engine supports the given play mode.
     *
     * @param engine The engine to check.
     * @param playMode The playmode requested to be supported.
     */
    private static boolean gameEngineSupportsPlaymode(final GameEngine engine, final GameOptions.GamePlayMode playMode) {
        return engine.mode().equals(playMode);
    }

    /**
     * Returns a list of available game boards.
     *
     * @return The list of game boards.
     */
    public static List<Class<AbstractGameEngine>> getAvailableGameEngines() {
        return ResourceHelper.getClassesByAnnotation(GameEngine.class, AbstractGameEngine.class);
    }

    /**
     * Returns the name of the scene class.
     *
     * @param engine The engine given.
     * @return The name of the engine class given.
     */
    public static String getEngineName(final Class<? extends AbstractGameEngine> engine) {
        if (engine == null) {
            return "";
        }
        return I18nProxy.getInstance().get(engine.getAnnotationsByType(GameEngine.class)[0].i18nName()).get();
    }

    /**
     * Returns the annotation given on the class which extends the
     * <code>AbstractGameEngine</code> class.
     *
     * @param engine The game engine to get the annotation from.
     * @return The <code>GameEngine</code> annotation of the class.
     */
    public static GameEngine getEngineAnnotation(final Class<? extends AbstractGameEngine> engine) {
        return engine.getAnnotationsByType(GameEngine.class)[0];
    }

    /**
     * Returns an instance of a game engine to play/view the game.
     *
     * @param <T> The class which extends <code>AbstractGameEngine</code>
     * @param engine The game engine class.
     * @return Instance of the engine the end player is going to use.
     * @throws java.lang.Exception When instantiation fails.
     */
    public static <T extends AbstractGameEngine> AbstractGameEngine createEngineInstance(final Class<T> engine) throws Exception {
        try {
            final Constructor<?> constructor = (Constructor<T>) engine.getConstructor();
            return (AbstractGameEngine) constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new Exception(ex);
        }
    }

    /**
     * Game view type preview.
     *
     * @param viewType The view type.
     * @return The image for the view type.
     */
    public static Image getViewPreview(final GameViewType viewType) throws Exception {
        try {
            return loadImageFromPackage(viewType.getPreviewImage());
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return getNoPreviewImage();
    }

    /**
     * Returns the preview image path of the given scene resource.
     *
     * @param sceneResource The scene resource given.
     * @return The preview image of the scene resource given.
     * @throws java.lang.Exception when a scene preview is unable to be loaded.
     */
    public static Image getScenePreview(final SceneResource sceneResource) throws Exception {
        if (sceneResource != null) {
            final String previewImage = sceneResource.getScenePreviewImage();
            if (!Objects.isNull(previewImage) && !previewImage.isBlank()) {
                try {
                    return loadImage(previewImage);
                } catch (Exception ex) {
                    return getNoPreviewImage();
                }
            } else {
                return getNoPreviewImage();
            }
        }
        return getNoPreviewImage();
    }

    /**
     * Returns the <code>GameViewType</code> as defined in the annotation for
     * the given scene.
     *
     * @param scene The scene.
     * @return The view type.
     */
    public static GameOptions.GameViewType getViewSupportedViewType(final Class<? extends AbstractGameView> scene) {
        return GameOptions.GameViewType.byConstant(scene.getAnnotationsByType(GameView.class)[0].boardViewType());
    }

    /**
     * Returns a list of available game boards.
     *
     * @param internal When true only internal scenes are returned.
     * @return The list of game boards.
     */
    public static List<SceneResource> getAvailableGameScenes(final ResourceLocation internal) {
        availableScenes.putIfAbsent(internal,
                ResourceHelper.getAvailableScenes(internal).stream()
                        .filter(scene -> {
                            return (isSupported(ConditionalFeature.SCENE3D)
                                    && scene.getSceneViewType().equals(SceneViewType.PERSPECTIVE))
                                    || scene.getSceneViewType() == SceneViewType.FLAT;
                        })
                        .collect(Collectors.toList()));
        return availableScenes.get(internal);
    }

    /**
     * Get a scene by it's configuration name.
     *
     * @param type View type.
     * @param configurationName The configuration name.
     * @return The scene resource.
     */
    public static SceneResource getDefaultSceneResourceByConfiguration(final SceneViewType type, final ResourceLocation internal, final String configurationName) {
        return getAvailableGameScenes(internal).stream()
                .filter(scene -> scene.getSceneViewType().equals(type))
                .filter(scene -> configurationName.equals(scene.getProperty("SCENE_CONFIGURATION")))
                .findAny().get();
    }

    /**
     * Returns an instance of a board to play on.
     *
     * @param <T> The class which extends <code>AbstractBoard</code>
     * @param scene The scene class.
     * @param genericGameOptions Generic options of the game.
     * @return Instance of the scene to show to the end user.
     * @throws java.lang.Exception When instantiation fails.
     */
    public static <T extends AbstractGameView> AbstractGameView createViewInstance(final Class<T> scene, final AbstractGenericGameOptions genericGameOptions) throws Exception {
        try {
            final Constructor<?> constructor = (Constructor<T>) scene.getConstructor(AbstractGenericGameOptions.class);
            return (AbstractGameView) constructor.newInstance(genericGameOptions);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new Exception(ex);
        }
    }

    /**
     * Sets the perspective type for the given board.
     *
     * @param board The board to check if it is a perspective board.
     * @return true if the voard needs a perspective scene.
     */
    public static boolean is3DGameBoard(final Class<? extends AbstractBoard> board) {
        return board.getAnnotationsByType(GameBoard.class)[0].type().equals(GameBoard.GameboardType.BOARD_3D);
    }

    /**
     * Sets the perspective type for the given board.
     *
     * @param board The board to check if it is a perspective board.
     * @param boardType The board type to check against.
     * @return true if the voard needs a perspective scene.
     */
    public static boolean isGameBoardType(final Class<? extends AbstractBoard> board, final GameBoard.GameboardType boardType) {
        return board.getAnnotationsByType(GameBoard.class)[0].type().equals(boardType);
    }

    /**
     * Returns the name of the game board.
     *
     * @param board The board given.
     * @return The name of the board given.
     */
    public static String getGameBoardName(final Class<? extends AbstractBoard> board) {
        if (board == null) {
            return "";
        }
        return board.getAnnotationsByType(GameBoard.class)[0].name();
    }

    /**
     * Returns the image preview of the game board.
     *
     * @param board The board given.
     * @return The image preview of the board given.
     * @throws java.lang.Exception When there is an error in image load.
     */
    public static Image getGameBoardPreview(final BoardResource board) throws Exception {
        if (board != null) {
            try {
                /// Currently no custom 2D boards yet
                if (board.getSceneViewType().equals(SceneViewType.FLAT)) {
                    return loadImageFromPackage("/org/pidome/games/chess/images/flatassets/board.png");
                }
                return loadImage(
                        board.getPreviewImage()
                );
            } catch (Exception ex) {
                return getNoPreviewImage();
            }
        }
        return getNoPreviewImage();
    }

    /**
     * Returns an instance of a board to play on.
     *
     * @param <T> The class which extends <code>AbstractBoard</code>
     * @param boardResource The board resource.
     * @return Instance of a board to play on.
     * @throws java.lang.Exception When instantiation fails.
     */
    public static <T extends AbstractBoard> AbstractBoard createBoardInstance(final BoardResource boardResource) throws Exception {
        AbstractBoard loadedBoard = null;
        switch (boardResource.getSceneViewType()) {
            case PERSPECTIVE:
                loadedBoard = new PerspectiveBoard(boardResource);
                break;
            case FLAT:
                loadedBoard = new CustomFlatBoard();
                break;
        }
        if (Objects.isNull(loadedBoard)) {
            throw new Exception("Board could not be loaded");
        }
        return loadedBoard;
    }

    /**
     * Prefetch the available boards.
     */
    private static void preFetchAvailableBoards() {
        if (availableBoards.isEmpty()) {
            availableBoards
                    = ResourceHelper.getAvailableBoards().stream()
                            .filter(scene -> {
                                return (isSupported(ConditionalFeature.SCENE3D)
                                        && scene.getSceneViewType().equals(SceneViewType.PERSPECTIVE))
                                        || scene.getSceneViewType() == SceneViewType.FLAT;
                            })
                            .collect(Collectors.toList());
        }
    }

    /**
     * Returns a list of available game boards.
     *
     * @param forViewType returns boards suitable for the given view type.
     * @return The list of game boards.
     */
    public static List<BoardResource> getAvailableGameBoards(final GameViewType forViewType) {
        return availableBoards.stream()
                .filter(board -> board.getSceneViewType().compatibleWithGameViewType(forViewType))
                .collect(Collectors.toList());
    }

    /**
     * Returns a board by given file name which is available in game by default.
     *
     * @param forViewType returns boards suitable for the given view type.
     * @param objectName The boardfile name.
     *
     * @return The found board.
     */
    public static BoardResource getDefaultGameBoardByObject(final GameViewType forViewType, final String objectName) {
        return availableBoards.stream()
                .filter(board -> board.getSceneViewType().compatibleWithGameViewType(forViewType))
                .filter(board -> objectName.equals(board.getProperty("BOARD")))
                .findAny().get();
    }

    /**
     * Sets the perspective type for the given board.
     *
     * @param piece The piece to check if it is a perspective piece.
     * @return true if the piece needs a perspective scene.
     */
    public static boolean is3DGamePiece(final Class<? extends AbstractPieces> piece) {
        return piece.getAnnotationsByType(GamePiece.class)[0].type().equals(GamePiece.GamepieceType.PIECE_3D);
    }

    /**
     * Returns the image preview of the game pieces.
     *
     * @param piecesResource The pieces given.
     * @return The image preview of the piece given.
     * @throws java.lang.Exception When there is an error in image load.
     */
    public static Image getPiecesPreview(final PiecesResource piecesResource) throws Exception {
        if (piecesResource != null) {
            /// Currently no custom 2D boards yet
            if (piecesResource.getSceneViewType().equals(SceneViewType.FLAT)) {
                return loadImageFromPackage("/org/pidome/games/chess/images/flatassets/pieces.png");
            }
            try {
                return loadImage(piecesResource.getPreviewImage());
            } catch (Exception ex) {
                return getNoPreviewImage();
            }
        }
        return getNoPreviewImage();
    }

    /**
     * Returns an instance of a piece to play with.
     *
     * @param piecesResource The piece class.
     * @return Instance of a piece to play with.
     * @throws java.lang.Exception When instantiation fails.
     */
    public static <T extends AbstractPieces> AbstractPieces createPiecesInstance(final PiecesResource piecesResource) throws Exception {
        AbstractPieces pieces = null;
        switch (piecesResource.getSceneViewType()) {
            case PERSPECTIVE:
                pieces = new PerspectivePieces(piecesResource.getPiecesPath(), piecesResource.getPropertiesfile());
                break;
            case FLAT:
                pieces = new FlatPieces();
                break;
        }
        if (Objects.isNull(pieces)) {
            throw new Exception("Pieces could not be loaded");
        }
        return pieces;
    }

    /**
     * Preloads the available pieces.
     */
    private static void preFetchAvailablePieces() {
        if (availablePieces.isEmpty()) {
            availablePieces
                    = ResourceHelper.getAvailablePieces().stream()
                            .filter(pieces -> {
                                return (isSupported(ConditionalFeature.SCENE3D)
                                        && pieces.getSceneViewType().equals(SceneViewType.PERSPECTIVE))
                                        || pieces.getSceneViewType() == SceneViewType.FLAT;
                            })
                            .collect(Collectors.toList());
        }
    }

    /**
     * Returns a list of available pieces.
     *
     * @param forViewType returns pieces suitable for the given view type.
     * @return The list of pieces.
     */
    public static List<PiecesResource> getAvailablePieces(final GameViewType forViewType) {
        return availablePieces.stream()
                .filter(pieces -> pieces.getSceneViewType().compatibleWithGameViewType(forViewType))
                .collect(Collectors.toList());
    }

    /**
     * Returns a board by given file name which is available in game by default.
     *
     * @param forViewType returns boards suitable for the given view type.
     * @param configurationName The configuration file name.
     *
     * @return The found board.
     */
    public static PiecesResource getDefaultPiecesByConfiguration(final GameViewType forViewType, final String configurationName) {
        return availablePieces.stream()
                .filter(pieces -> pieces.getSceneViewType().compatibleWithGameViewType(forViewType))
                .filter(pieces -> configurationName.equals(pieces.getProperty("CONFIG")))
                .findAny().get();
    }

    /**
     * Loads an object asset from a package.
     *
     * @param location The item to load.
     * @return Meshview of the loaded object.
     * @throws java.lang.Exception When loading fails.
     */
    public static MeshView[] loadObject(final String location) throws Exception {
        /*
        // thanks to Jose Pereda to supply the code required to use FXyz objectloader
        // which is actively maintened. Currently the library jimObjModelImporterJFX.jar
        // is used and behaves differently. At a later moment when i'm able to figure
        // out what is done differently (time wise), moving to FXyz is the only option.
        // Self hint: Normals/smoothing(devisions?)/orientations/mesh amount loaded
        Model3D reader = new ObjImporter().load(new File("." + location).toURI().toURL());
        Set<String> meshes = reader.getMeshNames();
        Affine affineIni = new Affine();
        affineIni.prepend(new Rotate(180, Rotate.X_AXIS));

        MeshView[] mesh = meshes.stream()
                .map(key -> {
                    MeshView meshView = (MeshView) reader.getMeshView(key);
                    meshView.getTransforms().add(affineIni);
                    return meshView;
                })
                .toArray(MeshView[]::new);
         */
        final ObjModelImporter objImporter = new ObjModelImporter();
        objImporter.setOptions(ObjImportOption.GENERATE_NORMALS);
        objImporter.read(
                new File(
                        "." + location
                )
        );
        final MeshView[] mesh = objImporter.getImport();
        objImporter.close();
        if (mesh == null) {
            throw new Exception("Import failed");
        }
        return mesh;
    }

    /**
     * Loads an <code>Image</code> from a package.
     *
     * @param location The location from where to load.
     * @return The image.
     * @throws java.lang.Exception When an asset is unable to be loaded.
     */
    public static Image loadImageFromPackage(final String location) throws Exception {
        return new Image(GameResourcesHelper.class.getResourceAsStream(location));
    }

    /**
     * Loads an <code>Image</code> from external.
     *
     * @param location The location from where to load.
     * @return The image.
     * @throws java.lang.Exception When an asset is unable to be loaded.
     */
    public static Image loadImage(final String location) throws Exception {
        File toLoad = new File(location);
        if (!toLoad.exists()) {
            throw new Exception("Image [" + location + "] not found");
        }
        return new Image(toLoad.toURI().toString());
    }

    /**
     * Loads an <code>ImageView</code> from a package.
     *
     * @param location The location from where to load.
     * @return The image view.
     * @throws java.lang.Exception When an asset is unable to be loaded.
     */
    public static ImageView loadImageViewAssetFromPackage(final String location) throws Exception {
        return new ImageView(loadImageFromPackage(location));
    }

    /**
     * Returns the image for when a preview is not available.
     *
     * @return Not available image.
     * @throws Exception When loading the not available image is not available
     * (lol).
     */
    private static final Image getNoPreviewImage() throws Exception {
        return loadImageFromPackage("/org/pidome/games/chess/images/no_preview.jpg");
    }

}
