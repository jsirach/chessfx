/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.pidome.games.chess.model.options.GameOptions;

/**
 * A board resource.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class BoardResource extends Properties {

    /**
     * Path to game boards.
     */
    public static final String PATH = "./resources/assets/perspective/boards/";

    /**
     * The extension used for scene property files.
     */
    public static final String PROPERTIES_EXTENSION = ".properties";

    /**
     * Path to the scene configuration.
     */
    private final String boardConfigurationPath;

    /**
     * A resource in the game resource properties.
     */
    public enum Resource {
        NAME,
        DESCRIPTION,
        VIEWTYPE,
        PREVIEW_IMAGE,
        BOARD
    }

    /**
     * For boards without configuration files.
     */
    public BoardResource() {
        boardConfigurationPath = "";
    }

    /**
     * Constructor to load the file.
     *
     * @param file The file to load.
     */
    public BoardResource(final File file) throws IllegalArgumentException {
        this(file.getParent(), file.getName());
    }

    /**
     * A single scene resource.
     *
     * @param path path to the resource.
     * @param name The file describing the resource.
     * @throws IllegalArgumentException On failing of the file to be loaded.
     */
    public BoardResource(final String path, final String name) throws IllegalArgumentException {
        boardConfigurationPath = new StringBuilder(path).toString();
        final String configurationFile = new StringBuilder(boardConfigurationPath)
                .append("/").append(name).toString();
        try ( InputStream is = new FileInputStream(
                configurationFile
        )) {
            super.load(is);
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * Return the scene name.
     *
     * @return The scene.
     */
    public String getName() {
        return (String) this.get(Resource.NAME.name());
    }

    /**
     * Return the scene description.
     *
     * @return Description.
     */
    public String getDescription() {
        return (String) this.get(Resource.DESCRIPTION.name());
    }

    /**
     * Return the viewtype supported by this scene.
     *
     * @return The view type.
     */
    public GameOptions.SceneViewType getSceneViewType() {
        return GameOptions.SceneViewType.valueOf(
                (String) this.get(Resource.VIEWTYPE.name())
        );
    }

    /**
     * Returns the configuration file.
     *
     * @return
     */
    public String getBoardFile() {
        return new StringBuilder(boardConfigurationPath).replace(0, 1, "")
                .append("/")
                .append(this.get(Resource.BOARD.name()))
                .toString();
    }

    /**
     * Scene image preview path.
     *
     * @return The preview path.
     */
    public String getPreviewImage() {
        return new StringBuilder(boardConfigurationPath)
                .append("/")
                .append(this.get(Resource.PREVIEW_IMAGE.name()))
                .toString();
    }
}
