/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.pidome.games.chess.model.options.GameOptions.SceneViewType;

/**
 * A resource which points to configured game scenes.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class SceneResource extends Properties {

    /**
     * Path to game scenes.
     */
    public static final String GAMESCENES_PATH = "./resources/scenes/";

    /**
     * The extension used for scene property files.
     */
    public static final String GAMESCENE_PROPERTIES_EXTENSION = ".properties";

    /**
     * Path to the scene configuration.
     */
    private final String sceneConfigurationPath;

    /**
     * The full path to the configuration.
     */
    private final String configurationFile;

    /**
     * A resource in the game resource properties.
     */
    public enum Resource {
        NAME,
        DESCRIPTION,
        VIEWTYPE,
        PREVIEW_IMAGE,
        SCENE_CONFIGURATION,
        AUTHOR,
        AUTHOR_WEBSITE,
        AUTHOR_EMAIL;
    }

    /**
     * Constructor to load the file.
     *
     * @param file The file to load.
     */
    public SceneResource(final File file) throws IllegalArgumentException {
        this(file.getParent(), file.getName());
    }

    /**
     * A single scene resource.
     *
     * @param path path to the resource.
     * @param name The file describing the resource.
     * @throws IllegalArgumentException On failing of the file to be loaded.
     */
    public SceneResource(final String path, final String name) throws IllegalArgumentException {
        sceneConfigurationPath = new StringBuilder(path).toString();
        configurationFile = new StringBuilder(sceneConfigurationPath)
                .append("/").append(name).toString();
        try ( InputStream is = new FileInputStream(
                configurationFile
        )) {
            super.load(is);
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * Return the scene name.
     *
     * @return The scene.
     */
    public String getName() {
        return (String) this.get(Resource.NAME.name());
    }

    /**
     * Sets the name.
     *
     * @param name The name to set.
     */
    public void setName(final String name) {
        this.setProperty(Resource.NAME.name(), name);
    }

    /**
     * Return the scene description.
     *
     * @return Description.
     */
    public String getDescription() {
        return (String) this.get(Resource.DESCRIPTION.name());
    }

    /**
     * Sets the description.
     *
     * @param description The description to set.
     */
    public void setDescription(final String description) {
        this.setProperty(Resource.DESCRIPTION.name(), description);
    }

    /**
     * Return author.
     *
     * @return The author.
     */
    public String getAuthor() {
        return (String) this.get(Resource.AUTHOR.name());
    }

    /**
     * Sets the author.
     *
     * @param author The author to set.
     */
    public void setAuthor(final String author) {
        this.setProperty(Resource.AUTHOR.name(), author);
    }

    /**
     * Author's website.
     *
     * @return The author's website.
     */
    public String getAuthorWebsite() {
        return (String) this.get(Resource.AUTHOR_WEBSITE.name());
    }

    /**
     * Sets the author website.
     *
     * @param authorWebsite The author website to set.
     */
    public void setAuthorWebsite(final String authorWebsite) {
        this.setProperty(Resource.AUTHOR_WEBSITE.name(), authorWebsite);
    }

    /**
     * The author's email address.
     *
     * @return The email address.
     */
    public String getAuthorEmail() {
        return (String) this.get(Resource.AUTHOR_EMAIL.name());
    }

    /**
     * Sets the author email.
     *
     * @param authorEmail The author email to set.
     */
    public void setAuthorEmail(final String authorEmail) {
        this.setProperty(Resource.AUTHOR_EMAIL.name(), authorEmail);
    }

    /**
     * Return the viewtype supported by this scene.
     *
     * @return The view type.
     */
    public SceneViewType getSceneViewType() {
        return SceneViewType.valueOf(
                (String) this.get(Resource.VIEWTYPE.name())
        );
    }

    /**
     * Set the sceneViewType.
     *
     * @param sceneViewType The sceneViewType to set.
     */
    public void setSceneViewType(final SceneViewType sceneViewType) {
        this.setProperty(Resource.VIEWTYPE.name(), sceneViewType.name());
    }

    /**
     * Returns the configuration file.
     *
     * @return
     */
    public String getSceneConfigurationFile() {
        return new StringBuilder(sceneConfigurationPath)
                .append("/")
                .append(this.get(Resource.SCENE_CONFIGURATION.name()))
                .toString();
    }

    /**
     * Sets the scene configuration file name.
     *
     * @param fileName The file name without path and extension.
     */
    public void setSceneConfigurationFileName(final String fileName) {
        this.setProperty(Resource.SCENE_CONFIGURATION.name(), fileName + ".json");
    }

    /**
     * Scene image preview path.
     *
     * @return The preview path.
     */
    public String getScenePreviewImage() {
        return new StringBuilder(sceneConfigurationPath)
                .append("/")
                .append(this.get(Resource.PREVIEW_IMAGE.name()))
                .toString();
    }

    /**
     * Creates or saves this scene resource.
     */
    public final void save() throws IOException {
        try ( FileOutputStream fr = new FileOutputStream(configurationFile)) {
            store(fr, null);
        }
    }

}
