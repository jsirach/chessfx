/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.pidome.games.chess.engines.GamePlayerEngine;
import org.pidome.games.chess.engines.LocalPlayModeEngine;
import org.pidome.games.chess.resources.GameResourcesHelper.ResourceLocation;

/**
 * Misc helpers.
 *
 * @author johns
 */
public class ResourceHelper {

    /**
     * Path for default created scenes.
     */
    private static final String PATH_DEFAULT = "default/";
    /**
     * Path for community/custom created scenes.
     */
    private static final String PATH_COMMUNITY = "community/";

    private static final List<Class<?>> resources = new ArrayList<>() {
        {
            // Play modes
            add(LocalPlayModeEngine.class);
            add(GamePlayerEngine.class);
        }
    };

    /**
     * The list of available scenes read from disk.
     *
     * @param location the resource path.
     * @return Scene resources from disk.
     */
    public static List<SceneResource> getAvailableScenes(final ResourceLocation location) {
        List<SceneResource> returnList = new ArrayList<>();
        File baseFolder = new File(SceneResource.GAMESCENES_PATH + location.getPath());
        FilenameFilter propertyFileFilter
                = (folder, name) -> name.toLowerCase()
                        .endsWith(SceneResource.GAMESCENE_PROPERTIES_EXTENSION);
        Arrays.asList(baseFolder.listFiles()).stream()
                .filter(File::isDirectory)
                .map(file -> {
                    List<File> files = Arrays.asList(file.listFiles(propertyFileFilter));
                    return files;
                })
                .filter(Objects::nonNull)
                .forEach(files -> {
                    files.stream().forEach(file -> {
                        returnList.add(new SceneResource(file));
                    });
                });
        return returnList;
    }

    /**
     * The list of available scenes read from disk.
     *
     * @return Scene resources from disk.
     */
    public static List<BoardResource> getAvailableBoards() {
        List<BoardResource> returnList = new ArrayList<>();
        File baseFolder = new File(BoardResource.PATH);
        FilenameFilter propertyFileFilter
                = (folder, name) -> name.toLowerCase()
                        .endsWith(BoardResource.PROPERTIES_EXTENSION);
        Arrays.asList(baseFolder.listFiles()).stream()
                .filter(File::isDirectory)
                .map(file -> {
                    List<File> files = Arrays.asList(file.listFiles(propertyFileFilter));
                    return files;
                })
                .filter(Objects::nonNull)
                .forEach(files -> {
                    files.stream().forEach(file -> {
                        returnList.add(new BoardResource(file));
                    });
                });
        /// Until we have configurable 2D boards, add to the resource manually.
        BoardResource flatBoard = new BoardResource();
        flatBoard.setProperty(BoardResource.Resource.NAME.name(), "Default 2D board");
        flatBoard.setProperty(BoardResource.Resource.DESCRIPTION.name(), "A simple black white flat board view");
        flatBoard.setProperty(BoardResource.Resource.VIEWTYPE.name(), "FLAT");
        returnList.add(flatBoard);
        return returnList;
    }

    /**
     * The list of available scenes read from disk.
     *
     * @return Scene resources from disk.
     */
    public static List<PiecesResource> getAvailablePieces() {
        List<PiecesResource> returnList = new ArrayList<>();
        File baseFolder = new File(PiecesResource.PATH);
        FilenameFilter propertyFileFilter
                = (folder, name) -> name.toLowerCase()
                        .endsWith(PiecesResource.PROPERTIES_EXTENSION);
        Arrays.asList(baseFolder.listFiles()).stream()
                .filter(File::isDirectory)
                .map(file -> {
                    List<File> files = Arrays.asList(file.listFiles(propertyFileFilter));
                    return files;
                })
                .filter(Objects::nonNull)
                .forEach(files -> {
                    files.stream().forEach(file -> {
                        returnList.add(new PiecesResource(file));
                    });
                });
        /// Until we have configurable 2D PIECES, add to the resource manually.
        PiecesResource flatPieces = new PiecesResource();
        flatPieces.setProperty(BoardResource.Resource.NAME.name(), "Default 2D pieces");
        flatPieces.setProperty(BoardResource.Resource.DESCRIPTION.name(), "Simple pieces");
        flatPieces.setProperty(BoardResource.Resource.VIEWTYPE.name(), "FLAT");
        returnList.add(flatPieces);
        return returnList;
    }

    /**
     * Returns a list of classes defined by the given annotation.
     *
     * @param <A> The annotation class type.
     * @param <T> The base class type expected.
     * @param annotation The annotation.
     * @param expectedAbtractType The expected class.
     * @return List of classes with the given annotation.
     */
    @SuppressWarnings("unchecked")
    public static <A extends Annotation, T> List<Class<T>> getClassesByAnnotation(final Class<A> annotation, Class<T> expectedAbtractType) {
        List<Class<?>> list = resources.stream()
                .filter(klazz -> klazz.isAnnotationPresent(annotation))
                .filter(type -> expectedAbtractType.isAssignableFrom(type))
                .collect(Collectors.toList());
        List<Class<T>> returnList = new ArrayList<>();
        list.forEach(clazz -> returnList.add((Class<T>) clazz));
        return returnList;
    }

}
