/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.game.Game;
import com.github.bhlangonijr.chesslib.game.Player;
import com.github.bhlangonijr.chesslib.pgn.PgnHolder;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.game.ChessEventModel;
import org.pidome.games.chess.model.game.ChessEventRoundModel;
import org.pidome.games.chess.model.game.ChessEvents;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.gui.desktop.dialog.newgame.PlayerGameTypeSelectorFieldSet;

/**
 * Utility methods for interacting with PGN data.
 *
 * @author johns
 */
public class PgnResourceHelper {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(PlayerGameTypeSelectorFieldSet.class);

    /**
     * Date formatter of a game
     */
    private static final DateTimeFormatter GAME_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd");

    /**
     * Reads a PGN file with chess games.This method supports both single and
     * multi game PGN files.
     *
     *
     * @param pgnFile The File pointing to a PGN file.
     * @return List of Chess games.
     * @throws java.lang.Exception When loading from a pgn file fails.
     */
    public static ChessEvents readPgnLocalFile(File pgnFile) throws Exception {
        PgnHolder pgn = new PgnHolder(pgnFile.getAbsolutePath());
        pgn.loadPgn();
        return pgnMapper(pgn);
    }

    /**
     * Maps a library PGN file model data to local model data.
     *
     * @param pgn The pgn data.
     */
    private static ChessEvents pgnMapper(PgnHolder pgn) {
        ChessEvents eventsList = new ChessEvents();
        final String unknownEventId = UUID.randomUUID().toString();

        pgn.getGames().forEach(game -> {
            ChessEventModel eventModel = null;
            if (game.getRound().getEvent() == null) {
                eventModel = new ChessEventModel();
                eventModel.setId(unknownEventId);
                eventModel.setName(I18nProxy.getString(I18nApplicationKey.GAME_UNKNOWN_EVENT));
                eventModel.setSite(I18nProxy.getString(I18nApplicationKey.GAME_UNKNOWN_LOCATION));
                eventModel.setEventDate(LocalDate.now());
                eventsList.add(eventModel);
            } else if (!eventsList.hasEventModel(game.getRound().getEvent().getName())) {
                eventModel = new ChessEventModel();
                eventModel.setId(game.getRound().getEvent().getId());
                eventModel.setName(game.getRound().getEvent().getName());
                eventModel.setSite(game.getRound().getEvent().getSite());
                eventModel.setEventDate(LocalDate.parse(game.getRound().getEvent().getStartDate(), GAME_DATE_FORMATTER));
                eventsList.add(eventModel);
            } else {
                eventModel = eventsList.getEventModel(game.getRound().getEvent().getName());
            }
            ChessEventRoundModel roundModel = null;
            if (!eventModel.hasRound(String.valueOf(game.getRound().getNumber()))) {
                roundModel = new ChessEventRoundModel();
                roundModel.setChessEvent(eventModel);
                roundModel.setRoundNumber(game.getRound().getNumber());
                roundModel.setNotation(String.valueOf(game.getRound().getNumber()));
                eventModel.addRound(roundModel);
            } else {
                roundModel = eventModel.getChessEventRoundModel(String.valueOf(game.getRound().getNumber()));
            }
            assignNewGameModelToRound(roundModel, game);
        });
        return eventsList;
    }

    /**
     * Assign a single game to a round model.
     *
     * @param roundModel The round model.
     * @param game The game to attach.
     */
    private static void assignNewGameModelToRound(ChessEventRoundModel roundModel, final Game game) {
        Player whitePlayer = game.getWhitePlayer();
        Player blackPlayer = game.getBlackPlayer();

        ChessGamePlayerModel playerWhiteModel = new ChessGamePlayerModel();
        playerWhiteModel.nameProperty().setValue(whitePlayer.getName());
        playerWhiteModel.descriptionProperty().setValue(whitePlayer.getDescription());
        playerWhiteModel.longDescriptionProperty().setValue(whitePlayer.getLongDescription());
        playerWhiteModel.eloProperty().setValue(whitePlayer.getElo());
        playerWhiteModel.idProperty().setValue(whitePlayer.getId());
        playerWhiteModel.playerTypeProperty().setValue(whitePlayer.getType());
        playerWhiteModel.sideProperty().setValue(Side.WHITE);

        ChessGamePlayerModel playerBlackModel = new ChessGamePlayerModel();
        playerBlackModel.nameProperty().setValue(blackPlayer.getName());
        playerBlackModel.descriptionProperty().setValue(whitePlayer.getDescription());
        playerBlackModel.longDescriptionProperty().setValue(whitePlayer.getLongDescription());
        playerBlackModel.eloProperty().setValue(whitePlayer.getElo());
        playerBlackModel.idProperty().setValue(whitePlayer.getId());
        playerBlackModel.playerTypeProperty().setValue(whitePlayer.getType());
        playerWhiteModel.sideProperty().setValue(Side.BLACK);

        ChessGameModel gameModel = new ChessGameModel();
        gameModel.setChessEventRound(roundModel);
        gameModel.setAnnotator(game.getAnnotator());
        gameModel.setDate(LocalDate.parse(game.getDate(), GAME_DATE_FORMATTER));
        gameModel.setEco(game.getEco());
        gameModel.setGameId(game.getGameId());
        gameModel.setInitialPosition(game.getInitialPosition() == 1);
        gameModel.setOpening(game.getOpening());
        gameModel.setPlyCount(Integer.valueOf(game.getPlyCount()));
        gameModel.setResult(game.getResult());
        gameModel.setTermination(game.getTermination());
        if (game.getTime() != null && !game.getTime().isBlank()) {
            gameModel.setTime(LocalTime.parse(game.getTime()));
        }
        gameModel.setGame(game);
        gameModel.setVariation(game.getVariation());
        gameModel.setFen(game.getFen());

        gameModel.setPlayerBlack(playerBlackModel);
        gameModel.setPlayerWhite(playerWhiteModel);
        roundModel.addGame(gameModel);
    }

}
