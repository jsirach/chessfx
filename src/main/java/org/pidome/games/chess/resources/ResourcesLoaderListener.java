/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.resources;

/**
 * A listener for the resource loader.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public interface ResourcesLoaderListener {

    /**
     * Informs the progress listener about the amount of items going to be
     * loaded.
     *
     * @param amount The amount to be loaded.
     */
    public void toLoad(double amount);

    /**
     * A signal to inform an item is loaded.
     */
    public void update();

}
