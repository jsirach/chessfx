/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.pieces;

import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.CacheHint;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.world.config.PieceModelConfig;

/**
 * A piece with a perspective view.
 *
 * @author John Sirach
 */
public final class FlatPiece extends VisualPiece {

    /**
     * If images or text should be used.
     */
    private final boolean useImages = false;

    /**
     * Constructor of the piece.
     *
     * @param color The piece color.
     * @param variant The piece variant type.
     */
    public FlatPiece(final Variant variant, final VariantColor color) {
        super(false, variant, color);
    }

    /**
     * Loads a piece.
     *
     * @throws org.pidome.games.chess.game.world.pieces.PieceLoadException
     */
    @Override
    public void load(final String location, final PieceModelConfig modelConfig) throws PieceLoadException {
        if (!useImages) {
            final String pawnText;
            switch (this.getVariant()) {
                case K:
                    pawnText = this.getVariantColor().equals(VariantColor.WHITE) ? "\u2654" : "\u265A";
                    break;
                case Q:
                    pawnText = this.getVariantColor().equals(VariantColor.WHITE) ? "\u2655" : "\u265B";
                    break;
                case R:
                    pawnText = this.getVariantColor().equals(VariantColor.WHITE) ? "\u2656" : "\u265C";
                    break;
                case B:
                    pawnText = this.getVariantColor().equals(VariantColor.WHITE) ? "\u2657" : "\u265D";
                    break;
                case N:
                    pawnText = this.getVariantColor().equals(VariantColor.WHITE) ? "\u2658" : "\u265E";
                    break;
                default:
                    pawnText = this.getVariantColor().equals(VariantColor.WHITE) ? "\u2659" : "\u265F";
                    break;
            }
            final StackPane pieceContainer = new StackPane();
            pieceContainer.getStyleClass().add("transparent-background");
            final Text pieceText = new Text(pawnText);
            pieceText.getStyleClass().add("piece-color");
            pieceText.setBoundsType(TextBoundsType.VISUAL);
            pieceText.setPickOnBounds(true);
            pieceText.setLineSpacing(0);
            pieceText.setTextOrigin(VPos.CENTER);
            pieceText.setTextAlignment(TextAlignment.CENTER);
            pieceContainer.getChildren().add(pieceText);
            this.getChildren().add(pieceContainer);
        } else {
            try {
                final String fileLoad = new StringBuilder(location)
                        .append(this.getVariant().getText())
                        .append("_")
                        .append(this.getVariantColor().getText())
                        .append(".png")
                        .toString();
                final ImageView pieceView = GameResourcesHelper.loadImageViewAssetFromPackage(fileLoad);
                pieceView.setPreserveRatio(true);
                pieceView.setSmooth(true);
                this.getChildren().add(pieceView);
            } catch (Exception ex) {
                throw new PieceLoadException(ex);
            }
        }
    }

    /**
     * Returns the piece's width.
     *
     * @return The width.
     */
    @Override
    public double getWidth() {
        if (!useImages) {
            return ((StackPane) getChildren().get(0)).getLayoutBounds().getWidth();
        } else {
            return ((ImageView) getChildren().get(0)).getImage().getWidth();
        }
    }

    /**
     * Sets the resize ratio.
     *
     * @param fieldSize The resize ratio.
     */
    public final void setResizeRatio(double fieldSize) {
        if (!useImages) {
            final StackPane pieceContainer = (StackPane) getChildren().get(0);
            pieceContainer.setAlignment(Pos.CENTER);
            final Text imgLabel = (Text) pieceContainer.getChildren().get(0);
            imgLabel.setFont(new Font(fieldSize));
            pieceContainer.setPrefSize(fieldSize, fieldSize);
            FXUtils.setCache(pieceContainer, CacheHint.SPEED);
        } else {
            final ImageView imgPiece = (ImageView) getChildren().get(0);
            imgPiece.setFitWidth(getWidth() * (fieldSize / getWidth()));
            FXUtils.setCache(imgPiece, CacheHint.SPEED);
        }
    }

    /**
     * Returns the resize ratio of the flat piece.
     *
     * @return The resize ratio.
     */
    public final double getResizeRatio() {
        if (!useImages) {
            return ((Text) ((StackPane) getChildren().get(0)).getChildren().get(0)).getFont().getSize();
        } else {
            return ((ImageView) getChildren().get(0)).getFitWidth();
        }
    }

    /**
     * Marking a flat piece.
     *
     * @param marked if marked or not.
     */
    @Override
    public void setMarked(final boolean marked) {
        super.setMarked(marked);
        this.getChildren().forEach((node) -> {
            if (this.isMarked()) {
                node.setEffect(new DropShadow(node.getLayoutBounds().getWidth() / 8, Color.AQUA));
            } else {
                node.setEffect(null);
            }
        });
    }

}
