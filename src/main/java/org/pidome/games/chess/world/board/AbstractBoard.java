/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Scale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.engines.HighlightType;
import org.pidome.games.chess.events.GameEvent;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.events.SquareIntentEvent;
import org.pidome.games.chess.events.SquareSelectEvent;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.model.options.GameOptions.GameViewType;
import org.pidome.games.chess.model.options.boards.BoardOptions;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.world.config.BoardModelConfig;
import org.pidome.games.chess.world.config.WorldModel;
import org.pidome.games.chess.world.pieces.AbstractPieces;
import org.pidome.games.chess.world.pieces.FlatPiece;
import org.pidome.games.chess.world.pieces.VisualPiece;

/**
 * A board base.
 *
 * The loading of the board is currently only supported by loading obj files.
 *
 * Every board must come with a json file which exposes the size of single
 * square on the playfield and the x, y and z coordinates of the left bottom
 * square center point.
 *
 * The board will then be able to place all the pieces following the rule four
 * pawns should fit in a single square.
 *
 * @author John Sirach
 */
public abstract class AbstractBoard extends WorldModel<BoardModelConfig> {

    /**
     * Class logger.
     */
    private final Logger LOG = LogManager.getLogger(this.getClass());

    /**
     * Content place holder for 2D boards.
     */
    private StackPane flatPane;

    /**
     * Observable list of the pieces in the board game.
     */
    private final ObservableList<VisualPiece> pieces = FXCollections.observableArrayList();

    /**
     * The fields to layout over the board.
     */
    private final ObservableList<BoardSquare> fields = FXCollections.observableArrayList();

    /**
     * What to do when a piece is clicked.
     */
    private final EventHandler<MouseEvent> pieceClicked = this::notifyPieceClicked;

    /**
     * When a square is hovered in or out.
     */
    private final EventHandler<MouseEvent> squareHovered = this::notifySquareHover;

    /**
     * When a square is hovered in or out.
     */
    private final EventHandler<MouseEvent> squareForMoveClicked = this::notifyMoveSelection;

    /**
     * The board options which has been set.
     */
    private BoardOptions boardOptions;

    /**
     * If is perspective board or not.
     */
    private boolean isPerspective;

    /**
     * If used the 2d board size.
     */
    private final ObjectProperty<Point2D> boardSize = new SimpleObjectProperty<>();

    /**
     * The diagonal length of a board.
     */
    private double diagonalLength;

    /**
     * The game options.
     */
    protected AbstractGenericGameOptions gameOptions;

    /**
     * Constructor.
     */
    public AbstractBoard() {
        super(BoardModelConfig.class);
    }

    /**
     * Sets the game board.
     *
     * @param gameOptions The game options.
     * @param boardOptions The options for displaying the board.
     */
    public void setOptions(final AbstractGenericGameOptions gameOptions, final BoardOptions boardOptions) {
        if (this.boardOptions == null) {
            this.boardOptions = boardOptions;
            this.gameOptions = gameOptions;
            this.isPerspective = this.boardOptions.viewTypeProperty().get().equals(GameViewType.PERSPECTIVE);
            if (this.isPerspective) {
                Scale boardScale = new Scale();
                boardScale.setX(4);
                boardScale.setY(4);
                boardScale.setZ(4);
                this.getTransforms().addAll(boardScale);
            } else {
                flatPane = new StackPane();
            }
        }
    }

    /**
     * Starts a new game.
     */
    public void build() {
        registerPlayFields();
        registerRestFields();

        if (this.isPerspective) {
            this.getChildren().addAll(getFields());
            this.getChildren().addAll(getPieces());
        } else {
            flatPane.getChildren().addAll(getFields());
            flatPane.getChildren().addAll(getPieces());

        }
        buildAdditionalFeatures();
        connectListeners();
    }

    /**
     * Destroys board contents.
     */
    public void destroy() {
        getFields().forEach(field -> {
            field.destroy();
        });
        this.removeListeners();
        this.boardOptions = null;
    }

    /**
     * Returns the 2D board.
     *
     * A 2D board is only available when it is instantiated as a 2d board.
     *
     * @return StackPane with a 2D board.
     */
    public final StackPane getFlatBoard() {
        return this.flatPane;
    }

    /**
     * Returns the list with the registered fields on the board.
     *
     * @return List of registered fields.
     */
    protected final ObservableList<BoardSquare> getFields() {
        return this.fields;
    }

    /**
     * Returns the list with the registered pieces on the board.
     *
     * @return List of registered pieces on the board.
     */
    protected final ObservableList<VisualPiece> getPieces() {
        return this.pieces;
    }

    /**
     * Notification for a move intention.
     *
     * @param move The move to perform.
     */
    protected void notifyMoveIntent(final Move move) {
        final MoveEvent moveEvent = new MoveEvent(gameOptions.getGameId(), GameEvent.GameEventType.MOVE_INTENT, move, this.getPiece(move.getFrom()).getRulesEnginePiece());
        if (this.getBoardSquare(move.getTo()).hasPiece()) {
            moveEvent.addMoveType(MoveEvent.MoveType.MOVE_CAPTURE);
        }
        GameEventsPublisher.publishMoveIntentEvent(moveEvent);
    }

    /**
     * Performs a move on the board.
     *
     * @param move The move to perform.
     * @return if the move succeeded.
     */
    public boolean performMove(final Move move) {
        this.setPiece(this.getPiece(move.getFrom()), move.getTo().name());
        return true;
    }

    /**
     * Performs a move and at the end replaces the moved piece with the given
     * piece.
     *
     * @param move The move to perform.
     * @param replacementPiece The visual piece to replace the moved piece with.
     * @return true when done.
     */
    public boolean performMoveReplace(final Move move, final VisualPiece replacementPiece) {
        this.setPiece(this.getPiece(move.getFrom()), move.getTo().name(), replacementPiece);
        return true;
    }

    /**
     * What to do when a field is clicked.
     */
    private void notifyPieceClicked(final MouseEvent event) {
        event.consume();
        if (event.getSource() != null) {
            final VisualPiece selectedPiece = (VisualPiece) event.getSource();
            final SquareSelectEvent pieceEvent = new SquareSelectEvent(gameOptions.getGameId(), this.getSquare(selectedPiece), !selectedPiece.isMarked());
            GameEventsPublisher.publishPieceSelectedEvent(pieceEvent);
        }
    }

    /**
     * What to do when a square is hovered.
     */
    private void notifyMoveSelection(final MouseEvent event) {
        event.consume();
        Move move = null;
        for (BoardSquare field : this.fields) {
            if (field.getVisualPiece() != null
                    && !field.getVisualPiece().getRulesEnginePiece().equals(Piece.NONE)
                    && field.getVisualPiece().isMarked()) {
                move = new Move(field.getIndexForRuleEngine(), ((BoardSquare) event.getSource()).getIndexForRuleEngine());
                break;
            }
        }
        if (move != null) {
            notifyMoveIntent(move);
        }
    }

    /**
     * What to do when a square is hovered.
     */
    private void notifySquareHover(final MouseEvent event) {
        final BoardSquare hoveredSquare = (BoardSquare) event.getSource();
        SquareIntentEvent hoveredSquareEvent = new SquareIntentEvent(
                gameOptions.getGameId(),
                hoveredSquare.getIndexForRuleEngine(),
                event.getEventType().equals(MouseEvent.MOUSE_ENTERED) ? SquareIntentEvent.SquareIntent.OVER : SquareIntentEvent.SquareIntent.OUT,
                hoveredSquare.getCurrentHighlighting()
        );
        GameEventsPublisher.publishSquareOverEvent(hoveredSquareEvent);
    }

    /**
     * If a square should be highlighted or not.
     *
     * @param square The square involved.
     * @param type type to highlight a square with.
     */
    public final void highlightSquare(final Square square, final HighlightType type) {
        for (BoardSquare field : this.fields) {
            if (field.getIndexForRuleEngine().equals(square)) {
                field.setMark(type);
                break;
            }
        }
    }

    /**
     * Marks a piece on the board as selected.
     *
     * @param square The square to select piece on.
     * @param select To select or deselect.
     */
    public final void selectPiece(final Square square, final boolean select) {
        final VisualPiece visualPiece = this.getVisualPiece(square);
        if (visualPiece != null) {
            visualPiece.setMarked(select);
            this.getBoardSquare(square).setMark(select ? HighlightType.SELECT : HighlightType.NONE);
            setOppositeSidePiecesMouseTransparent(visualPiece.getRulesEnginePiece().getPieceSide(), select);
        }
    }

    /**
     * Helper to make the opponent pieces transparent for mouse handles.
     *
     * This fixes at lest on 2D boards where pieces are sized to fields and in
     * that way cover the square which causes the hover notification not to be
     * fired.
     *
     * @param side The side that needs to retain mouse events.
     * @param ignoreMouse To enable or disable mouse ignore.
     */
    private void setOppositeSidePiecesMouseTransparent(final Side side, final boolean ignoreMouse) {
        for (BoardSquare field : this.fields) {
            if (field.getVisualPiece() != null && !field.getVisualPiece().getRulesEnginePiece().getPieceSide().equals(side)) {
                field.getVisualPiece().ignoreMouse(ignoreMouse);
            }
        }
    }

    /**
     * Removes listeners.
     */
    private void removeListeners() {
        this.fields.forEach(square -> {
            if (!square.getIndexForRuleEngine().equals(Square.NONE)) {
                square.setOnMouseEntered(null);
                square.setOnMouseExited(null);
                square.setOnMouseClicked(null);
            }
        });
        pieces.forEach(piece -> {
            piece.setOnMouseClicked(null);
        });
    }

    /**
     * Registers listeners.
     */
    private void connectListeners() {
        LOG.debug("Enabling board listeners");
        this.fields.forEach(square -> {
            if (!square.getIndexForRuleEngine().equals(Square.NONE)) {
                LOG.trace("Enabling listener on [{}]", square);
                square.setOnMouseEntered(squareHovered);
                square.setOnMouseExited(squareHovered);
                square.setOnMouseClicked(squareForMoveClicked);
            }
        });
        pieces.forEach(piece -> {
            piece.setOnMouseClicked(pieceClicked);
        });
    }

    /**
     * Places pieces in their start position according to the given FEN
     * notation.
     *
     * @param fenNotation The notation of the start position of the pieces.
     */
    public final void placePiecesInStartPosition(String fenNotation) {
        this.pieces.forEach((piece) -> {
            piece.inGame(false);
        });
    }

    /**
     * Places the pieces on the board in new game position.
     */
    public final void placePiecesInStartPosition() {
        this.pieces.forEach((piece) -> {
            piece.inGame(false);
        });
        /// remove all pieces from rest field registration.
        this.fields.stream().filter((square) -> (square.getIndexForRuleEngine().equals(Square.NONE))).forEach((square) -> {
            square.removePiece();
        });
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_ROOK), "A1");
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_KNIGHT), "B1");
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_BISHOP), "C1");
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_QUEEN), "D1");
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_KING), "E1");
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_BISHOP), "F1");
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_KNIGHT), "G1");
        this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_ROOK), "H1");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_ROOK), "A8");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_KNIGHT), "B8");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_BISHOP), "C8");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_QUEEN), "D8");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_KING), "E8");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_BISHOP), "F8");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_KNIGHT), "G8");
        this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_ROOK), "H8");
        for (int x = 0; x < 8; x++) {
            this.setPiece(this.getNextNotInGamePiece(Piece.WHITE_PAWN), BoardSquare.getCombinedSquareIndex(x, 1));
            this.setPiece(this.getNextNotInGamePiece(Piece.BLACK_PAWN), BoardSquare.getCombinedSquareIndex(x, 6));
        }
    }

    /**
     * Copy the transformations from one piece to another.
     *
     * @param copyFrom The copy source.
     * @param copyTo The paste target.
     */
    private void copyTransforms(final VisualPiece copyFrom, final VisualPiece copyTo) {
        if (copyTo.isPerspective() && copyFrom.isPerspective()) {
            copyFrom.getTransforms().forEach((transform) -> {
                copyTo.getTransforms().add(transform.clone());
            });
        } else {
            ((FlatPiece) copyTo).setResizeRatio(((FlatPiece) copyFrom).getResizeRatio());
        }
    }

    /**
     * Sets the board size.
     *
     * Use this when implementing a custom board which is not loaded from any
     * asset.
     *
     * @param boardSize Sets the size of the board.
     */
    protected void setBoardSize(final Point2D boardSize) {
        this.boardSize.setValue(boardSize);
        this.diagonalLength = Math.hypot(boardSize.getX(), boardSize.getY());
    }

    /**
     * Returns the board size.
     *
     * @return The size of a board.
     */
    public final ObjectProperty<Point2D> getBoardSize() {
        return this.boardSize;
    }

    /**
     * Returns the diagonal length of the board.
     *
     * @return The diagonal length.
     */
    public final double getBoardDiagonalLength() {
        return this.diagonalLength;
    }

    /**
     * Sets a piece on a square.
     *
     * @param piece The piece to set.
     * @param square The square to put it on.
     */
    private void setPiece(final VisualPiece piece, final String square) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Moving piece [{}] to square [{}] on board type [{}]",
                    piece, square,
                    this.isPerspective ? "3D" : "FLAT"
            );
        }
        this.setPiece(piece, square, null);
    }

    /**
     * Sets a piece on a square.
     *
     * @param piece The piece to set.
     * @param square The square to put it on.
     * @param visualPieceReplacement If not null, the original piece will be
     * replaced with the given piece.
     */
    private void setPiece(final VisualPiece piece, String square, final VisualPiece visualPieceReplacement) {
        final BoardSquare boardquare = this.getBoardSquare(piece);
        for (BoardSquare field : fields) {
            if (field.getIndex().equals(square)) {
                setMoveLine(piece, field);
                if (boardquare != null) {
                    boardquare.removePiece();
                }
                if (visualPieceReplacement != null) {
                    VisualPiece fromPiece = this.getPiece(visualPieceReplacement.getRulesEnginePiece());
                    visualPieceReplacement.setOnMouseClicked(pieceClicked);
                    copyTransforms(fromPiece, visualPieceReplacement);
                }
                field.setPiece(piece, getPieceDistanceTravelPercentage(boardquare, field), visualPieceReplacement);
            }
        }
    }

    /**
     * Takes a piece from the opponent.
     *
     * @param square The square to take the piece from.
     */
    public void takePiece(final Square square) {
        final VisualPiece visualPiece = this.getVisualPiece(square);
        final BoardSquare boardSquareFrom = this.getBoardSquare(square);
        if (visualPiece != null) {
            LOG.debug("Taking piece from square: [{}]", visualPiece.getRulesEnginePiece(), square);
            for (BoardSquare field : fields) {
                if (field.getIndexForRuleEngine().equals(Square.NONE) && !field.getSide().equals(visualPiece.getRulesEnginePiece().getPieceSide()) && !field.hasPiece()) {
                    setMoveLine(visualPiece, field);
                    field.setPiece(visualPiece, getPieceDistanceTravelPercentage(boardSquareFrom, field));
                    field.getVisualPiece().inGame(false);
                    LOG.trace("Moving piece [{}] from to square: [{}], side: [{}], hasPiece: [{}]", visualPiece.getRulesEnginePiece(), field.getIndexForRuleEngine(), field.getSide(), field.hasPiece());
                    break;
                }
            }
        } else {
            LOG.warn("No piece known on [{}]", square);
        }
    }

    /**
     * Puts all the pieces in rest.
     *
     * @param animated if the pieces should be placed in rest in an animated
     * style.
     */
    public void placePiecesInRest(final boolean animated) {
        this.pieces.forEach((piece) -> {
            this.restPiece(piece, animated);
        });
    }

    /**
     * Puts a piece in rest.
     *
     * @param piece The piece to remove from the game.
     */
    private void restPiece(final VisualPiece piece, final boolean animated) {
        fields.stream()
                .filter(field -> field.getIndexForRuleEngine().equals(Square.NONE))
                .filter(field -> field.getSide().equals(piece.getRulesEnginePiece().getPieceSide()))
                .filter(field -> !field.hasPiece() || !field.getVisualPiece().getRulesEnginePiece().getPieceSide().equals(field.getSide()))
                .findFirst()
                .ifPresent(field -> {
                    field.setPiece(piece, animated ? 100 : 0);
                    field.getVisualPiece().inGame(false);
                });
    }

    /**
     * Returns the next piece of the given piece type that's not in game.
     *
     * @param pieceType The piece type to get.
     * @return The visual piece, null when not available.
     */
    public final VisualPiece getNextNotInGamePiece(final Piece pieceType) {
        for (VisualPiece piece : this.pieces) {
            if (piece.getRulesEnginePiece().equals(pieceType) && !piece.isInGame()) {
                return piece;
            }
        }
        return null;
    }

    /**
     * Returns the visual piece based on the engine piece.
     *
     * @param enginePiece The engine piece.
     * @return The visual piece when found, otherwise null.
     */
    public final VisualPiece getPiece(final Piece enginePiece) {
        for (VisualPiece piece : this.pieces) {
            if (piece.getRulesEnginePiece().equals(enginePiece)) {
                return piece;
            }
        }
        return null;
    }

    /**
     * Returns the square for the rules engine.
     *
     * @param squareIndex The index to get the square from.
     * @return The square for the rules engine.
     */
    public final Square getSquare(final String squareIndex) {
        for (BoardSquare square : this.fields) {
            if (square.getIndex().equals(squareIndex)) {
                return square.getIndexForRuleEngine();
            }
        }
        return null;
    }

    /**
     * Returns the square for the rules engine.
     *
     * @param piece The piece to get the square from.
     * @return The square for the rules engine.
     */
    public final Square getSquare(final VisualPiece piece) {
        for (BoardSquare square : this.fields) {
            if (square.hasPiece() && square.getVisualPiece().equals(piece)) {
                return square.getIndexForRuleEngine();
            }
        }
        return null;
    }

    /**
     * Returns the square for the rules engine.
     *
     * @param squareIndex The index to get the square from.
     * @return The square for the rules engine.
     */
    public final Square getSquare(final Square squareIndex) {
        for (BoardSquare square : this.fields) {
            if (square.getIndexForRuleEngine().equals(squareIndex)) {
                return square.getIndexForRuleEngine();
            }
        }
        return null;
    }

    /**
     * Returns the square for the rules engine.
     *
     * @param piece The piece to get the square from.
     * @return The square for the board.
     */
    public final BoardSquare getBoardSquare(final VisualPiece piece) {
        for (BoardSquare square : this.fields) {
            if (square.hasPiece() && square.getVisualPiece().equals(piece)) {
                return square;
            }
        }
        return null;
    }

    /**
     * Returns the square for the rules engine.
     *
     * @param square The square to get the boardsquare from.
     * @return The square for the board.
     */
    public final BoardSquare getBoardSquare(final Square square) {
        for (BoardSquare boardSquare : this.fields) {
            if (boardSquare.getIndexForRuleEngine().equals(square)) {
                return boardSquare;
            }
        }
        return null;
    }

    /**
     * Returns the square for the rules engine.
     *
     * @param squareIndex The index to get the square from.
     * @return The square for the rules engine.
     */
    public final VisualPiece getPiece(final Square squareIndex) {
        for (BoardSquare square : this.fields) {
            if (square.getIndexForRuleEngine().equals(squareIndex)) {
                return square.getVisualPiece();
            }
        }
        return null;
    }

    /**
     * Returns the visual piece on the board by the given game board piece.
     *
     * @param square The square to get the visual piece from.
     * @return The visual piece when found, otherwise null.
     */
    public final VisualPiece getVisualPiece(final Square square) {
        for (BoardSquare boardSquare : this.fields) {
            if (boardSquare.getIndexForRuleEngine().equals(square) && boardSquare.hasPiece()) {
                return boardSquare.getVisualPiece();
            }
        }
        return null;
    }

    /**
     * Loads a board.
     *
     * @param resourcesLoader The resource loader to schedule asset loading on.
     */
    public abstract void scheduleAssetLoading(final ResourcesLoader resourcesLoader);

    /**
     * Build board additional board features.
     */
    protected abstract void buildAdditionalFeatures();

    /**
     * Destroys additional features.
     */
    protected abstract void destroyAdditionalFeatures();

    /**
     * Draws a line from piece current location to piece new location.
     *
     * @param piece The piece to draw for.
     * @param endSquare The square moving to.
     */
    protected abstract void setMoveLine(final VisualPiece piece, final BoardSquare endSquare);

    /**
     * Registers the loaded pieces into to the board.
     *
     * This method resizes pieces to board fields.
     *
     * Perspective board: The first calculation is to fit 4 pieces in a field
     * based on field width. The result is then applied to scale all pieces.
     *
     * Flat board: Pieces are fit as such the square image covers 90% of the
     * field.
     *
     * @param piecesSet The pieces set.
     */
    public abstract void registerPieces(final AbstractPieces piecesSet);

    /**
     * Registers the fields on the board.
     */
    protected abstract void registerPlayFields();

    /**
     * Registers the fields on the board.
     */
    protected abstract void registerRestFields();

    /**
     * Calculates the distance percentage to travel.
     *
     * @param from The square coming from.
     * @param to The square moving to.
     * @return The percentage to travel in respect to the board diagonal.
     */
    protected abstract double getPieceDistanceTravelPercentage(final BoardSquare from, final BoardSquare to);

    /**
     * Returns the coordinates of the center point of the bottom left field.
     *
     * The board squares are often not on the ground plane, but a little above.
     * The 3D point indicates x,y and z for negative depth.
     *
     * @return The coordinates of the bottom left field.
     */
    public abstract ObjectProperty<Point3D> getFirstSquareCoordinate();

    /**
     * Returns the size of a field.
     *
     * @return The size of a field.
     */
    public abstract ObjectProperty<Point3D> getFieldSize();

    /**
     * Cleans any additional features which needs to be cleaned.
     *
     * Cleaning does not removing, it only tidies up the board.
     */
    public abstract void cleanAdditionalFeatures();

}
