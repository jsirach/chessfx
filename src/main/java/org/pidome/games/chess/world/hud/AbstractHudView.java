/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.hud;

import javafx.scene.layout.BorderPane;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.utils.Disposable;

/**
 * The base for the hud view.
 *
 * Extending this class provides basic HUD views. It's up to the hud view to be
 * compatible between perspective and flat typed views.
 *
 * @author John Sirach
 */
public abstract class AbstractHudView extends BorderPane implements Disposable {

    /**
     * The game used in the HUD view.
     */
    public final AbstractGenericGameOptions gameOptions;

    /**
     * Constructor.
     *
     * @param options The options of the game being played.
     */
    public AbstractHudView(final AbstractGenericGameOptions options) {
        this.gameOptions = options;
    }

    /**
     * Returns the game options of the current game.
     *
     * @return Game options.
     */
    protected final AbstractGenericGameOptions getGameOptions() {
        return this.gameOptions;
    }
}
