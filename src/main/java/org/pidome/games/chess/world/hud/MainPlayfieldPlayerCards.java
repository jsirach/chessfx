/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.hud;

import com.github.bhlangonijr.chesslib.Side;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;

/**
 * Container for holding player cards.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class MainPlayfieldPlayerCards extends BorderPane {

    /**
     * Player card for white.
     */
    private final PlayerCard playerCardWhite;
    /**
     * Player card for black.
     */
    private final PlayerCard playerCardBlack;

    /**
     * Constructor.
     *
     * @param gameOptions The game's options.
     */
    public MainPlayfieldPlayerCards(final AbstractGenericGameOptions gameOptions) {
        this.getStyleClass().add("player-card-container");
        setMouseTransparent(true);
        setPadding(new Insets(10));
        setMaxSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        playerCardWhite = new PlayerCard(gameOptions, Side.WHITE);
        playerCardBlack = new PlayerCard(gameOptions, Side.BLACK);
        setLeft(playerCardWhite);
        setRight(playerCardBlack);
    }

}
