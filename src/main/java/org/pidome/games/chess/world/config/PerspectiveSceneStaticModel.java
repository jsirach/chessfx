/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import org.pidome.games.chess.world.config.Location3D;
import org.pidome.games.chess.world.config.RotationXConfig;
import org.pidome.games.chess.world.config.RotationYConfig;

/**
 * Configuration of placement of a single static model in a perspective game
 * scene.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PerspectiveSceneStaticModel {

    /**
     * Base path for static items.
     */
    public static final String BASE_PATH = "/resources/assets/perspective/environment";

    /**
     * Item category.
     */
    private String category;

    /**
     * The sub category.
     */
    private String subCategory;

    /**
     * Item reference.
     */
    private String item;

    /**
     * The file identifying the item.
     */
    private String file;

    /**
     * Location in the world.
     */
    private Location3D location;

    /**
     * The rotation config on X axis.
     */
    private RotationXConfig rotationXConfig;

    /**
     * The rotation on Y Axis.
     */
    private RotationYConfig rotationYConfig;

    /**
     * set the item category.
     *
     * @param category Item category.
     */
    public void setCategory(final String category) {
        this.category = category;
    }

    /**
     * The item category.
     *
     * @return Item category.
     */
    public String getCategory() {
        return this.category;
    }

    /**
     * Set the subcategory.
     *
     * @param subCategory The subcategory to set.
     */
    public void setSubCategory(final String subCategory) {
        this.subCategory = subCategory;
    }

    /**
     * @return the subCategory
     */
    public String getSubCategory() {
        return subCategory;
    }

    /**
     * Set the item reference
     *
     * @param item Item reference.
     */
    public void setItem(final String item) {
        this.item = item;
    }

    /**
     * The item reference.
     *
     * @return Item reference.
     */
    public String getItem() {
        return this.item;
    }

    /**
     * Set the file for the item.
     *
     * @param file The file for the item.
     */
    public void setFile(final String file) {
        this.file = file;
    }

    /**
     * Return the file used for the item.
     *
     * @return The file.
     */
    public String getFile() {
        return this.file;
    }

    /**
     * @return the location
     */
    public Location3D getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location3D location) {
        this.location = location;
    }

    /**
     * @return the rotationXConfig
     */
    public RotationXConfig getRotationXConfig() {
        return rotationXConfig;
    }

    /**
     * @param rotationXConfig the rotationXConfig to set
     */
    public void setRotationXConfig(final RotationXConfig rotationXConfig) {
        this.rotationXConfig = rotationXConfig;
    }

    /**
     * @return the rotationYConfig
     */
    public RotationYConfig getRotationYConfig() {
        return rotationYConfig;
    }

    /**
     * @param rotationYConfig the rotationYConfig to set
     */
    public void setRotationYConfig(final RotationYConfig rotationYConfig) {
        this.rotationYConfig = rotationYConfig;
    }

}
