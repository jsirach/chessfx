/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.statics.plants;

import javafx.scene.transform.Rotate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.world.config.StaticWorldModel;

/**
 * A small cactus to place somewhere nice.
 *
 * @author John
 */
public class PerspectiveSmallCactus extends StaticWorldModel {

    /**
     * Class logger.
     */
    private final Logger LOG = LogManager.getLogger(PerspectiveSmallCactus.class);

    /**
     * Constructor.
     */
    public PerspectiveSmallCactus() {
        super("/resources/assets/perspective/environment/plants/smallcactus/small_cactus.obj");
    }

    /**
     * Loads the plant.
     */
    @Override
    public void loadAssetOldSchool() {
        try {
            super.loadAssetOldSchool();
            final Rotate rotate = new Rotate();
            rotate.setAxis(Rotate.Y_AXIS);
            rotate.setAngle(-45);
            this.getTransforms().add(rotate);
            this.setTranslateX(2000);
            this.setTranslateZ(760);
        } catch (Exception ex) {
            LOG.error("Unable to load small cactus plant", ex);
        }
    }

}
