/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.transform.Rotate;

/**
 * A base model config object.
 *
 * @author johns
 */
public class ModelConfig {

    /**
     * Model ingame given name.
     */
    private StringProperty name = new SimpleStringProperty("");

    /**
     * Configuration per mesh node.
     */
    private List<MeshConfig> meshConfigurations;

    /**
     * The scale configuration.
     */
    private ScaleConfig scaleConfig;

    /**
     * Rotation on X axis.
     */
    @JsonIgnore
    private Rotate rotationX;

    /**
     * Rotation on Y axis.
     */
    @JsonIgnore
    private Rotate rotationY;

    /**
     * Rotation on Z axis.
     */
    @JsonIgnore
    private Rotate rotationZ;

    /**
     * Set the model name.
     *
     * @param name The name to set.
     * @return this.
     */
    @JsonSetter
    public final ModelConfig setName(final String name) {
        this.name.setValue(name);
        return this;
    }

    /**
     * Returns the model name.
     *
     * @return The name of the model.
     */
    @JsonGetter
    public final String getName() {
        return this.name.get();
    }

    /**
     * Returns the string property for the model name.
     *
     * @return The bindable name property.
     */
    @JsonIgnore
    public final StringProperty nameProperty() {
        return this.name;
    }

    /**
     * Returns node configurations.
     *
     * @return The nodes configuration.
     */
    @JsonGetter
    public final List<MeshConfig> getNodeConfigurations() {
        return this.meshConfigurations;
    }

    /**
     * Sets the nodes configurations.
     *
     * @param configurations The configurations to set.
     * @return this.
     */
    @JsonSetter
    public ModelConfig setNodeConfigurations(final List<MeshConfig> configurations) {
        this.meshConfigurations = configurations;
        return this;
    }

    /**
     * Set the scale config.
     *
     * @param scaleConfig The scale config to set.
     * @return this.
     */
    public final ModelConfig setScaleConfig(final ScaleConfig scaleConfig) {
        this.scaleConfig = scaleConfig;
        return this;
    }

    /**
     * Returns the scale configuration.
     *
     * @return The scale configuration.
     */
    public final ScaleConfig getScaleConfig() {
        return this.scaleConfig;
    }

    /**
     * @return the rotationX.
     */
    @JsonSetter
    public Double getRotationX() {
        if (!Objects.isNull(rotationX)) {
            return rotationX.getAngle();
        }
        return null;
    }

    /**
     * Returns the X rotation property.
     *
     * @return The X rotation property.
     */
    @JsonIgnore
    public Rotate getRotateX() {
        if (Objects.isNull(rotationX)) {
            this.rotationX = new Rotate(0, Rotate.X_AXIS);
        }
        return this.rotationX;
    }

    /**
     * @param rotationX the rotationX to set.
     */
    @JsonSetter
    public void setRotationX(final Double rotationX) {
        if (Objects.isNull(this.rotationX)) {
            this.rotationX = new Rotate(rotationX, Rotate.X_AXIS);
        } else {
            this.rotationX.setAngle(rotationX);
        }
    }

    /**
     * @return the rotationY.
     */
    @JsonSetter
    public Double getRotationY() {
        if (!Objects.isNull(rotationY)) {
            return rotationY.getAngle();
        }
        return null;
    }

    /**
     * Returns the Y rotation property.
     *
     * @return The Y rotation property.
     */
    @JsonIgnore
    public Rotate getRotateY() {
        if (Objects.isNull(rotationY)) {
            this.rotationY = new Rotate(0, Rotate.Y_AXIS);
        }
        return this.rotationY;
    }

    /**
     * @param rotationY the rotationY to set.
     */
    @JsonSetter
    public void setRotationY(final Double rotationY) {
        if (Objects.isNull(this.rotationY)) {
            this.rotationY = new Rotate(rotationY, Rotate.Y_AXIS);
        } else {
            this.rotationY.setAngle(rotationY);
        }
    }

    /**
     * @return the rotationZ.
     */
    @JsonSetter
    public Double getRotationZ() {
        if (!Objects.isNull(rotationZ)) {
            return rotationZ.getAngle();
        }
        return null;
    }

    /**
     * Returns the Z rotation property.
     *
     * @return The Z rotation property.
     */
    @JsonIgnore
    public Rotate getRotateZ() {
        if (Objects.isNull(rotationZ)) {
            this.rotationZ = new Rotate(0, Rotate.Z_AXIS);
        }
        return this.rotationZ;
    }

    /**
     * @param rotationZ the rotationZ to set.
     */
    @JsonSetter
    public void setRotationZ(final Double rotationZ) {
        if (Objects.isNull(this.rotationZ)) {
            this.rotationZ = new Rotate(rotationZ, Rotate.Z_AXIS);
        } else {
            this.rotationZ.setAngle(rotationZ);
        }
    }
}
