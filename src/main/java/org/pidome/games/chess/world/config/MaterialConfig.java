/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.io.File;
import java.util.Objects;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;

/**
 * Configuration to apply to PhongMaterial.
 *
 * @author johns
 */
public class MaterialConfig {

    /**
     * The material.
     */
    private PhongMaterial material = new PhongMaterial();

    /**
     * @see PhongMaterial#setBumpMap(javafx.scene.image.Image)
     * @param fileLocation The location of the bump map.
     * @return this.
     */
    @JsonSetter
    public final MaterialConfig setBumpMap(final String fileLocation) {
        material.setBumpMap(
                new Image(
                        new File(
                                fileLocation
                        ).toURI().toString()
                )
        );
        return this;
    }

    /**
     * @see PhongMaterial#getBumpMap()
     * @return The location of the map.
     */
    @JsonGetter
    public final String getBumpMap() {
        return Objects.nonNull(material.getBumpMap())
                ? resetPath(material.getBumpMap().getUrl())
                : null;
    }

    /**
     * @see PhongMaterial#setDiffuseColor(javafx.scene.paint.Color)
     * @see ColorConfig
     * @param color The color config.
     * @return this.
     */
    @JsonSetter
    public final MaterialConfig setDiffuseColor(final ExtendedColorConfig color) {
        if (Objects.nonNull(color)) {
            material.setDiffuseColor(Color.hsb(color.getHue(), color.getSaturation(), color.getBrightness(), color.getOpacity()));
        }
        return this;
    }

    /**
     * @see PhongMaterial#getDiffuseColor()
     * @see ColorConfig
     * @return The color configuration.
     */
    @JsonGetter
    public final ExtendedColorConfig getDiffuseColor() {
        if (!Objects.isNull(material.getDiffuseColor())) {
            final ExtendedColorConfig extendedColor = new ExtendedColorConfig();
            extendedColor.setHue(material.getDiffuseColor().getHue());
            extendedColor.setSaturation(material.getDiffuseColor().getSaturation());
            extendedColor.setBrightness(material.getDiffuseColor().getBrightness());
            extendedColor.setOpacity(material.getDiffuseColor().getOpacity());
            return extendedColor;
        }
        return null;
    }

    /**
     * @see PhongMaterial#setDiffuseMap(javafx.scene.image.Image)
     * @param fileLocation The location of the diffuse map.
     * @return this.
     */
    @JsonSetter
    public final MaterialConfig setDiffuseMap(final String fileLocation) {
        material.setDiffuseMap(
                new Image(
                        new File(
                                fileLocation
                        ).toURI().toString()
                )
        );
        return this;
    }

    /**
     * @see PhongMaterial#getDiffuseMap()
     * @return The location of the map.
     */
    @JsonGetter
    public final String getDiffuseMap() {
        return Objects.nonNull(material.getDiffuseMap())
                ? resetPath(material.getDiffuseMap().getUrl())
                : null;
    }

    /**
     * @see PhongMaterial#setSelfIlluminationMap(javafx.scene.image.Image)
     * @param fileLocation The location of the bump map.
     * @return this.
     */
    @JsonSetter
    public final MaterialConfig setSelfIlluminationMap(final String fileLocation) {
        material.setDiffuseMap(
                new Image(
                        new File(
                                fileLocation
                        ).toURI().toString()
                )
        );
        return this;
    }

    /**
     * @see PhongMaterial#getSelfIlluminationMap()
     * @return The location of the map.
     */
    @JsonGetter
    public final String getSelfIlluminationMap() {
        return Objects.nonNull(material.getSelfIlluminationMap())
                ? resetPath(material.getSelfIlluminationMap().getUrl())
                : null;
    }

    /**
     * @see PhongMaterial#setSpecularColor(javafx.scene.paint.Color)
     * @see ColorConfig
     * @param color The color config.
     * @return this.
     */
    @JsonSetter
    public final MaterialConfig setSpecularColor(final ExtendedColorConfig color) {
        if (Objects.nonNull(color)) {
            material.setSpecularColor(Color.hsb(color.getHue(), color.getSaturation(), color.getBrightness(), color.getOpacity()));
        }
        return this;
    }

    /**
     * @see PhongMaterial#getSpecularColor()
     * @see ColorConfig
     * @return The color configuration.
     */
    @JsonGetter
    public final ExtendedColorConfig getSpecularColor() {
        if (!Objects.isNull(material.getSpecularColor())) {
            final ExtendedColorConfig color = new ExtendedColorConfig();
            color.setHue(material.getSpecularColor().getHue());
            color.setSaturation(material.getSpecularColor().getSaturation());
            color.setBrightness(material.getSpecularColor().getBrightness());
            color.setOpacity(material.getSpecularColor().getOpacity());
            return color;
        }
        return null;
    }

    /**
     * @see PhongMaterial#setSpecularMap(javafx.scene.image.Image)
     * @param fileLocation The location of the bump map.
     * @return this.
     */
    @JsonSetter
    public final MaterialConfig setSpecularMap(final String fileLocation) {
        material.setSpecularMap(
                new Image(
                        new File(
                                fileLocation
                        ).toURI().toString()
                )
        );
        return this;
    }

    /**
     * @see PhongMaterial#getSpecularMap()
     * @return The location of the map.
     */
    @JsonGetter
    public final String getSpecularMap() {
        return Objects.nonNull(material.getSpecularMap())
                ? resetPath(material.getSpecularMap().getUrl())
                : null;
    }

    /**
     * @see PhongMaterial#setSpecularPower(double)
     * @param power value.
     * @return this.
     */
    @JsonSetter
    public final MaterialConfig setSpecularPower(final double power) {
        this.material.setSpecularPower(power);
        return this;
    }

    /**
     * @see PhongMaterial#getSpecularPower()
     * @return Power value.
     */
    @JsonGetter
    public final double getSpecularPower() {
        return this.material.getSpecularPower();
    }

    /**
     * Set the PhongMaterial.
     *
     * Sets the phong material used as base for the configuration.
     *
     * @param material The PhongMaterial to set.
     * @return this.
     */
    public final MaterialConfig setPhongMaterial(final PhongMaterial material) {
        this.material = material;
        return this;
    }

    /**
     * Returns the PhongMaterial.
     *
     * The PhongMaterial is used for the configuration.
     *
     * @return The PhongMaterial.
     */
    @JsonIgnore
    public final PhongMaterial getPhongMaterial() {
        return this.material;
    }

    /**
     * Resets path to relative path
     *
     * @param fullPath The full path.
     * @return The relative path.
     */
    private String resetPath(final String fullPath) {
        return fullPath.replace("file:/" + System.getProperty("user.dir").replace("\\", "/") + "/", "");
    }

}
