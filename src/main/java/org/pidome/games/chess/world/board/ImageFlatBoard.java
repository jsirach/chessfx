/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point3D;
import org.pidome.games.chess.resources.ResourceLoadResult;
import org.pidome.games.chess.resources.ResourcesLoader;

/**
 * A default flat chess board.
 *
 * @author John
 */
//@GameBoard(
//        type = GameBoard.GameboardType.BOARD_IMAGE,
//        name = "Black white image board"
//)
public class ImageFlatBoard extends Abstract2DBoard {

    /**
     * The most bottom left centered playfield.
     */
    private final ObjectProperty<Point3D> firstSquare = new SimpleObjectProperty<>() {
        {
            setValue(new Point3D(148, 1642.0, 1.0));
        }
    };

    /**
     * The size of a single field on the board.
     */
    private final ObjectProperty<Point3D> fieldSize = new SimpleObjectProperty<>() {
        {
            setValue(new Point3D(211.5, 210.0, 0.0));
        }
    };

    /**
     * Constructor.
     */
    public ImageFlatBoard() {
        super("/org/pidome/games/chess/assets/flat/boards/generic/Chess-board-with-letters.png");
    }

    /**
     * @inheritDoc
     */
    @Override
    public void scheduleAssetLoading(final ResourcesLoader resourceLoader) {
        resourceLoader.schedule(() -> {
            ResourceLoadResult loadResult = new ResourceLoadResult();
            try {
                this.loadBoard();
                loadResult.complete();
            } catch (Exception ex) {
                loadResult.complete(ex);
            }
            return loadResult;
        });
    }

    /**
     * The most bottom left centered playfield <code>A1</code>.
     *
     * @return Point with 3D coordinates.
     */
    @Override
    public final ObjectProperty<Point3D> getFirstSquareCoordinate() {
        return firstSquare;
    }

    /**
     * Return the field size.
     *
     * @return The field size.
     */
    @Override
    public final ObjectProperty<Point3D> getFieldSize() {
        return this.fieldSize;
    }

}
