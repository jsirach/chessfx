/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.hud;

import com.github.bhlangonijr.chesslib.Side;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.utils.controls.AvatarImage;
import org.pidome.games.chess.utils.controls.StyledText;

/**
 * Hud view for the flat game.
 *
 * @author John Sirach
 */
public class SinglePersonHudView extends AbstractHudView {

    /**
     * The current player side.
     */
    private final Side side;

    /**
     * Name of the person to be displayed.
     */
    private final Text personName = new StyledText();

    /**
     * Constructor.
     *
     * @param options The options of the game being played.
     * @param side The side to display for this single person view.
     */
    public SinglePersonHudView(final AbstractGenericGameOptions options, final Side side) {
        super(options);
        this.side = side;
        constructContent();
    }

    /**
     * Creates the hud.
     */
    private void constructContent() {
        createPersonDetails();
    }

    /**
     * creates the pane in which the player details are being shown.
     */
    private void createPersonDetails() {
        ChessGamePlayerModel player;

        if (this.getGameOptions().currentGameProperty().get()
                .getPlayerWhite().sideProperty().get().equals(this.side)) {
            player = this.getGameOptions().currentGameProperty().get()
                    .getPlayerWhite();
        } else {
            player = this.getGameOptions().currentGameProperty().get()
                    .getPlayerBlack();
        }

        final GridPane personPane = new GridPane();

        personPane.setVgap(10);
        personPane.setHgap(10);

        final AvatarImage avatarImage = new AvatarImage();
        avatarImage.setPlayerAvatar(player);
        avatarImage.prefHeightProperty().bind(this.prefHeightProperty());
        personPane.add(avatarImage, 0, 0, 1, 3);

        personName.getStyleClass().add("text");
        this.heightProperty().addListener((observable, oldValue, newValue) -> {
            personName.setFont(new Font(personName.getFont().getFamily(), newValue.doubleValue() / 3));
        });
        personName.textProperty().bind(player.nameProperty());

        personPane.add(personName, 1, 0);
        this.setCenter(personPane);

    }

    /**
     * Dispose any bound properties.
     */
    @Override
    public final void dispose() {
        personName.textProperty().unbind();
    }

}
