/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.world.pieces.PieceInterface;

/**
 * Configuration for piece model.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PieceModelConfig extends ModelConfig {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PieceModelConfig.class);

    /**
     * Piece variant.
     */
    private PieceInterface.Variant variant;

    /**
     * Piece color.
     */
    private PieceInterface.VariantColor color;

    /**
     * Constructor.
     */
    public PieceModelConfig() {
    }

    /**
     * @return the variant
     */
    public PieceInterface.Variant getVariant() {
        return variant;
    }

    /**
     * @param variant the variant to set
     */
    public void setVariant(PieceInterface.Variant variant) {
        this.variant = variant;
    }

    /**
     * @return the color
     */
    public PieceInterface.VariantColor getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(PieceInterface.VariantColor color) {
        this.color = color;
    }

}
