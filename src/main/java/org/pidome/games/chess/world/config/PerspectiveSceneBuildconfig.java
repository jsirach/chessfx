/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Configuration to build a scene.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PerspectiveSceneBuildconfig {

    /**
     * Ambient lighting configuration.
     */
    private ColorConfig ambientLighting;

    /**
     * List of lights used in the scene.
     *
     * I would now suggest only to use just one source and an ambient light set.
     */
    private List<PerspectiveSceneLightSourceConfig> lights = new ArrayList<>();

    /**
     * The observable list with the lights.
     */
    @JsonIgnore
    private ObservableList<PerspectiveSceneLightSourceConfig> observableLights = FXCollections.observableArrayList(lights);

    /**
     * Collection of static models to place.
     */
    private List<PerspectiveSceneStaticModel> staticModels;

    /**
     * Set the ambient lighting configuration.
     *
     * @param ambientColorConfig The ambient lighting configuration.
     */
    public void setAmbientLighting(final ColorConfig ambientColorConfig) {
        this.ambientLighting = ambientColorConfig;
    }

    /**
     * Returns the ambient lighting.
     *
     * @return Color configuration for ambient lighting.
     */
    public ColorConfig getAmbientLighting() {
        return this.ambientLighting;
    }

    /**
     * @return the lights
     */
    public List<PerspectiveSceneLightSourceConfig> getLights() {
        return this.observableLights.subList(0, this.observableLights.size());
    }

    /**
     * @param lights the lights to set
     */
    public void setLights(List<PerspectiveSceneLightSourceConfig> lights) {
        this.observableLights.addAll(lights);
    }

    /**
     * Observable list with lights.
     *
     * @return The lights list.
     */
    @JsonIgnore
    public ObservableList<PerspectiveSceneLightSourceConfig> observableLights() {
        return this.observableLights;
    }

    /**
     * Sets a list of static models.
     *
     * @param staticModels The static models to set.
     */
    public void setStaticModels(List<PerspectiveSceneStaticModel> staticModels) {
        this.staticModels = staticModels;
    }

    /**
     * Returns the list of configured static models.
     *
     * @return The static models.
     */
    public List<PerspectiveSceneStaticModel> getStaticModels() {
        return this.staticModels;
    }

}
