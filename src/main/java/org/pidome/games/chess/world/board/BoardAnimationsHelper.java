/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

import com.github.bhlangonijr.chesslib.Side;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.util.Duration;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.world.pieces.VisualPiece;

/**
 * Animations helperfor pieces on the board.
 *
 * @author John
 */
public class BoardAnimationsHelper {

    /**
     * The default angle a piece needs to travel from A1 to H8
     */
    private static final double DEFAULT_MOVE_ANGLE = 30;

    /**
     * Animating a piece move.
     *
     * @param square The square to move to.
     * @param piece The piece to move.
     * @param travelDistancePercentage The distance a piece needs to travel in
     * percentage.
     * @param replacementPiece When not null, the piece will be replace with the
     * given replacement piece.
     */
    protected static void perspectiveMotion(final BoardSquare square, final VisualPiece piece, final double travelDistancePercentage, final VisualPiece replacementPiece) {
        final Translate yMove = new Translate();
        final Rotate rotate = new Rotate();
        rotate.setPivotZ(piece.getLayoutBounds().getDepth());
        rotate.setAxis(Rotate.Z_AXIS);

        piece.getTransforms().addAll(yMove, rotate);

        final double rotationAngle = calculateRotationAngle(30);

        final Timeline liftTimeline = new Timeline(
                new KeyFrame(
                        Duration.seconds(calculateDistanceSpeed(1, travelDistancePercentage)),
                        new KeyValue(yMove.yProperty(), -(calculateLiftHeight(piece.getLayoutBounds().getHeight() - square.getTranslateY(), travelDistancePercentage)), Interpolator.EASE_OUT),
                        new KeyValue(rotate.angleProperty(), piece.getRulesEnginePiece().getPieceSide().equals(Side.WHITE) ? rotationAngle : -rotationAngle, Interpolator.EASE_OUT)
                )
        );
        liftTimeline.setCycleCount(1);

        final Timeline dropTimeline = new Timeline(
                new KeyFrame(
                        Duration.seconds(calculateDistanceSpeed(1, travelDistancePercentage)),
                        new KeyValue(yMove.yProperty(), 0, Interpolator.EASE_IN),
                        new KeyValue(rotate.angleProperty(), 0, Interpolator.EASE_IN),
                        new KeyValue(piece.translateYProperty(), square.getTranslateY(), Interpolator.EASE_IN)
                )
        );
        dropTimeline.setCycleCount(1);

        final Timeline moveTimeline = new Timeline(new KeyFrame(
                Duration.seconds(calculateDistanceSpeed(1.6, travelDistancePercentage)),
                new KeyValue(piece.translateXProperty(), square.getTranslateX(), Interpolator.EASE_BOTH),
                new KeyValue(piece.translateZProperty(), square.getTranslateZ(), Interpolator.EASE_BOTH)
        ));

        final SequentialTransition sequentialLiftDropTransition = new SequentialTransition();
        sequentialLiftDropTransition.getChildren().addAll(liftTimeline, dropTimeline);

        final ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(sequentialLiftDropTransition, moveTimeline);

        parallelTransition.setOnFinished(finishEvent -> {
            boolean transitionFinished = (Math.round(piece.translateXProperty().get()) == Math.round(square.getTranslateX()))
                    && (Math.round(piece.translateYProperty().get()) == Math.round(square.getTranslateY()));
            // Correct placing if needed.
            if (!transitionFinished) {
                FXUtils.runOnFXThread(() -> {
                    noMotion(square, piece, null);
                });
            }
            piece.getTransforms().removeAll(rotate, yMove);
            if (replacementPiece != null) {
                replacementAnimation(piece, replacementPiece);
            }
        });

        parallelTransition.play();
    }

    /**
     * Motion to be used on the 2D board.
     *
     * @param square The square to move to.
     * @param piece The piece to move to the given square.
     * @param travelDistancePercentage The distance a piece needs to travel in
     * percentage.
     * @param replacementPiece When not null, the piece will be replace with the
     * given replacement piece.
     */
    protected static void flatMotion(final BoardSquare square, final VisualPiece piece, final double travelDistancePercentage, final VisualPiece replacementPiece) {
        final double xPos = square.getTranslateX();
        final double yPos = square.getTranslateY();
        final double zPos = square.getTranslateZ();
        final Timeline moveTimeline = new Timeline(new KeyFrame(
                Duration.seconds(calculateDistanceSpeed(1.6, travelDistancePercentage)),
                new KeyValue(piece.translateXProperty(), xPos, Interpolator.EASE_BOTH),
                new KeyValue(piece.translateYProperty(), yPos, Interpolator.EASE_BOTH),
                new KeyValue(piece.translateZProperty(), zPos, Interpolator.EASE_BOTH)
        ));
        if (replacementPiece != null) {
            moveTimeline.setOnFinished((event) -> {
                event.consume();
                replacementAnimation(piece, replacementPiece);
            });
        }
        moveTimeline.setAutoReverse(false);
        moveTimeline.setCycleCount(1);
        moveTimeline.setOnFinished((event) -> noMotion(square, piece, replacementPiece));
        moveTimeline.play();
    }

    /**
     * Runs the animation for the replacement.
     *
     * @param original The original piece.
     * @param replacement The replacement piece.
     */
    private static void replacementAnimation(final VisualPiece original, final VisualPiece replacement) {
        if (replacement != null) {
            replacement.setOpacity(0.0);
            if (original.getParent() instanceof Group) {
                ((Group) original.getParent()).getChildren().add(replacement);
            } else {
                ((StackPane) original.getParent()).getChildren().add(replacement);
            }
            replacement.setTranslateX(original.getTranslateX());
            replacement.setTranslateZ(original.getTranslateZ());
            final Timeline replaceTimeline;
            if (original.isPerspective()) {
                replacement.setTranslateY(replacement.getBoundsInParent().getHeight());
                replacement.setOpacity(1.0);
                replaceTimeline = new Timeline(new KeyFrame(
                        Duration.seconds(1.0),
                        new KeyValue(original.translateYProperty(), original.getBoundsInParent().getHeight(), Interpolator.LINEAR),
                        new KeyValue(replacement.translateYProperty(), original.getTranslateY(), Interpolator.LINEAR)
                ));
            } else {
                replacement.setTranslateY(original.getTranslateY());
                replaceTimeline = new Timeline(new KeyFrame(
                        Duration.seconds(1.0),
                        new KeyValue(original.opacityProperty(), 0.0, Interpolator.LINEAR),
                        new KeyValue(replacement.opacityProperty(), 1.0, Interpolator.LINEAR)
                ));
            }
            replaceTimeline.setOnFinished(event -> {
                event.consume();
                if (original.getParent() instanceof Group) {
                    ((Group) original.getParent()).getChildren().remove(original);
                } else {
                    ((StackPane) original.getParent()).getChildren().remove(original);
                }
            });
            replaceTimeline.play();
        }
    }

    /**
     * When there is no motion, immediate place.
     *
     * @param square The square to move to.
     * @param piece The piece to move to the given square.
     * @param replacementPiece When not null, the piece will be replace with the
     * given replacement piece.
     */
    protected static void noMotion(final BoardSquare square, final VisualPiece piece, final VisualPiece replacementPiece) {
        if (replacementPiece != null) {
            replacementPiece.setTranslateX(square.getTranslateX());
            replacementPiece.setTranslateY(square.getTranslateY());
            replacementPiece.setTranslateZ(square.getTranslateZ());
        } else {
            piece.setTranslateX(square.getTranslateX());
            piece.setTranslateY(square.getTranslateY());
            piece.setTranslateZ(square.getTranslateZ());
        }
    }

    /**
     * The distance required.
     *
     * @param distance The distance to travel in percentage.
     */
    private static double calculateDistanceSpeed(final double defaultTime, final double distancePercentage) {
        return defaultTime * (distancePercentage / 100.0);
    }

    /**
     * The distance required.
     *
     * @param distance The distance to travel in percentage.
     */
    private static double calculateLiftHeight(final double liftHeight, final double distancePercentage) {
        return liftHeight * (distancePercentage / 100.0);
    }

    /**
     * The distance required.
     *
     * @param distance The distance to travel in percentage.
     */
    private static double calculateRotationAngle(final double distancePercentage) {
        return DEFAULT_MOVE_ANGLE * (distancePercentage / 100.0);
    }

}
