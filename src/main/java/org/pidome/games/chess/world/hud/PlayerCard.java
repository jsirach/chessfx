/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.hud;

import com.github.bhlangonijr.chesslib.Side;
import javafx.beans.Observable;
import javafx.css.PseudoClass;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;
import org.pidome.games.chess.events.GameEvent;
import org.pidome.games.chess.events.GameEventListener;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.events.SquareIntentEvent;
import org.pidome.games.chess.events.SquareSelectEvent;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.utils.controls.AvatarImage;
import org.pidome.games.chess.utils.controls.StyledText;

/**
 * A simple player card to show in game.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PlayerCard extends StackPane implements GameEventListener {

    /**
     * Pseudo class for when it's current turn.
     */
    private static final PseudoClass TURN_PSEUDO_CLASS = PseudoClass.getPseudoClass("turn");

    /**
     * The current player side.
     */
    private final Side side;

    /**
     * the game's options.
     */
    private final AbstractGenericGameOptions options;

    /**
     * Name of the person to be displayed.
     */
    private final Text personName = new StyledText();

    /**
     * Indicator who's turn it is.
     */
    private final Circle currentTurnIndicator = new Circle(7);

    /**
     * The avatar image.
     */
    private final AvatarImage avatarImage = new AvatarImage();

    /**
     * Constructor.
     *
     * @param options The game options.
     * @param side The player side.
     */
    public PlayerCard(final AbstractGenericGameOptions options, final Side side) {
        setMaxSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        setPrefHeight(175);
        this.getStyleClass().add("player-card");
        this.side = side;
        this.options = options;
        setInfo(null, null, this.options.currentGameProperty().getValue());
        buildCard();
        bind(options.getGameId());
    }

    /**
     * Set player info.
     *
     * @param playerModel The player model.
     */
    private void setInfo(final Observable observable, final ChessGameModel oldValue, final ChessGameModel newValue) {
        final ChessGamePlayerModel player = Side.WHITE.equals(side)
                ? newValue.getPlayerWhite()
                : newValue.getPlayerBlack();
        personName.textProperty().setValue(player.nameProperty().getValueSafe());
        avatarImage.setPlayerAvatar(player);
    }

    /**
     * creates the pane in which the player details are being shown.
     */
    private void buildCard() {
        BorderPane infoPane = new BorderPane();
        HBox nameContainer = new HBox();
        nameContainer.getStyleClass().add("info");

        currentTurnIndicator.getStyleClass().addAll("turn-indicator",
                this.side.equals(Side.WHITE) ? "white" : "black");
        HBox.setMargin(currentTurnIndicator, new Insets(5));

        personName.getStyleClass().addAll("text");
        HBox.setMargin(personName, new Insets(3, 3, 3, 0));

        nameContainer.getChildren().addAll(currentTurnIndicator, personName);
        infoPane.setTop(nameContainer);

        avatarImage.prefHeightProperty().bind(this.prefHeightProperty());

        this.getChildren().addAll(avatarImage, infoPane);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onGameStateChangeEvent(GameStateEvent gameState) {
        switch (gameState.getSource()) {
            case READY:
                isCurrentTurn(Side.WHITE);
                break;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPieceMoveIntentEvent(MoveEvent moveEvent) {
        //Nope.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPieceMovedEvent(MoveEvent moveEvent) {
        if (moveEvent.getEventType().equals(GameEvent.GameEventType.MOVE)) {
            Side lastMoveSide = moveEvent.getPiece().getPieceSide();
            isCurrentTurn(lastMoveSide.equals(Side.WHITE) ? Side.BLACK : Side.WHITE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSquareSelectedEvent(SquareSelectEvent selectEvent) {
        //Nope
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSquareIntentEvent(SquareIntentEvent squareIntentEvent) {
        //Nope
    }

    /**
     * {@inheritDoc}
     */
    private void isCurrentTurn(final Side side) {
        this.pseudoClassStateChanged(TURN_PSEUDO_CLASS,
                side.equals(this.side));
    }

    /**
     * Unbind a game.
     *
     * @param gameId the game id to unbind.
     */
    public final void unbind(final String gameId) {
        this.options.currentGameProperty().removeListener(this::setInfo);
        GameEventsPublisher.unRegisterForEvents(gameId, this);
    }

    /**
     * Bind to a game.
     *
     * @param gameId The game id to bind to.
     */
    public final void bind(final String gameId) {
        this.options.currentGameProperty().addListener(this::setInfo);
        GameEventsPublisher.registerForEvents(gameId, this);
    }

}
