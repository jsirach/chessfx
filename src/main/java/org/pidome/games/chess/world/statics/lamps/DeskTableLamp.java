/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.statics.lamps;

import javafx.scene.transform.Rotate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.world.config.StaticWorldModel;

/**
 * An old on desk gold color table lamp
 *
 * @author johns
 */
public class DeskTableLamp extends StaticWorldModel {

    /**
     * Class logger.
     */
    private final Logger LOG = LogManager.getLogger(DeskTableLamp.class);

    /**
     * Constructor.
     */
    public DeskTableLamp() {
        super("/resources/assets/perspective/environment/lamps/desk/old/TableLamp.obj");
    }

    /**
     * Loads the table.
     */
    @Override
    public final void loadAssetOldSchool() {
        try {
            super.loadAssetOldSchool();
            final Rotate rotate = new Rotate();
            rotate.setPivotX(0);
            rotate.setPivotX(0);
            rotate.setPivotZ(0);
            rotate.setAxis(Rotate.Y_AXIS);
            rotate.setAngle(-45);
            this.getTransforms().add(rotate);
            this.setTranslateY(-430);
            this.setTranslateX(-1100);
            this.setTranslateZ(900);
        } catch (Exception ex) {
            LOG.error("Unable to load lamp", ex);
        }
    }

}
