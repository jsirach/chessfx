/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.model.options.boards.BoardOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.world.board.AbstractBoard;
import org.pidome.games.chess.world.pieces.AbstractPieces;

/**
 * Base for the board views.
 *
 * @author John Sirach
 */
public abstract class AbstractSceneView extends StackPane {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(AbstractSceneView.class);

    /**
     * Scene primary parent.
     */
    private AbstractBoard visualBoard;

    /**
     * The options for the board.
     */
    private final BoardOptions boardOptions;

    /**
     * The game options.
     */
    private final AbstractGenericGameOptions gameOptions;

    /**
     * the pieces on the board.
     */
    private final AbstractPieces pieces;

    /**
     * Abstract constructor.
     *
     * @param gameHash The hash of the game.
     * @param gameOptions The game's options.
     * @param boardOptions This specific boards options.
     * @throws java.lang.Exception thrown when initializations of components
     * fails.
     */
    public AbstractSceneView(final String gameHash, final AbstractGenericGameOptions gameOptions, BoardOptions boardOptions) throws Exception {
        this.boardOptions = boardOptions;
        this.gameOptions = gameOptions;
        this.pieces = GameResourcesHelper.createPiecesInstance(boardOptions.gamePiecesProperty().get());
        createBoard();
    }

    /**
     * Returns the pieces loading object.
     *
     * @return The pieces load object.
     */
    public final AbstractPieces getPieces() {
        return this.pieces;
    }

    /**
     * Return the game options.
     *
     * @return The game options.
     */
    public final AbstractGenericGameOptions getGameOptions() {
        return this.gameOptions;
    }

    /**
     * Instructs a board to put's it's pieces in rest position.
     *
     * @param animated if the piece rest placement should be animated or not.
     */
    public final void placePiecesInRest(final boolean animated) {
        this.visualBoard.placePiecesInRest(animated);
        this.visualBoard.cleanAdditionalFeatures();
    }

    /**
     * Puts pieces in their start position on the board.
     */
    public final void placePiecesInstart() {
        this.visualBoard.placePiecesInStartPosition();
    }

    /**
     * Creates the board.
     */
    private void createBoard() {
        try {
            this.visualBoard = GameResourcesHelper.createBoardInstance(this.boardOptions.gameBoardProperty().getValue());
            this.visualBoard.setOptions(this.gameOptions, boardOptions);
        } catch (Exception ex) {
            LOG.error("Error creating new game board", ex);
        }
    }

    /**
     * Returns the group.
     *
     * This is the parent being used in the scene.
     *
     * @return The scene root.
     */
    public final AbstractBoard getGameBoard() {
        return this.visualBoard;
    }

    /**
     * Loads the assets for the view to be displayed..
     *
     * @param resourcesLoader The resources loader
     */
    public abstract void scheduleAssetLoading(final ResourcesLoader resourcesLoader);

    /**
     * Composes the board view.
     */
    public abstract void compose();

    /**
     * Destroys a board.
     */
    public abstract void destroy();

}
