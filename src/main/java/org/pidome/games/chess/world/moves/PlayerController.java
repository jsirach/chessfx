/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.moves;

import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import java.util.Objects;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import org.pidome.games.chess.model.game.ChessGameModel;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.model.options.PlayerGameOptions;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;

/**
 * The controller for the game player.
 *
 * Initial release, play, pause, next, previous game.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PlayerController extends HBox {

    /**
     * The game options.
     */
    private final PlayerGameOptions genericGameOptions;

    /**
     * The current game played.
     */
    private final Text gameName = new Text();

    /**
     * The game options.
     *
     * @param genericGameOptions
     */
    public PlayerController(final PlayerGameOptions genericGameOptions) {
        this.getStyleClass().add("game-player");
        this.genericGameOptions = genericGameOptions;
        build();
        this.genericGameOptions.currentGameProperty().addListener(this::gameChanger);
    }

    private void build() {
        HBox.setHgrow(gameName, Priority.ALWAYS);
        this.setAlignment(Pos.CENTER_LEFT);
        HBox.setMargin(gameName, new Insets(0, 0, 0, 10));
        this.getChildren().addAll(previousButton(), pauseButton(), playButton(), nextButton(),
                gameName);
    }

    /**
     * Updates display when game changes.
     *
     * @param obs The observable.
     * @param oldVal The old value.
     * @param newVal The new value.
     */
    private void gameChanger(final ObservableValue obs, final ChessGameModel oldVal, final ChessGameModel newVal) {
        if (!Objects.isNull(newVal)) {
            gameName.setText(
                    new StringBuilder(newVal.getGame().getRound().getEvent().getName())
                            .append(": ")
                            .append(newVal.getGame().getRound().getNumber())
                            .toString()
            );
        }
    }

    /**
     * Create a play button.
     *
     * @return The play button.
     */
    private Button playButton() {
        return createButton(MaterialIcon.PLAY_ARROW,
                I18nApplicationKey.GAME_PLAYER_ACTION_PLAY,
                GameStateEvent.GameState.GAME_PLAYER_PLAY);
    }

    /**
     * Create a play button.
     *
     * @return The play button.
     */
    private Button pauseButton() {
        return createButton(MaterialIcon.PAUSE,
                I18nApplicationKey.GAME_PLAYER_ACTION_PAUSE,
                GameStateEvent.GameState.GAME_PLAYER_PAUSE);
    }

    /**
     * Create a play button.
     *
     * @return The play button.
     */
    private Button nextButton() {
        return createButton(MaterialIcon.SKIP_NEXT,
                I18nApplicationKey.GAME_PLAYER_ACTION_PLAY_NEXT,
                GameStateEvent.GameState.GAME_PLAYER_NEXT);
    }

    /**
     * Create a play button.
     *
     * @return The play button.
     */
    private Button previousButton() {
        return createButton(MaterialIcon.SKIP_PREVIOUS,
                I18nApplicationKey.GAME_PLAYER_ACTION_PLAY_PREVIOUS,
                GameStateEvent.GameState.GAME_PLAYER_PREVIOUS);
    }

    /**
     * Creates the buttons basics.
     *
     * @param icon The icon to show.
     * @param textKey The text to show.
     */
    private Button createButton(final MaterialIcon icon, final I18nApplicationKey textKey, final GameStateEvent.GameState event) {
        MaterialIconView view = new MaterialIconView(icon, "1.3em");
        Button button = new Button();
        button.setOnAction((actionEvent) -> {
            GameEventsPublisher.publishGameState(new GameStateEvent(genericGameOptions.getGameId(), event), 0);
        });
        button.textProperty().bind(I18nProxy.getStringProperty(textKey));
        button.setGraphic(view);
        button.getStyleClass().add("action-button");
        return button;
    }

}
