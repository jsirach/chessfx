/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.tablet;

import com.github.bhlangonijr.chesslib.Side;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.world.hud.SinglePersonHudView;
import org.pidome.games.chess.world.moves.MovesView;
import org.pidome.games.chess.world.views.FlatSceneView;

/**
 * View to show a tablet.
 *
 * @author John
 */
public class TabletView extends Group {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(TabletView.class);

    /**
     * Moves view.
     */
    private final MovesView movesView;

    /**
     * Board view.
     */
    private final FlatSceneView board;

    /**
     * Tablet width.
     */
    private final DoubleProperty width = new SimpleDoubleProperty(0.0);

    /**
     * Tablet height.
     */
    private final DoubleProperty height = new SimpleDoubleProperty(0.0);

    /**
     * The x rotate.
     */
    private final Rotate rotateX = new Rotate();

    /**
     * The y rotate.
     */
    private final Rotate rotateY = new Rotate();

    /**
     * The z rotate.
     */
    private final Rotate rotateZ = new Rotate();

    /**
     * Device start X angle.
     */
    final double startXAngle = 70;
    /**
     * Device start Y angle.
     */
    final double startYAngle = 19;
    /**
     * Device start Z angle.
     */
    final double startZAngle = -42;

    /**
     * Device start X position in group.
     */
    final double startXTrans = 1825;
    /**
     * Device start Y position in group.
     */
    final double startYTrans = -325;
    /**
     * Device start Z position in group.
     */
    final double startZTrans = 500;

    /**
     * The tablet shown.
     */
    private final GenericTablet tablet = new GenericTablet();

    /**
     * The game's options.
     */
    private final AbstractGenericGameOptions genericGameOptions;

    /**
     * Constructor.
     *
     * @param gameHash The hash of the game.
     * @param genericGameOptions The game options.
     * @param board The 2D board.
     */
    public TabletView(final String gameHash, final AbstractGenericGameOptions genericGameOptions, final FlatSceneView board) {
        movesView = new MovesView(genericGameOptions);
        movesView.build();
        this.board = board;
        this.genericGameOptions = genericGameOptions;
    }

    /**
     * Loads the assets for the tablet.
     */
    public void scheduleAssetLoading() {
        tablet.loadAsset(() -> {
        });
    }

    /**
     * Build the tablet view.
     */
    public void build() {

        this.getChildren().add(tablet);
        width.setValue((tablet.getLayoutBounds().getWidth() * tablet.getModelConfig().getScaleConfig().getScale().getX()) - (40 * tablet.getModelConfig().getScaleConfig().getScale().getX()));
        height.setValue((tablet.getLayoutBounds().getDepth() * tablet.getModelConfig().getScaleConfig().getScale().getZ()) - (40 * tablet.getModelConfig().getScaleConfig().getScale().getZ()));
        this.getChildren().add(createTabletContent());

        getRotateX().setPivotX(0);
        getRotateX().setPivotY(0);
        getRotateX().setPivotZ(0);
        getRotateX().setAxis(Rotate.X_AXIS);

        getRotateY().setPivotX(0);
        getRotateY().setPivotY(0);
        getRotateY().setPivotZ(0);
        getRotateY().setAxis(Rotate.Y_AXIS);

        getRotateZ().setPivotX(0);
        getRotateZ().setPivotY(0);
        getRotateZ().setPivotZ(0);
        getRotateZ().setAxis(Rotate.Z_AXIS);

        this.getTransforms().addAll(getRotateX(), getRotateY(), getRotateZ());
        goStartPosition(false);

    }

    /**
     * Creates the content for the tablet.
     */
    private StackPane createTabletContent() {
        final ColumnConstraints contentColumn = new ColumnConstraints();
        contentColumn.setPercentWidth(80);

        final ColumnConstraints infoColumn = new ColumnConstraints();
        infoColumn.setPercentWidth(20);

        final RowConstraints topRow = new RowConstraints();
        topRow.setPercentHeight(12.5);

        final RowConstraints centerRow = new RowConstraints();
        centerRow.setPercentHeight(75);

        final RowConstraints bottomRow = new RowConstraints();
        bottomRow.setPercentHeight(12.5);

        final StackPane contentPlane = new StackPane();

        contentPlane.prefWidthProperty().bind(width);
        contentPlane.prefHeightProperty().bind(height);
        contentPlane.maxWidthProperty().bind(contentPlane.prefWidthProperty());
        contentPlane.maxHeightProperty().bind(contentPlane.prefHeightProperty());
        contentPlane.translateXProperty().bind(width.divide(2).negate());
        contentPlane.setRotationAxis(Rotate.X_AXIS);
        contentPlane.setRotate(-90);
        contentPlane.setTranslateY(-349);
        contentPlane.getStyleClass().add("tablet-display-content-pane");

        final GridPane contentGrid = new GridPane();

        contentGrid.getColumnConstraints().addAll(contentColumn, infoColumn);
        contentGrid.getRowConstraints().addAll(topRow, centerRow, bottomRow);

        this.movesView.setMaxWidth(Double.MAX_VALUE);
        GridPane.setFillWidth(this.movesView, true);

        GridPane.setHalignment(this.board, HPos.CENTER);
        GridPane.setValignment(this.board, VPos.CENTER);

        final SinglePersonHudView sideWhite = new SinglePersonHudView(this.genericGameOptions, Side.WHITE);
        GridPane.setFillWidth(sideWhite, true);
        final SinglePersonHudView sideBlack = new SinglePersonHudView(this.genericGameOptions, Side.BLACK);
        GridPane.setFillWidth(sideBlack, true);

        contentGrid.add(sideBlack, 0, 0);
        contentGrid.add(this.board, 0, 1);
        contentGrid.add(sideWhite, 0, 2);
        contentGrid.add(this.movesView, 1, 0, 1, 3);

        contentPlane.getChildren().add(contentGrid);

        return contentPlane;

    }

    /**
     * Set's the otiginal position for the tablet.
     *
     * @param animated If moving to start position should be animated or not.
     */
    public void goStartPosition(final boolean animated) {
        if (animated) {
            animateMotion(startXTrans, startYTrans, startZTrans, startXAngle, startYAngle, startZAngle);
        } else {
            getRotateX().setAngle(startXAngle);
            getRotateY().setAngle(startYAngle);
            getRotateZ().setAngle(startZAngle);

            this.setTranslateX(startXTrans);
            this.setTranslateY(startYTrans);
            this.setTranslateZ(startZTrans);
        }
    }

    /**
     * display details by putting the tablet to a specific location.
     *
     * @param xTrans Translate x position
     * @param yTrans Translate y position
     * @param zTrans Translate z position
     * @param xAngle Rotate x position
     * @param yAngle Rotate y position
     * @param zAngle Rotate z position
     * @param animated if animated or not.
     */
    public final void details(final double xTrans, final double yTrans, final double zTrans, final double xAngle, final double yAngle, final double zAngle, final boolean animated) {
        if (animated) {
            animateMotion(xTrans, yTrans, zTrans, xAngle, yAngle, zAngle);
        } else {

            getRotateX().setAngle(xAngle);
            getRotateY().setAngle(yAngle);
            getRotateZ().setAngle(zAngle);

            this.translateXProperty().setValue(xTrans);
            this.translateYProperty().setValue(yTrans);
            this.translateZProperty().setValue(zTrans);
        }
    }

    /**
     * Animate item to the given position and rotation.
     *
     * @param xTrans Translate x position
     * @param yTrans Translate y position
     * @param zTrans Translate z position
     * @param xAngle Rotate x position
     * @param yAngle Rotate y position
     * @param zAngle Rotate z position
     */
    private void animateMotion(double xTrans, double yTrans, double zTrans, double xAngle, double yAngle, double zAngle) {

        final Timeline angleTimeLine = new Timeline(new KeyFrame(
                Duration.seconds(1.0),
                new KeyValue(getRotateX().angleProperty(), xAngle, Interpolator.EASE_BOTH),
                new KeyValue(getRotateY().angleProperty(), yAngle, Interpolator.EASE_BOTH),
                new KeyValue(getRotateZ().angleProperty(), zAngle, Interpolator.EASE_BOTH)
        ));

        final Timeline moveTimeline = new Timeline(new KeyFrame(
                Duration.seconds(.9),
                new KeyValue(this.translateXProperty(), xTrans, Interpolator.EASE_BOTH),
                new KeyValue(this.translateYProperty(), yTrans, Interpolator.EASE_BOTH),
                new KeyValue(this.translateZProperty(), zTrans, Interpolator.EASE_BOTH)
        ));

        final Timeline dropTimeline = new Timeline(new KeyFrame(
                Duration.seconds(1.0),
                new KeyValue(this.translateYProperty(), yTrans, Interpolator.EASE_BOTH)
        ));

        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(moveTimeline, angleTimeLine, dropTimeline);

        parallelTransition.setCycleCount(1);
        parallelTransition.setAutoReverse(false);
        parallelTransition.play();
    }

    /**
     * @return the rotateX
     */
    public Rotate getRotateX() {
        return rotateX;
    }

    /**
     * @return the rotateY
     */
    public Rotate getRotateY() {
        return rotateY;
    }

    /**
     * @return the rotateZ
     */
    public Rotate getRotateZ() {
        return rotateZ;
    }

}
