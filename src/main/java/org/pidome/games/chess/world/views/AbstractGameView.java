/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import org.pidome.games.chess.engines.HighlightType;
import org.pidome.games.chess.events.GameEvent;
import org.pidome.games.chess.events.GameEventsPublisher;
import org.pidome.games.chess.events.GameStateEvent;
import org.pidome.games.chess.events.MoveEvent;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.resources.ResourcesLoader;

/**
 * Base to be extended by scene views.
 *
 * A scene is a view which is being displayed on the screen of an end user. By
 * default a scene supports a maximum of two boards.
 *
 * What this class not does is construct the view, this is completely up to the
 * implementer. As this class supplies a grid pane you are completely free what
 * you put where.
 *
 * There are some rules to keep the view loading convenient for the end user as
 * everything is called a-synchronized and are inside completable futures:
 * <ul>
 * <li>Keep constructors as clean as possible and no FX thread actions.</li>
 * <li>Load your assets in the <code>loadViewAssets()</code> method without any
 * FX thread actions.</li>
 * <li>The method <code>buildView()</code> is done on the FX thread. Here you do
 * anything that's required to do on the FX thread.</li>
 * </ul>
 *
 * This class abstracts away application logic from scene views and builds the
 * boards available in a scene.
 *
 * @author John
 */
public abstract class AbstractGameView extends GridPane {

    /**
     * The game options.
     */
    private final AbstractGenericGameOptions genericGameOptions;

    /**
     * The perspective view.
     */
    private PerspectiveSceneView perspectiveBoardView;

    /**
     * The view for the flat board.
     */
    private FlatSceneView flatBoardView;

    /**
     * Constructor.
     *
     * @param genericGameOptions Game options.
     * @throws java.lang.Exception Thrown when instantiation fails.
     */
    public AbstractGameView(final AbstractGenericGameOptions genericGameOptions) throws Exception {
        this.getStyleClass().add("game-scene");
        this.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
        GameEventsPublisher.publishGameState(new GameStateEvent(genericGameOptions.getGameId(), GameStateEvent.GameState.LOADING), 0);
        this.genericGameOptions = genericGameOptions;
        switch (this.genericGameOptions.gameReadonlyViewTypeProperty().get()) {
            case PERSPECTIVE:
                createPerspectiveView();
                break;
            case FLAT:
                createFlatView();
                break;
            case BOTH:
                createPerspectiveView();
                createFlatView();
                break;
        }
    }

    /**
     * Instantiates the perspective view.
     *
     * @throws Exception Thrown when the perspective view is unable to be
     * instantiated.
     */
    private void createPerspectiveView() throws Exception {
        perspectiveBoardView = new PerspectiveSceneView(genericGameOptions.getGameId(), this.genericGameOptions);
    }

    /**
     * Instantiates the flat view.
     *
     * @throws Exception Thrown when the flat view is unable to be instantiated.
     */
    private void createFlatView() throws Exception {
        flatBoardView = new FlatSceneView(genericGameOptions.getGameId(), this.genericGameOptions);
    }

    /**
     * Ends views.
     */
    public final void destroy() {
        if (this.hasFlatView()) {
            this.getFlatBoardView().destroy();
        }
        if (this.hasPerspectiveView()) {
            this.getPerspectiveBoardView().destroy();
        }
    }

    /**
     * Returns the perspective board.
     *
     * @return The perspective visual game board.
     */
    protected final PerspectiveSceneView getPerspectiveBoardView() {
        return this.perspectiveBoardView;
    }

    /**
     * Returns the flat board.
     *
     * @return The flat visual game board.
     */
    protected final FlatSceneView getFlatBoardView() {
        return this.flatBoardView;
    }

    /**
     * If perspective view is active.
     *
     * @return true when active.
     */
    protected boolean hasPerspectiveView() {
        return perspectiveBoardView != null;
    }

    /**
     * If flat view is active.
     *
     * @return true when active.
     */
    protected boolean hasFlatView() {
        return flatBoardView != null;
    }

    /**
     * Loads the assets required to show the game.
     *
     * @param resourcesLoader Assets loading scheduler.
     */
    public final void scheduleAssetsLoading(final ResourcesLoader resourcesLoader) {
        if (hasPerspectiveView()) {
            perspectiveBoardView.scheduleAssetLoading(resourcesLoader);
        }
        if (hasFlatView()) {
            flatBoardView.scheduleAssetLoading(resourcesLoader);
        }
    }

    /**
     * Builds the view that is specific to the scene and will be displayed to
     * the end user.
     */
    public abstract void buildView();

    /**
     * Builds the view.
     */
    public final void compose() {
        if (this.hasPerspectiveView()) {
            perspectiveBoardView.compose();
        }
        if (this.hasFlatView()) {
            flatBoardView.compose();
        }
        buildView();
        GameEventsPublisher.publishGameState(new GameStateEvent(genericGameOptions.getGameId(), GameStateEvent.GameState.LOADED), 3);
    }

    /**
     * Resets all the boards in the view.
     */
    public final void resetBoard() {
        resetBoard(true);
    }

    /**
     * Reset's a board to it's rest view.
     *
     * This method removes all pieces from the board.
     *
     * @param animated Set to true to animate, false to put immediately
     */
    public final void resetBoard(final boolean animated) {
        if (this.hasPerspectiveView()) {
            perspectiveBoardView.placePiecesInRest(animated);
        }
        if (this.hasFlatView()) {
            flatBoardView.placePiecesInRest(animated);
        }
        GameEventsPublisher.publishGameState(new GameStateEvent(genericGameOptions.getGameId(), GameStateEvent.GameState.RESET), 3);
    }

    /**
     * Places the pieces on the board so the game can be started.
     */
    public final void prepareBoard() {
        if (this.hasPerspectiveView()) {
            perspectiveBoardView.placePiecesInstart();
        }
        if (this.hasFlatView()) {
            flatBoardView.placePiecesInstart();
        }
        GameEventsPublisher.publishGameState(new GameStateEvent(genericGameOptions.getGameId(), GameStateEvent.GameState.READY), 5);
    }

    /**
     * Selects a piece on the board.
     *
     * @param square The square to select.
     * @param select Select or deselect a piece.
     */
    public final void selectPiece(final Square square, final boolean select) {
        if (this.hasPerspectiveView()) {
            perspectiveBoardView.getGameBoard().selectPiece(square, select);
        }
        if (this.hasFlatView()) {
            flatBoardView.getGameBoard().selectPiece(square, select);
        }
    }

    /**
     * Highlights or removes an highlight from a square.
     *
     * @param square The square involved.
     * @param type The highlighting to apply use <code>HighlightType.NONE</code>
     * to disable any highlighting currently applied.
     */
    /**
     * Sets or removes an highlight on a square.
     *
     * @param square The square involved.
     * @param type The type of highlighting to apply.
     */
    public final void highlightSquare(final Square square, final HighlightType type) {
        if (this.hasPerspectiveView()) {
            perspectiveBoardView.getGameBoard().highlightSquare(square, type);
        }
        if (this.hasFlatView()) {
            flatBoardView.getGameBoard().highlightSquare(square, type);
        }
    }

    /**
     * Performs a move of a single piece on the board.
     *
     * Events by default are published so engines are able to interact on a move
     * done. There are cased where events should not be published, like for game
     * players. Example: If a castling event is being done, you do not want to
     * publish the move of one of the pieces, as a player will possibly then
     * react twice in what is considered a single move.
     *
     * if a move of a single piece should not be published, use
     * <code>performMove(Move move, Piece piece, boolean publishEvent)</code>
     *
     * @param move The move to perform.
     * @param piece The piece to move.
     */
    public final void performMove(final Move move, final Piece piece) {
        performMove(move, piece, true);
    }

    /**
     * Performs a move of a single piece on the board.
     *
     * @param move The move to perform.
     * @param piece The piece to move.
     * @param publishEvent If the move event should be published.
     */
    public final void performMove(final Move move, final Piece piece, final boolean publishEvent) {
        if (!move.getPromotion().equals(Piece.NONE)) {
            if (this.hasPerspectiveView()) {
                perspectiveBoardView.getGameBoard().performMoveReplace(move,
                        this.perspectiveBoardView.getPieces().loadPiece(
                                move.getPromotion().getPieceSide(),
                                move.getPromotion().getPieceType()
                        ));
            }
            if (this.hasFlatView()) {
                flatBoardView.getGameBoard().performMoveReplace(move,
                        this.flatBoardView.getPieces().loadPiece(
                                move.getPromotion().getPieceSide(),
                                move.getPromotion().getPieceType()
                        )
                );
            }
        } else {
            if (this.hasPerspectiveView()) {
                perspectiveBoardView.getGameBoard().performMove(move);
            }
            if (this.hasFlatView()) {
                flatBoardView.getGameBoard().performMove(move);
            }
        }
        if (publishEvent) {
            final MoveEvent moveEvent = new MoveEvent(genericGameOptions.getGameId(), GameEvent.GameEventType.MOVE, move, piece);
            GameEventsPublisher.publishMovedEvent(moveEvent);
        }
    }

    /**
     * Takes a piece from the board.
     *
     * @param square The square to take the piece from.
     */
    public final void takePiece(final Square square) {
        if (this.hasPerspectiveView()) {
            perspectiveBoardView.getGameBoard().takePiece(square);
        }
        if (this.hasFlatView()) {
            flatBoardView.getGameBoard().takePiece(square);
        }
    }

    /**
     * Returns the game options.
     *
     * @return The game options.
     */
    public final AbstractGenericGameOptions getGenericGameOptions() {
        return this.genericGameOptions;
    }

}
