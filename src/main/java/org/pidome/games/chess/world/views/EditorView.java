/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.RowConstraints;
import org.pidome.games.chess.gui.desktop.editor.EditorPane;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;

/**
 * View for the editor.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class EditorView extends AbstractGameView {

    /**
     * Primary view column.
     */
    private final ColumnConstraints perspectiveViewColumn = new ColumnConstraints() {
        {
            setPercentWidth(80);
        }
    };

    /**
     * Primary view column.
     */
    private final ColumnConstraints editorViewColumn = new ColumnConstraints() {
        {
            setPercentWidth(20);
        }
    };

    /**
     * Primary view row.
     */
    private final RowConstraints primaryViewRow = new RowConstraints() {
        {
            setPercentHeight(100);
        }
    };

    public EditorView(final AbstractGenericGameOptions genericGameOptions) throws Exception {
        super(genericGameOptions);
        this.getColumnConstraints().addAll(perspectiveViewColumn, editorViewColumn);
        this.getRowConstraints().add(primaryViewRow);
        this.getPerspectiveBoardView().prefWidthProperty().bind(
                this.widthProperty().multiply(perspectiveViewColumn.percentWidthProperty().divide(100))
        );
        this.getPerspectiveBoardView().prefHeightProperty().bind(this.heightProperty());
    }

    @Override
    public void buildView() {
        PerspectiveSceneView perspectiveView = this.getPerspectiveBoardView();
        EditorPane editorPane = new EditorPane(
                this.getGenericGameOptions().getScene3DResource().get(),
                perspectiveView
        );
        editorPane.getStyleClass().add("content");
        this.addRow(0, perspectiveView, editorPane);
    }

}
