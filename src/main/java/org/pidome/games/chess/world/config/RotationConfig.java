/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.geometry.Point3D;
import javafx.scene.transform.Rotate;

/**
 * A single rotation configuration.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public abstract class RotationConfig {

    /**
     * The literal rotate config.
     */
    private Rotate rotate;

    /**
     * The axis.
     */
    public enum Axis {
        /**
         * Rotate on X.
         */
        X(Rotate.X_AXIS),
        /**
         * Rotate on Y.
         */
        Y(Rotate.Y_AXIS);

        /**
         * Rotation point.
         */
        private final Point3D point3D;

        /**
         * Enum constructor.
         *
         * @param point3D The rotation point.
         */
        private Axis(final Point3D point3D) {
            this.point3D = point3D;
        }
    }

    /**
     * Protected consuctor.
     *
     * @param axis The axis to set.
     */
    protected RotationConfig(final Axis axis) {
        rotate = new Rotate();
        rotate.setAxis(axis.point3D);
    }

    /**
     * Set the rotation angle.
     *
     * @param rotation The rotation to set.
     */
    @JsonSetter
    public void setAngle(final double rotation) {
        this.rotate.setAngle(rotation);
    }

    /**
     * Get the rotation.
     *
     * @return The rotation set.
     */
    @JsonGetter
    public double getAngle() {
        return this.rotate.getAngle();
    }

    /**
     * Set X pivot.
     *
     * @param x Pivot.
     */
    @JsonSetter
    public void setPivotX(final double x) {
        this.rotate.setPivotX(x);
    }

    /**
     * Get X Pivot.
     *
     * @return Pivot.
     */
    @JsonGetter
    public double getPivotX() {
        return this.rotate.getPivotX();
    }

    /**
     * Set Y pivot.
     *
     * @param y Pivot.
     */
    @JsonSetter
    public void setPivotY(final double y) {
        this.rotate.setPivotY(y);
    }

    /**
     * Get Y Pivot.
     *
     * @return Pivot.
     */
    @JsonGetter
    public double getPivotY() {
        return this.rotate.getPivotY();
    }

    /**
     * Set Z pivot.
     *
     * @param z Pivot.
     */
    @JsonSetter
    public void setPivotZ(final double z) {
        this.rotate.setPivotZ(z);
    }

    /**
     * Get Z Pivot.
     *
     * @return Pivot.
     */
    @JsonGetter
    public double getPivotZ() {
        return this.rotate.getPivotZ();
    }

    /**
     * The rotation object.
     *
     * @return the rotation object.
     */
    @JsonIgnore
    public Rotate getRotateObject() {
        return this.rotate;
    }
}
