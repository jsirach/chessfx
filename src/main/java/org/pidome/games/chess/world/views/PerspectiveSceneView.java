/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.animation.Animation;
import javafx.animation.Animation.Status;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fxyz3d.utils.CameraTransformer;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.model.options.PlayerGameOptions;

/**
 * The game in a perspective view.
 *
 * The view presented in 3D.
 *
 * @author John Sirach
 */
public final class PerspectiveSceneView extends PerspectiveWorld {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PerspectiveSceneView.class);

    /**
     * Known rotate x pos.
     */
    private double mousePosX = 0;
    /**
     * Known rotate y pos.
     */
    private double mousePosY = 0;

    /**
     * If mouse/gesture events are enabled or not.
     */
    private final BooleanProperty demoMode = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * If mouse/gesture events are enabled or not.
     */
    private final BooleanProperty enableNavigation = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * When press event occurs.
     */
    private final EventHandler<? super MouseEvent> onGesturePressed = this::onGesturePressed;

    /**
     * When drag event occurs.
     */
    private final EventHandler<? super MouseEvent> onGestureDragged = this::onGestureDragged;

    /**
     * When scrolling occurs.
     */
    private final EventHandler<? super ScrollEvent> onGestureScrolled = this::onGestureScrolled;

    /**
     * The camera transformer used to direct the camera's position.
     *
     * Zoom is handled by the camera it self.
     */
    private final CameraTransformer cameraTransform;

    /**
     * The camera for the viewPort.
     */
    private final PerspectiveCamera camera = new PerspectiveCamera(true);

    /**
     * Holds the position previous to a forced position.
     *
     * This does not hold a generic x,y,z coordinate but contains: x:
     * <code>CAMERA_START_X_ANGLE</code><br/> y:
     * <code>CAMERA_START_Y_ANGLE</code><br/> z:
     * <code>CAMERA_START_Z_INDEX</code><br/> This is for the camera positioning
     * and is refreshed every time a camera is position forced.
     */
    private Point3D previousPosition = new Point3D(CAMERA_START_X_ANGLE, CAMERA_START_Y_ANGLE, CAMERA_START_Z_INDEX);

    /**
     * The camera start Z position.
     */
    private static final double CAMERA_START_Z_INDEX = -3900;

    /**
     * The camera start angle on the x-axis.
     */
    private static final double CAMERA_START_X_ANGLE = -50;

    /**
     * The camera start position on the Y-axis
     */
    private static final double CAMERA_START_Y_ANGLE = 0;

    /**
     * The scene to be returned.
     */
    private SubScene scene;

    /**
     * The demo camera
     */
    private DemoCamera demoCamera;

    /**
     * Constructor.
     *
     * @param gameHash Game identifier.
     * @param gameOptions The game's options.
     * @throws java.lang.Exception when loading fails.
     */
    public PerspectiveSceneView(final String gameHash, final AbstractGenericGameOptions gameOptions) throws Exception {
        super(gameHash, gameOptions);
        cameraTransform = new CameraTransformer();
        setMouseHandlers();
        setOrientationEnabled(true);
        if (gameOptions instanceof PlayerGameOptions) {
            demoMode.bind(((PlayerGameOptions) gameOptions).demoMode());
            enableNavigation.setValue(demoMode.not().getValue());
        }
    }

    /**
     * Returns the scene used perspective camera.
     *
     * @return The perspective camera.
     */
    public final PerspectiveCamera getPerspectiveCamera() {
        return this.camera;
    }

    /**
     * @inheritDoc
     */
    @Override
    public final void compose() {
        scene = new SubScene(getGroup3D(), this.prefWidthProperty().get(), this.prefHeightProperty().get(), true, SceneAntialiasing.BALANCED);
        scene.widthProperty().bind(this.prefWidthProperty());
        scene.heightProperty().bind(this.prefHeightProperty());
        scene.setFill(Color.BLACK);
        buildWorld();
        setCameraSettings();
        setCameraStartPosition(false);
        this.getChildren().add(this.scene);
    }

    /**
     * Destroys a perspective scene.
     */
    @Override
    public final void destroy() {
        scene.widthProperty().unbind();
        scene.heightProperty().unbind();
        destroyWorld();
        this.getChildren().remove(this.scene);
        this.scene = null;
    }

    /**
     * Returns the camera transformer.
     *
     * @return The camera transformer.
     */
    public final CameraTransformer getCameraTransformer() {
        return this.cameraTransform;
    }

    /**
     * Sets the view settings for a camera.
     */
    private void setCameraSettings() {
        this.camera.setNearClip(200);
        this.camera.setFarClip(15000.0);
        this.camera.setFieldOfView(30);
        this.cameraTransform.getChildren().add(camera);
        add3dObjects(cameraTransform);
        scene.setCamera(camera);
        if (demoMode.get()) {
            demoCamera = new DemoCamera();
            demoCamera.start(2);
        }
    }

    /**
     * If scene orientation should be enabled or not.
     *
     * @param enabled When set to true the user is able to move around in the
     * scene.
     */
    public final void setOrientationEnabled(boolean enabled) {
        enableNavigation.setValue(enabled);
    }

    /**
     * Sets the handler for the mouse/gesture events.
     */
    private void setMouseHandlers() {
        enableNavigation.addListener((obsVal, oldVal, newVal) -> {
            if (newVal) {
                getGroup3D().setOnMousePressed(onGesturePressed);
                getGroup3D().setOnMouseDragged(onGestureDragged);
                getGroup3D().addEventHandler(ScrollEvent.ANY, onGestureScrolled);
            } else {
                getGroup3D().setOnMousePressed(null);
                getGroup3D().setOnMouseDragged(null);
                getGroup3D().removeEventHandler(ScrollEvent.ANY, onGestureScrolled);
            }
        });
    }

    /**
     * When a mouse press has occurred.
     *
     * @param me The mouse event.
     */
    private void onGesturePressed(MouseEvent me) {
        mousePosX = me.getSceneX();
        mousePosY = me.getSceneY();
    }

    /**
     * When pressed and dragged.
     *
     * @param me The mouse event.
     */
    private void onGestureDragged(final MouseEvent me) {
        final double dx = (mousePosX - me.getSceneX());
        final double dy = (mousePosY - me.getSceneY());
        if (me.isPrimaryButtonDown()) {
            double xAngle = cameraTransform.rx.getAngle() - (dy / scene.getHeight() * -360);
            if (xAngle > -90 && xAngle < -10) {
                cameraTransform.rx.setAngle(xAngle);
            }
            cameraTransform.ry.setAngle(cameraTransform.ry.getAngle()
                    + (dx / scene.getWidth() * -360));
        } else if (me.isSecondaryButtonDown()) {

        }
        mousePosX = me.getSceneX();
        mousePosY = me.getSceneY();
    }

    /**
     * When scrolled.
     *
     * @param event The scroll event.
     */
    private void onGestureScrolled(final ScrollEvent event) {
        //Get how much scroll was done in Y axis.
        double zoomFactor = 50;
        final double deltaY = event.getDeltaY();
        if (deltaY < 0) {
            zoomFactor = 2.0 - zoomFactor;
        }
        //Add it to the Z-axis location.
        final double totalZoom = camera.getTranslateZ() + zoomFactor;
        if (totalZoom > -6300 && totalZoom < -1822) {
            camera.setTranslateZ(totalZoom);
        }
    }

    /**
     * When a position has been forced this method moves to the previous
     * position.
     *
     * @param animated It the motion should be animated.
     */
    public final void setCameraPreviousPosition(final boolean animated) {
        if (animated) {
            final Timeline angleTimeLine = new Timeline(new KeyFrame(
                    Duration.seconds(1.0),
                    new KeyValue(cameraTransform.rx.angleProperty(), previousPosition.getX(), Interpolator.EASE_BOTH),
                    new KeyValue(cameraTransform.ry.angleProperty(), previousPosition.getY(), Interpolator.EASE_BOTH)
            ));

            final Timeline distanceTimeline = new Timeline(new KeyFrame(
                    Duration.seconds(.9),
                    new KeyValue(camera.translateZProperty(), previousPosition.getZ(), Interpolator.EASE_BOTH)
            ));

            final ParallelTransition parallelTransition = new ParallelTransition();
            parallelTransition.getChildren().addAll(distanceTimeline, angleTimeLine);

            parallelTransition.setCycleCount(1);
            parallelTransition.setAutoReverse(false);
            parallelTransition.play();
        } else {
            camera.setTranslateZ(previousPosition.getZ());
            cameraTransform.rx.setAngle(previousPosition.getX());
            cameraTransform.ry.setAngle(previousPosition.getY());
        }
    }

    /**
     * Set's the camera in start position.
     *
     * @param animated if the motion to the start position should be animated.
     */
    public final void setCameraStartPosition(final boolean animated) {
        double rotationFromAngle = cameraTransform.ry.getAngle();
        final double fullCircels = Math.round(rotationFromAngle / 360);
        if (rotationFromAngle < -360 || rotationFromAngle > 360) {
            rotationFromAngle = rotationFromAngle - (fullCircels * 360);
        }
        if (rotationFromAngle > 180) {
            cameraTransform.ry.setAngle(rotationFromAngle - 360);
        } else if (rotationFromAngle < -180) {
            cameraTransform.ry.setAngle(rotationFromAngle + 360);
        } else {
            cameraTransform.ry.setAngle(rotationFromAngle);
        }
        previousPosition = new Point3D(cameraTransform.rx.getAngle(), cameraTransform.ry.getAngle(), camera.getTranslateZ());
        if (animated) {
            final Timeline angleTimeLine = new Timeline(new KeyFrame(
                    Duration.seconds(1.0),
                    new KeyValue(cameraTransform.rx.angleProperty(), CAMERA_START_X_ANGLE, Interpolator.EASE_BOTH),
                    new KeyValue(cameraTransform.ry.angleProperty(), CAMERA_START_Y_ANGLE, Interpolator.EASE_BOTH)
            ));

            final Timeline distanceTimeline = new Timeline(new KeyFrame(
                    Duration.seconds(.9),
                    new KeyValue(camera.translateZProperty(), CAMERA_START_Z_INDEX, Interpolator.EASE_BOTH)
            ));

            final ParallelTransition parallelTransition = new ParallelTransition();
            parallelTransition.getChildren().addAll(distanceTimeline, angleTimeLine);

            parallelTransition.setCycleCount(1);
            parallelTransition.setAutoReverse(false);
            parallelTransition.play();
        } else {
            camera.setTranslateZ(CAMERA_START_Z_INDEX);
            cameraTransform.rx.setAngle(CAMERA_START_X_ANGLE);
            cameraTransform.ry.setAngle(CAMERA_START_Y_ANGLE);
        }
    }

    /**
     * The default camera start Z position.
     *
     * @return Camera start.
     */
    public static double getDefaultCameraStartZIndex() {
        return CAMERA_START_Z_INDEX;
    }

    /**
     * The default camera start angle on the x-axis.
     *
     * @return Camera start.
     */
    public static double getDefaultCameraStartXAngle() {
        return CAMERA_START_X_ANGLE;
    }

    /**
     * The default camera start position on the Y-axis.
     *
     * @return Camera start.
     */
    public static double getDefaultCameraStartYAngle() {
        return CAMERA_START_Y_ANGLE;
    }

    /**
     * Class for demo camera.
     */
    private class DemoCamera {

        /**
         * If started or not.
         */
        private BooleanProperty started = new SimpleBooleanProperty(Boolean.FALSE);

        private int secondsDelay = 1;

        final Timeline rotateTimeline = new Timeline(new KeyFrame(
                Duration.seconds(300),
                new KeyValue(cameraTransform.ry.angleProperty(), 360, Interpolator.LINEAR)
        ));

        final Timeline zoomInTimeline = new Timeline(new KeyFrame(
                Duration.seconds(60),
                new KeyValue(camera.translateZProperty(), -1822, Interpolator.EASE_BOTH)
        ));

        final Timeline zoomOutTimeline = new Timeline(new KeyFrame(
                Duration.seconds(60),
                new KeyValue(camera.translateZProperty(), CAMERA_START_Z_INDEX, Interpolator.EASE_BOTH)
        ));

        final Timeline angleLowTimeline = new Timeline(new KeyFrame(
                Duration.seconds(120),
                new KeyValue(cameraTransform.rx.angleProperty(), -10, Interpolator.EASE_BOTH)
        ));

        final Timeline angleHighTimeline = new Timeline(new KeyFrame(
                Duration.seconds(120),
                new KeyValue(cameraTransform.rx.angleProperty(), CAMERA_START_X_ANGLE, Interpolator.EASE_BOTH)
        ));

        final ParallelTransition parallelTransition = new ParallelTransition();

        private ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        /**
         * Constructor.
         */
        private DemoCamera() {
            final SequentialTransition angleTransition = new SequentialTransition();
            angleTransition.getChildren().addAll(angleLowTimeline, angleHighTimeline);
            angleTransition.setCycleCount(Animation.INDEFINITE);

            final SequentialTransition zoomTransition = new SequentialTransition();
            zoomTransition.getChildren().addAll(zoomInTimeline, zoomOutTimeline);
            zoomTransition.setCycleCount(Animation.INDEFINITE);

            rotateTimeline.setCycleCount(Animation.INDEFINITE);

            parallelTransition.getChildren().addAll(angleTransition, zoomTransition, rotateTimeline);

            parallelTransition.setCycleCount(Animation.INDEFINITE);

            started.addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    setCameraStartPosition(true);
                    executorService.schedule(() -> parallelTransition.play(), secondsDelay + 1, TimeUnit.SECONDS);
                } else {
                    executorService.shutdownNow();
                    if (parallelTransition.statusProperty().get().equals(Status.RUNNING)) {
                        parallelTransition.stop();
                        setCameraStartPosition(true);
                    }
                }
            });

        }

        private void start(final int secondsDelay) {
            this.secondsDelay = secondsDelay;
            started.setValue(Boolean.TRUE);
        }

        private void stop() {
            started.setValue(Boolean.FALSE);
        }

    }

}
