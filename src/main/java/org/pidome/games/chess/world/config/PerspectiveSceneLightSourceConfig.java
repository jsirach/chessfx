/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.UUID;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Configuration file for light sources in a scene.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PerspectiveSceneLightSourceConfig {

    /**
     * the lighting type.
     */
    public enum Type {
        POINT;
    }

    @JsonIgnore
    private final String identity = UUID.randomUUID().toString();

    /**
     * The light name.
     */
    @JsonIgnore
    private StringProperty name = new SimpleStringProperty();

    /**
     * The light type.
     */
    private Type type;

    /**
     * the light color configuration.
     */
    private ColorConfig color;

    /**
     * Location in the world.
     */
    private Location3D location;

    /**
     * The light attenuation configuration.
     */
    private PerspectiveSceneLightAttenuationConfig attenuation = new PerspectiveSceneLightAttenuationConfig();

    @JsonIgnore
    public String getIdentity() {
        return this.identity;
    }

    /**
     * @return the name
     */
    @JsonGetter
    public String getName() {
        return name.getValueSafe();
    }

    /**
     * @param name the name to set
     */
    @JsonSetter
    public void setName(final String name) {
        this.name.setValue(name);
    }

    /**
     * The name as property.
     *
     * @return Name property.
     */
    public StringProperty nameProperty() {
        return this.name;
    }

    /**
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(final Type type) {
        this.type = type;
    }

    /**
     * @return the color
     */
    public ColorConfig getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(final ColorConfig color) {
        this.color = color;
    }

    /**
     * @return the location
     */
    public Location3D getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(final Location3D location) {
        this.location = location;
    }

    /**
     * @param attenuation The light attenuation configuration
     */
    public void setAttenuation(final PerspectiveSceneLightAttenuationConfig attenuation) {
        this.attenuation = attenuation;
    }

    /**
     * @return The light attenuation configuration
     */
    public PerspectiveSceneLightAttenuationConfig getAttenuation() {
        return this.attenuation;
    }

}
