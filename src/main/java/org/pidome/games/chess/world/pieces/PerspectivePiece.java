/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.pieces;

import javafx.scene.Node;
import org.pidome.games.chess.world.config.PieceModelConfig;

/**
 * A piece with a perspective view.
 *
 * @author John Sirach
 */
public final class PerspectivePiece extends VisualPiece {

    /**
     * File name of the piece.
     */
    private final String fileName;

    /**
     * Returns a piece largest with bounds size.
     */
    private Double width;

    /**
     * Constructor of the piece.
     *
     * @param color The piece color.
     * @param variant The piece variant type.
     */
    public PerspectivePiece(final Variant variant, final VariantColor color) {
        super(true, variant, color);
        fileName = new StringBuilder(this.getVariant().getText())
                .append("_")
                .append(this.getVariantColor().getText())
                .append(".obj")
                .toString();
    }

    /**
     * Loads a piece.
     *
     * @throws org.pidome.games.chess.game.world.pieces.PieceLoadException
     */
    @Override
    public void load(final String location, final PieceModelConfig modelConfig) throws PieceLoadException {
        try {
            final String fileLoad = new StringBuilder(location)
                    .append(fileName)
                    .toString();
            loadModel(fileLoad, modelConfig);
        } catch (Exception ex) {
            throw new PieceLoadException(ex);
        }
    }

    /**
     * Returns the piece's width.
     *
     * @return The width.
     */
    @Override
    public double getWidth() {
        double size = 0;
        if (width == null) {
            for (Node node : this.getChildren()) {
                if (node.getLayoutBounds().getWidth() > size) {
                    width = node.getLayoutBounds().getWidth();
                }
            }
        }
        return width;
    }

    /**
     * Marking a piece.
     *
     * @param marked if marked or not.
     */
    @Override
    public void setMarked(final boolean marked) {
        super.setMarked(marked);
        /*
        this.getChildren().forEach((node) -> {
            PhongMaterial meshNode = (PhongMaterial) ((MeshView) node).getMaterial();
            if (this.isMarked()) {
                defaultSpecularPower = meshNode.getSpecularPower();
                meshNode.setSpecularPower(HIGHLIGHT_SPECULAR);
            } else {
                meshNode.setSpecularPower(defaultSpecularPower);
            }
        });
         */
    }

}
