/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.resources.ResourcesLoader;

/**
 * The game in a flat view.
 *
 * @author John Sirach
 */
public class FlatSceneView extends AbstractSceneView {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(FlatSceneView.class);

    /**
     * A flat chess view.
     *
     * @param gameHash Game identifier.
     * @param gameOptions the game's options.
     * @throws java.lang.Exception
     */
    public FlatSceneView(final String gameHash, final AbstractGenericGameOptions gameOptions) throws Exception {
        super(gameHash, gameOptions, gameOptions.getFlatBoardOptions().get());
        this.getStyleClass().add("flat-board-view");
    }

    /**
     * Registers assets to be loaded.
     */
    @Override
    public void scheduleAssetLoading(final ResourcesLoader resourcesLoader) {
        this.getGameBoard().scheduleAssetLoading(resourcesLoader);
        this.getPieces().scheduleAssetLoading(resourcesLoader);
    }

    /**
     * Put's it all on the screen.
     */
    @Override
    public void compose() {
        this.getChildren().add(this.getGameBoard().getFlatBoard());
        this.getGameBoard().registerPieces(this.getPieces());
        this.getGameBoard().build();
    }

    /**
     * Destroy the board view.
     */
    @Override
    public void destroy() {
        this.getGameBoard().destroy();
    }

}
