/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.pidome.games.chess.resources.ResourceLoadResult;

/**
 * A static world model without interaction.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class StaticWorldModel extends WorldModel<ModelConfig> {

    /**
     * Path to the world model.
     */
    private final String modelPath;

    /**
     * the model config.
     */
    private final PerspectiveSceneStaticModel modelConfig;

    /**
     * Model constructor.
     *
     * @param modelPath The path to the model.
     */
    public StaticWorldModel(final String modelPath) {
        super(ModelConfig.class);
        this.modelPath = modelPath;
        this.modelConfig = null;
    }

    /**
     * Model constructor.
     *
     * @param model The model configuration.
     */
    public StaticWorldModel(final PerspectiveSceneStaticModel model) {
        super(ModelConfig.class);
        this.modelPath = new StringBuilder(PerspectiveSceneStaticModel.BASE_PATH)
                .append("/")
                .append(model.getCategory()).append("/")
                .append(model.getSubCategory()).append("/")
                .append(model.getItem()).append("/")
                .append(model.getFile()).toString();
        this.modelConfig = model;
    }

    /**
     * Load the model asset.
     */
    public void loadAssetOldSchool() {
        try {
            super.loadModel(modelPath);
        } catch (Exception ex) {
            Logger.getLogger(StaticWorldModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Load and place the asset.
     *
     * @param callback callback to be executed when loading succeeded.
     * @return The loaded static world model.
     */
    public Callable<ResourceLoadResult> loadAsset(final Runnable callback) {
        return () -> {
            ResourceLoadResult loadResult = new ResourceLoadResult();
            try {
                this.loadModel(modelPath);
                if (modelConfig != null) {
                    if (modelConfig.getLocation() != null) {
                        this.setTranslateX(modelConfig.getLocation().getX());
                        this.setTranslateY(modelConfig.getLocation().getY());
                        this.setTranslateZ(modelConfig.getLocation().getZ());
                    }
                    if (modelConfig.getRotationXConfig() != null) {
                        this.getTransforms().add(modelConfig.getRotationXConfig().getRotateObject());
                    }
                    if (modelConfig.getRotationYConfig() != null) {
                        this.getTransforms().add(modelConfig.getRotationYConfig().getRotateObject());
                    }
                    loadResult.complete(callback);
                }
            } catch (Exception ex) {
                Logger.getLogger(StaticWorldModel.class.getName()).log(Level.SEVERE, null, ex);
                loadResult.complete(ex);
            }
            return loadResult;
        };
    }

}
