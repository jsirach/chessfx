/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Level;
import java.util.stream.IntStream;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Group;
import javafx.scene.paint.Material;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.MeshView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.utils.SerializationUtil;

/**
 * A 3D model group.
 *
 * @author John
 * @param <T> The configuration type to be applied.
 */
public class WorldModel<T extends ModelConfig> extends Group {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(WorldModel.class);

    /**
     * A configuration file extension name.
     */
    private static final String CONFIG_EXTENTION = "_config.json";

    /**
     * Set to true when loaded.
     */
    private final BooleanProperty modelLoaded = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * The model configuration.
     */
    private T modelConfig;

    /**
     * Class used for reflective configuration load.
     */
    private final Class<T> configurationClass;

    /**
     * The expected loading configuration.
     *
     * @param expectedConfiguration Expected configuration type class.
     */
    public WorldModel(final Class<T> expectedConfiguration) {
        this.configurationClass = expectedConfiguration;
    }

    /**
     * Return the model configuration.
     *
     * @return The model configuration.
     */
    public final T getModelConfig() {
        return this.modelConfig;
    }

    /**
     * The model file name.
     *
     * @param modelFile The path to the model file.
     * @throws Exception Thrown when loading the object fails.
     */
    public final void loadModel(final String modelFile) throws Exception {
        loadModel(modelFile, this);
    }

    /**
     * The model file name.
     *
     * @param modelFile The path to the model file.
     * @param group the group to add the loaded model to.
     * @throws Exception Thrown when loading the object fails.
     */
    public final void loadModel(final String modelFile, final Group group) throws Exception {
        final MeshView[] meshViews = GameResourcesHelper.loadObject(modelFile);
        final T modelConfig = createModelConfig(modelFile, configurationClass, meshViews);
        applyModelConfig(meshViews, modelConfig, group);
    }

    /**
     * The model file name.
     *
     * @param modelFile The path to the model file.
     * @param modelConfig A pre-loaded model configuration.
     * @throws Exception Thrown when loading the object fails.
     */
    public final void loadModel(final String modelFile, final T modelConfig) throws Exception {
        loadModel(modelFile, modelConfig, this);
    }

    /**
     * The model file name.
     *
     * @param modelFile The path to the model file.
     * @param modelConfig A pre-loaded model configuration.
     * @param modelGroup The model group to apply the loaded model to.
     * @throws Exception Thrown when loading the object fails.
     */
    public final void loadModel(final String modelFile, final T modelConfig, final Group modelGroup) throws Exception {
        final MeshView[] meshViews = GameResourcesHelper.loadObject(modelFile);
        applyModelConfig(meshViews, modelConfig, modelGroup);
    }

    /**
     * Applies the configuration to the current model.
     *
     * The group to apply the model to.
     *
     * @param meshViews The mesh views available.
     */
    private void applyModelConfig(final MeshView[] meshViews, final T modelConfig, final Group group) {
        this.modelConfig = modelConfig;
        if (!Objects.isNull(this.modelConfig.getNodeConfigurations())) {
            IntStream.range(0, meshViews.length).forEach(meshIndex -> {
                applyMaterial(meshViews[meshIndex].getMaterial(),
                        this.modelConfig.getNodeConfigurations().get(meshIndex)
                                .getMaterialConfig().getPhongMaterial());
            });
        }
        if (!Objects.isNull(this.modelConfig.getScaleConfig())) {
            this.getTransforms().add(this.getModelConfig().getScaleConfig().getScale());
        }
        group.getTransforms().addAll(
                this.getModelConfig().getRotateX(),
                this.getModelConfig().getRotateY(),
                this.getModelConfig().getRotateZ()
        );
        group.getChildren().addAll(meshViews);
        modelLoaded.setValue(Boolean.TRUE);
    }

    /**
     * Applies from saved material configuration to the model.
     *
     * The whole purpose is to overwrite existing properties, not replacing. By
     * overwriting pure existing properties model default properties can be
     * retained.
     *
     * @param originalMaterial The original <code>Matrial</code>.
     * @param phongMaterialConfig The phongMaterial from the configuration.
     */
    private void applyMaterial(final Material originalMaterial, final PhongMaterial phongMaterialConfig) {
        PhongMaterial original = (PhongMaterial) originalMaterial;
        if (Objects.nonNull(phongMaterialConfig.getBumpMap())) {
            original.setBumpMap(phongMaterialConfig.getBumpMap());
        }
        if (Objects.nonNull(phongMaterialConfig.getDiffuseColor())) {
            original.setDiffuseColor(phongMaterialConfig.getDiffuseColor());
        }
        if (Objects.nonNull(phongMaterialConfig.getDiffuseMap())) {
            original.setDiffuseMap(phongMaterialConfig.getDiffuseMap());
        }
        if (Objects.nonNull(phongMaterialConfig.getSelfIlluminationMap())) {
            original.setSelfIlluminationMap(phongMaterialConfig.getSelfIlluminationMap());
        }
        if (Objects.nonNull(phongMaterialConfig.getSpecularColor())) {
            original.setSpecularColor(phongMaterialConfig.getSpecularColor());
        }
        if (Objects.nonNull(phongMaterialConfig.getSpecularMap())) {
            original.setSpecularMap(phongMaterialConfig.getSpecularMap());
        }
        if (Objects.nonNull(phongMaterialConfig.getSpecularPower())) {
            original.setSpecularPower(phongMaterialConfig.getSpecularPower());
        }
    }

    /**
     * Creates model configuration from the given meshView.
     *
     * @param meshConfigArray The meshviews array to create a model config from.
     */
    private static <T extends ModelConfig> T createModelConfig(final String modelFile,
            final Class<T> modelConfig, final MeshView[] meshConfigArray) {
        final T localModalConfig = loadModelConfig("." + modelFile, modelConfig);
        if (Objects.isNull(localModalConfig.getNodeConfigurations())) {
            localModalConfig.setNodeConfigurations(Arrays.asList(
                    new MeshConfig[meshConfigArray.length]
            ));
            IntStream.range(0, meshConfigArray.length).forEach(meshIndex -> {
                MeshConfig meshConfig = new MeshConfig();
                MaterialConfig materialConfig = new MaterialConfig();
                materialConfig.setPhongMaterial((PhongMaterial) meshConfigArray[meshIndex].getMaterial());
                meshConfig.setMaterialConfig(materialConfig);
                localModalConfig.getNodeConfigurations().set(meshIndex, meshConfig);
            });
        }
        return localModalConfig;
    }

    /**
     * Boolean property if loaded or not.
     *
     * @return The boolean property set to true if loaded.
     */
    public BooleanProperty modelLoadedProperty() {
        return modelLoaded;
    }

    /**
     * Loads a model configuration.
     *
     * If no model configuration is found, a new model configuration with
     * defaults is returned.
     *
     * @param path The path to load from.
     * @return A model configuration.
     */
    private static <T extends ModelConfig> T loadModelConfig(final String objectFilePath, final Class<T> configurationClass) {
        final File configFilePath = getConfigFile(
                Paths.get(objectFilePath).toFile());
        ObjectMapper mapper = SerializationUtil.getObjectMapper();
        try {
            return mapper.readValue(configFilePath, configurationClass);
        } catch (IOException ex) {
            try {
                java.util.logging.Logger.getLogger(WorldModel.class.getName()).log(Level.SEVERE, null, ex);
                return configurationClass.getDeclaredConstructor().newInstance();
            } catch (NoSuchMethodException | SecurityException | InstantiationException
                    | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex1) {
                java.util.logging.Logger.getLogger(WorldModel.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return null;
    }

    /**
     * Set the config file naming.
     *
     * @param file The original object file.
     * @return File for the configuration.
     */
    private static File getConfigFile(File objectFile) {
        int extentionIndex = objectFile.getName().lastIndexOf('.');
        String name = objectFile.getName().substring(0, extentionIndex);
        return new File(objectFile.getParent(),
                new StringBuilder(name).append(CONFIG_EXTENTION).toString());
    }

    /**
     * Prints the configuration to System.out with pretty printer.
     */
    public final void printConfig() {
        try {
            System.out.println(
                    SerializationUtil.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this.modelConfig)
            );
        } catch (JsonProcessingException ex) {
            java.util.logging.Logger.getLogger(WorldModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
