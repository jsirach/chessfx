/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javafx.collections.ListChangeListener;
import javafx.scene.AmbientLight;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PointLight;
import javafx.scene.paint.Color;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.SerializationUtil;
import org.pidome.games.chess.world.config.ColorConfig;
import org.pidome.games.chess.world.config.PerspectiveSceneBuildconfig;
import org.pidome.games.chess.world.config.PerspectiveSceneLightAttenuationConfig;
import org.pidome.games.chess.world.config.PerspectiveSceneLightSourceConfig;
import org.pidome.games.chess.world.config.StaticWorldModel;

/**
 * The base class for the perspective scene to split object handling.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public abstract class PerspectiveWorld extends AbstractSceneView {

    /**
     * The scene configuration.
     */
    private final PerspectiveSceneBuildconfig perspectiveConfig;

    /**
     * Used world models.
     */
    private final List<StaticWorldModel> worldModels = new ArrayList<>();

    /**
     * The group to place the objects in.
     */
    private final Group group3D = new Group();

    /**
     * Listener for light objects.
     */
    private final ListChangeListener<PerspectiveSceneLightSourceConfig> lightsListener = this::listChangeListener;

    /**
     * Constructor.
     *
     * @param gameHash The game hash.
     * @param gameOptions The game options.
     */
    protected PerspectiveWorld(final String gameHash, final AbstractGenericGameOptions gameOptions) throws Exception {
        super(gameHash, gameOptions, gameOptions.getPerspectiveBoardOptions().get());
        perspectiveConfig = SerializationUtil.getObjectMapper().readValue(
                new File(gameOptions.getScene3DResource().get().getSceneConfigurationFile()),
                PerspectiveSceneBuildconfig.class);
    }

    /**
     * Returns the perspective configuration applied.
     *
     * @return The perspective configuration.
     */
    public PerspectiveSceneBuildconfig perspectiveConfig() {
        return this.perspectiveConfig;
    }

    /**
     * Adds (a) node(s) to the 3D scene.
     *
     * @param node The node(s) to add.
     */
    public final void add3dObjects(Node... node) {
        group3D.getChildren().addAll(Arrays.asList(node));
    }

    /**
     * Adds a node to the 3D scene.
     *
     * @param nodes The nodes to add.
     */
    public final void add3dObjects(List<StaticWorldModel> nodes) {
        group3D.getChildren().addAll(nodes);
    }

    /**
     * Remove 3d objects.
     *
     * @param node The nodes to remove.
     */
    public final void remove3dObjects(Node... node) {
        FXUtils.runOnFXThread(() -> {
            group3D.getChildren().removeAll(Arrays.asList(node));
        });
    }

    public boolean contains3dObject(Node node) {
        return group3D.getChildren().contains(node);
    }

    /**
     * @inheritDoc
     */
    @Override
    public void scheduleAssetLoading(final ResourcesLoader resourcesLoader) {
        this.getGameBoard().scheduleAssetLoading(resourcesLoader);
        this.getPieces().scheduleAssetLoading(resourcesLoader);
        if (!Objects.isNull(perspectiveConfig.getStaticModels())) {
            perspectiveConfig.getStaticModels().stream()
                    .map(StaticWorldModel::new)
                    .forEach(model -> resourcesLoader.schedule(
                    model.loadAsset(() -> worldModels.add(model))
            ));
        }
    }

    /**
     * Builds the 3D world.
     */
    protected void buildWorld() {
        add3dObjects(this.getGameBoard());
        add3dObjects(worldModels);
        this.getGameBoard().registerPieces(this.getPieces());
        this.getGameBoard().build();
        setAmbiance();
        setLights();
        this.perspectiveConfig.observableLights().addListener(lightsListener);
    }

    /**
     * Destroyer of worlds.
     */
    protected void destroyWorld() {
        this.perspectiveConfig.observableLights().removeListener(lightsListener);
        this.getGameBoard().destroy();
        getGroup3D().getChildren().clear();
    }

    /**
     * Returns the group.
     *
     * @return The group containing it all.
     */
    protected Group getGroup3D() {
        return this.group3D;
    }

    /**
     * Sets the viewport main ambiance color.
     */
    private void setAmbiance() {
        if (!Objects.isNull(this.perspectiveConfig.getAmbientLighting())) {
            final ColorConfig ambientConfig = this.perspectiveConfig.getAmbientLighting();
            AmbientLight ambientLight = new AmbientLight(
                    Color.hsb(ambientConfig.getHue(), ambientConfig.getSaturation(), ambientConfig.getBrightness())
            );
            if (this.getGameOptions().isEditMode()) {
                ambientConfig.hueProperty().addListener((obs, oldVal, newVal) -> {
                    Color newColor = Color.hsb(newVal.doubleValue(), ambientConfig.getSaturation(), ambientConfig.getBrightness());
                    ambientLight.setColor(newColor);
                });
                ambientConfig.saturationProperty().addListener((obs, oldVal, newVal) -> {
                    Color newColor = Color.hsb(ambientConfig.getHue(), newVal.doubleValue(), ambientConfig.getBrightness());
                    ambientLight.setColor(newColor);
                });
                ambientConfig.brightnessProperty().addListener((obs, oldVal, newVal) -> {
                    Color newColor = Color.hsb(ambientConfig.getHue(), ambientConfig.getSaturation(), newVal.doubleValue());
                    ambientLight.setColor(newColor);
                });
            }
            add3dObjects(ambientLight);
        }
    }

    private void listChangeListener(final ListChangeListener.Change<? extends PerspectiveSceneLightSourceConfig> change) {
        while (change.next()) {
            if (change.wasAdded()) {
                change.getAddedSubList().forEach(lightSource -> {
                    addLight(lightSource, true);
                });
            }
            if (change.wasRemoved()) {
                change.getRemoved().forEach(lightSource -> {
                    removeLight(lightSource);
                });
            }
        }
    }

    /**
     * Puts the lights in the scene.
     */
    private void setLights() {
        if (!Objects.isNull(this.perspectiveConfig.getLights())) {
            this.perspectiveConfig.getLights().forEach(lightSource -> {
                addLight(lightSource, false);
            });
        }
    }

    /**
     * Removes a light configuration.
     *
     * @param lightSource The configuration source.
     */
    private void removeLight(final PerspectiveSceneLightSourceConfig lightSource) {
        FXUtils.runOnFXThread(() -> {
            group3D.getChildren().removeIf(node -> lightSource.getIdentity().equals(node.getUserData()));
        });
    }

    /**
     * Adds a light to the scene.
     *
     * @param lightSource The light configuration source.
     */
    private void addLight(final PerspectiveSceneLightSourceConfig lightSource, final boolean fromEditor) {
        final ColorConfig colorConfig = lightSource.getColor();
        final PointLight light = new PointLight(
                Color.hsb(lightSource.getColor().getHue(),
                        lightSource.getColor().getSaturation(),
                        lightSource.getColor().getBrightness())
        );
        light.setUserData(lightSource.getIdentity());
        light.translateXProperty().bind(lightSource.getLocation().xProperty());
        light.translateYProperty().bind(lightSource.getLocation().yProperty());
        light.translateZProperty().bind(lightSource.getLocation().zProperty());

        setLightAttenuationProperties(lightSource, light);

        lightSource.getAttenuation().typeProperty().addListener((obs, oldVal, newVal) -> {
            setLightAttenuationProperties(lightSource, light);
        });
        if (this.getGameOptions().isEditMode()) {
            colorConfig.hueProperty().addListener((obs, oldVal, newVal) -> {
                Color newColor = Color.hsb(newVal.doubleValue(), colorConfig.getSaturation(), colorConfig.getBrightness());
                light.setColor(newColor);
            });
            colorConfig.saturationProperty().addListener((obs, oldVal, newVal) -> {
                Color newColor = Color.hsb(colorConfig.getHue(), newVal.doubleValue(), colorConfig.getBrightness());
                light.setColor(newColor);
            });
            colorConfig.brightnessProperty().addListener((obs, oldVal, newVal) -> {
                Color newColor = Color.hsb(colorConfig.getHue(), colorConfig.getSaturation(), newVal.doubleValue());
                light.setColor(newColor);
            });
        }
        if (fromEditor) {
            FXUtils.runOnFXThread(() -> {
                add3dObjects(light);
            });
        } else {
            add3dObjects(light);
        }
    }

    private void setLightAttenuationProperties(final PerspectiveSceneLightSourceConfig lightSource, final PointLight light) {
        light.linearAttenuationProperty().unbind();
        light.quadraticAttenuationProperty().unbind();
        light.constantAttenuationProperty().unbind();
        resetAttenuationValues(light);
        if (!PerspectiveSceneLightAttenuationConfig.Type.DEFAULT.equals(lightSource.getAttenuation().getType())) {
            switch (lightSource.getAttenuation().getType()) {
                case LINEAR:
                    light.linearAttenuationProperty().setValue(lightSource.getAttenuation().getValue());
                    light.linearAttenuationProperty().bind(lightSource.getAttenuation().valueProperty());
                    break;
                case QUADRATIC:
                    light.quadraticAttenuationProperty().setValue(lightSource.getAttenuation().getValue());
                    light.quadraticAttenuationProperty().bind(lightSource.getAttenuation().valueProperty());
                    break;
                case CONSTANT:
                    light.constantAttenuationProperty().setValue(lightSource.getAttenuation().getValue());
                    light.constantAttenuationProperty().bind(lightSource.getAttenuation().valueProperty());
                    break;
            }
            if (!light.maxRangeProperty().isBound()) {
                light.setMaxRange(lightSource.getAttenuation().getMaxRange());
            }
            light.maxRangeProperty().bind(lightSource.getAttenuation().maxRangeProperty());
        } else {
            light.maxRangeProperty().unbind();
            light.setConstantAttenuation(1d);
            light.setMaxRange(16000);
        }
    }

    private void resetAttenuationValues(final PointLight light) {
        light.linearAttenuationProperty().setValue(0d);
        light.quadraticAttenuationProperty().setValue(0d);
        light.constantAttenuationProperty().setValue(0d);
    }

}
