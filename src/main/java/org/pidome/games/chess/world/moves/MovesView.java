/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.moves;

import java.io.File;
import java.util.Objects;
import java.util.WeakHashMap;
import javafx.collections.ListChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import org.pidome.games.chess.model.move.MoveSet;
import org.pidome.games.chess.model.move.Moves;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.model.options.PlayerGameOptions;
import static org.pidome.games.chess.resources.i18n.I18nInGameKey.PIECE_COLOR_BLACK_UPPER;
import static org.pidome.games.chess.resources.i18n.I18nInGameKey.PIECE_COLOR_WHITE_UPPER;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.world.pieces.PieceInterface.Variant;

/**
 * List of moves.
 *
 * @author John
 */
public class MovesView extends BorderPane {

    /**
     * The moves done.
     */
    private final Moves moves;

    /**
     * The list view.
     */
    private final VBox movesView = new VBox();

    /**
     * List of moves done.
     */
    private final ScrollPane movesListPane = new ScrollPane(movesView);

    /**
     * The moves list change listener.
     */
    private final ListChangeListener<MoveSet> movesListener = this::moveBuilder;

    /**
     * Map caching the loaded images.
     */
    private final WeakHashMap<String, Image> loadedImages = new WeakHashMap<>();

    /**
     * Use images for pieces or text.
     */
    private final boolean useImagePieces = false;

    /**
     * The header shown above the pieces sets.
     */
    private final GridPane header = new GridPane();

    /**
     * The player controller.
     */
    private PlayerController playerController = null;

    /**
     * Constructor.
     *
     * @param genericGameOptions The game options.
     */
    public MovesView(final AbstractGenericGameOptions genericGameOptions) {
        this.getStyleClass().add("moves-view");
        this.movesListPane.getStyleClass().add("sroll-pane");
        this.moves = new Moves(genericGameOptions.getGameId());
        this.moves.registerForEvents();
        if (genericGameOptions instanceof PlayerGameOptions) {
            this.playerController = new PlayerController((PlayerGameOptions) genericGameOptions);
        }
    }

    /**
     * Builds the header for the moves view.
     */
    private void buildHeader() {
        header.getStyleClass().add("header");

        header.getColumnConstraints().addAll(
                new ColumnConstraints() {
            {
                setPercentWidth(50);
            }
        },
                new ColumnConstraints() {
            {
                setPercentWidth(50);
            }
        }
        );
        Label movesWhite = new Label();
        movesWhite.textProperty().bind(I18nProxy.getStringProperty(PIECE_COLOR_WHITE_UPPER));
        movesWhite.setPadding(new Insets(10, 0, 5, 43));
        movesWhite.getStyleClass().add("moves-size");

        Label movesBlack = new Label();
        movesBlack.textProperty().bind(I18nProxy.getStringProperty(PIECE_COLOR_BLACK_UPPER));
        movesBlack.setPadding(new Insets(10, 0, 5, 22));
        movesBlack.getStyleClass().add("moves-size");

        header.addRow(0, movesWhite, movesBlack);
    }

    /**
     * Build the moves view.
     */
    public final void build() {
        buildHeader();
        movesListPane.setFitToWidth(true);
        movesListPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        this.setTop(header);
        this.setCenter(movesListPane);
        if (!Objects.isNull(this.playerController)) {
            this.setBottom(this.playerController);
        }
        moves.getMovesObservable().addListener(movesListener);
    }

    /**
     * Method handling the changes in the list.
     *
     * @param moveChange The change in the moves.
     */
    private void moveBuilder(final ListChangeListener.Change<? extends MoveSet> moveChange) {
        if (moveChange.next()) {
            if (moveChange.wasAdded()) {
                moveChange.getAddedSubList().forEach(moveSet -> {
                    addMoveSetToList(moveSet);
                });
            }
            if (moveChange.wasRemoved()) {
                if (this.moves.getMovesObservable().isEmpty()) {
                    FXUtils.runOnFXThread(() -> {
                        this.movesView.getChildren().clear();
                    });
                } else {
                    moveChange.getRemoved().forEach(moveSet -> {
                        removeFromSetList(moveSet);
                    });
                }
            }
        }
    }

    /**
     * Adds a move set to the list.
     *
     * @param moveSet The set of moves for a turn to add.
     */
    private void addMoveSetToList(final MoveSet moveSet) {
        final BoxedMoveView view = new BoxedMoveView(moveSet);
        FXUtils.runOnFXThread(() -> {
            this.movesView.getChildren().add(view);
        });
    }

    /**
     * Remove a single move from a set.
     *
     * @param moveSet The item to remove.
     */
    private void removeFromSetList(final MoveSet moveSet) {
        this.movesView.getChildren().stream()
                .filter(maybeBoxView -> (maybeBoxView instanceof BoxedMoveView))
                .map(maybeBoxView -> (BoxedMoveView) maybeBoxView)
                .findFirst()
                .ifPresent((found) -> {
                    FXUtils.runOnFXThread(() -> {
                        this.movesView.getChildren().remove(found);
                    });
                });
    }

    /**
     * Container for showing the moves in the list view.
     */
    private final class BoxedMoveView extends GridPane {

        /**
         * The moveset to be displayed.
         */
        private final MoveSet moveSet;

        /**
         * Displayed move.
         */
        private final Label moveText = new Label();

        /**
         * Displayed white piece.
         */
        private final Label whiteText = new Label();
        /**
         * The white piece image.
         */
        private ImageView whitePieceImage;
        /**
         * The white piece text.
         */
        private Label whitePieceText;

        /**
         * Displayed black piece.
         */
        private Label blackText = new Label();

        /**
         * The black piece image.
         */
        private ImageView blackPieceImage;
        /**
         * The black piece text.
         */
        private Label blackPieceText;

        /**
         * Constructor.
         *
         * @param moveView turn text.
         * @param whiteView white text.
         * @param blackView black text.
         */
        public BoxedMoveView(final MoveSet moveSet) {
            if (useImagePieces) {
                whitePieceImage = new ImageView();
                blackPieceImage = new ImageView();
            } else {
                whitePieceText = new Label();
                blackPieceText = new Label();
                whitePieceText.getStyleClass().add("moves-size");
                blackPieceText.getStyleClass().add("moves-size");
            }
            moveText.getStyleClass().add("moves-size");
            whiteText.getStyleClass().add("moves-size");
            blackText.getStyleClass().add("moves-size");
            moveText.setPadding(new Insets(0, 0, 0, 10));

            this.moveSet = moveSet;

            final Image whiteImg = loadPieceImage(this.moveSet.getMoveWhiteStringProperty().getValueSafe(), "white");

            this.moveText.setText(String.valueOf(this.moveSet.getTurn()));
            if (useImagePieces) {
                whitePieceImage.setImage(whiteImg);
            } else {
                whitePieceText.setText(loadPieceUnicode(this.moveSet.getMoveWhiteStringProperty().getValueSafe(), "white"));
            }
            this.whiteText.textProperty().setValue(this.moveSet.getMoveWhiteStringProperty().getValueSafe());

            this.moveSet.getMoveWhiteStringProperty().addListener((obsVal, oldVal, newVal) -> {
                if (newVal != null && !newVal.isBlank()) {
                    final Image image = loadPieceImage(newVal, "white");
                    FXUtils.runOnFXThread(() -> {
                        if (useImagePieces) {
                            whitePieceImage.setImage(image);
                        } else {
                            whitePieceText.setText(loadPieceUnicode(this.moveSet.getMoveWhiteStringProperty().getValueSafe(), "white"));
                        }
                        this.whiteText.textProperty().setValue(newVal);
                    });
                } else {
                    FXUtils.runOnFXThread(() -> {
                        if (useImagePieces) {
                            whitePieceImage.setImage(null);
                        } else {
                            whitePieceText.setText("");
                        }
                        this.whiteText.textProperty().setValue("");
                    });
                }
            });

            this.moveSet.getMoveBlackStringProperty().addListener((obsVal, oldVal, newVal) -> {
                if (newVal != null && !newVal.isBlank()) {
                    final Image image = loadPieceImage(newVal, "black");
                    FXUtils.runOnFXThread(() -> {
                        if (useImagePieces) {
                            blackPieceImage.setImage(image);
                        } else {
                            blackPieceText.setText(loadPieceUnicode(this.moveSet.getMoveBlackStringProperty().getValueSafe(), "black"));
                        }
                        this.blackText.textProperty().setValue(newVal);
                    });
                } else {
                    FXUtils.runOnFXThread(() -> {
                        if (useImagePieces) {
                            blackPieceImage.setImage(null);
                        } else {
                            blackPieceText.setText("");
                        }
                        this.blackText.textProperty().setValue("");
                    });
                }
            });

            final ColumnConstraints moveColumn = new ColumnConstraints();
            moveColumn.setPrefWidth(35);
            moveColumn.setHalignment(HPos.LEFT);

            final ColumnConstraints pieceWhiteColumn = new ColumnConstraints();
            pieceWhiteColumn.setPrefWidth(25);
            pieceWhiteColumn.setHalignment(HPos.CENTER);

            final ColumnConstraints positionWhiteColumn = new ColumnConstraints();
            positionWhiteColumn.setHgrow(Priority.ALWAYS);
            positionWhiteColumn.setHalignment(HPos.LEFT);

            final ColumnConstraints pieceBlackColumn = new ColumnConstraints();
            pieceBlackColumn.setPrefWidth(25);
            pieceBlackColumn.setHalignment(HPos.CENTER);

            final ColumnConstraints positionBlackColumn = new ColumnConstraints();
            positionBlackColumn.setHgrow(Priority.ALWAYS);
            positionBlackColumn.setHalignment(HPos.LEFT);

            final RowConstraints rowConstraint = new RowConstraints();
            rowConstraint.setValignment(VPos.CENTER);
            rowConstraint.setPrefHeight(30);

            this.getRowConstraints().add(rowConstraint);

            this.getColumnConstraints().addAll(moveColumn, pieceWhiteColumn, positionWhiteColumn, pieceBlackColumn, positionBlackColumn);

            GridPane.setHgrow(whiteText, Priority.ALWAYS);
            GridPane.setHgrow(blackText, Priority.ALWAYS);

            this.add(moveText, 0, 0);
            if (useImagePieces) {
                this.add(whitePieceImage, 1, 0);
            } else {
                this.add(whitePieceText, 1, 0);
            }
            this.add(whiteText, 2, 0);
            if (useImagePieces) {
                this.add(blackPieceImage, 3, 0);
            } else {
                this.add(blackPieceText, 3, 0);
            }
            this.add(blackText, 4, 0);

        }

        /**
         * Returns the turn of this move set.
         */
        public int getTurn() {
            return this.moveSet.getTurn();
        }

    }

    /**
     * Loads the unicode version of a chess piece.
     *
     * To use the unicode version a font must be selected which includes the
     * chess pieces.
     *
     * @param pieceNotation The move notation.
     * @param color The color to load.
     * @return The unicode version of a piece.
     */
    private String loadPieceUnicode(final String pieceNotation, final String color) {
        switch (Variant.valueOf(pieceNotation.substring(0, 1))) {
            case K:
                return color.equals("white") ? "\u2654" : "\u265A";
            case Q:
                return color.equals("white") ? "\u2655" : "\u265B";
            case R:
                return color.equals("white") ? "\u2656" : "\u265C";
            case B:
                return color.equals("white") ? "\u2657" : "\u265D";
            case N:
                return color.equals("white") ? "\u2658" : "\u265E";
            default: ///pawn
                return color.equals("white") ? "\u2659" : "\u265F";
        }
    }

    /**
     * Loads the piece image to display.
     *
     * This method caches loaded images for pieces.
     *
     * @param pieceNotation The move notation.
     * @param color The color to load.
     * @return <code>Image</code> for displaying in an imageview.
     */
    private Image loadPieceImage(final String pieceNotation, final String color) {
        final String fileLoad = new StringBuilder("/resources/assets/flat/pieces/generic/")
                .append(Variant.valueOf(pieceNotation.substring(0, 1)).getText())
                .append("_")
                .append(color)
                .append(".png")
                .toString();
        if (!loadedImages.containsKey(fileLoad)) {
            loadedImages.put(fileLoad, new Image(new File(fileLoad).toURI().toString(), 15, 15, true, true));
        }
        return loadedImages.get(fileLoad);
    }

}
