/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to define game scenes.
 *
 * @author John
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface GameView {

    /**
     * The name of the scene shown in interfaces.
     *
     * @return The name of the scene.
     */
    String name();

    /**
     * The description of the scene shown in interfaces.
     *
     * @return The scene description.
     */
    String description();

    /**
     * What board view type is supported.
     *
     * Based on the view type set here you influence what boards a user can use
     * in your view. Also only the boards compatible with this viewtype will be
     * made available for your scene. This must exactly match one of constants
     * available in <code>GameOptions.GameViewType.TypeConstant</code>
     *
     * @return The view type.
     */
    int boardViewType();

}
