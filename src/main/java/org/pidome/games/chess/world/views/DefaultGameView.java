/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.world.hud.MainPlayfieldPlayerCards;
import org.pidome.games.chess.world.moves.MovesView;

/**
 * The default view to play a game.
 *
 * @author John
 */
@GameView(
        name = "Default game scene",
        description = "Default setup with a 3D and 2D board",
        boardViewType = GameOptions.GameViewType.TypeConstant.FLAT
        + GameOptions.GameViewType.TypeConstant.PERSPECTIVE
)
public class DefaultGameView extends AbstractGameView {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(DefaultGameView.class);

    /**
     * The moves view to be displayed.
     */
    private final MovesView movesView;

    /**
     * Primary view column.
     */
    private final ColumnConstraints primaryViewColumn = new ColumnConstraints() {
        {
            setPercentWidth(70);
            setHgap(0);
            setVgap(0);
        }
    };

    /**
     * Primary view row.
     */
    private final RowConstraints primaryViewRow = new RowConstraints() {
        {
            setPercentHeight(60);
            setHgap(0);
            setVgap(0);
        }
    };

    /**
     * Primary view column.
     */
    private final ColumnConstraints secondaryViewColumn = new ColumnConstraints() {
        {
            setPercentWidth(30);
            setHgap(0);
            setVgap(0);
        }
    };

    /**
     * Primary view row.
     */
    private final RowConstraints secondaryViewRow = new RowConstraints() {
        {
            setPercentHeight(40);
            setHgap(0);
            setVgap(0);
        }
    };

    /**
     * View constructor.
     *
     * @param genericGameOptions Game options.
     * @throws java.lang.Exception When instantiating fails.
     */
    public DefaultGameView(final AbstractGenericGameOptions genericGameOptions) throws Exception {
        super(genericGameOptions);

        this.movesView = new MovesView(genericGameOptions);
        this.getColumnConstraints().addAll(primaryViewColumn, secondaryViewColumn);
        this.getRowConstraints().addAll(primaryViewRow, secondaryViewRow);

        this.getPerspectiveBoardView().prefWidthProperty().bind(this.widthProperty().multiply(primaryViewColumn.percentWidthProperty().divide(100)));
        this.getPerspectiveBoardView().prefHeightProperty().bind(this.heightProperty());
    }

    /**
     * Builds the view.
     */
    @Override
    public void buildView() {
        this.movesView.build();
        this.getFlatBoardView().getStyleClass().add("flatboard-combined-view");
        StackPane perspectiveView = new StackPane();
        perspectiveView.getChildren().addAll(this.getPerspectiveBoardView(), createPlayerCards());
        this.add(perspectiveView, 0, 0, 1, 2);
        this.add(this.movesView, 1, 0);
        this.add(this.getFlatBoardView(), 1, 1);
    }

    /**
     * Creates the player cards for in the main view.
     *
     * @return The player cards.
     */
    private BorderPane createPlayerCards() {
        MainPlayfieldPlayerCards cardsContainer = new MainPlayfieldPlayerCards(getGenericGameOptions());
        cardsContainer.prefWidthProperty().bind(this.getPerspectiveBoardView().widthProperty());
        StackPane.setAlignment(cardsContainer, Pos.TOP_CENTER);
        return cardsContainer;
    }

}
