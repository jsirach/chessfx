/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

import org.pidome.games.chess.resources.BoardResource;
import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Side;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.engines.HighlightType;
import org.pidome.games.chess.model.options.boards.PathTravelOptions;
import org.pidome.games.chess.resources.ResourceLoadResult;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.world.pieces.AbstractPieces;
import org.pidome.games.chess.world.pieces.PieceInterface;
import org.pidome.games.chess.world.pieces.VisualPiece;

/**
 * Base class for 2D abstract boards.
 *
 * @author johns
 */
public class PerspectiveBoard extends AbstractBoard {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PerspectiveBoard.class);

    /**
     * the board itself as a group.
     *
     * This is currently only available for 3D instances to be able to rotate a
     * board if required.
     */
    private final Group boardGroup = new Group();

    /**
     * Line showing last piece move path for white.
     */
    private final Line lastWhiteLine = new Line();

    /**
     * Line showing last piece move path for black.
     */
    private final Line lastBlackLine = new Line();

    /**
     * Width of the board.
     */
    private final DoubleProperty boardWidth = new SimpleDoubleProperty(0.0);

    /**
     * The location of the board.
     */
    private final String boardLocation;

    /**
     * The last move lines if turned on.
     */
    private final Map<PieceInterface.VariantColor, Line> lastMovesLines = new HashMap<>(2) {
        {
            put(PieceInterface.VariantColor.WHITE, lastWhiteLine);
            put(PieceInterface.VariantColor.BLACK, lastBlackLine);
        }
    };

    /**
     * Constructor.
     *
     * @param boardResource The location of the board to load.
     */
    public PerspectiveBoard(final BoardResource boardResource) {
        this.boardLocation = boardResource.getBoardFile();
    }

    /**
     * @inheritDoc
     */
    @Override
    public void scheduleAssetLoading(final ResourcesLoader resourcesLoader) {
        resourcesLoader.schedule(() -> {
            ResourceLoadResult loadResult = new ResourceLoadResult();
            try {
                this.loadModel(this.boardLocation, this.boardGroup);
                loadResult.complete(() -> {
                    this.getChildren().addAll(boardGroup);
                    this.setBoardSize(new Point2D(this.boardGroup.getLayoutBounds().getWidth(), this.boardGroup.getLayoutBounds().getDepth()));
                });
            } catch (Exception ex) {
                loadResult.complete(ex);
            }
            return loadResult;
        });
    }

    /**
     * Build board additional specific functionalities.
     */
    @Override
    public final void buildAdditionalFeatures() {

        this.boardWidth.setValue(this.boardGroup.getLayoutBounds().getWidth());

        final Rotate rotate = new Rotate();
        rotate.setAxis(Rotate.X_AXIS);
        rotate.setAngle(90);

        lastWhiteLine.setManaged(false);
        lastWhiteLine.setStrokeLineCap(StrokeLineCap.ROUND);
        lastWhiteLine.strokeProperty().bind(this.gameOptions.showTraveledPathProperty().whitePathColorProperty());
        lastWhiteLine.opacityProperty().bind(this.gameOptions.showTraveledPathProperty().pathOpacityProperty());
        lastWhiteLine.strokeWidthProperty().bind(boardWidth.divide(90).divide(PathTravelOptions.DEFAULT_TRAVEL_PATH_THICKNESS).multiply(this.gameOptions.showTraveledPathProperty().lineThicknessProperty()));

        lastBlackLine.setManaged(false);
        lastBlackLine.setStrokeLineCap(StrokeLineCap.ROUND);
        lastBlackLine.strokeProperty().bind(this.gameOptions.showTraveledPathProperty().blackPathColorProperty());
        lastBlackLine.opacityProperty().bind(this.gameOptions.showTraveledPathProperty().pathOpacityProperty());
        lastBlackLine.strokeWidthProperty().bind(boardWidth.divide(90).divide(PathTravelOptions.DEFAULT_TRAVEL_PATH_THICKNESS).multiply(this.gameOptions.showTraveledPathProperty().lineThicknessProperty()));

        lastWhiteLine.getTransforms().add(rotate);
        lastBlackLine.getTransforms().add(rotate);

        lastWhiteLine.translateYProperty().set(getFirstSquareCoordinate().get().getY() - .2);
        lastBlackLine.translateYProperty().set(getFirstSquareCoordinate().get().getY() - .21);

        lastWhiteLine.visibleProperty().bind(this.gameOptions.showTraveledPathProperty().and(lastWhiteLine.endXProperty().isNotEqualTo(0)));
        lastBlackLine.visibleProperty().bind(this.gameOptions.showTraveledPathProperty().and(lastBlackLine.endXProperty().isNotEqualTo(0)));

        this.getChildren().addAll(lastWhiteLine, lastBlackLine);
    }

    /**
     * Draws a line from piece current location to piece new location.
     *
     * @param piece The piece to draw for.
     * @param endSquare The square moving to.
     */
    @Override
    protected void setMoveLine(final VisualPiece piece, final BoardSquare endSquare) {
        final BoardSquare startSquare = this.getBoardSquare(piece);
        if (startSquare != null && endSquare != null && !startSquare.equals(endSquare)) {
            try {
                unbindLastMovesLines(piece.getVariantColor());
                bindLastMovesLines(piece, startSquare);
            } catch (Exception ex) {
                LOG.error("Error on placing line", ex);
            }
        }
    }

    /**
     * Returns the board as a group.
     *
     * Only used for positioning.
     *
     * @return The board as a group.
     */
    protected Group getBoardGroup() {
        return this.boardGroup;
    }

    /**
     * Calculates the distance percentage to travel.
     *
     * @param from The square coming from.
     * @param to The square moving to.
     * @return The distance traveled in percentage in perspective of the board
     * diagonal.
     */
    @Override
    protected double getPieceDistanceTravelPercentage(final BoardSquare from, final BoardSquare to) {
        double result = 100.0;
        if (to != null && from != null) {
            result = (100.0 / this.getBoardDiagonalLength()) * Math.hypot(
                    Math.abs(
                            from.getBoundsInParent().getCenterX() - to.getBoundsInParent().getCenterX()),
                    Math.abs(from.getBoundsInParent().getCenterZ() - to.getBoundsInParent().getCenterZ())
            );
        }
        return result > 100.0 ? 100.0 : result < 20.0 ? 20.0 : result;
    }

    /**
     * Registers the loaded pieces into to the board.
     *
     * This method resizes pieces to board fields.
     *
     * Perspective board: The first calculation is to fit 4 pieces in a field
     * based on field width. The result is then applied to scale all pieces.
     *
     * Flat board: Pieces are fit as such the square image covers 90% of the
     * field.
     *
     * @param piecesSet The pieces set.
     */
    @Override
    public void registerPieces(final AbstractPieces piecesSet) {
        this.getPieces().clear();
        this.getPieces().addAll(piecesSet.getSet());
        final VisualPiece checkPiece = getNextNotInGamePiece(Piece.WHITE_PAWN);
        if (checkPiece.getScaleY() == 1.0) {
            double ratio = ((getFieldSize().get().getX() / 4)
                    / checkPiece.getWidth());
            double resizeRatio = 1 - ratio;
            if (resizeRatio != 0.0) {
                getPieces().forEach(piece -> {
                    Scale scale = new Scale();
                    scale.setX(resizeRatio);
                    scale.setY(resizeRatio);
                    scale.setZ(resizeRatio);
                    scale.setPivotX(0);
                    scale.setPivotY(0);
                    scale.setPivotZ(0);
                    piece.getTransforms().add(scale);
                    piece.setTranslateY(
                            this.getFirstSquareCoordinate().get().getY()
                    );
                });
            }
        }
    }

    /**
     * Registers the fields on a 3D board.
     */
    @Override
    protected final void registerPlayFields() {
        final Point3D bottomLeft = getFirstSquareCoordinate().get();
        for (int row = 0; row < 8; row++) {
            for (int column = 0; column < 8; column++) {
                final BoardSquare square = new BoardSquare(
                        this.gameOptions,
                        true,
                        (row % 2) == 0 ? ((column % 2) == 0 ? Side.WHITE : Side.BLACK) : ((column % 2) == 1 ? Side.BLACK : Side.WHITE),
                        row,
                        column,
                        getFieldSize()
                );
                square.setTranslateX(bottomLeft.getX() + (row * getFieldSize().get().getX()));
                square.setTranslateY(bottomLeft.getY());
                square.setTranslateZ(bottomLeft.getZ() + (column * getFieldSize().get().getY()));

                /*
                final Label label = new Label(BoardSquare.getCombinedSquareIndex(row, column));
                label.setTranslateX(square.getTranslateX() - (getFieldSize().get().getX() / 2));
                label.setTranslateY(square.getTranslateY() - 4);
                label.setTranslateZ(square.getTranslateZ() - (getFieldSize().get().getY() / 2));
                label.setCache(true);
                label.setCacheHint(CacheHint.SPEED);

                this.getBoardGroup().getChildren().add(label);
                 */
                getFields().add(square);
            }
        }
    }

    /**
     * Registers the fields on a 3D board.
     */
    @Override
    protected final void registerRestFields() {
        // get largest piece, so it does not mather how big a piece is
        // being put next to the board so it will always fit without
        // weird visuals.
        final DoubleProperty size = new SimpleDoubleProperty(0);
        for (VisualPiece piece : this.getPieces()) {
            double width = piece.getLayoutBounds().getWidth();
            if (width > size.get()) {
                size.setValue(width);
            }
        }
        final ObjectProperty<Point3D> spaceSize = new SimpleObjectProperty<>() {
            {
                setValue(new Point3D(size.get(), size.get(), 0.0));
            }
        };
        final Point2D restStartLocation = new Point2D(-this.getBoardSize().get().getX() / 2, this.getBoardSize().get().getY() / 2);
        for (int side = 1; side < 3; side++) {
            for (int sidePieces = 0; sidePieces < 16; sidePieces++) {
                final BoardSquare square = new BoardSquare(
                        this.gameOptions,
                        true,
                        side == 1 ? Side.WHITE : Side.BLACK,
                        side,
                        sidePieces,
                        spaceSize,
                        true
                );
                double xSide = restStartLocation.getX() - (((sidePieces % 2) + 1) * size.get());
                double zSide = restStartLocation.getY() - (Math.round(sidePieces / 2) * size.get());
                square.setTranslateX(square.getSide().equals(Side.WHITE) ? xSide : Math.abs(xSide));
                square.setTranslateZ(square.getSide().equals(Side.WHITE) ? -zSide : zSide);
                getFields().add(square);
            }
        }
    }

    /**
     * Destroys the board.
     */
    @Override
    protected void destroyAdditionalFeatures() {
        super.destroy();
        lastWhiteLine.strokeProperty().unbind();
        lastWhiteLine.opacityProperty().unbind();
        lastBlackLine.strokeProperty().unbind();
        lastBlackLine.opacityProperty().unbind();
    }

    /**
     * The most bottom left centered playfield <code>A1</code>.
     *
     * @return Point with 3D coordinates.
     */
    @Override
    public final ObjectProperty<Point3D> getFirstSquareCoordinate() {
        return this.getModelConfig().bottomLeftCenteredProperty();
    }

    /**
     * Return the field size.
     *
     * @return The field size.
     */
    @Override
    public final ObjectProperty<Point3D> getFieldSize() {
        return this.getModelConfig().fieldSizeProperty();
    }

    /**
     * Removes binding from a follow line.
     *
     * @param variantColor The color to unbind for.
     */
    private void bindLastMovesLines(final VisualPiece piece, final BoardSquare startSquare) {
        lastMovesLines.get(piece.getVariantColor()).startXProperty().bind(startSquare.translateXProperty());
        lastMovesLines.get(piece.getVariantColor()).startYProperty().bind(startSquare.translateZProperty());
        lastMovesLines.get(piece.getVariantColor()).endXProperty().bind(piece.translateXProperty());
        lastMovesLines.get(piece.getVariantColor()).endYProperty().bind(piece.translateZProperty());
    }

    /**
     * Removes binding from a follow line.
     *
     * @param variantColor The color to unbind for.
     */
    private void unbindLastMovesLines(PieceInterface.VariantColor variantColor) {
        lastMovesLines.get(variantColor).endXProperty().unbind();
        lastMovesLines.get(variantColor).endYProperty().unbind();
        lastMovesLines.get(variantColor).startXProperty().unbind();
        lastMovesLines.get(variantColor).startYProperty().unbind();
    }

    /**
     * Hides last move lines.
     */
    private void hideLastMoveLines(PieceInterface.VariantColor variantColor) {
        lastMovesLines.get(variantColor).endXProperty().setValue(0.0);
        lastMovesLines.get(variantColor).endYProperty().setValue(0.0);
        lastMovesLines.get(variantColor).startXProperty().setValue(0.0);
        lastMovesLines.get(variantColor).startYProperty().setValue(0.0);
    }

    /**
     * {@inheritDoc}
     */
    public void cleanAdditionalFeatures() {
        unbindLastMovesLines(PieceInterface.VariantColor.WHITE);
        hideLastMoveLines(PieceInterface.VariantColor.WHITE);
        unbindLastMovesLines(PieceInterface.VariantColor.BLACK);
        hideLastMoveLines(PieceInterface.VariantColor.BLACK);
        this.getFields().forEach(square -> {
            square.markSquare(HighlightType.NONE);
        });
    }

}
