/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point3D;

/**
 * The model config for a board.
 *
 * A board needs more configuration then a single model. A board model contains
 * location of the lowest left play field in 3D and the size of it for proper
 * placements of all pieces.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class BoardModelConfig extends ModelConfig {

    /**
     * The object used for bottomLeftCentered using Point3D.
     */
    @JsonIgnore
    private Location3D locationBottomLeftCentered;

    /**
     * The most bottom left centered playfield.
     */
    @JsonIgnore
    private final ObjectProperty<Point3D> bottomLeftCentered = new SimpleObjectProperty<>();

    /**
     * The object used for fieldSize using Point3D.
     */
    @JsonIgnore
    private Location3D locationFieldSize;

    /**
     * The size of a single field on the board.
     */
    @JsonIgnore
    private final ObjectProperty<Point3D> fieldSize = new SimpleObjectProperty<>();

    /**
     * Returns the exact center point in 3D space within board model for a
     * field.
     *
     * @return The center location in the bottom left field in 3D space.
     */
    @JsonGetter
    public Location3D getBottomLeftCentered() {
        return this.locationBottomLeftCentered;
    }

    /**
     * Set the exact center point in 3D space within board model for a field.
     *
     * @param location3D The points to set for Point3D.
     */
    @JsonSetter
    public void setBottomLeftCentered(final Location3D location3D) {
        this.locationBottomLeftCentered = location3D;
        this.bottomLeftCentered.setValue(
                new Point3D(
                        locationBottomLeftCentered.getX(),
                        locationBottomLeftCentered.getY(),
                        locationBottomLeftCentered.getZ()
                )
        );
    }

    /**
     * Returns the field size in 3D.
     *
     * @return The field size.
     */
    @JsonGetter
    public Location3D getFieldSize() {
        return locationFieldSize;
    }

    /**
     * Returns the field size in 3D.
     *
     * The three parameters are set for x and y for surface size.
     *
     * @param location3D dimensions for the fields size.
     */
    @JsonSetter
    public void setFieldSize(final Location3D location3D) {
        this.locationFieldSize = location3D;
        this.fieldSize.setValue(
                new Point3D(
                        locationFieldSize.getX(),
                        locationFieldSize.getY(),
                        locationFieldSize.getZ()
                )
        );
    }

    /**
     * Returns the exact center point in 3D space within board model.
     *
     * @return The center location in the bottom left field in 3D space.
     */
    @JsonIgnore
    public ObjectProperty<Point3D> bottomLeftCenteredProperty() {
        return this.bottomLeftCentered;
    }

    /**
     * Returns the field size in 3D.
     *
     * @return The object property with the Point3D.
     */
    @JsonIgnore
    public final ObjectProperty<Point3D> fieldSizeProperty() {
        return fieldSize;
    }

}
