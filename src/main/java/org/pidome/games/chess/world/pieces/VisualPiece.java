/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.pieces;

import com.github.bhlangonijr.chesslib.Piece;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import org.pidome.games.chess.world.config.PieceModelConfig;
import org.pidome.games.chess.world.config.WorldModel;

/**
 * A piece to show.
 *
 * @author John Sirach
 */
public abstract class VisualPiece extends WorldModel<PieceModelConfig> implements PieceInterface {

    /**
     * Piece variant.
     */
    private final Variant variant;
    /**
     * Piece color.
     */
    private final VariantColor color;

    /**
     * If this piece is on the board or not.
     */
    private boolean inGame = false;

    /**
     * If the piece is marked.
     */
    private boolean marked = false;

    /**
     * If the piece is a perspective based piece.
     */
    private final boolean isPerspective;

    /**
     * The rules engine piece.
     */
    private final Piece rulesEnginePiece;

    /**
     * Abstract constructor.
     *
     * @param isPerspective to identify this piece as 3D or 2D.
     * @param variant The piece variant.
     * @param color The piece color.
     */
    public VisualPiece(final boolean isPerspective, final Variant variant, final VariantColor color) {
        super(PieceModelConfig.class);
        this.isPerspective = isPerspective;
        this.variant = variant;
        this.color = color;
        rulesEnginePiece = determineRulesEnginePiece(variant, color);
    }

    /**
     * Returns the piece variant.
     *
     * @return The variant.
     */
    public Variant getVariant() {
        return this.variant;
    }

    /**
     * Returns the variant color.
     *
     * @return The variant color.
     */
    public VariantColor getVariantColor() {
        return this.color;
    }

    /**
     * Loads from a given location.
     *
     * @param location Loads from a given location.
     * @param modelConfig The model configuration.
     * @throws PieceLoadException When the piece can not be loaded.
     */
    public abstract void load(String location, PieceModelConfig modelConfig) throws PieceLoadException;

    /**
     * Returns the piece for the rules engine.
     *
     * @return The rules engine piece.
     */
    public final Piece getRulesEnginePiece() {
        return rulesEnginePiece;
    }

    /**
     * If the piece is a 3D or 2D piece.
     *
     * @return The piece perspective type.
     */
    public boolean isPerspective() {
        return this.isPerspective;
    }

    /**
     * If a piece is used on the board.
     *
     * @return if used on the board.
     */
    public boolean isInGame() {
        return this.inGame;
    }

    /**
     * If this piece is in game or not.
     *
     * @param isInGame set if the piece is in game.
     */
    public void inGame(boolean isInGame) {
        this.inGame = isInGame;
    }

    /**
     * Returns the width of the visual piece.
     *
     * @return the width of a piece.
     */
    public abstract double getWidth();

    /**
     * @return the marked
     */
    public boolean isMarked() {
        return marked;
    }

    /**
     * @param marked the marked to set
     */
    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    /**
     * To have the piece ignore any mouse events.
     *
     * @param ignore true to ignore, false to enable.
     */
    public void ignoreMouse(final boolean ignore) {
        if (this.getChildren().size() > 0) {
            final Node node = this.getChildren().get(0);
            if (node instanceof StackPane && ((StackPane) node).getChildren().size() > 0) {
                ((StackPane) node).getChildren().get(0).setMouseTransparent(ignore);
            }
            node.setMouseTransparent(ignore);
        }
    }

    /**
     * This as string.
     *
     * @return this as string,
     */
    @Override
    public String toString() {
        return new StringBuilder("VisualPiece: [Variant: ").append(variant)
                .append(", VariantColor:").append(color)
                .append(", inGame:").append(inGame)
                .append(", marked:").append(marked)
                .append(", isPerspective:").append(isPerspective)
                .append(", rulesEnginePiece:").append(rulesEnginePiece)
                .append("]").toString();
    }

}
