/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.pieces;

import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.PieceType;

/**
 * A piece base.
 *
 * @author John Sirach
 */
public interface PieceInterface {

    /**
     * The piece color.
     */
    public enum VariantColor {
        /**
         * A white piece.
         */
        WHITE("white"),
        /**
         * A black piece.
         */
        BLACK("black");

        /**
         * Textual representation.
         *
         * Used for loading purposes.
         */
        private final String text;

        /**
         * Private constructor.
         *
         * @param text text representation.
         */
        private VariantColor(final String text) {
            this.text = text;
        }

        /**
         * Returns the textual representation.
         *
         * @return The text used for loading.
         */
        public String getText() {
            return this.text;
        }

    }

    /**
     * Piece variant type.
     */
    public enum Variant {
        /**
         * King.
         */
        K("king", 1),
        /**
         * Queen.
         */
        Q("queen", 1),
        /**
         * Rook.
         */
        R("rook", 2),
        /**
         * Bishop.
         */
        B("bishop", 2),
        /**
         * Knight.
         */
        N("knight", 2),
        /**
         * Pawn.
         */
        P("pawn", 8);

        /**
         * Textual representation.
         *
         * Used for loading purposes.
         */
        private final String text;

        /**
         * Pieces amount per color set.
         */
        private final int piecesCount;

        /**
         * Private constructor.
         *
         * @param text text representation.
         * @param piecesCount The amount of pieces required per color set.
         */
        private Variant(final String text, final int piecesCount) {
            this.text = text;
            this.piecesCount = piecesCount;
        }

        /**
         * Returns the textual representation.
         *
         * @return The text used for loading.
         */
        public String getText() {
            return this.text;
        }

        /**
         * The amount of pieces per color set.
         *
         * @return The amount of pieces per color set.
         */
        public int getPiecesCount() {
            return this.piecesCount;
        }

        /**
         * Returns the variant by given piece type.
         *
         * Text given is automatically converted to lowercase to do the check.
         *
         * @param type The type to get the variant for.
         * @return The variant, null if not found.
         */
        public static Variant getByType(final String type) {
            for (Variant variant : Variant.values()) {
                if (variant.getText().equals(type.toLowerCase())) {
                    return variant;
                }
            }
            return null;
        }

        /**
         * Returns the variant by given piece type.
         *
         * Text given is automatically converted to lowercase to do the check.
         *
         * @param type The type to get the variant for.
         * @return The variant, null if not found.
         */
        public static Variant getByType(final PieceType type) {
            return getByType(type.name().toLowerCase());
        }

    }

    /**
     * Returns a rule engine piece based on given color and variant.
     *
     * @param variant The variant of the piece.
     * @param variantColor The color of the piece.
     * @return The piece based on variant and color, when not found NONE
     */
    public default Piece determineRulesEnginePiece(final Variant variant, final VariantColor variantColor) {
        switch (variantColor) {
            case WHITE:
                return determineRulesEngineWhitePiece(variant);
            case BLACK:
                return determineRulesEngineBlackPiece(variant);
        }
        return Piece.NONE;
    }

    /**
     * Returns a rule engine white piece based on given variant.
     *
     * @param variant The variant of the piece.
     * @return The white piece based on variant, when not found NONE.
     */
    public default Piece determineRulesEngineWhitePiece(final Variant variant) {
        switch (variant) {
            case K:
                return Piece.WHITE_KING;
            case Q:
                return Piece.WHITE_QUEEN;
            case R:
                return Piece.WHITE_ROOK;
            case B:
                return Piece.WHITE_BISHOP;
            case N:
                return Piece.WHITE_KNIGHT;
            case P:
                return Piece.WHITE_PAWN;
        }
        return Piece.NONE;
    }

    /**
     * Returns a rule engine black piece based on given variant.
     *
     * @param variant The variant of the piece.
     * @return The black piece based on variant, when not found NONE.
     */
    public default Piece determineRulesEngineBlackPiece(final Variant variant) {
        switch (variant) {
            case K:
                return Piece.BLACK_KING;
            case Q:
                return Piece.BLACK_QUEEN;
            case R:
                return Piece.BLACK_ROOK;
            case B:
                return Piece.BLACK_BISHOP;
            case N:
                return Piece.BLACK_KNIGHT;
            case P:
                return Piece.BLACK_PAWN;
        }
        return Piece.NONE;
    }

}
