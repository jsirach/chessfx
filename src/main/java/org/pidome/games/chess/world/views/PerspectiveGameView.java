/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.world.hud.MainPlayfieldPlayerCards;

/**
 * A container for the view of a game.
 *
 * the desktop game frame consist of a 3D and a 2D game view.
 *
 * @author John
 */
@GameView(
        name = "Leisure",
        description = "A non head to head view in a simple rustic view.",
        boardViewType = GameOptions.GameViewType.TypeConstant.PERSPECTIVE
)
public class PerspectiveGameView extends AbstractGameView {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PerspectiveGameView.class);

    /**
     * The tablet shown.
     */
    //private final TabletView tabletView;
    /**
     * When the in scene tablet has been clicked.
     */
    private final BooleanProperty tabletViewEnabled = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * Primary view column.
     */
    private final ColumnConstraints primaryViewColumn = new ColumnConstraints() {
        {
            setPercentWidth(100);
        }
    };

    /**
     * Primary view row.
     */
    private final RowConstraints primaryViewRow = new RowConstraints() {
        {
            setPercentHeight(100);
        }
    };

    /**
     * View constructor.
     *
     * @param genericGameOptions Game options when spectating.
     * @throws java.lang.Exception When the game scene is unable to be loaded.
     */
    public PerspectiveGameView(final AbstractGenericGameOptions genericGameOptions) throws Exception {
        super(genericGameOptions);
        this.getColumnConstraints().add(primaryViewColumn);
        this.getRowConstraints().add(primaryViewRow);
        this.widthProperty().addListener((obsValue, oldValue, newValue) -> {
            this.getPerspectiveBoardView().setPrefWidth(newValue.doubleValue());
        });
        this.heightProperty().addListener((obsValue, oldValue, newValue) -> {
            this.getPerspectiveBoardView().setPrefHeight(newValue.doubleValue());
        });
        //tabletView = new TabletView(gameHash, genericGameOptions, this.getFlatBoardView());
        //tabletView.loadAssets();
    }

    /**
     * Building up.
     */
    @Override
    public void buildView() {
        StackPane perspectiveView = new StackPane();
        perspectiveView.getChildren().addAll(this.getPerspectiveBoardView(), createPlayerCards());
        this.add(perspectiveView, 0, 0);
        /*
        if (tabletView != null) {
            tabletViewEnabled.addListener((obsVal, oldVal, newVal) -> {
                if (newVal) {
                    this.getPerspectiveBoardView().setOrientationEnabled(false);
                    this.getPerspectiveBoardView().setCameraStartPosition(true);

                    // Calculations are in radians
                    double radians = Math.toRadians(PerspectiveSceneView.getDefaultCameraStartXAngle());
                    // double radians = Math.toRadians(this.perspectiveView.getCameraTransformer().rx.getAngle());

                    /// Calculate the distance of the camera on y and z axis.
                    //double horizontalDistance = Math.abs(this.perspectiveView.getCameraTransformer().getLayoutBounds().getMaxZ()) * Math.cos(radians);
                    double horizontalDistance = Math.abs(PerspectiveSceneView.getDefaultCameraStartZIndex()) * Math.cos(radians);
                    double heightDistance = horizontalDistance * Math.tan(radians);

                    // Distance required to be away from the camera to fill the view vertically in the current FOV
                    // and give some slack room to see the some of the scene above and below the object.
                    double distanceFromCamera = (tabletView.getBoundsInParent().getHeight() / 2) / Math.tan(Math.toRadians(this.getPerspectiveBoardView().getPerspectiveCamera().getFieldOfView() / 2)) * 1.15;

                    /// Calculate how much to subtract from the current camera position for y and z axis compensation.
                    double horizontalDistanceSubtract = Math.abs(distanceFromCamera) * Math.cos(radians);
                    double heightDistanceSubtract = horizontalDistanceSubtract * Math.tan(radians);

                    tabletView.details(0,
                            (heightDistance - heightDistanceSubtract),
                            -(horizontalDistance - horizontalDistanceSubtract),
                            90 + PerspectiveSceneView.getDefaultCameraStartXAngle(), //this.perspectiveView.getCameraTransformer().rx.getAngle(),
                            0,
                            0,
                            true);
                } else {
                    this.getPerspectiveBoardView().setOrientationEnabled(true);
                    this.getPerspectiveBoardView().setCameraPreviousPosition(true);
                    this.tabletView.goStartPosition(true);
                }
            });

            tabletView.setOnMouseClicked(event -> {
                tabletViewEnabled.setValue(!tabletViewEnabled.getValue());
            });
            tabletView.build();
            this.getPerspectiveBoardView().add3dObjects(tabletView);
        }
         */
    }

    /**
     * Creates the player cards for in the main view.
     *
     * @return The player cards.
     */
    private BorderPane createPlayerCards() {
        MainPlayfieldPlayerCards cardsContainer = new MainPlayfieldPlayerCards(getGenericGameOptions());
        cardsContainer.prefWidthProperty().bind(this.getPerspectiveBoardView().widthProperty());
        StackPane.setAlignment(cardsContainer, Pos.TOP_CENTER);
        return cardsContainer;
    }

}
