/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

/**
 * Exception thrown when a board does not load.
 *
 * @author John Sirach
 */
public class BoardLoadException extends Exception {

    /**
     * Version.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of <code>BoardLoadException</code> without detail
     * message.
     */
    public BoardLoadException() {
    }

    /**
     * Constructs an instance of <code>BoardLoadException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     * @param throwable The original exception.
     */
    public BoardLoadException(final String msg, final Throwable throwable) {
        super(msg, throwable);
    }
}
