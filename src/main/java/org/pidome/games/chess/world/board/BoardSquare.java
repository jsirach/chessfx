/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.engines.HighlightType;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.settings.GraphicsSettings;
import org.pidome.games.chess.settings.Settings;
import org.pidome.games.chess.world.pieces.VisualPiece;

/**
 * A single square on a board.
 *
 * @author John Sirach
 */
public class BoardSquare extends Group {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(BoardSquare.class);

    /**
     * Row index.
     */
    private static final String FILES[] = {"A", "B", "C", "D", "E", "F", "G", "H"};

    /**
     * Rectangle used for highlighting fields.
     */
    private final Rectangle highlightRectangle = new Rectangle();

    /**
     * The index as a string.
     */
    private final String indexAsString;

    /**
     * The square as known by the rules engine.
     */
    private final Square indexForRuleEngine;

    /**
     * Default color not marked.
     */
    private static final Color DEFAULT_UNCOLORED = Color.TRANSPARENT;

    /**
     * Marked error on black fields.
     */
    private static final Color COLOR_MATE = new Color(1.0, 0.0, 0.0, 1.0);
    /**
     * Marked error on white fields.
     */
    private static final Color COLOR_DANGER = new Color(1.0, 1.0, 0.0, 1.0);
    /**
     * Allowed move.
     */
    private static final Color COLOR_ALLOWED = new Color(0.22, 1.0, 0.0, 1.0);

    /**
     * When a field is selected.
     */
    private static final Color COLOR_SELECT = Color.CHOCOLATE;

    /**
     * Color used to determine field size during development.
     */
    private static final Color SIZE_COLOR = new Color(1.0, .0, .0, 0.2);

    /**
     * The piece on this field.
     */
    private VisualPiece piece;

    /**
     * If the field should be marked as black colored.
     */
    private final Side side;

    /**
     * If in perspective mode or not.
     */
    private final boolean perspective;

    /**
     * If this field is used to place captured pieces.
     */
    private final boolean restField;

    /**
     * Field width.
     */
    private final DoubleProperty fieldWidth = new SimpleDoubleProperty();

    /**
     * Field height.
     */
    private final DoubleProperty fieldHeight = new SimpleDoubleProperty();

    /**
     * Highlighting listener.
     */
    private final ObjectProperty<HighlightType> highlighting = new SimpleObjectProperty<>(HighlightType.NONE);

    /**
     * The field size.
     */
    private final ObjectProperty<Point3D> fieldSize;

    /**
     * the generic game options.
     */
    private AbstractGenericGameOptions gameOptions;

    /**
     * The graphics settings.
     */
    private final GraphicsSettings graphicsSettings;

    /**
     * Constructor.
     *
     * @param gameOptions the generic game options.
     * @param perspective If the field is in 3D mode.
     * @param side If the field is a black square.
     * @param row The field row.
     * @param column The field column.
     * @param fieldSize The size of the field.
     */
    public BoardSquare(final AbstractGenericGameOptions gameOptions, final boolean perspective, final Side side, final int row, final int column, final ObjectProperty<Point3D> fieldSize) {
        this(gameOptions, perspective, side, row, column, fieldSize, false);
    }

    /**
     * Constructor.
     *
     * @param gameOptions the generic game options.
     * @param perspective If the field is in 3D mode.
     * @param side If the field is a black square.
     * @param row The field row.
     * @param column The field column.
     * @param fieldSize The size of the field.
     * @param restField If the field is used for a captured piece.
     */
    public BoardSquare(final AbstractGenericGameOptions gameOptions, final boolean perspective, final Side side, final int row, final int column, final ObjectProperty<Point3D> fieldSize, final boolean restField) {
        fieldWidth.setValue(fieldSize.getValue().getX());
        fieldHeight.setValue(fieldSize.getValue().getY());
        this.perspective = perspective;
        this.restField = restField;
        this.gameOptions = gameOptions;
        this.highlightRectangle.widthProperty().bind(fieldWidth);
        this.highlightRectangle.heightProperty().bind(fieldHeight);
        if (this.perspective) {
            this.highlightRectangle.setTranslateY(fieldSize.getValue().getZ());
            this.highlightRectangle.setRotationAxis(Rotate.X_AXIS);
            this.highlightRectangle.setRotate(90);
            this.highlightRectangle.translateXProperty().bind(this.highlightRectangle.widthProperty().divide(2).negate());
        }
        this.highlightRectangle.setFill(DEFAULT_UNCOLORED);
        this.getChildren().add(this.highlightRectangle);
        if (!this.restField) {
            this.indexAsString = getCombinedSquareIndex(row, column);
            this.indexForRuleEngine = Square.fromValue(this.indexAsString);
        } else {
            this.indexForRuleEngine = Square.NONE;
            this.indexAsString = "REST_" + side.name() + "_" + row + "_" + column;
        }
        this.side = side;
        highlighting.addListener((obsValue, oldValue, newValue) -> {
            markSquare(newValue);
        });
        this.fieldSize = fieldSize;
        this.fieldSize.addListener(this::fieldSizeListener);
        this.graphicsSettings = Settings.get(GraphicsSettings.class);
    }

    /**
     * Destroys the field.
     */
    public final void destroy() {
        this.fieldSize.removeListener(this::fieldSizeListener);
        this.piece = null;
    }

    /**
     * Method to be bound to the fieldsize listener
     *
     * @param obsValue The observable value.
     * @param oldValue The old value.
     * @param newValue The new value.
     */
    private void fieldSizeListener(final ObservableValue obsValue, final Point3D oldValue, final Point3D newValue) {
        fieldWidth.setValue(newValue.getX());
        fieldHeight.setValue(newValue.getY());
        if (this.piece != null) {
            this.piece.setTranslateX(this.getTranslateX());
            this.piece.setTranslateY(this.getTranslateY());
        }
    }

    /**
     * Returns the side.
     *
     * @return The side.
     */
    public final Side getSide() {
        return this.side;
    }

    /**
     * Make final.
     *
     * @return Children.
     */
    @Override
    public final ObservableList<Node> getChildren() {
        return super.getChildren();
    }

    /**
     * To mark the step as possible allowed step.
     *
     * @param type The highlight type.
     */
    protected final void setMark(final HighlightType type) {
        if (!highlighting.getValue().equals(type)) {
            if (!type.equals(HighlightType.NONE) && !type.isAttackMark()) {
                if (this.piece != null) {
                    this.piece.setMouseTransparent(true);
                }
            } else {
                if (this.piece != null) {
                    this.piece.setMouseTransparent(false);
                }
            }
            highlighting.setValue(type);
        }
    }

    /**
     * Returns the current highlighting.
     *
     * @return The current highlight type.
     */
    public final HighlightType getCurrentHighlighting() {
        return this.highlighting.getValue();
    }

    /**
     * Marks the field with the given type.
     *
     * @param type The color highlight type to mark this field.
     */
    protected void markSquare(final HighlightType type) {
        Color markColor = DEFAULT_UNCOLORED;
        if (!type.equals(HighlightType.NONE)) {
            switch (type) {
                case SELECT:
                    markColor = COLOR_SELECT;
                    break;
                case MOVE:
                    if (this.gameOptions.showLegalStepsProperty().get()) {
                        markColor = COLOR_ALLOWED;
                    }
                    break;
                case CHECK:
                    if (this.gameOptions.showKingCheckProperty().get()) {
                        markColor = COLOR_DANGER;
                    }
                    break;
                case CHECK_MATE:
                case STALE_MATE:
                    markColor = COLOR_MATE;
                    break;
                default:
                    break;
            }
        }
        if (!markColor.equals(this.highlightRectangle.getFill())) {
            this.highlightRectangle.setFill(markColor);
        }
    }

    /**
     * The piece to drop.
     *
     * TODO: Check settings if animation is enabled.
     *
     * @param piece The piece to set.
     * @param distancePercentage The distance related to full board diagonal a
     * piece needs to travel in percentages.
     */
    public void setPiece(final VisualPiece piece, final double distancePercentage) {
        this.setPiece(piece, distancePercentage, null);
    }

    /**
     * The piece to drop.
     *
     * TODO: Check settings if animation is enabled.
     *
     * @param piece The piece to set.
     * @param distancePercentage The distance related to full board diagonal a
     * piece needs to travel in percentages.
     * @param replacementPiece The replacement piece. When not null the original
     * piece will be replaced with the given one.
     */
    public void setPiece(final VisualPiece piece, final double distancePercentage, final VisualPiece replacementPiece) {
        removePiece();
        if (replacementPiece != null) {
            piece.inGame(false);
            replacementPiece.inGame(true);
            this.piece = replacementPiece;
        } else {
            this.piece = piece;
            this.piece.inGame(true);
        }
        if (distancePercentage != 0) {
            if (perspective) {
                set3DPiece(piece, distancePercentage, replacementPiece);
            } else {
                set2DPiece(piece, distancePercentage, replacementPiece);
            }
        } else {
            BoardAnimationsHelper.noMotion(this, piece, replacementPiece);
        }
    }

    private void set3DPiece(final VisualPiece piece, final double distancePercentage, final VisualPiece replacementPiece) {
        if (graphicsSettings.motion3DPieces().getValue().equals(Boolean.TRUE)) {
            BoardAnimationsHelper.perspectiveMotion(this, piece, distancePercentage, replacementPiece);
        } else {
            BoardAnimationsHelper.noMotion(this, piece, replacementPiece);
        }
    }

    private void set2DPiece(final VisualPiece piece, final double distancePercentage, final VisualPiece replacementPiece) {
        if (graphicsSettings.motion2DPieces().getValue().equals(Boolean.TRUE)) {
            BoardAnimationsHelper.flatMotion(this, piece, distancePercentage, replacementPiece);
        } else {
            BoardAnimationsHelper.noMotion(this, piece, replacementPiece);
        }
    }

    /**
     * Removes the registered piece.
     */
    public void removePiece() {
        this.piece = null;
    }

    /**
     * If this field occupies a piece.
     *
     * @return If a piece is placed.
     */
    public boolean hasPiece() {
        return this.piece != null;
    }

    /**
     * Returns the current piece.
     *
     * @return The current piece on this square.
     */
    public VisualPiece getVisualPiece() {
        return this.piece;
    }

    /**
     * Returns the square identifiable by the rules engine.
     *
     * @return The rules engine compatible square on the board.
     */
    public String getIndex() {
        return this.indexAsString;
    }

    /**
     * Returns the square identifiable by the rules engine.
     *
     * @return The rules engine compatible square on the board.
     */
    public Square getIndexForRuleEngine() {
        return this.indexForRuleEngine;
    }

    /**
     * Returns the field notation based on board rows.
     *
     * @param row The horizontal rows.
     * @param column The columns.
     * @return The field notation.
     */
    protected static final String getCombinedSquareIndex(final int row, final int column) {
        return getBoardColumnFromIndex(row) + getBoardRowFromIndex(column + 1);
    }

    /**
     * Returns column from index.
     *
     * @param index The column index.
     * @return The column notation.
     */
    private static String getBoardColumnFromIndex(final int index) {
        return FILES[index];
    }

    /**
     * Returns the files naming fields.
     *
     * @return The files.
     */
    public static final String[] getFiles() {
        return FILES;
    }

    /**
     * Returns the the row index.
     *
     * Be aware, the board numeric index is 0 based, the returned index is 1
     * based.
     *
     * @param index The index on the board.
     * @return The row notation.
     */
    private static String getBoardRowFromIndex(final int index) {
        return String.valueOf(index);
    }

}
