/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.statics.table;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.world.config.StaticWorldModel;

/**
 * A small sidetable.
 *
 * @author John
 */
public class SideTable extends StaticWorldModel {

    /**
     * Class logger.
     */
    private final Logger LOG = LogManager.getLogger(SideTable.class);

    /**
     * Constructor.
     */
    public SideTable() {
        super("/resources/assets/perspective/environment/tables/sidetable/table.obj");
    }

    /**
     * Loads the table.
     */
    @Override
    public void loadAssetOldSchool() {
        try {
            super.loadAssetOldSchool();
            this.setTranslateX(680);
        } catch (Exception ex) {
            LOG.error("Unable to load side table", ex);
        }
    }

}
