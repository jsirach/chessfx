/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.pieces;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.pidome.games.chess.resources.GamePiece;
import org.pidome.games.chess.resources.ResourceLoadResult;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.utils.SerializationUtil;
import org.pidome.games.chess.world.config.PiecesModelConfig;

/**
 * Load perspective pieces.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public final class PerspectivePieces extends AbstractPieces {

    /**
     * Pieces configuration.
     */
    private PiecesModelConfig piecesConfig;

    /**
     * The configuration file for gathering the material configuration.
     */
    private final String configFile;

    /**
     * Constructor.
     *
     * @param path The pieces path.
     * @param configFile The pieces configuration file.
     */
    public PerspectivePieces(final String path, final String configFile) {
        super(path, GamePiece.GamepieceType.PIECE_3D);
        this.configFile = getMaterialConfigFile(configFile);
    }

    /**
     * Returns the path to the material configuration file.
     *
     * @param path The path to the pieces.
     * @param configFile The name of the main configuration file.
     * @return full path to the material configuration file.
     */
    private String getMaterialConfigFile(final String configFile) {
        Properties props = new Properties();
        try ( InputStream propsIS = new FileInputStream("." + getPiecesBaseLocation() + configFile)) {
            props.load(propsIS);
        } catch (Exception ex) {
            Logger.getLogger(PerspectivePieces.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "." + getPiecesBaseLocation() + props.getProperty("CONFIG");
    }

    /**
     * @inhertDoc
     */
    @Override
    public final void scheduleAssetLoading(final ResourcesLoader resourcesLoader) {
        resourcesLoader.schedule(() -> {
            ResourceLoadResult loadResult = new ResourceLoadResult();
            try {
                piecesConfig = SerializationUtil.getObjectMapper().readValue(
                        new File(this.configFile), PiecesModelConfig.class
                );
                this.loadPieceSet(GamePiece.GamepieceType.PIECE_3D, piecesConfig);
                loadResult.complete();
            } catch (Exception ex) {
                loadResult.complete(ex);
            }
            return loadResult;
        });
    }

}
