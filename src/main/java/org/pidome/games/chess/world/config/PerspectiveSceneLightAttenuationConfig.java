/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Class for applying light attenuation.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PerspectiveSceneLightAttenuationConfig {

    /**
     * The attenuation type.
     */
    public enum Type {
        /**
         * Defaults applied.
         */
        DEFAULT("Default"),
        /**
         * Linear type.
         */
        LINEAR("Linear"),
        /**
         * Quadratic type.
         */
        QUADRATIC("Quadratic"),
        /**
         * Constant type.
         */
        CONSTANT("Constant");

        private final String label;

        Type(final String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }

    }

    /**
     * The attenuation type.
     */
    @JsonIgnore
    private final ObjectProperty<Type> type = new SimpleObjectProperty(Type.DEFAULT);

    /**
     * The amount to apply.
     */
    @JsonIgnore
    private final DoubleProperty value = new SimpleDoubleProperty(0);

    /**
     * The maximum distance to apply to.
     */
    @JsonIgnore
    private final DoubleProperty maxRange = new SimpleDoubleProperty(0);

    /**
     * @return the type
     */
    @JsonGetter
    public Type getType() {
        return type.get();
    }

    /**
     * @param type the type to set
     */
    @JsonSetter
    public void setType(final Type type) {
        this.type.setValue(type);
    }

    /**
     * @return the amount
     */
    @JsonGetter
    public double getValue() {
        return value.getValue();
    }

    /**
     * @param value the value to set
     */
    @JsonSetter
    public void setValue(final double value) {
        this.value.setValue(value);
    }

    /**
     * @return the maximum range.
     */
    @JsonGetter
    public double getMaxRange() {
        return maxRange.getValue();
    }

    /**
     * @param maxRange The maximum range.
     */
    @JsonSetter
    public void setMaxRange(final double maxRange) {
        this.maxRange.setValue(maxRange);
    }

    /**
     * Return the type property.
     *
     * @return The type property.
     */
    @JsonIgnore
    public ObjectProperty<Type> typeProperty() {
        return this.type;
    }

    /**
     * The max range property.
     *
     * @return Max range as property.
     */
    @JsonIgnore
    public DoubleProperty maxRangeProperty() {
        return this.maxRange;
    }

    /**
     * The value as a property.
     *
     * @return The value property.
     */
    public DoubleProperty valueProperty() {
        return this.value;
    }

}
