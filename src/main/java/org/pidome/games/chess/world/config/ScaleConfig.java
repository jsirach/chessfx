/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.scene.transform.Scale;

/**
 * Scale configuration.
 *
 * @author johns
 */
public class ScaleConfig {

    /**
     * The scale object being used.
     */
    private final Scale scale = new Scale();

    /**
     * Set x scaling.
     *
     * @param x scale value.
     */
    @JsonSetter
    public final ScaleConfig setX(final double x) {
        scale.xProperty().set(x);
        return this;
    }

    /**
     * Get x scaling.
     *
     * @return The x scale.
     */
    @JsonGetter
    public final double getX() {
        return scale.xProperty().get();
    }

    /**
     * Set y scale.
     *
     * @param y the y scale value.
     */
    @JsonSetter
    public final ScaleConfig setY(final double y) {
        scale.yProperty().set(y);
        return this;
    }

    /**
     * Return the y scale.
     *
     * @return The y scale value.
     */
    @JsonGetter
    public final double getY() {
        return scale.yProperty().get();
    }

    /**
     * Set the z scale.
     *
     * @param z The z scale value.
     */
    @JsonSetter
    public final ScaleConfig setZ(final double z) {
        scale.zProperty().set(z);
        return this;
    }

    /**
     * Return the z scale.
     *
     * @return The z scale value.
     */
    @JsonGetter
    public final double getZ() {
        return scale.zProperty().get();
    }

    /**
     * Set scale x pivot.
     *
     * @param x The x value.
     * @return this.
     */
    public final ScaleConfig setPivotX(final double x) {
        scale.pivotXProperty().set(x);
        return this;
    }

    /**
     * Returns the scale pivot x.
     *
     * @return The pivot x value.
     */
    public final double getPivotX() {
        return scale.pivotXProperty().get();
    }

    /**
     * Set scale y pivot.
     *
     * @param y The y value.
     * @return this.
     */
    public final ScaleConfig setPivotY(final double y) {
        scale.pivotYProperty().set(y);
        return this;
    }

    /**
     * Returns the scale pivot y.
     *
     * @return The pivot y value.
     */
    public final double getPivotY() {
        return scale.pivotYProperty().get();
    }

    /**
     * Set scale z pivot.
     *
     * @param z The z value.
     * @return this.
     */
    public final ScaleConfig setPivotZ(final double z) {
        scale.pivotZProperty().set(z);
        return this;
    }

    /**
     * Returns the scale pivot z.
     *
     * @return The pivot z value.
     */
    public final double getPivotZ() {
        return scale.pivotZProperty().get();
    }

    /**
     * Returns the scale object.
     *
     * @return The scale object for using internally.
     */
    @JsonIgnore
    public final Scale getScale() {
        return this.scale;
    }

}
