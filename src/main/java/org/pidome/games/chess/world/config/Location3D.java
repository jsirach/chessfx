/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Proxy for point 3D.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class Location3D {

    /**
     * X axis.
     */
    @JsonIgnore
    private final DoubleProperty x = new SimpleDoubleProperty();

    /**
     * Y axis.
     */
    @JsonIgnore
    private final DoubleProperty y = new SimpleDoubleProperty();

    /**
     * Z axis.
     */
    @JsonIgnore
    private final DoubleProperty z = new SimpleDoubleProperty();

    /**
     * @return the x
     */
    @JsonGetter
    public double getX() {
        return x.doubleValue();
    }

    /**
     * @param x the x to set
     */
    @JsonSetter
    public void setX(double x) {
        this.x.setValue(x);
    }

    /**
     * @return the y
     */
    @JsonGetter
    public double getY() {
        return y.doubleValue();
    }

    /**
     * @param y the y to set
     */
    @JsonSetter
    public void setY(double y) {
        this.y.setValue(y);
    }

    /**
     * @return the z
     */
    @JsonGetter
    public double getZ() {
        return z.doubleValue();
    }

    /**
     * @param z the z to set
     */
    @JsonSetter
    public void setZ(double z) {
        this.z.setValue(z);
    }

    /**
     * Bindable X property.
     *
     * @return X.
     */
    @JsonIgnore
    public DoubleProperty xProperty() {
        return this.x;
    }

    /**
     * Bindable Y property.
     *
     * @return Y.
     */
    @JsonIgnore
    public DoubleProperty yProperty() {
        return this.y;
    }

    /**
     * Bindable Z property.
     *
     * @return Z.
     */
    @JsonIgnore
    public DoubleProperty zProperty() {
        return this.z;
    }
}
