/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.config;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 * Color config.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class ColorConfig {

    /**
     * Hue.
     */
    @JsonIgnore
    private DoubleProperty hue = new SimpleDoubleProperty();

    /**
     * Stauration.
     */
    @JsonIgnore
    private DoubleProperty saturation = new SimpleDoubleProperty();

    /**
     * Brightness.
     */
    @JsonIgnore
    private DoubleProperty brightness = new SimpleDoubleProperty();

    /**
     * @return the h
     */
    @JsonGetter
    public double getHue() {
        return hue.doubleValue();
    }

    /**
     * @param h the h to set
     */
    @JsonSetter
    public ColorConfig setHue(double h) {
        this.hue.setValue(h);
        return this;
    }

    /**
     * @return the s
     */
    @JsonGetter
    public double getSaturation() {
        return saturation.doubleValue();
    }

    /**
     * @param s the s to set
     */
    @JsonSetter
    public ColorConfig setSaturation(double s) {
        this.saturation.setValue(s);
        return this;
    }

    /**
     * @return the b
     */
    @JsonGetter
    public double getBrightness() {
        return brightness.doubleValue();
    }

    /**
     * @param b the b to set
     */
    @JsonSetter
    public ColorConfig setBrightness(double b) {
        this.brightness.setValue(b);
        return this;
    }

    /**
     * Property for hue.
     *
     * @return Hue property.
     */
    @JsonIgnore
    public DoubleProperty hueProperty() {
        return this.hue;
    }

    /**
     * Property for saturation.
     *
     * @return Saturation property.
     */
    @JsonIgnore
    public DoubleProperty saturationProperty() {
        return this.saturation;
    }

    /**
     * Property for brightness.
     *
     * @return Brightness property.
     */
    @JsonIgnore
    public DoubleProperty brightnessProperty() {
        return this.brightness;
    }

}
