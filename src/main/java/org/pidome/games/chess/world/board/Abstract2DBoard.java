/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.Side;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.engines.HighlightType;
import org.pidome.games.chess.model.options.boards.PathTravelOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.world.pieces.AbstractPieces;
import org.pidome.games.chess.world.pieces.FlatPiece;
import org.pidome.games.chess.world.pieces.PieceInterface;
import org.pidome.games.chess.world.pieces.VisualPiece;

/**
 * Base class for 2D boards.
 *
 * @author johns
 */
public abstract class Abstract2DBoard extends AbstractBoard {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(Abstract2DBoard.class);

    /**
     * Size of the board in X.
     */
    private final DoubleProperty boardsizeX = new SimpleDoubleProperty();
    /**
     * Size of the board in Y.
     */
    private final DoubleProperty boardsizeY = new SimpleDoubleProperty();

    /**
     * Line showing last piece move path for white.
     */
    private final Line lastWhiteLine = new Line();

    /**
     * Line showing last piece move path for black.
     */
    private final Line lastBlackLine = new Line();

    /**
     * Location of the image to load.
     */
    private final String imageLocation;

    /**
     * Constructor.
     *
     * @param imageLocation The image location.
     */
    public Abstract2DBoard(final String imageLocation) {
        this.imageLocation = imageLocation;
    }

    /**
     * The last move lines if turned on.
     */
    private final Map<PieceInterface.VariantColor, Line> lastMovesLines = new HashMap<>(2) {
        {
            put(PieceInterface.VariantColor.WHITE, lastWhiteLine);
            put(PieceInterface.VariantColor.BLACK, lastBlackLine);
        }
    };

    /**
     * Loads assets from an included application package.
     *
     * @throws BoardLoadException on failing loading.
     */
    protected void loadBoard() throws BoardLoadException {
        try {
            final ImageView imgView = GameResourcesHelper.loadImageViewAssetFromPackage(this.imageLocation);
            this.getChildren().add(imgView);
            this.setBoardSize(new Point2D(imgView.getImage().getWidth(), imgView.getImage().getHeight()));
        } catch (Exception ex) {
            throw new BoardLoadException("Could not load board", ex);
        }
    }

    /**
     * Build board additional specific functionalities.
     */
    @Override
    public final void buildAdditionalFeatures() {
        lastWhiteLine.setManaged(false);
        lastWhiteLine.setMouseTransparent(true);
        lastWhiteLine.setStrokeLineCap(StrokeLineCap.ROUND);
        lastWhiteLine.strokeProperty().bind(this.gameOptions.showTraveledPathProperty().whitePathColorProperty());
        lastWhiteLine.opacityProperty().bind(this.gameOptions.showTraveledPathProperty().pathOpacityProperty());
        lastWhiteLine.strokeWidthProperty().bind(getFlatBoard().widthProperty().divide(90).divide(PathTravelOptions.DEFAULT_TRAVEL_PATH_THICKNESS).multiply(this.gameOptions.showTraveledPathProperty().lineThicknessProperty()));

        lastBlackLine.setManaged(false);
        lastBlackLine.setMouseTransparent(true);
        lastBlackLine.setStrokeLineCap(StrokeLineCap.ROUND);
        lastBlackLine.strokeProperty().bind(this.gameOptions.showTraveledPathProperty().blackPathColorProperty());
        lastBlackLine.opacityProperty().bind(this.gameOptions.showTraveledPathProperty().pathOpacityProperty());
        lastBlackLine.strokeWidthProperty().bind(getFlatBoard().widthProperty().divide(90).divide(PathTravelOptions.DEFAULT_TRAVEL_PATH_THICKNESS).multiply(this.gameOptions.showTraveledPathProperty().lineThicknessProperty()));

        lastWhiteLine.visibleProperty().bind(this.gameOptions.showTraveledPathProperty().and(lastWhiteLine.endXProperty().isNotEqualTo(0)));
        lastBlackLine.visibleProperty().bind(this.gameOptions.showTraveledPathProperty().and(lastBlackLine.endXProperty().isNotEqualTo(0)));

        lastWhiteLine.translateXProperty().bind(getFlatBoard().widthProperty().divide(2));
        lastWhiteLine.translateYProperty().bind(getFlatBoard().heightProperty().divide(2));
        lastBlackLine.translateXProperty().bind(getFlatBoard().widthProperty().divide(2));
        lastBlackLine.translateYProperty().bind(getFlatBoard().heightProperty().divide(2));

        getFlatBoard().getChildren().addAll(lastWhiteLine, lastBlackLine);

    }

    /**
     * Draws a line from piece current location to piece new location.
     *
     * @param piece The piece to draw for.
     * @param endSquare The square moving to.
     */
    @Override
    protected void setMoveLine(final VisualPiece piece, final BoardSquare endSquare) {
        final BoardSquare startSquare = this.getBoardSquare(piece);
        if (startSquare != null && endSquare != null) {
            try {
                unbindLastMovesLines(piece.getVariantColor());
                bindLastMovesLines(piece, startSquare);
            } catch (Exception ex) {
                LOG.error("Error on placing line", ex);
            }
        }
    }

    /**
     * Calculates the distance percentage to travel.
     *
     * @param from The square coming from.
     * @param to The square moving to.
     * @return The distance traveled in percentage in perspective of the board
     * diagonal.
     */
    @Override
    protected double getPieceDistanceTravelPercentage(final BoardSquare from, final BoardSquare to) {
        double result = 100.0;
        if (to != null && from != null) {
            result = (100.0 / this.getBoardDiagonalLength()) * Math.hypot(Math.abs(from.getTranslateX() - to.getTranslateX()), Math.abs(from.getTranslateY() - to.getTranslateY()));
        }
        return result > 100.0 ? 100.0 : result < 20.0 ? 20.0 : result;
    }

    /**
     * Registers the loaded pieces into to the board.
     *
     * Pieces are fit as such the square image covers 90% of the field.
     *
     * @param piecesSet The pieces set.
     */
    @Override
    public void registerPieces(final AbstractPieces piecesSet) {
        this.getPieces().clear();
        this.getPieces().addAll(piecesSet.getSet());
        final VisualPiece checkPiece = getNextNotInGamePiece(Piece.WHITE_PAWN);
        if (checkPiece.getScaleY() == 1.0) {
            getPieces().forEach(piece -> {
                ((FlatPiece) piece).setResizeRatio(getFieldSize().get().getX() * .9);
            });
        }
        getFieldSize().addListener((obsValue, oldValue, newValue) -> {
            getPieces().forEach(piece -> {
                ((FlatPiece) piece).setResizeRatio(newValue.getX() * .9);
            });
        });
    }

    /**
     * Registers the fields on a 2D board.
     */
    @Override
    protected final void registerPlayFields() {
        final ObjectProperty<Point3D> bottomLeft = getFirstSquareCoordinate();
        final DoubleProperty bottomLeftX = new SimpleDoubleProperty(bottomLeft.get().getX());
        final DoubleProperty bottomLeftY = new SimpleDoubleProperty(bottomLeft.get().getY());

        final ObjectProperty<Point3D> fieldSize = getFieldSize();
        final DoubleProperty fieldSizeX = new SimpleDoubleProperty(fieldSize.get().getX());
        final DoubleProperty fieldSizeY = new SimpleDoubleProperty(fieldSize.get().getY());

        for (int row = 0; row < 8; row++) {
            for (int column = 0; column < 8; column++) {
                final BoardSquare square = new BoardSquare(
                        this.gameOptions,
                        false,
                        (row % 2) == 0 ? ((column % 2) == 0 ? Side.WHITE : Side.BLACK) : ((column % 2) == 1 ? Side.BLACK : Side.WHITE),
                        row,
                        column,
                        getFieldSize()
                );
                square.translateXProperty().bind(bottomLeftX.add(fieldSizeX.multiply(row)).subtract(boardsizeX.divide(2)));
                square.translateYProperty().bind(bottomLeftY.add(fieldSizeY.multiply(column).add(boardsizeY.divide(2)).negate()));
                this.getFields().add(square);
            }
        }
        this.getBoardSize().addListener((obsVal, oldVal, newVal) -> {
            boardsizeX.setValue(newVal.getX());
            boardsizeY.setValue(newVal.getY());
        });
        bottomLeft.addListener((obsValue, oldValue, newValue) -> {
            bottomLeftX.setValue(newValue.getX());
            bottomLeftY.setValue(newValue.getY());
        });
        fieldSize.addListener((obsValue, oldValue, newValue) -> {
            fieldSizeX.setValue(newValue.getX());
            fieldSizeY.setValue(newValue.getY());
        });
    }

    /**
     * Registers the fields next to the board.
     */
    @Override
    protected final void registerRestFields() {
        final DoubleProperty size = new SimpleDoubleProperty(0);
        VisualPiece largestPiece = null;
        for (VisualPiece piece : this.getPieces()) {
            double width = piece.getLayoutBounds().getWidth();
            if (width > size.get()) {
                size.setValue(width);
                largestPiece = piece;
            }
        }
        final ObjectProperty<Point3D> spaceSize = new SimpleObjectProperty<>() {
            {
                setValue(new Point3D(size.get(), size.get(), 0.0));
            }
        };
        if (largestPiece != null) {
            largestPiece.layoutBoundsProperty().addListener((obsValue, oldValue, newValue) -> {
                size.setValue(newValue.getWidth());
                spaceSize.setValue(new Point3D(newValue.getWidth(), newValue.getWidth(), 0.0));
            });
        }

        for (int side = 1; side < 3; side++) {
            for (int sidePieces = 0; sidePieces < 16; sidePieces++) {
                final BoardSquare square = new BoardSquare(
                        this.gameOptions,
                        false,
                        side == 1 ? Side.WHITE : Side.BLACK,
                        side,
                        sidePieces,
                        spaceSize,
                        true
                );
                final DoubleProperty xSide = new SimpleDoubleProperty();
                final DoubleProperty ySide = new SimpleDoubleProperty();

                if (square.getSide().equals(Side.WHITE)) {
                    xSide.bind(size.multiply(((sidePieces % 2) + 1)).add(boardsizeX.divide(2)).negate());
                    ySide.bind(size.multiply(Math.round(sidePieces / 2)).negate().add(boardsizeY.divide(2)));
                } else {
                    xSide.bind(size.multiply(sidePieces % 2).add(boardsizeY.divide(2).add(size)));
                    ySide.bind(size.multiply(Math.round(sidePieces / 2)).subtract(boardsizeY.divide(2)));
                }
                square.translateXProperty().bind(xSide);
                square.translateYProperty().bind(ySide);
                getFields().add(square);
            }
        }
        size.addListener((obsVal, oldVal, newVal) -> {
            spaceSize.setValue(new Point3D(newVal.doubleValue(), newVal.doubleValue(), 0.0));
        });
    }

    /**
     * Destroys the board.
     */
    @Override
    protected void destroyAdditionalFeatures() {
        super.destroy();
        lastWhiteLine.strokeProperty().unbind();
        lastWhiteLine.opacityProperty().unbind();
        lastBlackLine.strokeProperty().unbind();
        lastBlackLine.opacityProperty().unbind();
    }

    /**
     * Removes binding from a follow line.
     *
     * @param variantColor The color to unbind for.
     */
    private void bindLastMovesLines(final VisualPiece piece, final BoardSquare startSquare) {
        lastMovesLines.get(piece.getVariantColor()).startXProperty().bind(startSquare.translateXProperty());
        lastMovesLines.get(piece.getVariantColor()).startYProperty().bind(startSquare.translateYProperty());
        lastMovesLines.get(piece.getVariantColor()).endXProperty().bind(piece.translateXProperty());
        lastMovesLines.get(piece.getVariantColor()).endYProperty().bind(piece.translateYProperty());
    }

    /**
     * Removes binding from a follow line.
     *
     * @param variantColor The color to unbind for.
     */
    private void unbindLastMovesLines(PieceInterface.VariantColor variantColor) {
        lastMovesLines.get(variantColor).endXProperty().unbind();
        lastMovesLines.get(variantColor).endYProperty().unbind();
        lastMovesLines.get(variantColor).startXProperty().unbind();
        lastMovesLines.get(variantColor).startYProperty().unbind();
    }

    /**
     * Hides last move lines.
     */
    private void hideLastMoveLines(PieceInterface.VariantColor variantColor) {
        lastMovesLines.get(variantColor).endXProperty().setValue(0.0);
        lastMovesLines.get(variantColor).endYProperty().setValue(0.0);
        lastMovesLines.get(variantColor).startXProperty().setValue(0.0);
        lastMovesLines.get(variantColor).startYProperty().setValue(0.0);
    }

    /**
     * {@inheritDoc}
     */
    public void cleanAdditionalFeatures() {
        unbindLastMovesLines(PieceInterface.VariantColor.WHITE);
        hideLastMoveLines(PieceInterface.VariantColor.WHITE);
        unbindLastMovesLines(PieceInterface.VariantColor.BLACK);
        hideLastMoveLines(PieceInterface.VariantColor.BLACK);
        this.getFields().forEach(square -> {
            square.markSquare(HighlightType.NONE);
        });
    }

}
