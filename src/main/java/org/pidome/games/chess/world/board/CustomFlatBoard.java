/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.board;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.CacheHint;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import static javafx.scene.layout.Region.USE_PREF_SIZE;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import org.pidome.games.chess.resources.GameBoard;
import org.pidome.games.chess.resources.ResourceLoadResult;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.utils.FXUtils;

/**
 * A flat board with custom presets.
 *
 * @author johns
 */
@GameBoard(
        type = GameBoard.GameboardType.BOARD_CUSTOM,
        name = "Customisable"
)
public class CustomFlatBoard extends Abstract2DBoard {

    /**
     * The layout in which the board is placed.
     */
    private final BorderPane visualBoardPlane = new BorderPane();

    /**
     * The board to show.
     */
    private final GridPane visualBoard = new GridPane();

    /**
     * Default row height.
     */
    private static final double DEFAULT_ROW_HEIGHT = 50;

    /**
     * Default column width.
     */
    private static final double DEFAULT_COLUMN_WIDTH = 50;

    /**
     * The row height.
     */
    private final DoubleProperty rowHeight = new SimpleDoubleProperty(DEFAULT_ROW_HEIGHT);

    /**
     * the row width.
     */
    private final DoubleProperty rowWidth = new SimpleDoubleProperty(DEFAULT_COLUMN_WIDTH);

    /**
     * To bind the borderpane to a fixed width and height size.
     */
    private final DoubleProperty visualplaneconstrainedSize = new SimpleDoubleProperty();

    /**
     * Field size.
     */
    private final ObjectProperty<Point3D> FIELD_SIZE = new SimpleObjectProperty<>() {
        {
            setValue(new Point3D(DEFAULT_COLUMN_WIDTH, DEFAULT_ROW_HEIGHT, 0.0));
        }
    };

    /**
     * the play fields border width.
     */
    private static final double PLAYFIELD_BORDER_WIDTH = 1;

    /**
     * First field position.
     */
    private final ObjectProperty<Point3D> FIELD_POS = new SimpleObjectProperty<>() {
        {
            setValue(new Point3D(
                    (FIELD_SIZE.get().getX() / 2) + PLAYFIELD_BORDER_WIDTH,
                    (FIELD_SIZE.get().getY() * 8) - (FIELD_SIZE.get().getY() / 2) + PLAYFIELD_BORDER_WIDTH, 0
            ));
        }
    };

    /**
     * Files for the right and left axis.
     */
    private final String[] files = BoardSquare.getFiles();

    /**
     * Top notation grid.
     */
    GridPane notationTop = new GridPane();

    /**
     * Bottom notation grid.
     */
    GridPane notationBottom = new GridPane();

    /**
     * Left notation grid.
     */
    GridPane notationLeft = new GridPane();

    /**
     * Right notation grid.
     */
    GridPane notationRight = new GridPane();

    /**
     * Constructor.
     */
    public CustomFlatBoard() {
        super(null);
        notationTop.getStyleClass().add("transparent-background");
        notationBottom.getStyleClass().add("transparent-background");
        notationLeft.getStyleClass().add("transparent-background");
        notationRight.getStyleClass().add("transparent-background");
    }

    /**
     * @inheritDoc
     */
    @Override
    public void scheduleAssetLoading(final ResourcesLoader resourcesLoader) {
        resourcesLoader.schedule(() -> {
            ResourceLoadResult loadResult = new ResourceLoadResult();
            try {
                createNotationGrids();

                visualBoardPlane.setStyle("-fx-background-color: " + FXUtils.colorToRgb(Color.WHITESMOKE));
                visualBoardPlane.getStyleClass().add("flat-board");
                visualBoard.setBorder(
                        new Border(
                                new BorderStroke(Color.DIMGRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))
                        )
                );

                for (int rowFill = 0; rowFill < 8; rowFill++) {
                    final ColumnConstraints boardColumn = new ColumnConstraints();
                    boardColumn.setHgrow(Priority.NEVER);
                    boardColumn.setPercentWidth(12.5);

                    final RowConstraints boardRow = new RowConstraints();
                    boardRow.setVgrow(Priority.NEVER);
                    boardRow.setPercentHeight(12.5);

                    visualBoard.getColumnConstraints().add(boardColumn);
                    visualBoard.getRowConstraints().add(boardRow);

                    for (int colFill = 0; colFill < 8; colFill++) {
                        final StackPane field = new StackPane();
                        field.setStyle("-fx-background-color: " + FXUtils.colorToRgb((rowFill % 2) == 0 ? ((colFill % 2) == 0 ? Color.WHITESMOKE : Color.DIMGRAY) : ((colFill % 2) == 1 ? Color.WHITESMOKE : Color.DIMGRAY)));
                        visualBoard.add(field, colFill, rowFill);
                    }

                }

                BorderPane.setAlignment(notationTop, Pos.CENTER);
                BorderPane.setAlignment(notationBottom, Pos.CENTER);

                BorderPane.setAlignment(notationRight, Pos.CENTER);
                BorderPane.setAlignment(notationLeft, Pos.CENTER);

                visualBoardPlane.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);

                visualBoardPlane.setTop(notationTop);
                visualBoardPlane.setRight(notationRight);
                visualBoardPlane.setBottom(notationBottom);
                visualBoardPlane.setLeft(notationLeft);
                visualBoardPlane.setCenter(visualBoard);

                BorderPane.setAlignment(visualBoard, Pos.CENTER);

                FXUtils.setCache(visualBoardPlane, CacheHint.SPEED);

                loadResult.complete(() -> {
                    this.getFlatBoard().getChildren().add(visualBoardPlane);
                    FIELD_SIZE.addListener((obsValue, oldVal, newVal) -> {
                        rowWidth.setValue(newVal.getX());
                        rowHeight.setValue(newVal.getY());
                        FIELD_POS.setValue(new Point3D(
                                (newVal.getX() / 2) + PLAYFIELD_BORDER_WIDTH,
                                (newVal.getY() * 8) - (newVal.getY() / 2) + PLAYFIELD_BORDER_WIDTH,
                                0
                        ));
                    });

                    this.getFlatBoard().widthProperty().addListener(this::boundBoardSize);
                    this.getFlatBoard().heightProperty().addListener(this::boundBoardSize);

                    visualBoardPlane.prefWidthProperty().bind(visualplaneconstrainedSize);
                    visualBoardPlane.prefHeightProperty().bind(visualplaneconstrainedSize);
                    visualBoardPlane.widthProperty().addListener(this::publicizeUpdatedSize);
                    visualBoardPlane.heightProperty().addListener(this::publicizeUpdatedSize);
                });
            } catch (Exception ex) {
                loadResult.complete(ex);
            }
            return loadResult;
        });
    }

    /**
     * Sets a generic number to bound the playboard to a squared size.
     *
     * @param obsValue The observable value.
     * @param oldValue The original value.
     * @param newValue The new value.
     */
    private void boundBoardSize(final ObservableValue obsValue, final Number oldValue, final Number newValue) {
        final double restpiecesSpacing = newValue.doubleValue() / 4;
        visualplaneconstrainedSize.setValue(
                (this.getFlatBoard().getHeight() > this.getFlatBoard().getWidth() ? this.getFlatBoard().getWidth() : this.getFlatBoard().getHeight())
                - restpiecesSpacing
        );
    }

    /**
     * Publicizes any updates on the main board size.
     *
     * @param obsValue The observable value.
     * @param oldValue The original value.
     * @param newValue The new value.
     */
    private void publicizeUpdatedSize(final ObservableValue obsValue, final Number oldValue, final Number newValue) {
        final double size = visualplaneconstrainedSize.getValue() / 9;
        FIELD_SIZE.setValue(new Point3D(size, size, 0.0));
        this.setBoardSize(new Point2D(size * 8, size * 8));
    }

    /**
     * Creates the notation grids.
     */
    private void createNotationGrids() {
        final RowConstraints boardNotationRow = new RowConstraints();
        boardNotationRow.setValignment(VPos.CENTER);
        boardNotationRow.prefHeightProperty().bind(rowHeight.divide(2));
        notationTop.getRowConstraints().add(boardNotationRow);
        notationBottom.getRowConstraints().add(boardNotationRow);

        notationTop.setMaxWidth(USE_PREF_SIZE);
        notationBottom.setMaxWidth(USE_PREF_SIZE);
        notationTop.prefWidthProperty().bind(visualBoard.prefWidthProperty());
        notationBottom.prefWidthProperty().bind(visualBoard.prefWidthProperty());

        for (int notationHorizontal = 0; notationHorizontal < 8; notationHorizontal++) {
            final ColumnConstraints boardColumn = new ColumnConstraints();
            boardColumn.prefWidthProperty().bind(rowWidth);
            boardColumn.setHalignment(HPos.CENTER);

            notationTop.getColumnConstraints().add(boardColumn);
            notationBottom.getColumnConstraints().add(boardColumn);

            notationTop.add(new Label(files[notationHorizontal]), notationHorizontal, 0);
            notationBottom.add(new Label(files[notationHorizontal]), notationHorizontal, 0);
        }

        final ColumnConstraints boardNotationColumn = new ColumnConstraints();
        boardNotationColumn.prefWidthProperty().bind(rowWidth.divide(2));
        boardNotationColumn.setHalignment(HPos.CENTER);

        notationLeft.getColumnConstraints().add(boardNotationColumn);
        notationRight.getColumnConstraints().add(boardNotationColumn);

        notationLeft.prefHeightProperty().bind(visualBoard.heightProperty());
        notationRight.prefHeightProperty().bind(visualBoard.heightProperty());

        for (int notationVertical = 0; notationVertical < 8; notationVertical++) {
            final RowConstraints boardRow = new RowConstraints();
            boardRow.prefHeightProperty().bind(rowHeight);
            boardRow.setValignment(VPos.CENTER);

            notationLeft.getRowConstraints().add(boardRow);
            notationRight.getRowConstraints().add(boardRow);

            notationLeft.add(new Label(String.valueOf(8 - notationVertical)), 0, notationVertical);
            notationRight.add(new Label(String.valueOf(8 - notationVertical)), 0, notationVertical);
        }

    }

    /**
     * Returns the coordinate of the first square.
     *
     * @return The coordinate.
     */
    @Override
    public ObjectProperty<Point3D> getFirstSquareCoordinate() {
        return FIELD_POS;
    }

    /**
     * Returns the size of a field.
     *
     * @return The size of a field.
     */
    @Override
    public ObjectProperty<Point3D> getFieldSize() {
        return FIELD_SIZE;
    }

}
