/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.views;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.AbstractGenericGameOptions;
import org.pidome.games.chess.world.hud.MainPlayfieldPlayerCards;
import org.pidome.games.chess.world.moves.MovesView;

/**
 * A 2d only board scene.
 *
 * @author john.sirach
 */
@GameView(
        name = "Scene with only a 2D board",
        description = "A default non 3D view",
        boardViewType = GameOptions.GameViewType.TypeConstant.FLAT
)
public class FlatGameView extends AbstractGameView {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(FlatGameView.class);

    /**
     * The moves view to be displayed.
     */
    private final MovesView movesView;

    /**
     * Primary view column.
     */
    private final ColumnConstraints primaryViewColumn = new ColumnConstraints() {
        {
            setPercentWidth(70);
        }
    };

    /**
     * Primary view column.
     */
    private final ColumnConstraints secondaryViewColumn = new ColumnConstraints() {
        {
            setPercentWidth(30);
        }
    };

    /**
     * Primary view row.
     */
    private final RowConstraints primaryViewRow = new RowConstraints() {
        {
            setPercentHeight(20);
        }
    };

    /**
     * Primary view row.
     */
    private final RowConstraints secundaryViewRow = new RowConstraints() {
        {
            setPercentHeight(80);
        }
    };

    /**
     * Constructor.
     *
     * @param genericGameOptions The options for the game.
     * @throws Exception When loading fails.
     */
    public FlatGameView(final AbstractGenericGameOptions genericGameOptions) throws Exception {
        super(genericGameOptions);
        this.movesView = new MovesView(genericGameOptions);
        this.getColumnConstraints().addAll(primaryViewColumn, secondaryViewColumn);
        this.getRowConstraints().addAll(primaryViewRow, secundaryViewRow);
    }

    /**
     * Builds the view.
     */
    @Override
    public void buildView() {
        this.movesView.build();
        this.add(createPlayerCards(), 1, 0);
        this.add(this.movesView, 1, 1);
        this.add(this.getFlatBoardView(), 0, 0, 1, 2);
    }

    /**
     * Creates the player cards for in the main view.
     *
     * @return The player cards.
     */
    private BorderPane createPlayerCards() {
        MainPlayfieldPlayerCards cardsContainer = new MainPlayfieldPlayerCards(getGenericGameOptions());
        cardsContainer.prefWidthProperty().bind(this.movesView.widthProperty());
        cardsContainer.prefHeightProperty().bind(this.heightProperty().subtract(this.movesView.heightProperty()));
        cardsContainer.setPadding(new Insets(10, 50, 0, 50));
        cardsContainer.getStyleClass().addAll("flat-view");
        StackPane.setAlignment(cardsContainer, Pos.TOP_CENTER);
        return cardsContainer;
    }

}
