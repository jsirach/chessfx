/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.pieces;

import org.pidome.games.chess.resources.GamePiece;
import org.pidome.games.chess.resources.ResourceLoadResult;
import org.pidome.games.chess.resources.ResourcesLoader;

/**
 * Pieces based on images.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class ImagePieces extends AbstractPieces {

    /**
     * Constructor.
     *
     * @param path The pieces path.
     * @param configFile The config file path.
     */
    protected ImagePieces(final String path, final String configFile) {
        super(path, GamePiece.GamepieceType.PIECE_IMAGE);
    }

    /**
     * @inhertDoc
     */
    @Override
    public final void scheduleAssetLoading(final ResourcesLoader resourcesLoader) {
        resourcesLoader.schedule(() -> {
            ResourceLoadResult loadResult = new ResourceLoadResult();
            loadResult.complete(new UnsupportedOperationException("Not supported yet."));
            return loadResult;
        });
    }

}
