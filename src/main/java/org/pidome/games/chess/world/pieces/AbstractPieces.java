/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.world.pieces;

import com.github.bhlangonijr.chesslib.PieceType;
import com.github.bhlangonijr.chesslib.Side;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.resources.GamePiece;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.world.config.PieceModelConfig;
import org.pidome.games.chess.world.config.PiecesModelConfig;
import org.pidome.games.chess.world.pieces.PieceInterface.Variant;
import org.pidome.games.chess.world.pieces.PieceInterface.VariantColor;

/**
 * Pieces collection.
 *
 * @author John Sirach
 */
public abstract class AbstractPieces {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(AbstractPieces.class);

    /**
     * List of pieces loaded.
     */
    private final Collection<VisualPiece> pieces = new ArrayList<>();

    /**
     * The base location to load from.
     */
    private final String baseLocation;

    /**
     * Piece type (2d/3d).
     */
    private final GamePiece.GamepieceType type;

    /**
     * The model material config collection.
     */
    private PiecesModelConfig piecesModelConfig;

    /**
     * constructor.
     *
     * @param baseLocation The base pieces location.
     * @param type The pieces type.
     */
    public AbstractPieces(final String baseLocation, final GamePiece.GamepieceType type) {
        this.baseLocation = baseLocation;
        this.type = type;
    }

    /**
     * The pieces base location.
     *
     * @return The base location.
     */
    public final String getPiecesBaseLocation() {
        return this.baseLocation;
    }

    /**
     * Loads a set of pieces.
     *
     * @param type The piece type to load.
     * @param piecesModelConfig The pieces material configuration.
     */
    protected void loadPieceSet(final GamePiece.GamepieceType type,
            final PiecesModelConfig piecesModelConfig) {
        this.piecesModelConfig = piecesModelConfig;
        List<LoadConfig> toLoad = new ArrayList<>();
        for (VariantColor color : VariantColor.values()) {
            for (Variant variant : Variant.values()) {
                for (int loadAmount = 0; loadAmount < variant.getPiecesCount(); loadAmount++) {
                    PieceModelConfig config = piecesModelConfig == null ? null : piecesModelConfig.get(color).get(variant);
                    toLoad.add(new LoadConfig(color, variant, config));
                }
            }
        }
        pieces.addAll(
                toLoad.parallelStream()
                        .map(config -> loadSinglePiece(config.getColor(), config.getVariant(), config.getPieceModelConfig()))
                        .collect(Collectors.toList())
        );

    }

    /**
     * Load a single piece.
     *
     * @param side The side to load.
     * @param type The type to load.
     * @return The visual piece of the corresponding side and type.
     */
    public final VisualPiece loadPiece(final Side side, final PieceType type) {
        VariantColor color = Side.WHITE.equals(side) ? VariantColor.WHITE : VariantColor.BLACK;
        Variant variant = Variant.getByType(type);
        PieceModelConfig config = piecesModelConfig == null ? null
                : piecesModelConfig.get(color)
                        .get(variant);
        VisualPiece piece = loadSinglePiece(color, variant, config);
        pieces.add(piece);
        return piece;
    }

    /**
     * Loads a single piece.
     *
     * @param color The piece color.
     * @param variant The piece variant.
     * @param materialConfig A piece it's specific material configuration.
     * @return The piece loaded.
     */
    private VisualPiece loadSinglePiece(final VariantColor color, final Variant variant,
            final PieceModelConfig materialConfig) {
        final VisualPiece piece;
        switch (type) {
            case PIECE_3D:
                piece = new PerspectivePiece(variant, color);
                break;
            default:
                piece = new FlatPiece(variant, color);
        }
        try {
            piece.load(baseLocation, materialConfig);
        } catch (PieceLoadException ex) {
            LOG.error("Unable to load pieces", ex);
        }
        return piece;
    }

    /**
     * Loads the pieces.
     *
     * @resourcesLoader Resource schedule loader.
     */
    public abstract void scheduleAssetLoading(final ResourcesLoader resourcesLoader);

    /**
     * Returns the set of pieces.
     *
     * @return The set.
     */
    public Collection<VisualPiece> getSet() {
        return this.pieces;
    }

    /**
     * Small internal helper class for parallel loading.
     */
    private static class LoadConfig {

        /**
         * Color.
         */
        private final VariantColor color;
        /**
         * Variant.
         */
        private final Variant variant;
        /**
         * Single model config.
         */
        private final PieceModelConfig pieceModelConfig;

        /**
         * constructor.
         *
         * @param color The color.
         * @param variant The variant.
         * @param config The single model config.
         */
        private LoadConfig(final VariantColor color, final Variant variant, final PieceModelConfig config) {
            this.color = color;
            this.variant = variant;
            this.pieceModelConfig = config;
        }

        /**
         * The color.
         *
         * @return The variant color.
         */
        private VariantColor getColor() {
            return this.color;
        }

        /**
         * the variant.
         *
         * @return The variant side.
         */
        private Variant getVariant() {
            return this.variant;
        }

        /**
         * the single model material config.
         *
         * @return The material config.
         */
        private PieceModelConfig getPieceModelConfig() {
            return this.pieceModelConfig;
        }

    }

}
