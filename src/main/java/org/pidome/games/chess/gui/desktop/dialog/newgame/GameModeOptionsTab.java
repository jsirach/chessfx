/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.scene.layout.GridPane;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.model.options.PlayerGameOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.controls.FieldSet;
import org.pidome.games.chess.utils.controls.GameTab;

/**
 * Tab for game type options.
 *
 * @author johns
 */
public class GameModeOptionsTab extends GameTab {

    /**
     * The game options.
     */
    private final AbstractInteractiveGameOptions options;

    /**
     * The content pane for the game mode options.
     */
    private final GridPane contentPane = new GridPane();

    /**
     * the additional options pane added.
     */
    private GameFeedBackOptionsFieldset gameUiFeedbackOptionsFieldSet;

    /**
     * Constructor.
     *
     * @param options The game options.
     */
    public GameModeOptionsTab(final AbstractInteractiveGameOptions options) {
        super(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME);
        this.options = options;
        constructContent();
    }

    /**
     * Creates the content.
     */
    private void constructContent() {
        contentPane.setHgap(15);
        contentPane.setVgap(10);

        switch (this.options.gamePlayModeProperty().get()) {
            case BROADCASTER:

                break;
            case PLAYER:
                this.options.gameEngineProperty().setValue(
                        GameResourcesHelper.getPlaymodeTypeGameEngines(GameOptions.GamePlayMode.PLAYER).get(0)
                );
                contentPane.add(new PlayerGameTypeSelectorFieldSet((PlayerGameOptions) this.options), 0, 0, 2, 1);
                break;
            case SPECTATOR:

                break;
            case VERSUS:
                contentPane.add(new VersusGameTypeSelectorFieldSet(this.options), 0, 0);
                contentPane.add(new VersusGamePlayersOptionsFieldset(this.options), 1, 0, 1, 2);
                break;
        }

        gameUiFeedbackOptionsFieldSet = new GameFeedBackOptionsFieldset(this.options);
        contentPane.add(gameUiFeedbackOptionsFieldSet, 0, 1);

        setContent(contentPane);

        this.contentValid().bind(Bindings.createBooleanBinding(()
                -> getFieldSets().stream().allMatch(fieldSet -> fieldSet.validProperty().get() == Boolean.TRUE),
                getFieldSets().stream().map(FieldSet::validProperty).toArray(Observable[]::new)));

    }

    /**
     * Performed when a game starts.
     */
    @Override
    public void onAction() {
        getFieldSets().forEach(FieldSet::onAction);
    }

    /**
     * Called when the options screen is closed.
     */
    @Override
    public final void dispose() {
        getFieldSets().forEach(FieldSet::dispose);
        this.contentValid().unbind();
    }

}
