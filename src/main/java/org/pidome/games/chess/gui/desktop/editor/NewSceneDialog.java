/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import org.pidome.games.chess.gui.desktop.dialog.ErrorMessagePopup;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.gui.desktop.dialog.newgame.SceneSelectorCombobox;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.resources.SceneResource;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.controls.H3;
import org.pidome.games.chess.utils.controls.HR;

/**
 * Dialog used to start editing a scene.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class NewSceneDialog extends GamePopup {

    private static final String COMMUNITY_FOLDER = File.separator + "resources" + File.separator + "scenes" + File.separator
            + GameResourcesHelper.ResourceLocation.COMMUNITY.getPath();

    /**
     * First column.
     */
    private final ColumnConstraints firstColumn = new ColumnConstraints() {
        {
            setPrefWidth(170);
        }
    };

    /**
     * Second column.
     */
    private final ColumnConstraints secondColumn = new ColumnConstraints() {
        {
            setPrefWidth(250);
        }
    };

    /**
     * Third column.
     */
    private final ColumnConstraints thirdColumn = new ColumnConstraints() {
        {
            setPrefWidth(75);
        }
    };

    /**
     * The game options.
     */
    private final AbstractInteractiveGameOptions options;

    /**
     * The content pane.
     */
    private final GridPane contentPane = new GridPane();

    /**
     * 3D scene selector.
     */
    private final SceneSelectorCombobox sceneSelector = new SceneSelectorCombobox();

    /**
     * Directory chooser to store.
     */
    final DirectoryChooser directoryChooser = new DirectoryChooser();

    private SceneResource sceneResource;

    private final Text description = new Text();
    private final Text author = new Text();
    private final Text authorWebsite = new Text();
    private final Text authorEmail = new Text();

    private final TextField configurationNameField = new TextField();
    private final Text temlateName = new Text();
    private final TextField nameField = new TextField();
    private final TextField descriptionField = new TextField();
    private final TextField authorField = new TextField();
    private final TextField authorWebsiteField = new TextField();
    private final TextField authorEmailField = new TextField();
    private final TextField selectedFolder = new TextField();

    private final ChangeListener<SceneResource> resourceListener = this::setSelectedValues;

    private final ObjectProperty<File> outputDir = new SimpleObjectProperty<>();

    /**
     * Constructor.
     *
     * @param options The game options.
     */
    public NewSceneDialog(final AbstractInteractiveGameOptions options) {
        super(I18nProxy.getInstance().get(I18nApplicationKey.SCENE_EDITOR), new SimpleStringProperty("Create scene"));
        this.options = options;
        setFolderSelectionDefaults();
        EditorDefaults.defaultOptions(this.options);
        EditorDefaults.defaultPerspectiveOptions(this.options);
        constructDialog();
    }

    private void constructDialog() {

        contentPane.setHgap(10);
        contentPane.setVgap(5);

        contentPane.getColumnConstraints().addAll(firstColumn, secondColumn, thirdColumn);

        this.contentPane.add(new H3("New scene details"), 0, 0, 2, 1);

        this.contentPane.add(new Label("Template"), 0, 1);
        this.contentPane.add(temlateName, 1, 1);

        this.contentPane.add(new Label("Name"), 0, 2);
        this.contentPane.add(nameField, 1, 2);

        this.contentPane.add(new Label("Description"), 0, 3);
        this.contentPane.add(descriptionField, 1, 3);

        this.contentPane.add(new Label("Author"), 0, 4);
        this.contentPane.add(authorField, 1, 4);

        this.contentPane.add(new Label("Website"), 0, 5);
        this.contentPane.add(authorWebsiteField, 1, 5);

        this.contentPane.add(new Label("Email"), 0, 6);
        this.contentPane.add(authorEmailField, 1, 6);

        this.contentPane.add(new HR(
                firstColumn.prefWidthProperty().add(secondColumn.prefWidthProperty()).add(thirdColumn.prefWidthProperty())
        ), 0, 7, 3, 1);

        this.contentPane.add(new Label("Configuration"), 0, 8);
        this.contentPane.add(configurationNameField, 1, 8);
        this.contentPane.add(new Label(".properties"), 2, 8);

        Pattern pattern = Pattern.compile("[a-z]*");
        UnaryOperator<TextFormatter.Change> filter = control -> {
            if (pattern.matcher(control.getControlNewText()).matches()) {
                return control;
            } else {
                return null;
            }
        };
        TextFormatter<String> formatter = new TextFormatter<>(filter);
        configurationNameField.setTextFormatter(formatter);

        selectedFolder.setEditable(false);
        Button folderSelector = new Button("Select");
        final String osFolder = COMMUNITY_FOLDER.substring(0, COMMUNITY_FOLDER.length() - 1);
        folderSelector.setOnAction(event -> {
            outputDir.setValue(directoryChooser.showDialog(this.getScene().getWindow()));
            if (!Objects.isNull(outputDir.get()) && !outputDir.get().getParent().endsWith(osFolder)) {
                ErrorMessagePopup errorMessage = new ErrorMessagePopup();
                errorMessage.setMessage("New scenes can only be placed in the community folder");
                errorMessage.display();
                outputDir.setValue(null);
            } else if (!Objects.isNull(outputDir) && outputDir.get().getParent().endsWith(osFolder) && !checkRequirements(outputDir.get())) {
                outputDir.setValue(null);
            } else {
                FXUtils.runOnFXThread(() -> {
                    selectedFolder.setText(outputDir.get().getName());
                });
            }
        });
        this.contentPane.add(new Label("Save at"), 0, 9);
        this.contentPane.add(selectedFolder, 1, 9);
        this.contentPane.add(folderSelector, 2, 9);

        this.contentPane.add(buildTemplateSelector(), 0, 10, 3, 1);

        confirmButtonDisableProperty().bind(
                outputDir.isNull().or(configurationNameField.textProperty().isEmpty().or(nameField.textProperty().isEmpty()))
        );
        this.setCenter(this.contentPane);
    }

    private TitledPane buildTemplateSelector() {
        TitledPane pane = new TitledPane();
        pane.setText("Select template");
        pane.setExpanded(false);
        pane.getStyleClass().add("new-scene-template-selector");

        GridPane content = new GridPane();

        content.setHgap(10);
        content.setVgap(5);
        final ColumnConstraints firstTplColumn = new ColumnConstraints();
        firstTplColumn.setPrefWidth(150);

        final ColumnConstraints secondTplColumn = new ColumnConstraints();
        secondTplColumn.setPrefWidth(90);

        final ColumnConstraints thirdTplColumn = new ColumnConstraints();
        thirdTplColumn.setPrefWidth(255);

        content.getColumnConstraints().addAll(firstTplColumn, secondTplColumn, thirdTplColumn);

        GridPane.setFillWidth(sceneSelector, Boolean.TRUE);
        sceneSelector.setMaxWidth(Double.MAX_VALUE);
        sceneSelector.valueProperty().addListener((obsValue, oldValue, newValue) -> {
            this.options.getScene3DResource().setValue(newValue);
        });
        sceneSelector.buildResourceList(
                GameResourcesHelper.getAvailableGameScenes(GameResourcesHelper.ResourceLocation.INTERNAL).stream()
                        .filter(resource -> resource.getName().equals("Minimal"))
                        .findFirst()
                        .orElse(null)
        );
        this.sceneSelector.getSelectionModel().selectedItemProperty().addListener(resourceListener);

        GridPane.setHalignment(sceneSelector.getPreviewImage(), HPos.CENTER);
        GridPane.setValignment(sceneSelector.getPreviewImage(), VPos.TOP);
        sceneSelector.getPreviewImage().setFitWidth(150);

        content.add(sceneSelector, 0, 1, 4, 1);
        content.add(sceneSelector.getPreviewImage(), 0, 2, 1, 5);

        content.add(new Label("Author"), 1, 2);
        content.add(author, 2, 2);

        content.add(new Label("Website"), 1, 3);
        content.add(authorWebsite, 2, 3);

        content.add(new Label("Email"), 1, 4);
        content.add(authorEmail, 2, 4);

        Label desc = new Label("Description");
        GridPane.setValignment(desc, VPos.TOP);
        GridPane.setValignment(description, VPos.TOP);
        description.wrappingWidthProperty().bind(thirdTplColumn.prefWidthProperty());
        content.add(desc, 1, 5);
        content.add(description, 2, 5, 2, 1);

        pane.setContent(content);
        return pane;

    }

    private boolean checkRequirements(final File dir) {
        ErrorMessagePopup errorMessage = new ErrorMessagePopup();
        if (configurationNameField.getText().isBlank()
                || nameField.getText().isBlank()) {
            errorMessage.setMessage("Both configuration name and scene name are required");
            errorMessage.display();
            return false;
        } else {
            File newResourceFile = new File(dir.getAbsolutePath() + File.separator + configurationNameField.getText() + ".properties");
            if (newResourceFile.exists()) {
                errorMessage.setMessage("A configuration with the name '" + configurationNameField.getText() + "' already exists");
                errorMessage.display();
                return false;
            }
        }
        return true;
    }

    public SceneResource createNewScene() {
        ErrorMessagePopup errorMessage = new ErrorMessagePopup();
        if (Objects.isNull(outputDir)) {
            errorMessage.setMessage("Directory/Folder selection is required");
            errorMessage.display();
        } else {
            File newResourceFile = new File(outputDir.get().getAbsolutePath() + File.separator + configurationNameField.getText() + ".properties");
            try {
                newResourceFile.createNewFile();
                sceneResource = new SceneResource(newResourceFile);
                sceneResource.setName(nameField.getText());
                sceneResource.setDescription(descriptionField.getText());
                sceneResource.setAuthor(authorField.getText());
                sceneResource.setAuthorEmail(authorEmailField.getText());
                sceneResource.setAuthorWebsite(authorWebsiteField.getText());
                sceneResource.setSceneViewType(GameOptions.SceneViewType.PERSPECTIVE);
                sceneResource.setSceneConfigurationFileName(configurationNameField.getText());

                Path copied = Paths.get(sceneResource.getSceneConfigurationFile());
                Path originalPath = new File(
                        this.sceneSelector.getValue().getSceneConfigurationFile()
                ).toPath();
                try {
                    Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
                    sceneResource.save();
                    GameResourcesHelper.getAvailableGameScenes(
                            GameResourcesHelper.ResourceLocation.COMMUNITY
                    ).add(sceneResource);
                    this.options.getScene3DResource().setValue(sceneResource);
                    return sceneResource;
                } catch (IOException ex) {
                    newResourceFile.delete();
                    copied.toFile().delete();
                    Logger.getLogger(NewSceneDialog.class.getName()).log(Level.SEVERE, null, ex);
                    errorMessage.setMessage("Could not create new scene");
                    errorMessage.display();
                }
            } catch (IOException | IllegalArgumentException ex) {
                newResourceFile.delete();
                Logger.getLogger(NewSceneDialog.class.getName()).log(Level.SEVERE, null, ex);
                errorMessage.setMessage("Could not create new configuration for scene");
                errorMessage.display();
            }
        }
        return null;
    }

    private void setSelectedValues(final ObservableValue obs, final SceneResource oldResource, final SceneResource newResource) {
        temlateName.setText(newResource.getName());
        description.setText(newResource.getDescription());
        author.setText(newResource.getAuthor());
        authorWebsite.setText(newResource.getAuthorWebsite());
        authorEmail.setText(newResource.getAuthorEmail());
    }

    private void setFolderSelectionDefaults() {
        directoryChooser.setInitialDirectory(new File("./" + COMMUNITY_FOLDER.substring(0, COMMUNITY_FOLDER.length() - 1)));
    }

}
