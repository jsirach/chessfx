/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import javafx.scene.Node;
import javafx.scene.control.TitledPane;

/**
 * Base controller region.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class EditorControlRegion extends TitledPane {

    public EditorControlRegion() {

    }

    public final void setPaneContent(final Node node) {
        node.getStyleClass().add("editor-control-region");
        this.setContent(node);
    }

}
