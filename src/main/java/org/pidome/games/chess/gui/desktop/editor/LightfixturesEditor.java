/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import java.util.Objects;
import java.util.stream.Collectors;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ListChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import org.pidome.games.chess.gui.desktop.dialog.ErrorMessagePopup;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.world.config.ColorConfig;
import org.pidome.games.chess.world.config.Location3D;
import org.pidome.games.chess.world.config.PerspectiveSceneLightSourceConfig;
import org.pidome.games.chess.world.views.PerspectiveSceneView;

/**
 * Control for editing light fixtures in a scene.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class LightfixturesEditor extends EditorControlRegion {

    private static final int TREE_ITEM_WIDTH = 250;

    private static final int MAX_LIGHTS = 3;

    private final PerspectiveSceneView sceneView;

    private final GridPane content = new GridPane();

    private final PointLightAxis axisGroup;

    private final LightFixtureProperties properties;

    private final TreeView<PerspectiveSceneLightSourceConfig> treeView = new TreeView<>();

    private final ListChangeListener<PerspectiveSceneLightSourceConfig> listChangeListener = this::listChangeListener;

    private TreeItem<PerspectiveSceneLightSourceConfig> currentItem;

    /**
     * Primary view column.
     */
    private final ColumnConstraints colmnWidth = new ColumnConstraints() {
        {
            setPercentWidth(100);
        }
    };

    public LightfixturesEditor(final PerspectiveSceneView sceneView) {
        this.sceneView = sceneView;
        axisGroup = new PointLightAxis(this.sceneView);
        properties = new LightFixtureProperties(this, axisGroup);
        content.getColumnConstraints().add(colmnWidth);
        this.buildView();
        content.setVgap(10);
    }

    private void buildView() {
        this.setText("Light sources");
        content.addRow(1, buildTreeView());
        content.addRow(2, properties);
        this.setPaneContent(content);
    }

    private TreeView<PerspectiveSceneLightSourceConfig> buildTreeView() {

        treeView.setPrefSize(USE_COMPUTED_SIZE, 175);
        treeView.setMinHeight(150);

        PerspectiveSceneLightSourceConfig rootItem = new PerspectiveSceneLightSourceConfig();
        rootItem.setName("Point lights");
        final TreeItem pointLightsRoot = new TreeItem(rootItem);

        treeView.setCellFactory(tree -> {
            TreeCell<PerspectiveSceneLightSourceConfig> cell = new TreeCell<>() {

                @Override
                public void updateItem(PerspectiveSceneLightSourceConfig light, boolean empty) {
                    super.updateItem(light, empty);
                    if (light == null || empty) {
                        setGraphic(null);
                        setText(null);
                    } else {
                        HBox itemRow = new HBox();
                        Label nameLabel = new Label(light.getName());
                        nameLabel.setMinWidth(TREE_ITEM_WIDTH);
                        nameLabel.setMaxWidth(nameLabel.getMinWidth());
                        itemRow.getChildren().add(nameLabel);
                        Insets iconMargins = new Insets(0, 3, 0, 3);
                        if (!light.equals(pointLightsRoot.getValue())) {
                            MaterialIconView editIcon = new MaterialIconView(MaterialIcon.EDIT, "1.2em");
                            MaterialIconView deleteIcon = new MaterialIconView(MaterialIcon.DELETE, "1.2em");
                            HBox.setMargin(editIcon, iconMargins);
                            HBox.setMargin(deleteIcon, iconMargins);
                            editIcon.setOnMousePressed(mouseEvent -> {
                                TextField textField = new TextField();
                                textField.setPrefWidth(TREE_ITEM_WIDTH);
                                textField.setText(light.getName());
                                textField.setOnKeyReleased(keyEvent -> {
                                    if (keyEvent.getCode() == KeyCode.ENTER) {
                                        light.setName(textField.getText());
                                        nameLabel.setText(textField.getText());
                                        setGraphic(itemRow);
                                    } else if (keyEvent.getCode() == KeyCode.ESCAPE) {
                                        setGraphic(itemRow);
                                    }
                                });
                                setGraphic(textField);
                            });
                            deleteIcon.setOnMousePressed(mouseEvent -> {
                                new DeleteLightDialog(light).display();
                            });
                            itemRow.getChildren().addAll(editIcon, deleteIcon);
                        } else {
                            MaterialIconView addIcon = new MaterialIconView(MaterialIcon.ADD, "1.2em");
                            addIcon.setOnMouseClicked(event -> {
                                if (sceneView.perspectiveConfig()
                                        .observableLights().size() == MAX_LIGHTS) {
                                    ErrorMessagePopup error = new ErrorMessagePopup();
                                    error.setMessage("Can only have a maximum of three lights");
                                    error.display();
                                } else {
                                    new NewLightDialog().display();
                                }
                            });
                            HBox.setMargin(addIcon, iconMargins);
                            itemRow.getChildren().add(addIcon);
                        }
                        setGraphic(itemRow);
                    }
                }

            };
            treeView.getSelectionModel().selectedItemProperty().addListener((obs, oldV, treeItem) -> {
                if (!Objects.isNull(treeItem) && !treeItem.equals(pointLightsRoot)) {
                    if (!treeItem.equals(currentItem)) {
                        properties.updateLightsource(treeItem.getValue());
                        properties.controlsDisabledProperty().setValue(Boolean.FALSE);
                        if (!this.sceneView.contains3dObject(axisGroup)) {
                            this.sceneView.add3dObjects(axisGroup);
                        }
                    }
                } else {
                    properties.updateLightsource(null);
                    properties.controlsDisabledProperty().setValue(Boolean.TRUE);
                    this.sceneView.remove3dObjects(axisGroup);
                }
                currentItem = treeItem;
            });
            return cell;
        });

        treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        treeView.setRoot(pointLightsRoot);
        treeView.getRoot().setExpanded(true);
        treeView.setEditable(true);
        addPointLights(pointLightsRoot);

        return treeView;

    }

    private void listChangeListener(final ListChangeListener.Change<? extends PerspectiveSceneLightSourceConfig> change) {
        while (change.next()) {
            if (change.wasAdded()) {
                change.getAddedSubList().forEach(lightSource -> {
                    treeView.getRoot().getChildren().add(new TreeItem(lightSource));
                });
            }
            if (change.wasRemoved()) {
                change.getRemoved().forEach(lightSource -> {
                    treeView.getRoot().getChildren()
                            .removeIf(
                                    item -> item.getValue().getIdentity().equals(lightSource.getIdentity())
                            );
                });
            }
        }
    }

    private void addPointLights(final TreeItem rootItem) {
        rootItem.getChildren().addAll(
                this.sceneView.perspectiveConfig().getLights().stream()
                        .map(light -> new TreeItem(light))
                        .collect(Collectors.toList())
        );
        this.sceneView.perspectiveConfig().observableLights().addListener(listChangeListener);
    }

    private class NewLightDialog extends GamePopup {

        private TextField nameField = new TextField();

        NewLightDialog() {
            super(new SimpleStringProperty("New light"));
            buildDialog();
        }

        private void buildDialog() {
            GridPane contentPane = new GridPane();
            contentPane.setHgap(10);
            contentPane.addRow(0, new Label("Light name"), nameField);
            this.setCenter(contentPane);
            this.setOnAction(() -> {
                PerspectiveSceneLightSourceConfig newLight = new PerspectiveSceneLightSourceConfig();
                newLight.setName(nameField.getText());
                newLight.setLocation(new Location3D() {
                    {
                        this.setX(0);
                        this.setY(-1000);
                        this.setZ(0);
                    }
                });
                newLight.setColor(new ColorConfig() {
                    {
                        this.setHue(28.928571428571427);
                        this.setSaturation(0.43921566009521484);
                        this.setBrightness(0.7);
                    }
                });
                new Thread(() -> {
                    LightfixturesEditor.this.sceneView.perspectiveConfig()
                            .observableLights().add(newLight);
                }).start();
            });
        }

    }

    private class DeleteLightDialog extends GamePopup {

        private final PerspectiveSceneLightSourceConfig light;

        DeleteLightDialog(final PerspectiveSceneLightSourceConfig light) {
            super(new SimpleStringProperty("Delete light"));
            this.light = light;
            buildDialog();
        }

        private void buildDialog() {
            this.setCenter(
                    new Label("Are you sure you want to delete " + light.getName() + " ?")
            );
            this.setOnCancel(() -> {
                this.close();
            });
            this.setOnAction(() -> {
                LightfixturesEditor.this.sceneView.perspectiveConfig()
                        .observableLights().remove(light);
            });
        }

    }

}
