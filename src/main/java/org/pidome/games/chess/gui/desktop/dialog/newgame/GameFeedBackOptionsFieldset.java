/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.GridPane;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.resources.i18n.I18nPlural;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nPluralKey;
import org.pidome.games.chess.utils.controls.FieldSet;

/**
 * Versus play type options.
 *
 * @author johns
 */
public class GameFeedBackOptionsFieldset extends FieldSet {

    /**
     * If traveled path should be shown.
     */
    private final CheckBox showTravelPathCheckbox = new CheckBox();

    /**
     * If legal steps should be shown.
     */
    private final CheckBox showLegalStepsCheckbox = new CheckBox();

    /**
     * If legal steps should be shown.
     */
    private final CheckBox showKingCheckedCheckbox = new CheckBox();

    /**
     * Color picker for piece paths.
     */
    private PieceMovePathOptions pathsColorPicker;

    /**
     * The game options.
     */
    private AbstractInteractiveGameOptions options;

    /**
     * Constructor binding the default options and building the options view.
     *
     * @param options The interactive options.
     */
    public GameFeedBackOptionsFieldset(final AbstractInteractiveGameOptions options) {
        super(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS);
        this.options = options;
        showTravelPathCheckbox.selectedProperty().bindBidirectional(options.showTraveledPathProperty());
        showLegalStepsCheckbox.selectedProperty().bindBidirectional(options.showLegalStepsProperty());
        showKingCheckedCheckbox.selectedProperty().bindBidirectional(options.showKingCheckProperty());

        constructContent();
    }

    /**
     * Creates the options content.
     */
    private void constructContent() {
        final GridPane optionsPane = new GridPane();
        optionsPane.setVgap(5);

        showLegalStepsCheckbox.textProperty().set(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS_SHOW_LEGAL_STEPS).get());
        optionsPane.add(showLegalStepsCheckbox, 0, 0);

        showKingCheckedCheckbox.textProperty().set(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS_SHOW_CHECK).get());
        optionsPane.add(showKingCheckedCheckbox, 0, 1);

        showTravelPathCheckbox.textProperty().set(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_FEEDBACK_OPTIONS_SHOW_PIECE_PATH).get());
        optionsPane.add(showTravelPathCheckbox, 0, 2);

        final Button changePathColorsbutton = new Button(I18nProxy.getInstance()
                .getNonPersistentPlural(I18nApplicationKey.COLOR_PICKER_POPUP_TITLE,
                        new I18nPlural(I18nPluralKey.count, 2)).get());
        changePathColorsbutton.setOnAction(event -> {
            pathsColorPicker = new PieceMovePathOptions(options.showTraveledPathProperty());
            pathsColorPicker.setOnAction(() -> {
                pathsColorPicker.close();
            });
            pathsColorPicker.display(false);
        });

        optionsPane.add(changePathColorsbutton, 1, 2);

        this.setContent(optionsPane);

    }

    /**
     * Being executed when player hits ok in the popup.
     */
    @Override
    public final void onAction() {
        /// Not used in this fieldset.
    }

    /**
     * When the options are closed.
     */
    @Override
    public final void dispose() {
        showTravelPathCheckbox.selectedProperty().unbindBidirectional(options.showTraveledPathProperty());
        showLegalStepsCheckbox.selectedProperty().unbindBidirectional(options.showLegalStepsProperty());
        showKingCheckedCheckbox.selectedProperty().unbindBidirectional(options.showKingCheckProperty());
        this.options = null;
    }

}
