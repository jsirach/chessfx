/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import org.pidome.games.chess.resources.SceneResource;
import org.pidome.games.chess.utils.controls.H2;
import org.pidome.games.chess.world.views.PerspectiveSceneView;

/**
 * The main editor pane.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class EditorPane extends GridPane {

    private final SceneResource scene;

    private final PerspectiveSceneView sceneView;

    private ColumnConstraints column = new ColumnConstraints() {
        {
            setPercentWidth(100);
        }
    };

    private RowConstraints headerRow = new RowConstraints();

    private RowConstraints contentRow = new RowConstraints() {
        {
            setFillHeight(true);
        }
    };

    public EditorPane(final SceneResource scene, final PerspectiveSceneView sceneView) {
        this.scene = scene;
        this.sceneView = sceneView;
        this.setVgap(10);
        getColumnConstraints().add(column);
        getRowConstraints().addAll(headerRow, contentRow);
        buildPane();
    }

    private void buildPane() {
        final ScrollPane scrollPane = new ScrollPane();
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        GridPane.setHgrow(scrollPane, Priority.ALWAYS);
        final VBox content = new VBox(10);

        this.getStyleClass().add("scene-editor-editor-pane");
        H2 header = new H2("Scene editor");
        header.setPadding(new Insets(10, 0, 0, 5));
        content.getChildren().addAll(
                header,
                new GenericEditPane(this.scene, this.sceneView),
                new AmbientViewEditor(this.sceneView),
                new LightfixturesEditor((this.sceneView))
        );
        scrollPane.setContent(content);

        this.addRow(0, header);
        this.addRow(1, scrollPane);
    }

    //// Add scrollPane below title.
}
