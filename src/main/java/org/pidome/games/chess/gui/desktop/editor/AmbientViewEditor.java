/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import javafx.beans.property.DoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import org.pidome.games.chess.world.config.ColorConfig;
import org.pidome.games.chess.world.views.PerspectiveSceneView;

/**
 * Ambient view editor.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class AmbientViewEditor extends EditorControlRegion {

    private final PerspectiveSceneView sceneView;

    private final ColorConfig colorConfig;

    private GridPane content = new GridPane();

    /**
     * Primary view column.
     */
    private final ColumnConstraints labelColumn = new ColumnConstraints() {
        {
            setPercentWidth(40);
        }
    };

    /**
     * Primary view column.
     */
    private final ColumnConstraints valueColumn = new ColumnConstraints() {
        {
            setPercentWidth(60);
        }
    };

    protected AmbientViewEditor(final PerspectiveSceneView sceneView) {
        this.sceneView = sceneView;
        colorConfig = this.sceneView.perspectiveConfig().getAmbientLighting();
        content.getColumnConstraints().addAll(labelColumn, valueColumn);
        buildControl();
        this.setPaneContent(content);
    }

    private void buildControl() {
        this.setText("Ambient light");
        controlSlider("Hue", "Adjust Hue value", colorConfig.hueProperty(), 0.0, 360.0);
        controlSlider("Saturation", "Adjust Saturation value", colorConfig.saturationProperty(), 0.0, 100.0);
        controlSlider("Brightness", "Adjust Brightness value", colorConfig.brightnessProperty(), 0.0, 100.0);
    }

    private void controlSlider(final String label, final String toolTip,
            final DoubleProperty property, final double min, final double max) {

        Label sliderLabel = new Label(label);

        Slider slider = new Slider();
        slider.setTooltip(new Tooltip(toolTip));
        slider.setMajorTickUnit(max);
        slider.setShowTickLabels(true);
        slider.setMin(min);
        slider.setMax(max);
        if (max == 100.0) {
            slider.setValue(property.doubleValue() * 100.0);
        } else {
            slider.setValue(property.doubleValue());
        }
        slider.valueProperty().addListener((obs, oldVal, newVal) -> {
            if (max == 100.0) {
                property.setValue(newVal.doubleValue() / 100.0);
            } else {
                property.setValue(newVal);
            }
        });
        content.addRow(content.getRowCount(), sliderLabel, slider);
    }

}
