/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import org.pidome.games.chess.gui.desktop.dialog.DialogTabPane;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.model.options.PlayerGameOptions;
import org.pidome.games.chess.model.options.VersusGameOptions;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;

/**
 * Popup to start a new game.
 *
 * @author John
 */
public class NewGameDialog extends GamePopup {

    /**
     * The game options.
     */
    private final AbstractInteractiveGameOptions options;

    /**
     * The tab pane showed.
     */
    private final DialogTabPane tabPane = new DialogTabPane();

    /**
     * Constructor.
     *
     * @param options The game options.
     */
    private NewGameDialog(final AbstractInteractiveGameOptions options) {
        super(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_TITLE), options.getGameTypeNameProperty());
        this.options = options;
        constructDialog();
    }

    /**
     * Constructor.
     *
     * @param options Versus game options.
     */
    public NewGameDialog(final VersusGameOptions options) {
        this((AbstractInteractiveGameOptions) options);
    }

    /**
     * Constructor.
     *
     * @param options Game player options.
     */
    public NewGameDialog(final PlayerGameOptions options) {
        this((AbstractInteractiveGameOptions) options);
    }

    /**
     * Creates the popup.
     */
    private void constructDialog() {
        GameModeOptionsTab gameTab = new GameModeOptionsTab(this.options);
        ViewSelectorTab viewSelectionTab = new ViewSelectorTab(this.options);
        tabPane.getGameTabs().addAll(gameTab, viewSelectionTab);
        tabPane.createValidationBindings();
        this.setCenter(tabPane);
        this.confirmButtonDisableProperty().bind(tabPane.contentValidated().not());
    }

    /**
     * Overrides the game popup default setOnAction to be able to perform more
     * methods.
     *
     * @param runnable The runnable to execute during onAction.
     */
    @Override
    public final void setOnAction(final Runnable runnable) {
        tabPane.getGameTabs().forEach(tab -> {
            tab.onAction();
        });
        super.setOnAction(runnable);
    }

    /**
     * Called when closed.
     */
    @Override
    public final void close() {
        super.close();
        tabPane.getGameTabs().forEach(tab -> {
            tab.dispose();
        });
    }

}
