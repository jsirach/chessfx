/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.settings;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.settings.GraphicsSettings;
import org.pidome.games.chess.settings.Settings;
import org.pidome.games.chess.utils.controls.FieldSet;
import org.pidome.games.chess.utils.controls.GameTab;

/**
 * Tab for graphics settings.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class GraphicsSettingsTab extends GameTab {

    /**
     * Tab content.
     */
    private final GridPane contentPane = new GridPane();

    /**
     * field set for the language.
     */
    private final FieldSet fieldSetGraphics = new FieldSet(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_TITLE);

    /**
     * Label for global graphics.
     */
    private final Label labelForGlobalGraphics = new Label();

    /**
     * Combobox for quality.
     */
    private final GlobalRenderQualityCombobox quality = new GlobalRenderQualityCombobox();

    /**
     * Fieldset for perspective quality options.
     */
    private final FieldSet perspectiveFieldSet = new FieldSet(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_3D);

    private final CheckBox movesAnimation3DEnabled = new CheckBox();
    private final CheckBox individualPiecesAnimation3DEnabled = new CheckBox();
    private final CheckBox highPieces3DQuality = new CheckBox();
    private final CheckBox highBoards3DQuality = new CheckBox();

    /**
     * Fieldset for flat board quality options.
     */
    private final FieldSet flatFieldSet = new FieldSet(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_2D);

    private final CheckBox movesAnimation2DEnabled = new CheckBox();
    private final CheckBox highPieces2DQuality = new CheckBox();
    private final CheckBox highBoards2DQuality = new CheckBox();

    private final GraphicsSettings settings;

    /**
     * Constructor.
     */
    public GraphicsSettingsTab() {
        super(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_TITLE);
        settings = Settings.get(GraphicsSettings.class);
        this.contentPane.setHgap(10);
        this.contentPane.setVgap(10);
        buildContent();
        this.setContent(contentPane);
    }

    /**
     * Builds the content to show.
     */
    private void buildContent() {
        this.contentPane.add(buildApplicationQualityfieldSet(), 0, 0, 2, 1);
        this.contentPane.add(build3dSettingsSection(), 0, 1);
        this.contentPane.add(build2dSettingsSection(), 1, 1);
    }

    /**
     * Graphiucs setttings fieldset.
     *
     * @return The graphic settings fieldset.
     */
    private FieldSet buildApplicationQualityfieldSet() {
        final GridPane graphicsPane = new GridPane();
        graphicsPane.setHgap(10);
        graphicsPane.setVgap(10);

        FieldSet.setSettingsLabelProperties(labelForGlobalGraphics, I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_GLOBAL);

        quality.valueProperty().bindBidirectional(settings.globalQuality());

        movesAnimation3DEnabled.selectedProperty().bindBidirectional(settings.motion3DPieces());

        movesAnimation2DEnabled.selectedProperty().bindBidirectional(settings.motion2DPieces());

        graphicsPane.add(labelForGlobalGraphics, 0, 0);
        graphicsPane.add(quality, 1, 0);

        fieldSetGraphics.setContent(graphicsPane);

        return fieldSetGraphics;
    }

    /**
     * Returns the fieldset with the 3D options.
     *
     * @return Fieldset pane
     */
    private FieldSet build3dSettingsSection() {
        final GridPane optionsPane = new GridPane();
        optionsPane.setHgap(15);
        optionsPane.setVgap(10);

        movesAnimation3DEnabled.textProperty().bind(I18nProxy.getStringProperty(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_MOTION));
        individualPiecesAnimation3DEnabled.textProperty().bind(I18nProxy.getStringProperty(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_3D_ANIMATION));
        highPieces3DQuality.textProperty().bind(I18nProxy.getStringProperty(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_PIECES));
        highBoards3DQuality.textProperty().bind(I18nProxy.getStringProperty(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_BOARDS));

        optionsPane.add(movesAnimation3DEnabled, 0, 0);
        //optionsPane.add(individualPiecesAnimation3DEnabled, 0, 1);
        //optionsPane.add(highPieces3DQuality, 0, 2);
        //optionsPane.add(highBoards3DQuality, 0, 3);

        perspectiveFieldSet.setContent(optionsPane);

        return perspectiveFieldSet;
    }

    /**
     * Returns the fieldset with the 2D options.
     *
     * @return Fieldset pane
     */
    private FieldSet build2dSettingsSection() {
        final GridPane optionsPane = new GridPane();
        optionsPane.setHgap(15);
        optionsPane.setVgap(10);

        movesAnimation2DEnabled.textProperty().bind(I18nProxy.getStringProperty(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_MOTION));
        highPieces2DQuality.textProperty().bind(I18nProxy.getStringProperty(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_PIECES));
        highBoards2DQuality.textProperty().bind(I18nProxy.getStringProperty(I18nApplicationKey.SETTINGS_APPLICATION_GAME_QUALITY_BOARDS));

        optionsPane.add(movesAnimation2DEnabled, 0, 0);
        //optionsPane.add(highPieces2DQuality, 0, 1);
        //optionsPane.add(highBoards2DQuality, 0, 2);

        flatFieldSet.setContent(optionsPane);
        return flatFieldSet;
    }

    /**
     * When on action is called.
     */
    @Override
    public void onAction() {
        // No action needed.
    }

    /**
     * When the tab needs to be disposed.
     */
    @Override
    public void dispose() {
        // Global
        labelForGlobalGraphics.textProperty().unbind();
        quality.valueProperty().unbindBidirectional(settings.globalQuality());

        // 3D pieces.
        movesAnimation3DEnabled.textProperty().unbind();
        movesAnimation3DEnabled.selectedProperty().unbindBidirectional(settings.motion3DPieces());

        individualPiecesAnimation3DEnabled.textProperty().unbind();
        highPieces3DQuality.textProperty().unbind();
        highBoards3DQuality.textProperty().unbind();

        // 2D Pieces
        movesAnimation2DEnabled.textProperty().unbind();
        movesAnimation2DEnabled.selectedProperty().unbindBidirectional(settings.motion2DPieces());

        highPieces2DQuality.textProperty().unbind();
        highBoards2DQuality.textProperty().unbind();

    }

}
