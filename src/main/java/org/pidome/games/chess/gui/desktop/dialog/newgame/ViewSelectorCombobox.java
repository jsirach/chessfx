/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;

/**
 * The view selector combobox.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class ViewSelectorCombobox extends ComboBox<GameOptions.GameViewType> {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(ViewSelectorCombobox.class);

    /**
     * Scene preview image.
     */
    private final ImageView scenePreviewImage = new ImageView();

    /**
     * List of the view types.
     */
    private static final ObservableList<GameOptions.GameViewType> viewTypes
            = FXCollections.observableArrayList(GameOptions.GameViewType.values());

    /**
     * Constructor.
     */
    public ViewSelectorCombobox() {
        this.scenePreviewImage.setPreserveRatio(true);
        this.setViewFactories();
        this.setChangeListener();
        this.setItems(viewTypes);
    }

    /**
     * Sets the view factory.
     */
    private void setViewFactories() {
        setCellFactory((ListView<GameOptions.GameViewType> param) -> {
            return new ListCell<GameOptions.GameViewType>() {
                @Override
                public void updateItem(final GameOptions.GameViewType viewType, final boolean empty) {
                    super.updateItem(viewType, empty);
                    if (viewType != null) {
                        setText(viewType.getDescription());
                    } else {
                        setText(null);
                    }
                }
            };
        });
        this.setConverter(new StringConverter<GameOptions.GameViewType>() {
            @Override
            public String toString(final GameOptions.GameViewType viewType) {
                if (viewType != null) {
                    return viewType.getDescription();
                } else {
                    return null;
                }
            }

            @Override
            public GameOptions.GameViewType fromString(final String viewName) {
                if (viewName != null) {
                    for (GameOptions.GameViewType viewType : ViewSelectorCombobox.this.getItems()) {
                        if (viewType.getDescription().equals(viewName)) {
                            return viewType;
                        }
                    }
                }
                return null;
            }

        });
    }

    /**
     * Returns the preview image holder for the scene preview image.
     *
     * @return Scene image view holder.
     */
    public final ImageView getPreviewImage() {
        return this.scenePreviewImage;
    }

    /**
     * The local change listener.
     */
    private void setChangeListener() {
        valueProperty().addListener((ov, oldV, newV) -> {
            try {
                scenePreviewImage.setImage(GameResourcesHelper.getViewPreview(newV));
            } catch (Exception ex) {
                LOG.error("Could not load preview", ex);
                scenePreviewImage.setImage(null);
            }
        });
    }

}
