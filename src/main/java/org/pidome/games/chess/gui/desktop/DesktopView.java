/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop;

import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.gui.desktop.dialog.ParentGameInteractionInterface;
import org.pidome.games.chess.gui.AbstractAppView;

/**
 * The main game class.
 *
 * @author John
 */
public class DesktopView extends AbstractAppView implements ParentGameInteractionInterface {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(DesktopView.class);

    /**
     * The menu of the game.
     */
    private final DesktopMenu gameMenu;

    /**
     * The game's status bar.
     */
    private final DesktopStatusBar statusBar;

    /**
     * Game constructor.
     */
    public DesktopView() {
        setBackDrop();
        gameMenu = new DesktopMenu(this);
        statusBar = new DesktopStatusBar();
        this.setLeft(new Pane());
        this.setRight(new Pane());
        this.setTop(gameMenu);
        this.setBottom(statusBar);
    }

    /**
     * Sets the main window background image.
     */
    private void setBackDrop() {
        getCenterContent().getStyleClass().add("backdrop");
    }

    /**
     * {@inheritDoc}
     */
    public final void postActions() {
        gameMenu.assignHotKeys();
    }

}
