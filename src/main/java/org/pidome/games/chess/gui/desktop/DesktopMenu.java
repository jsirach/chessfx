/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop;

import java.util.Objects;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import org.pidome.games.chess.director.GameDirector;
import org.pidome.games.chess.gui.desktop.dialog.ExitAppPopup;
import org.pidome.games.chess.gui.desktop.dialog.ExitGamePopup;
import org.pidome.games.chess.gui.desktop.dialog.ParentGameInteractionInterface;
import org.pidome.games.chess.gui.desktop.dialog.newgame.NewGameDialog;
import org.pidome.games.chess.gui.desktop.dialog.settings.ApplicationSettingsDialog;
import org.pidome.games.chess.gui.desktop.editor.EditSceneDialog;
import org.pidome.games.chess.gui.desktop.editor.NewSceneDialog;
import org.pidome.games.chess.model.options.EditorGameOptions;
import org.pidome.games.chess.model.options.PlayerGameOptions;
import org.pidome.games.chess.model.options.VersusGameOptions;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nStringProperty;
import org.pidome.games.chess.utils.FXUtils;

/**
 * The menu for the game.
 *
 * @author John
 */
public class DesktopMenu extends MenuBar {

    /**
     * The listener for the main menu events.
     */
    private final ParentGameInteractionInterface parentGameInteraction;

    /**
     * Menu constructor.
     *
     * @param parentGameInteraction interface for parent game interactions.
     */
    public DesktopMenu(final ParentGameInteractionInterface parentGameInteraction) {
        this.getStyleClass().add("top-menu-bar");
        this.parentGameInteraction = parentGameInteraction;
        build();
    }

    /**
     * Build the menu items.
     */
    private void build() {
        buildFileMenu();
        buildGameMenu();
        buildSettingsMenu();
        buildExtraMenu();
    }

    /**
     * Build the file menu.
     */
    private void buildFileMenu() {
        final Menu menuFile = new Menu();
        menuFile.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.MENU_FILE));

        final MenuItem exitAppItem = new MenuItem();
        exitAppItem.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.MENU_FILE_EXIT));
        exitAppItem.setOnAction(event -> {
            ExitAppPopup popup = new ExitAppPopup();
            popup.display();
        });

        menuFile.getItems().addAll(exitAppItem);

        this.getMenus().add(menuFile);
    }

    private void buildExtraMenu() {
        final Menu menuExtra = new Menu();
        menuExtra.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.MENU_EXTRA));

        final Menu sceneEditorMenu = new Menu();
        sceneEditorMenu.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.SCENE_EDITOR));

        final MenuItem newScene = new MenuItem();
        newScene.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.SCENE_EDITOR_NEW_SCENE));
        newScene.setOnAction(event -> {
            final EditorGameOptions sceneEditorOptions = new EditorGameOptions();
            FXUtils.runOnFXThread(() -> {
                NewSceneDialog newEditDialog = new NewSceneDialog(sceneEditorOptions);
                newEditDialog.setOnAction(() -> {
                    if (!Objects.isNull(newEditDialog.createNewScene())) {
                        FXUtils.runOnFXThread(() -> {
                            parentGameInteraction.newGame(new GameDirector(sceneEditorOptions));
                        });
                    }
                });
                newEditDialog.setOnCancel(() -> {
                });
                newEditDialog.display();
            });
        });

        final MenuItem editScene = new MenuItem();
        editScene.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.SCENE_EDITOR_EDIT_SCENE));
        editScene.setOnAction(event -> {
            final EditorGameOptions sceneEditorOptions = new EditorGameOptions();
            FXUtils.runOnFXThread(() -> {
                EditSceneDialog editDialog = new EditSceneDialog(sceneEditorOptions);
                editDialog.setOnAction(() -> {
                    FXUtils.runOnFXThread(() -> {
                        parentGameInteraction.newGame(new GameDirector(sceneEditorOptions));
                    });
                });
                editDialog.setOnCancel(() -> {
                });
                editDialog.display();
            });
        });

        sceneEditorMenu.getItems().addAll(newScene, editScene);

        menuExtra.getItems().addAll(sceneEditorMenu);

        this.getMenus().add(menuExtra);
    }

    /**
     * Build the file menu.
     */
    private void buildGameMenu() {
        final Menu menuGame = new Menu();
        menuGame.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.MENU_GAME));

        final Menu menuNewGame = new Menu();
        menuNewGame.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.MENU_GAME_NEW));

        TooltippableMenuItem newGameItem = new TooltippableMenuItem(
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_VERSUS),
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_VERSUS_DESCRIPTION));

        newGameItem.setOnAction(event -> initiateNewVersusGame().run());

        final TooltippableMenuItem newGameSpectator = new TooltippableMenuItem(
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_SPECTATE),
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_SPECTATE_DESCRIPTION));
        newGameSpectator.setDisable(true);

        final TooltippableMenuItem newGamePlayer = new TooltippableMenuItem(
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_PLAYER),
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_PLAYER_DESCRIPTION));
        newGamePlayer.setOnAction(event -> {
            final PlayerGameOptions playerGameOptions = new PlayerGameOptions();
            NewGameDialog newGame = new NewGameDialog(playerGameOptions);
            newGame.setOnAction(() -> {
                FXUtils.runOnFXThread(() -> {
                    parentGameInteraction.newGame(new GameDirector(playerGameOptions));
                });
            });
            newGame.setOnCancel(() -> {
            });
            newGame.display();
        });

        final TooltippableMenuItem newGameBroadcaster = new TooltippableMenuItem(
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_OBSERVE_BROADCAST),
                I18nProxy.getInstance().get(I18nApplicationKey.GAME_PLAY_TYPE_OBSERVE_BROADCAST_DESCRIPTION));
        newGameBroadcaster.setDisable(true);

        menuNewGame.getItems().addAll(newGameItem, newGameSpectator, newGamePlayer, new SeparatorMenuItem(), newGameBroadcaster);

        final Menu menuCurrentGame = new Menu("Current game");
        menuCurrentGame.disableProperty().bind(parentGameInteraction.currentGameProperty().isNull());

        final MenuItem currentGameSave = new MenuItem("Save game");
        currentGameSave.setDisable(true);

        final MenuItem currentGameQuit = new MenuItem("Quit game");
        currentGameQuit.setOnAction(event -> {
            ExitGamePopup exitGame = new ExitGamePopup();
            exitGame.setOnAction(() -> {
                FXUtils.runOnFXThread(() -> {
                    parentGameInteraction.currentGameProperty().setValue(null);
                });
            });
            exitGame.setOnCancel(() -> {
            });
            exitGame.display();
        });
        menuCurrentGame.getItems().addAll(currentGameSave, new SeparatorMenuItem(), currentGameQuit);

        menuGame.getItems().addAll(menuNewGame, menuCurrentGame);

        this.getMenus().add(menuGame);
    }

    /**
     * Builds the settings menu.
     */
    private void buildSettingsMenu() {
        final Menu settings = new Menu();
        settings.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.MENU_SETTINGS));
        final MenuItem applicationSettings = new MenuItem();
        applicationSettings.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.MENU_SETTINGS_APPLICATION));
        applicationSettings.setOnAction(event -> {
            event.consume();
            ApplicationSettingsDialog applicationSettingsPopup = new ApplicationSettingsDialog();
            applicationSettingsPopup.setOnCancel(applicationSettingsPopup.onCancelAction());
            applicationSettingsPopup.show();
        });
        settings.getItems().add(applicationSettings);

        this.getMenus().add(settings);
    }

    /**
     * Assign hot keys for quickly calling.
     */
    protected void assignHotKeys() {
        this.getScene().getAccelerators().put(new KeyCodeCombination(KeyCode.F2), initiateNewVersusGame());
    }

    /**
     * Initiate a new game.
     *
     * @return The runnable.
     */
    private Runnable initiateNewVersusGame() {
        return () -> {
            final VersusGameOptions versusGameOptions = new VersusGameOptions();
            NewGameDialog newGame = new NewGameDialog(versusGameOptions);
            newGame.setOnAction(() -> {
                FXUtils.runOnFXThread(() -> {
                    parentGameInteraction.newGame(new GameDirector(versusGameOptions));
                });
            });
            newGame.setOnCancel(() -> {
            });
            newGame.display();
        };
    }

    /**
     * A menu item capable of containing a tooltip.
     */
    private static class TooltippableMenuItem extends CustomMenuItem {

        /**
         * The label for the menu.
         */
        private final Label menuItemLabel = new Label();

        /**
         * Constructor.
         */
        private TooltippableMenuItem(I18nStringProperty menuText, I18nStringProperty descriptionText) {
            this.setContent(menuItemLabel);
            menuItemLabel.textProperty().bind(menuText);
            setTooltip(descriptionText);
        }

        /**
         * Sets the tooltip.
         *
         * @param textProperty text property for the tooltip.
         */
        private void setTooltip(I18nStringProperty textProperty) {
            Tooltip tooltip = new Tooltip();
            tooltip.textProperty().bind(textProperty);
            Tooltip.install(menuItemLabel, tooltip);
        }

    }

}
