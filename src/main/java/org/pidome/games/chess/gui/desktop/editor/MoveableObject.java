/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import org.pidome.games.chess.world.views.PerspectiveSceneView;

/**
 * A moveable object.
 *
 * This code has been gently stolen from
 * https://stackoverflow.com/questions/28731460/javafx-moving-3d-objects-with-mouse-on-a-virtual-plane
 * from Jose Pereda's answer with some small modifications.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public abstract class MoveableObject extends Group {

    private double mousePosX, mousePosY;

    private Point3D vecIni, vecPos;
    private double distance;
    private boolean isPicking;

    private final PerspectiveSceneView sceneView;

    public MoveableObject(final PerspectiveSceneView sceneView) {
        this.sceneView = sceneView;
        this.setHandlers();
    }

    private void setHandlers() {
        this.setOnMousePressed((MouseEvent me) -> {
            this.sceneView.setOrientationEnabled(false);
            Scene scene = this.sceneView.getPerspectiveCamera().getScene();
            mousePosX = me.getSceneX();
            mousePosY = me.getSceneY();
            PickResult pr = me.getPickResult();
            distance = pr.getIntersectedDistance();
            isPicking = true;
            vecIni = unProjectDirection(mousePosX, mousePosY, scene.getWidth(), scene.getHeight());
        });
        this.setOnMouseDragged((MouseEvent me) -> {
            Scene scene = this.sceneView.getPerspectiveCamera().getScene();
            mousePosX = me.getSceneX();
            mousePosY = me.getSceneY();
            if (isPicking) {
                vecPos = unProjectDirection(mousePosX, mousePosY, scene.getWidth(), scene.getHeight());
                Point3D p = vecPos.subtract(vecIni).multiply(distance);
                this.setTranslateX(this.getTranslateX() + p.getX());
                this.setTranslateY(this.getTranslateY() + p.getY());
                this.setTranslateZ(this.getTranslateZ() + p.getZ());
                vecIni = vecPos;
                PickResult pr = me.getPickResult();
                distance = pr.getIntersectedDistance();
            }
        });
        this.setOnMouseReleased(event -> {
            this.sceneView.setOrientationEnabled(true);
            isPicking = false;
        });
    }

    /*
     From fx83dfeatures.Camera3D
     http://hg.openjdk.java.net/openjfx/8u-dev/rt/file/5d371a34ddf1/apps/toys/FX8-3DFeatures/src/fx83dfeatures/Camera3D.java
     */
    public Point3D unProjectDirection(double sceneX, double sceneY, double sWidth, double sHeight) {
        PerspectiveCamera camera = sceneView.getPerspectiveCamera();
        double tanHFov = Math.tan(Math.toRadians(camera.getFieldOfView()) * 0.5f);
        Point3D vMouse = new Point3D(tanHFov * (2 * sceneX / sWidth - 1), tanHFov * (2 * sceneY / sWidth - sHeight / sWidth), 1);

        Point3D result = localToSceneDirection(vMouse);
        return result.normalize();
    }

    public Point3D localToScene(Point3D pt) {
        PerspectiveCamera camera = sceneView.getPerspectiveCamera();
        Point3D res = camera.localToParentTransformProperty().get().transform(pt);
        if (camera.getParent() != null) {
            res = camera.getParent().localToSceneTransformProperty().get().transform(res);
        }
        return res;
    }

    public Point3D localToSceneDirection(Point3D dir) {
        Point3D res = localToScene(dir);
        return res.subtract(localToScene(new Point3D(0, 0, 0)));
    }

}
