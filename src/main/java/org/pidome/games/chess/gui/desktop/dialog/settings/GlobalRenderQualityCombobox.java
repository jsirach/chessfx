/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.settings;

import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.settings.GraphicsSettings.RenderQuality;

/**
 * A scene selector.
 *
 * @author johns
 */
public class GlobalRenderQualityCombobox extends ComboBox<RenderQuality> {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(GlobalRenderQualityCombobox.class);

    /**
     * Constructor.
     */
    public GlobalRenderQualityCombobox() {
        this.setViewFactories();
        this.getItems().addAll(RenderQuality.values());
    }

    /**
     * Sets the view factory.
     */
    private void setViewFactories() {
        setCellFactory((ListView<RenderQuality> param) -> {
            return new ListCell<RenderQuality>() {
                @Override
                public void updateItem(final RenderQuality value, final boolean empty) {
                    super.updateItem(value, empty);
                    if (value == null) {
                        setText(null);
                    } else {
                        setText(value.getLabel());
                    }
                }
            };
        });
        this.setConverter(new StringConverter<RenderQuality>() {

            @Override
            public String toString(final RenderQuality value) {
                return value == null ? null : value.getLabel();
            }

            @Override
            public RenderQuality fromString(final String string) {
                return string == null ? null : RenderQuality.valueOf(string);
            }
        });
    }

}
