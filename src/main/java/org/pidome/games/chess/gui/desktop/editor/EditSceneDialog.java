/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.gui.desktop.dialog.newgame.SceneSelectorCombobox;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.resources.SceneResource;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;

/**
 * Dialog to continue editing a scene.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class EditSceneDialog extends GamePopup {

    /**
     * 3D scene selector.
     */
    private final SceneSelectorCombobox sceneSelector = new SceneSelectorCombobox();

    /**
     * The content pane.
     */
    private final GridPane contentPane = new GridPane();

    /**
     * First column.
     */
    private final ColumnConstraints firstColumn = new ColumnConstraints() {
        {
            setPrefWidth(170);
        }
    };

    /**
     * Second column.
     */
    private final ColumnConstraints secondColumn = new ColumnConstraints() {
        {
            setPrefWidth(85);
        }
    };

    /**
     * Third column.
     */
    private final ColumnConstraints thirdColumn = new ColumnConstraints() {
        {
            setPrefWidth(250);
        }
    };

    /**
     * Resource change listener
     */
    private final ChangeListener<SceneResource> resourceListener = this::setSelectedValues;

    /**
     * Scene description.
     */
    private final Text description = new Text();
    /**
     * Scene author.
     */
    private final Text author = new Text();
    /**
     * Scene author website.
     */
    private final Text authorWebsite = new Text();
    /**
     * Scene author email address.
     */
    private final Text authorEmail = new Text();

    /**
     * The game options.
     */
    private final AbstractInteractiveGameOptions options;

    public EditSceneDialog(final AbstractInteractiveGameOptions options) {
        super(I18nProxy.getInstance().get(I18nApplicationKey.SCENE_EDITOR), new SimpleStringProperty("Edit scene"));
        this.options = options;
        EditorDefaults.defaultOptions(this.options);
        EditorDefaults.defaultPerspectiveOptions(this.options);
        constructDialog();
    }

    private void constructDialog() {
        contentPane.setHgap(10);
        contentPane.setVgap(5);

        contentPane.getColumnConstraints().addAll(firstColumn, secondColumn, thirdColumn);

        sceneSelector.buildSelectiveResourceList(GameResourcesHelper.ResourceLocation.COMMUNITY);
        this.sceneSelector.getSelectionModel().selectedItemProperty().addListener(resourceListener);
        GridPane.setFillWidth(sceneSelector, Boolean.TRUE);
        sceneSelector.setMaxWidth(Double.MAX_VALUE);

        GridPane.setHalignment(sceneSelector.getPreviewImage(), HPos.CENTER);
        GridPane.setValignment(sceneSelector.getPreviewImage(), VPos.CENTER);
        GridPane.setMargin(sceneSelector, new Insets(0, 0, 5, 0));
        sceneSelector.getPreviewImage().setFitWidth(150);

        this.contentPane.add(sceneSelector, 0, 0, 3, 1);
        this.contentPane.add(sceneSelector.getPreviewImage(), 0, 1, 1, 5);

        this.contentPane.add(new Label("Author"), 1, 2);
        this.contentPane.add(author, 2, 2);

        this.contentPane.add(new Label("Website"), 1, 3);
        this.contentPane.add(authorWebsite, 2, 3);

        this.contentPane.add(new Label("Email"), 1, 4);
        this.contentPane.add(authorEmail, 2, 4);

        Label desc = new Label("Description");
        GridPane.setValignment(desc, VPos.TOP);
        GridPane.setValignment(description, VPos.TOP);
        description.wrappingWidthProperty().bind(thirdColumn.prefWidthProperty());
        this.contentPane.add(desc, 1, 5);
        this.contentPane.add(description, 2, 5, 2, 1);

        this.setCenter(this.contentPane);

    }

    private void setSelectedValues(final ObservableValue obs, final SceneResource oldResource, final SceneResource newResource) {
        description.setText(": " + newResource.getDescription());
        author.setText(": " + newResource.getAuthor());
        authorWebsite.setText(": " + newResource.getAuthorWebsite());
        authorEmail.setText(": " + newResource.getAuthorEmail());
        this.options.getScene3DResource().setValue(newResource);
    }
}
