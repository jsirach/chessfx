/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.pidome.games.chess.utils.FXUtils;

/**
 * base class for game popups.
 *
 * @author John
 */
public abstract class BasePopup extends Group {

    /**
     * Enum defining how the popup should be decorated.
     */
    public enum PopupDecoration {
        /**
         * Default decoration with application defined background.
         */
        DEFAULT,
        /**
         * No background.
         */
        TRANSPARENT;
    }

    /**
     * The content pane.
     */
    private final BorderPane contentPane = new BorderPane();

    /**
     * Stackpane used for blocking the view.
     */
    private StackPane BLOCK_PANE;

    /**
     * To blur the background.
     */
    private static final ColorAdjust BLUR_BACKGROUND;

    /**
     * Static initializer, reduces creation time.
     */
    static {
        BLUR_BACKGROUND = new ColorAdjust();
        final BoxBlur blur = new BoxBlur();
        BLUR_BACKGROUND.setInput(blur);
        BLUR_BACKGROUND.setBrightness(-0.5);
    }

    /**
     * To window root view.
     */
    private Pane root;

    /**
     * The popup decoration.
     */
    private PopupDecoration decoration = PopupDecoration.DEFAULT;

    /**
     * Base popup constructor.
     *
     * @param owner The owner of the popup.
     */
    public BasePopup(final Stage owner) {
        root = (Pane) owner.getScene().getRoot();
        this.contentPane.setMaxSize(root.getWidth(), root.getHeight());
    }

    /**
     * Sets how the popup should be decorated.
     *
     * @param decoration The decoration to apply.
     */
    public final void setDecoration(final GamePopup.PopupDecoration decoration) {
        this.decoration = decoration;
    }

    /**
     * Executed when a popup is cancelled.
     */
    protected abstract void runOnCancelAction();

    /**
     * Set the center content.
     *
     * @param node The content to set in the center.
     */
    public final void setTop(final Node node) {
        if (this.contentPane.getTop() == null || !this.contentPane.getTop().equals(node)) {
            node.getStyleClass().add("header");
            this.contentPane.setTop(node);
        }
    }

    /**
     * Set the center content.
     *
     * @param node The content to set in the center.
     */
    public final void setCenter(final Node node) {
        if (this.contentPane.getCenter() == null || !this.contentPane.getCenter().equals(node)) {
            node.getStyleClass().add("content");
            this.contentPane.setCenter(node);
        }
    }

    /**
     * Sets the bottom content.
     *
     * @param node The node to set.
     */
    public final void setBottom(final Node node) {
        if (this.contentPane.getBottom() == null || !this.contentPane.getBottom().equals(node)) {
            node.getStyleClass().add("footer");
            this.contentPane.setBottom(node);
        }
    }

    /**
     * Shows the display and waits for closing.
     */
    public void display() {
        display(false);
    }

    /**
     * Shows the display.
     *
     * @param showAndWait set to true for show and wait, false to be
     * dismissible.
     */
    public final void display(final boolean showAndWait) {
        FXUtils.runOnFXThread(() -> {
            this.getStyleClass().addAll("no-border", "transparent-background");
            this.contentPane.getStyleClass().add(this.decoration.equals(PopupDecoration.DEFAULT) ? "game-popup" : "game-popup.undecorated");
            buildSkeleton();
            getChildren().add(contentPane);
            BLOCK_PANE = new StackPane();
            BLOCK_PANE.getStyleClass().add("transparent-background");
            if (!showAndWait) {
                BLOCK_PANE.setOnMouseClicked(event -> {
                    runOnCancelAction();
                    close();
                });
            } else {
                root.getChildren().get(0).setEffect(BLUR_BACKGROUND);
            }
            root.getChildren().addAll(BLOCK_PANE, this);
        });
    }

    /**
     * Builds the popup skeleton.
     */
    protected abstract void buildSkeleton();

    /**
     * Clean any held pointers.
     */
    public void close() {
        if (root != null) {
            if (root.getChildren().get(0) != null) {
                root.getChildren().get(0).setEffect(null);
            }
            root.getChildren().removeAll(BLOCK_PANE, this);
            root = null;
        }
    }

}
