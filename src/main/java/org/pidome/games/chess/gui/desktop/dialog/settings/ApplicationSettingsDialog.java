/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.settings;

import org.pidome.games.chess.gui.desktop.dialog.DialogTabPane;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.settings.Settings;
import org.pidome.games.chess.utils.FXUtils;

/**
 * Application settings.
 *
 * @author johns
 */
public class ApplicationSettingsDialog extends GamePopup {

    /**
     * The tab pane showed.
     */
    private final DialogTabPane tabPane = new DialogTabPane();

    /**
     * Generic tab.
     */
    private final GenericSettingsTab genericTab = new GenericSettingsTab();

    /**
     * Game quality.
     */
    private final GraphicsSettingsTab gameQualityTab = new GraphicsSettingsTab();

    /**
     * Constructor.
     */
    public ApplicationSettingsDialog() {
        super(I18nProxy.getInstance().get(I18nApplicationKey.SETTINGS_APPLICATION_TITLE));
    }

    /**
     * Creates the content.
     */
    private void constructContent() {
        tabPane.getTabs().addAll(genericTab, gameQualityTab);
        this.setCenter(tabPane);
        setOnAction();
    }

    /**
     * Shows the popup.
     */
    public final void show() {
        constructContent();
        super.display(true);
    }

    /**
     * Overrides the game popup default setOnAction to be able to perform more
     * methods.
     */
    public final void setOnAction() {
        super.setOnAction(() -> {
            tabPane.getGameTabs().forEach(tab -> {
                tab.onAction();
            });
            Settings.save();
        });
    }

    /**
     * Cancel out any changes.
     */
    public Runnable onCancelAction() {
        return () -> {
            FXUtils.runOnFXThread(() -> {
                Settings.revert();
            });
        };
    }

    @Override
    public final void close() {
        super.close();
        tabPane.getGameTabs().forEach(tab -> {
            tab.dispose();
        });
    }

}
