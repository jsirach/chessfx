/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog;

import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TabPane;
import org.pidome.games.chess.utils.controls.GameTab;

/**
 * Helper class for assigning tabs in a dialog.
 *
 * use the *GameTabs methods for proper tab adding. Did not yet found another
 * way for nice adding and removing.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class DialogTabPane extends TabPane {

    /**
     * The game tabs.
     */
    private ObservableList<GameTab> translated = FXCollections.observableArrayList();

    /**
     * Helper property for identifying all tabs which need validation are set
     * correctly.
     */
    private BooleanProperty contentValidated = new SimpleBooleanProperty(Boolean.TRUE);

    /**
     * constructor.
     */
    public DialogTabPane() {
        super();
        translated.addListener((ListChangeListener<GameTab>) change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    this.getTabs().addAll(change.getAddedSubList());
                } else if (change.wasRemoved()) {
                    this.getTabs().removeAll(change.getRemoved());
                }
            }
        });
    }

    /**
     * Adds game tabs.
     *
     * @param tabs The tabs to add.
     */
    public void addGameTabs(GameTab... tabs) {
        translated.addAll(tabs);
    }

    /**
     * Returns the gametab list.
     *
     * @return The game tabs.
     */
    public ObservableList<GameTab> getGameTabs() {
        return translated;
    }

    /**
     * Returns the property for content validation binding updates.
     *
     * @return The boolean property for content validation.
     */
    public BooleanProperty contentValidated() {
        return this.contentValidated;
    }

    /**
     * Creates the validation bindings on the tabs.
     */
    public final void createValidationBindings() {
        contentValidated.bind(Bindings.createBooleanBinding(()
                -> getGameTabs().stream().allMatch(GameTab::isContentValid),
                getGameTabs().stream().map(GameTab::contentValid).toArray(Observable[]::new))
        );
    }

}
