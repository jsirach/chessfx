/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.StringConverter;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.controls.H3;
import org.pidome.games.chess.utils.controls.H4;
import org.pidome.games.chess.world.config.PerspectiveSceneLightAttenuationConfig;
import org.pidome.games.chess.world.config.PerspectiveSceneLightSourceConfig;

/**
 * The properties editor for a light.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class LightFixtureProperties extends GridPane {

    /**
     * Primary view column.
     */
    private final ColumnConstraints labelColumn = new ColumnConstraints() {
        {
            setPercentWidth(40);
        }
    };

    /**
     * Primary view column.
     */
    private final ColumnConstraints valueColumn = new ColumnConstraints() {
        {
            setPercentWidth(60);
        }
    };

    private final H3 lightName = new H3();

    private final Slider hueSlider = controlSlider("Adjust Hue value", 0.0, 360.0);
    private final Slider saturationSlider = controlSlider("Adjust Saturation value", 0.0, 1.0);
    private final Slider brightnessSlider = controlSlider("Adjust Brightness value", 0.0, 1.0);

    private final Spinner<Double> xAxisSpinner = new Spinner(new SpinnerValueFactory.DoubleSpinnerValueFactory(-Double.MAX_VALUE, Double.MAX_VALUE));
    private final Spinner<Double> yAxisSpinner = new Spinner(new SpinnerValueFactory.DoubleSpinnerValueFactory(-Double.MAX_VALUE, Double.MAX_VALUE));
    private final Spinner<Double> zAxisSpinner = new Spinner(new SpinnerValueFactory.DoubleSpinnerValueFactory(-Double.MAX_VALUE, Double.MAX_VALUE));

    private final LightAttenuationCombobox attenuationSelector = new LightAttenuationCombobox();
    private final Spinner<Double> attenuationValue = new Spinner(newAttenuationValueSpinnerFactory());
    private final Spinner<Double> attenuationMaxRange = new Spinner(new SpinnerValueFactory.DoubleSpinnerValueFactory(0d, Double.MAX_VALUE));

    private ObjectProperty<PerspectiveSceneLightSourceConfig> lightConfigProperty = new SimpleObjectProperty<>();
    private final ChangeListener<PerspectiveSceneLightSourceConfig> configListener = this::updateLightSource;

    private final PointLightAxis axisGroup;

    private BooleanProperty itemsDisabled = new SimpleBooleanProperty(Boolean.TRUE);

    private final LightfixturesEditor editor;

    public LightFixtureProperties(final LightfixturesEditor editor, final PointLightAxis axisGroup) {
        this.getColumnConstraints().addAll(labelColumn, valueColumn);
        this.editor = editor;
        this.axisGroup = axisGroup;
        setVgap(3);
        this.setPadding(new Insets(0, 0, 0, 10));
        buildView();
        lightConfigProperty.addListener(configListener);
    }

    private void buildView() {
        xAxisSpinner.disableProperty().bind(itemsDisabled);
        yAxisSpinner.disableProperty().bind(itemsDisabled);
        zAxisSpinner.disableProperty().bind(itemsDisabled);
        hueSlider.disableProperty().bind(itemsDisabled);
        saturationSlider.disableProperty().bind(itemsDisabled);
        brightnessSlider.disableProperty().bind(itemsDisabled);
        attenuationSelector.disableProperty().bind(itemsDisabled);
        attenuationValue.disableProperty().bind(
                itemsDisabled.or(attenuationSelector.valueProperty().isEqualTo(PerspectiveSceneLightAttenuationConfig.Type.DEFAULT))
        );
        attenuationMaxRange.disableProperty().bind(attenuationValue.disableProperty());

        xAxisSpinner.getValueFactory().setValue(0d);
        yAxisSpinner.getValueFactory().setValue(0d);
        zAxisSpinner.getValueFactory().setValue(0d);

        xAxisSpinner.setEditable(true);
        yAxisSpinner.setEditable(true);
        zAxisSpinner.setEditable(true);

        attenuationValue.setEditable(true);
        attenuationMaxRange.setEditable(true);

        set100PercentLabelFormatter(saturationSlider);
        set100PercentLabelFormatter(brightnessSlider);

        add(lightName, 0, 0, 2, 1);
        addRow(1, new H4("Location"));
        addRow(2, axisLabel(Color.RED, "X - Axis"), xAxisSpinner);
        addRow(3, axisLabel(Color.GREEN, "Y - Axis"), yAxisSpinner);
        addRow(4, axisLabel(Color.BLUE, "Z - Axis"), zAxisSpinner);

        addRow(5, new H4("Properties"));
        addRow(6, new Label("Attenuation"), attenuationSelector);
        addRow(7, new Label("Value"), attenuationValue);
        addRow(8, new Label("Maximum range"), attenuationMaxRange);

        addRow(9, new H4("Color"));
        addRow(10, new Label("Hue"), hueSlider);
        addRow(11, new Label("Saturation"), saturationSlider);
        addRow(12, new Label("Brightness"), brightnessSlider);

    }

    public BooleanProperty controlsDisabledProperty() {
        return this.itemsDisabled;
    }

    public void updateLightsource(final PerspectiveSceneLightSourceConfig lightConfig) {
        this.lightConfigProperty.setValue(lightConfig);
    }

    private void updateLightSource(final ObservableValue obs, PerspectiveSceneLightSourceConfig oldVal, PerspectiveSceneLightSourceConfig newVal) {
        if (!Objects.isNull(oldVal)) {

            this.axisGroup.bindToLightConfiguration(null);

            attenuationSelector.valueProperty().unbindBidirectional(oldVal.getAttenuation().typeProperty());

            lightName.textProperty().unbindBidirectional(oldVal.nameProperty());

            hueSlider.valueProperty().unbindBidirectional(oldVal.getColor().hueProperty());
            saturationSlider.valueProperty().unbindBidirectional(oldVal.getColor().saturationProperty());
            brightnessSlider.valueProperty().unbindBidirectional(oldVal.getColor().brightnessProperty());

            xAxisSpinner.getValueFactory().valueProperty().unbindBidirectional(oldVal.getLocation().xProperty().asObject());
            yAxisSpinner.getValueFactory().valueProperty().unbindBidirectional(oldVal.getLocation().yProperty().asObject());
            zAxisSpinner.getValueFactory().valueProperty().unbindBidirectional(oldVal.getLocation().zProperty().asObject());

            xAxisSpinner.setValueFactory(
                    new SpinnerValueFactory.DoubleSpinnerValueFactory(-Double.MAX_VALUE, 1.0)
            );
            yAxisSpinner.setValueFactory(
                    new SpinnerValueFactory.DoubleSpinnerValueFactory(-Double.MAX_VALUE, Double.MAX_VALUE)
            );
            zAxisSpinner.setValueFactory(
                    new SpinnerValueFactory.DoubleSpinnerValueFactory(0d, Double.MAX_VALUE)
            );

            FXUtils.runOnFXThread(() -> {
                attenuationSelector.setValue(null);
            });

            xAxisSpinner.getValueFactory().setValue(0d);
            yAxisSpinner.getValueFactory().setValue(0d);
            zAxisSpinner.getValueFactory().setValue(0d);

            attenuationValue.setValueFactory(newAttenuationValueSpinnerFactory());
            attenuationMaxRange.setValueFactory(
                    new SpinnerValueFactory.DoubleSpinnerValueFactory(0d, Double.MAX_VALUE)
            );
            attenuationValue.getValueFactory().setValue(0d);
            attenuationMaxRange.getValueFactory().setValue(0d);

            FXUtils.runOnFXThread(() -> {
                lightName.setText("");
            });
        }
        if (!Objects.isNull(newVal)) {

            this.axisGroup.bindToLightConfiguration(newVal);

            lightName.setText(newVal.nameProperty().getValueSafe());
            hueSlider.valueProperty().setValue(newVal.getColor().hueProperty().doubleValue());
            saturationSlider.valueProperty().setValue(newVal.getColor().saturationProperty().doubleValue());
            brightnessSlider.valueProperty().setValue(newVal.getColor().brightnessProperty().doubleValue());

            xAxisSpinner.getValueFactory().setValue(newVal.getLocation().xProperty().doubleValue());
            yAxisSpinner.getValueFactory().setValue(newVal.getLocation().yProperty().doubleValue());
            zAxisSpinner.getValueFactory().setValue(newVal.getLocation().zProperty().doubleValue());

            lightName.textProperty().bindBidirectional(newVal.nameProperty());

            hueSlider.valueProperty().bindBidirectional(newVal.getColor().hueProperty());
            saturationSlider.valueProperty().bindBidirectional(newVal.getColor().saturationProperty());
            brightnessSlider.valueProperty().bindBidirectional(newVal.getColor().brightnessProperty());

            xAxisSpinner.getValueFactory().valueProperty().bindBidirectional(newVal.getLocation().xProperty().asObject());
            yAxisSpinner.getValueFactory().valueProperty().bindBidirectional(newVal.getLocation().yProperty().asObject());
            zAxisSpinner.getValueFactory().valueProperty().bindBidirectional(newVal.getLocation().zProperty().asObject());

            attenuationSelector.valueProperty().bindBidirectional(newVal.getAttenuation().typeProperty());
            attenuationValue.getValueFactory().valueProperty().bindBidirectional(newVal.getAttenuation().valueProperty().asObject());
            attenuationMaxRange.getValueFactory().valueProperty().bindBidirectional(newVal.getAttenuation().maxRangeProperty().asObject());
        }
    }

    private Slider controlSlider(final String toolTip,
            final double min, final double max) {

        Slider slider = new Slider();
        slider.setTooltip(new Tooltip(toolTip));
        slider.setMajorTickUnit(max);
        slider.setShowTickLabels(true);
        slider.setMin(min);
        slider.setMax(max);
        return slider;
    }

    private Label axisLabel(final Paint color, final String title) {
        Rectangle rectangle = new Rectangle();
        rectangle.setFill(color);
        rectangle.setWidth(11);
        rectangle.setHeight(11);
        rectangle.setArcWidth(3);
        rectangle.setArcHeight(3);
        Label label = new Label(title);
        label.setGraphic(rectangle);
        return label;
    }

    private void set100PercentLabelFormatter(final Slider slider) {
        slider.setLabelFormatter(new StringConverter<Double>() {
            @Override
            public String toString(Double n) {
                return Math.round(n * 100.0) + " %";
            }

            @Override
            public Double fromString(String s) {
                return null;
            }
        });
    }

    private SpinnerValueFactory.DoubleSpinnerValueFactory newAttenuationValueSpinnerFactory() {
        SpinnerValueFactory.DoubleSpinnerValueFactory factory
                = new SpinnerValueFactory.DoubleSpinnerValueFactory(0d, 1.0);
        factory.setConverter(new StringConverter<Double>() {
            private final DecimalFormat df = new DecimalFormat("#.#########");

            @Override
            public String toString(Double value) {
                if (value == null) {
                    return "";
                }
                return df.format(value);
            }

            @Override
            public Double fromString(String value) {
                try {
                    if (value == null) {
                        return null;
                    }
                    value = value.trim();
                    if (value.length() < 1) {
                        return null;
                    }
                    return df.parse(value).doubleValue();
                } catch (ParseException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        return factory;
    }

}
