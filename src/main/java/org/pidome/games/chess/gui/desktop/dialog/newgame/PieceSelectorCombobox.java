/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.boards.BoardOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.ThreadTools;
import org.pidome.games.chess.resources.PiecesResource;

/**
 * The piece selector.
 *
 * @author johns
 */
public class PieceSelectorCombobox extends ComboBox<PiecesResource> {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(PieceSelectorCombobox.class);

    /**
     * Options property.
     */
    private final ObjectProperty<? extends BoardOptions> options;

    /**
     * The view type for this combobox.
     */
    private final GameOptions.GameViewType viewType;

    /**
     * Pieces preview image.
     */
    private final ImageView piecesPreviewImage = new ImageView();

    /**
     * Constructor.
     *
     * @param viewType The view type.
     * @param options The board options to set the value.
     */
    public PieceSelectorCombobox(final GameOptions.GameViewType viewType, final ObjectProperty<? extends BoardOptions> options) {
        this.options = options;
        this.viewType = viewType;
        this.piecesPreviewImage.setPreserveRatio(true);
        this.setViewFactories();

        valueProperty().addListener((obs, oldVal, newVal) -> {
            this.options.get().gamePiecesProperty().setValue(newVal);
        });
        valueProperty().addListener(this::previewImageChangeListener);
    }

    /**
     * Sets the view factory.
     */
    private void setViewFactories() {
        setCellFactory((ListView<PiecesResource> param) -> {
            return new ListCell<PiecesResource>() {
                @Override
                public void updateItem(final PiecesResource item, final boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(item.getName());
                    } else {
                        setText(null);
                    }
                }
            };
        });
        this.setConverter(new StringConverter<PiecesResource>() {
            @Override
            public String toString(final PiecesResource item) {
                if (item != null) {
                    return item.getName();
                }
                return null;
            }

            @Override
            public PiecesResource fromString(final String string) {
                if (string != null) {
                    for (PiecesResource piece : PieceSelectorCombobox.this.getItems()) {
                        if (piece.getName().equals(string)) {
                            return piece;
                        }
                    }
                }
                return null;
            }

        });
    }

    /**
     * Returns the preview image holder for the pieces preview image.
     *
     * @return Pieces image view holder.
     */
    public final ImageView getPreviewImage() {
        return this.piecesPreviewImage;
    }

    /**
     * Changelistener method for updating the preview image.
     *
     * @param ov The observable.
     * @param oldValue The old value.
     * @param newValue The new value.
     */
    private void previewImageChangeListener(final ObservableValue ov, final PiecesResource oldValue, final PiecesResource newValue) {
        try {
            piecesPreviewImage.setImage(GameResourcesHelper.getPiecesPreview(newValue));
        } catch (Exception ex) {
            LOG.error("Could not load preview", ex);
            piecesPreviewImage.setImage(null);
        }
    }

    /**
     * Collects the items.
     */
    protected void buildResourceList() {
        ThreadTools.threadExecuted(() -> {
            this.getItems().addAll(GameResourcesHelper.getAvailablePieces(this.viewType));
            if (!this.getItems().isEmpty()) {
                FXUtils.runOnFXThread(() -> {
                    this.setValue(this.getItems().get(0));
                });
            }
        });
    }

}
