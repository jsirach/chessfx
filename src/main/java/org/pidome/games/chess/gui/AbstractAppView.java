/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.Chess;
import org.pidome.games.chess.director.GameDirector;
import org.pidome.games.chess.gui.desktop.DesktopMenuViewListener;
import org.pidome.games.chess.gui.desktop.DesktopView;
import org.pidome.games.chess.resources.ResourceLoaderException;
import org.pidome.games.chess.resources.ResourcesLoader;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.controls.GameLoaderProgress;

/**
 * The base class for every interface view.
 *
 * The <code>AbstractView</code> is used for generically starting a game. A game
 * is started the same way on any platform.
 *
 * Extend this view to create a platform dependent view.
 *
 * @author johns
 */
public abstract class AbstractAppView extends BorderPane implements DesktopMenuViewListener {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(DesktopView.class);

    /**
     * The center content pane.
     */
    private final StackPane centerContent = new StackPane();

    /**
     * The current game being played.
     */
    private final ObjectProperty<GameDirector> currentGame = new SimpleObjectProperty<>();

    /**
     * Constructor for setting at least the center pane.
     */
    public AbstractAppView() {
        this.setCenter(centerContent);
        currentGame.addListener(this::gameActionBinding);
    }

    /**
     * Returns the current game property.
     *
     * @return The current game property.
     */
    public final ObjectProperty<GameDirector> currentGameProperty() {
        return currentGame;
    }

    /**
     * Returns the main content pane.
     *
     * @return A stackpane.
     */
    protected final StackPane getCenterContent() {
        return this.centerContent;
    }

    /**
     * Starts a new game.
     *
     * @param game The game to start.
     */
    @Override
    public final void newGame(final GameDirector game) {
        this.currentGame.setValue(game);
    }

    /**
     * Binding for starting a new game.
     *
     * @param observable The observableValue.
     * @param oldValue the old value.
     * @param newValue the new value.
     */
    private void gameActionBinding(final ObservableValue observable, final GameDirector currentGame, final GameDirector newGame) {
        if (currentGame != null) {
            destroyGame(currentGame);
        }
        if (newGame != null) {
            startNewGame(newGame);
        }
    }

    /**
     * Starts a new game.
     *
     * @param game The game to start
     */
    private void startNewGame(final GameDirector game) {
        final ResourcesLoader resourcesLoader = new ResourcesLoader();
        final GameLoaderProgress progressLoader
                = new GameLoaderProgress(Chess.getApplicationStage(), resourcesLoader);
        new Thread(() -> {
            FXUtils.runOnFXThread(() -> {
                progressLoader.display();
            });
            CompletableFuture<Boolean> loadAssetsFuture = CompletableFuture.supplyAsync(() -> {
                LOG.debug("Initializing and loading assets");
                game.initAndLoadAssets(resourcesLoader);
                try {
                    resourcesLoader.run();
                } catch (ResourceLoaderException ex) {
                    java.util.logging.Logger.getLogger(AbstractAppView.class.getName()).log(Level.SEVERE, null, ex);
                    return Boolean.FALSE;
                }
                return Boolean.TRUE;
            }).thenCombine(CompletableFuture.supplyAsync(() -> {
                LOG.debug("Loading routines");
                return game.loadGame();
            }), (assetsLoaded, gameLoaded) -> {
                if (assetsLoaded && gameLoaded) {
                    LOG.debug("Building view");
                    game.getGameView().compose();
                    game.resetGame(false);
                    FXUtils.runOnFXThread(() -> {
                        centerContent.getChildren().add(game.getGameView());
                        LOG.debug("View build");
                    });
                    return Boolean.TRUE;
                } else {
                    return Boolean.FALSE;
                }
            });
            LOG.debug("Starting new game");
            try {
                loadAssetsFuture.get();
                LOG.debug("New game started");
            } catch (InterruptedException | ExecutionException ex) {
                LOG.error("New game start failed", ex);
            }
            FXUtils.runOnFXThread(() -> {
                progressLoader.close();
            });
        }).start();
    }

    /**
     * Destroys a game instance.
     *
     * @param game The game to destroy/end.
     */
    private void destroyGame(final GameDirector game) {
        game.endGame();
        centerContent.getChildren().remove(game.getGameView());
    }

    /**
     * for any post actions if required.
     */
    public abstract void postActions();

}
