/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.beans.property.ObjectProperty;
import javafx.geometry.HPos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.model.options.GameOptions.GameViewType;
import org.pidome.games.chess.model.options.boards.BoardOptions;

/**
 * Selector for perspective boards.
 *
 * @author John
 */
public class BoardSelectorPane extends GridPane {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(BoardSelectorPane.class);

    /**
     * 3D board selector.
     */
    private final BoardSelectorCombobox board;

    /**
     * 3D board selector.
     */
    private final PieceSelectorCombobox pieces;

    /**
     * Constructor.
     *
     * @param viewType The view type for this selector.
     * @param boardOptions The board options object.
     */
    public BoardSelectorPane(final GameViewType viewType, final ObjectProperty<? extends BoardOptions> boardOptions) {
        setHgap(5);
        setVgap(5);
        board = new BoardSelectorCombobox(viewType, boardOptions);
        pieces = new PieceSelectorCombobox(viewType, boardOptions);
        build();
    }

    private void build() {
        this.getColumnConstraints().addAll(
                new ColumnConstraints() {
            {
                this.setPercentWidth(50);
            }
        },
                new ColumnConstraints() {
            {
                this.setPercentWidth(50);
            }
        });

        GridPane.setFillWidth(board, Boolean.TRUE);
        board.setMaxWidth(Double.MAX_VALUE);
        add(board, 0, 0);

        GridPane.setHalignment(board.getPreviewImage(), HPos.CENTER);
        board.getPreviewImage().setFitWidth(150);
        add(board.getPreviewImage(), 0, 1);

        GridPane.setFillWidth(pieces, Boolean.TRUE);
        pieces.setMaxWidth(Double.MAX_VALUE);
        add(pieces, 1, 0);

        GridPane.setHalignment(pieces.getPreviewImage(), HPos.CENTER);
        pieces.getPreviewImage().setFitWidth(150);
        add(pieces.getPreviewImage(), 1, 1);
    }

    /**
     * Sets the pane enabled or not.
     *
     * This enables or disables all <code>ComboBox</code>es on the pane.
     *
     * @param enabled True for enabled, false for disabled.
     */
    protected void setEnabled(boolean enabled) {
        this.board.setDisable(!enabled);
        this.pieces.setDisable(!enabled);
    }

    /**
     * Initializes the resources which depends on a selected scene.
     */
    protected void buildResourceList() {
        board.buildResourceList();
        pieces.buildResourceList();
    }

}
