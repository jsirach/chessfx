/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.GameOptions.GameViewType;
import org.pidome.games.chess.model.options.boards.FlatBoardOptions;
import org.pidome.games.chess.model.options.boards.PerspectiveBoardOptions;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.controls.FieldSet;
import org.pidome.games.chess.utils.controls.GameTab;

/**
 * Tab for view selection.
 *
 * @author John
 */
public class ViewSelectorTab extends GameTab {

    /**
     * 3D board selector.
     */
    private final ViewSelectorCombobox viewTypeSelector = new ViewSelectorCombobox();

    /**
     * 3D scene selector.
     */
    private final SceneSelectorCombobox sceneSelector = new SceneSelectorCombobox();

    /**
     * Perspective board pane.
     */
    private final BoardSelectorPane perspectiveBoardSelectorPane;

    /**
     * Flat board pane.
     */
    private final BoardSelectorPane flatBoardSelectorPane;

    /**
     * The content pane.
     */
    private final GridPane contentPane = new GridPane();

    /**
     * The board options.
     */
    private GameOptions options;

    /**
     * Constructor.
     *
     * @param boardOptions The board options object
     */
    public ViewSelectorTab(final GameOptions boardOptions) {
        super(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_VIEW);
        this.options = boardOptions;
        contentPane.setHgap(5);
        contentPane.setVgap(5);
        this.options.perspectiveBoardOptions().set(new PerspectiveBoardOptions());
        this.options.flatBoardOptions().set(new FlatBoardOptions());
        perspectiveBoardSelectorPane = new BoardSelectorPane(GameViewType.PERSPECTIVE, this.options.perspectiveBoardOptions());
        flatBoardSelectorPane = new BoardSelectorPane(GameViewType.FLAT, this.options.flatBoardOptions());
        build();
    }

    /**
     * Builds the content.
     */
    private void build() {
        contentPane.getColumnConstraints().addAll(
                new ColumnConstraints() {
            {
                this.setPercentWidth(50);
            }
        },
                new ColumnConstraints() {
            {
                this.setPercentWidth(50);
            }
        });

        contentPane.getRowConstraints().addAll(
                new RowConstraints() {
            {
                this.setPercentHeight(50);
            }
        },
                new RowConstraints() {
            {
                this.setPercentHeight(50);
            }
        }
        );

        GridPane.setFillWidth(sceneSelector, Boolean.TRUE);
        sceneSelector.setMaxWidth(Double.MAX_VALUE);
        sceneSelector.valueProperty().addListener((obsValue, oldValue, newValue) -> {
            this.options.getScene3DResource().setValue(newValue);
        });

        GridPane.setFillWidth(viewTypeSelector, Boolean.TRUE);
        viewTypeSelector.setMaxWidth(Double.MAX_VALUE);
        viewTypeSelector.valueProperty().addListener((obsValue, oldValue, viewType) -> {
            this.options.gameViewTypeProperty().setValue(viewType);
            this.options.getGameView().setValue(viewType.getGameViewClass());
            perspectiveBoardSelectorPane.setEnabled((viewType.equals(GameViewType.BOTH) || viewType.equals(GameViewType.PERSPECTIVE)));
            flatBoardSelectorPane.setEnabled((viewType.equals(GameViewType.BOTH) || viewType.equals(GameViewType.FLAT)));
        });

        sceneSelector.disableProperty().bind(
                Bindings.equal(viewTypeSelector.valueProperty(), GameViewType.FLAT)
        );

        contentPane.add(createViewFieldSet(), 0, 0, 1, 2);
        contentPane.add(create3DBoardSelectFieldSet(), 1, 0, 1, 1);
        contentPane.add(create2DBoardSelectFieldSet(), 1, 1, 1, 1);

        viewTypeSelector.setValue(GameViewType.BOTH);
        sceneSelector.buildResourceList();
        perspectiveBoardSelectorPane.buildResourceList();
        flatBoardSelectorPane.buildResourceList();

        this.setContent(contentPane);

    }

    private FieldSet createViewFieldSet() {
        final FieldSet selectViewField = new FieldSet(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_VIEW);
        final GridPane viewGridPane = new GridPane();
        viewGridPane.setVgap(5);
        viewGridPane.setHgap(5);
        viewGridPane.getColumnConstraints().addAll(
                new ColumnConstraints() {
            {
                this.setPercentWidth(50);
            }
        },
                new ColumnConstraints() {
            {
                this.setPercentWidth(50);
            }
        });

        viewGridPane.addRow(0, viewTypeSelector, sceneSelector);

        GridPane.setHalignment(sceneSelector.getPreviewImage(), HPos.CENTER);
        GridPane.setValignment(sceneSelector.getPreviewImage(), VPos.CENTER);
        sceneSelector.getPreviewImage().setFitWidth(150);

        GridPane.setHalignment(viewTypeSelector.getPreviewImage(), HPos.CENTER);
        GridPane.setValignment(viewTypeSelector.getPreviewImage(), VPos.CENTER);
        viewTypeSelector.getPreviewImage().setFitWidth(150);

        viewGridPane.addRow(1, viewTypeSelector.getPreviewImage(), sceneSelector.getPreviewImage());

        selectViewField.setContent(viewGridPane);
        return selectViewField;
    }

    /**
     * Creates the field set for the boards selection.
     *
     * @return fieldset.
     */
    private FieldSet create2DBoardSelectFieldSet() {
        final FieldSet selectBoardsField = new FieldSet(I18nApplicationKey.NEW_GAME_OPTIONS_SELECT_BOARD_2D);
        selectBoardsField.setContent(flatBoardSelectorPane);
        return selectBoardsField;
    }

    /**
     * Creates the field set for the boards selection.
     *
     * @return fieldset.
     */
    private FieldSet create3DBoardSelectFieldSet() {
        final FieldSet selectBoardsField = new FieldSet(I18nApplicationKey.NEW_GAME_OPTIONS_SELECT_BOARD_3D);
        selectBoardsField.setContent(perspectiveBoardSelectorPane);
        return selectBoardsField;
    }

    /**
     * Closes.
     */
    @Override
    public void dispose() {
        this.options = null;
    }

    @Override
    public void onAction() {
        /// Not used, yet.
    }

}
