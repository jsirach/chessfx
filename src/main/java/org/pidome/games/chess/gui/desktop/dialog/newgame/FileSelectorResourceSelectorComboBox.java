/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import java.util.Arrays;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.gui.desktop.dialog.newgame.FileSelectorResourceSelectorComboBox.FileResource;

/**
 * ComboBox for selecting the resource type.
 *
 * @author johns
 */
public class FileSelectorResourceSelectorComboBox extends ComboBox<FileResource> {

    /**
     * Resource to know where to read the file from.
     */
    public enum FileResource {
        /**
         * Read a PGN file from a local resource.
         */
        FILE_LOCAL,
        /**
         * Read a PGN file from the internet.
         */
        FILE_INTERNET;
    }

    /**
     * The description of the selected value.
     */
    private final StringProperty description = new SimpleStringProperty();

    /**
     * Constructor.
     */
    public FileSelectorResourceSelectorComboBox() {
        this.setViewFactories();
    }

    /**
     * Returns the description of the selected item.
     *
     * @return String property with the description of the selected item.
     */
    public final StringProperty descriptionProperty() {
        return this.description;
    }

    /**
     * Puts the resource items in the list.
     */
    protected final void setResourcesList() {
        this.getItems().addAll(Arrays.asList(FileResource.values()));
        this.setValue(FileResource.FILE_LOCAL);
    }

    /**
     * Sets the view factories.
     */
    private void setViewFactories() {
        setCellFactory((ListView<FileResource> param) -> {
            return new ListCell<FileResource>() {
                @Override
                public void updateItem(final FileResource item, final boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(I18nProxy.getInstance().get(I18nApplicationKey.valueOf(item.name())).get());
                    } else {
                        setText(null);
                    }
                }
            };
        });
        this.setConverter(new StringConverter<FileResource>() {
            @Override
            public String toString(final FileResource item) {
                if (item != null) {
                    return I18nProxy.getInstance().get(I18nApplicationKey.valueOf(item.name())).get();
                }
                return null;
            }

            @Override
            public FileResource fromString(final String string) {
                for (FileResource resource : FileSelectorResourceSelectorComboBox.this.getItems()) {
                    if (resource.name().equals(string)) {
                        return resource;
                    }
                }
                return null;
            }
        });
        this.valueProperty().addListener((observable, oldValue, newValue) -> {
            this.description.setValue(
                    I18nProxy.getInstance().get(
                            I18nApplicationKey.valueOf(
                                    newValue.name() + "_DESCRIPTION"
                            )
                    ).get()
            );
        });
    }

}
