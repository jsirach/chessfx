/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import org.pidome.games.chess.engines.EditorGameEngine;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.model.options.GameOptions;
import org.pidome.games.chess.model.options.boards.PerspectiveBoardOptions;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.world.views.EditorView;

/**
 * Simple class to set editor defaults on options etc..
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public final class EditorDefaults {

    private static final String DEFAULT_GAME_ID = "editor";

    /**
     * Default editor board.
     */
    private static final String EDITOR_BOARD = "10586_Chess_Board_v2.obj";

    /**
     * Default editor pieces.
     */
    private static final String EDITOR_PIECES = "staunton.json";

    private EditorDefaults() {
        /// No need for a constructor.
    }

    protected static final void defaultOptions(final AbstractInteractiveGameOptions options) {
        options.setGameId(DEFAULT_GAME_ID);
        options.gameEngineProperty().setValue(EditorGameEngine.class);
        options.gameViewTypeProperty().setValue(GameOptions.GameViewType.PERSPECTIVE);
        options.getGameView().setValue(EditorView.class);
        options.setEditMode(true);
    }

    protected static final void defaultPerspectiveOptions(final AbstractInteractiveGameOptions options) {
        PerspectiveBoardOptions perspectiveOptions = new PerspectiveBoardOptions();
        perspectiveOptions.gameBoardProperty().setValue(GameResourcesHelper
                .getDefaultGameBoardByObject(GameOptions.GameViewType.PERSPECTIVE, EDITOR_BOARD));
        perspectiveOptions.gamePiecesProperty().setValue(GameResourcesHelper
                .getDefaultPiecesByConfiguration(GameOptions.GameViewType.PERSPECTIVE, EDITOR_PIECES));
        options.perspectiveBoardOptions().setValue(perspectiveOptions);
    }

}
