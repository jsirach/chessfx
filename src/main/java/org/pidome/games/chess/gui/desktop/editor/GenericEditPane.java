/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import org.pidome.games.chess.gui.desktop.dialog.ErrorMessagePopup;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;
import org.pidome.games.chess.resources.SceneResource;
import org.pidome.games.chess.utils.SerializationUtil;
import org.pidome.games.chess.world.views.PerspectiveSceneView;

/**
 * The generic scene edit info.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class GenericEditPane extends EditorControlRegion {

    private final ColumnConstraints firstColumn = new ColumnConstraints() {
        {
            setPercentWidth(40);
        }
    };

    private final ColumnConstraints secondColumn = new ColumnConstraints() {
        {
            setPercentWidth(60);
            setHgrow(Priority.ALWAYS);
        }
    };

    private GridPane content = new GridPane();

    /**
     * The game options.
     */
    private final SceneResource sceneResource;

    /**
     * The scene view.
     */
    private final PerspectiveSceneView sceneView;

    private final TextField nameField = new TextField();
    private final TextField descriptionField = new TextField();
    private final TextField authorField = new TextField();
    private final TextField authorWebsiteField = new TextField();
    private final TextField authorEmailField = new TextField();

    private BooleanProperty doNotAskAgain = new SimpleBooleanProperty(Boolean.FALSE);

    public GenericEditPane(final SceneResource sceneResource, final PerspectiveSceneView sceneView) {
        this.getStyleClass().add("first");
        content.getColumnConstraints().addAll(firstColumn, secondColumn);
        content.setVgap(5);
        this.sceneResource = sceneResource;
        this.sceneView = sceneView;
        buildPane();
        setValues();
    }

    private void buildPane() {

        this.setText("Scene info");

        content.add(new Label("Name"), 0, 0);
        content.add(nameField, 1, 0);

        content.add(new Label("Description"), 0, 1);
        content.add(descriptionField, 1, 1);

        content.add(new Label("Author"), 0, 2);
        content.add(authorField, 1, 2);

        content.add(new Label("Website"), 0, 3);
        content.add(authorWebsiteField, 1, 3);

        content.add(new Label("Email"), 0, 4);
        content.add(authorEmailField, 1, 4);

        Button saveButton = new Button("Save");
        saveButton.setOnAction(event -> {
            if (doNotAskAgain.getValue().equals(Boolean.TRUE)) {
                saveData();
            } else {
                SaveData saveData = new SaveData();
                saveData.setOnCancel(GamePopup.PopupAction.EMPTY);
                saveData.display();
            }
        });
        GridPane.setHalignment(saveButton, HPos.RIGHT);
        content.add(saveButton, 1, 5);

        this.setPaneContent(content);
    }

    private void setValues() {
        nameField.textProperty().setValue(this.sceneResource.getName());
        descriptionField.textProperty().setValue(this.sceneResource.getDescription());
        authorField.textProperty().setValue(this.sceneResource.getAuthor());
        authorWebsiteField.textProperty().setValue(this.sceneResource.getAuthorWebsite());
        authorEmailField.textProperty().setValue(this.sceneResource.getAuthorEmail());
    }

    private void saveData() {
        if (nameField.textProperty().getValueSafe().isBlank()) {
            ErrorMessagePopup errorMessage = new ErrorMessagePopup();
            errorMessage.setMessage("Name can not be empty");
            errorMessage.display();
            return;
        }
        this.sceneResource.setName(nameField.textProperty().getValueSafe());
        this.sceneResource.setDescription(descriptionField.textProperty().getValueSafe());
        this.sceneResource.setAuthor(authorField.textProperty().getValueSafe());
        this.sceneResource.setAuthorWebsite(authorWebsiteField.textProperty().getValueSafe());
        this.sceneResource.setAuthorEmail(authorEmailField.textProperty().getValueSafe());

        try {
            this.sceneResource.save();
            File configFile = new File(this.sceneResource.getSceneConfigurationFile());
            SerializationUtil.getObjectMapper().writeValue(configFile, sceneView.perspectiveConfig());
        } catch (IOException ex) {
            Logger.getLogger(GenericEditPane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private class SaveData extends GamePopup {

        private SaveData() {
            super(new SimpleStringProperty("Save"));
            buildDialog();
        }

        private void buildDialog() {
            GridPane content = new GridPane();
            content.setHgap(5);
            content.setVgap(5);

            content.add(new Text("Do you want to save?"), 0, 0);
            CheckBox confirmSave = new CheckBox("Do not ask this session");
            confirmSave.selectedProperty().bindBidirectional(doNotAskAgain);
            content.add(confirmSave, 0, 1);

            this.setCenter(content);
            this.setOnAction(() -> {
                saveData();
            });

        }

    }

}
