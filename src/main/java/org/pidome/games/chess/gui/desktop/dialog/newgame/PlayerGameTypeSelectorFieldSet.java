/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.Chess;
import org.pidome.games.chess.model.game.ChessEventModel;
import org.pidome.games.chess.model.options.PlayerGameOptions;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.gui.desktop.dialog.ErrorMessagePopup;
import org.pidome.games.chess.utils.controls.FieldSet;
import org.pidome.games.chess.utils.controls.GamesListPopup;
import org.pidome.games.chess.utils.controls.UiGridPane;
import org.pidome.games.chess.gui.desktop.dialog.newgame.FileSelectorResourceSelectorComboBox.FileResource;
import org.pidome.games.chess.resources.PgnResourceHelper;

/**
 * Pane for selection of the game(s) to be replayed.
 *
 * @author johns
 */
public final class PlayerGameTypeSelectorFieldSet extends FieldSet {

    /**
     * Class logger.
     */
    private static final Logger LOG = LogManager.getLogger(PlayerGameTypeSelectorFieldSet.class);

    /**
     * The PGN file select resource type.
     */
    private final FileSelectorResourceSelectorComboBox selectResourceTypeComboBox;

    /**
     * Gridpane used to position the additional options bases on the selection
     * done in the combobox.
     */
    private final GridPane resourceOptions = new UiGridPane();

    /**
     * The game options.
     */
    private PlayerGameOptions playerGameOptions;

    /**
     * Content.
     */
    private UiGridPane content = new UiGridPane();

    /**
     * The loaded chess events.
     */
    private ObservableList<ChessEventModel> events = FXCollections.observableArrayList(new ArrayList<>());

    /**
     * Constructor.
     *
     * @param playerGameOptions The game options specific for the game player.
     */
    public PlayerGameTypeSelectorFieldSet(final PlayerGameOptions playerGameOptions) {
        super(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_PLAYER);
        this.playerGameOptions = playerGameOptions;
        selectResourceTypeComboBox = new FileSelectorResourceSelectorComboBox();
        selectResourceTypeComboBox.valueProperty().addListener(this::playlistResource);
        selectResourceTypeComboBox.setResourcesList();

        content.add(selectResourceTypeComboBox, 0, 0);
        content.add(resourceOptions, 1, 0, 1, 2);

        content.add(playListbutton(), 0, 1);
        content.add(demoCameraCheckBox(), 0, 2);

        this.setContent(content);
        this.validProperty().bind(Bindings.isNotEmpty(this.playerGameOptions.games()));
    }

    /**
     * Provides the interface based on the selection made in the combobox.
     *
     * @param observableValue The observable value.
     * @param oldValue The previously selected value.
     * @param newValue The newly selected value.
     */
    private void playlistResource(final ObservableValue observableValue,
            final FileResource oldValue, final FileResource newValue) {
        resourceOptions.getChildren().clear();
        if (newValue != null) {
            switch (newValue) {
                case FILE_INTERNET:
                    final TextField linkOnInternet = new TextField();
                    linkOnInternet.setMinWidth(200);
                    linkOnInternet.setPromptText(
                            I18nProxy.getString(I18nApplicationKey.PASTE_LINK_TO_ONLINE_FILE)
                    );
                    resourceOptions.add(linkOnInternet, 1, 0);
                    break;
                case FILE_LOCAL:
                    final TextField fileInput = new TextField();
                    fileInput.setMinWidth(200);
                    fileInput.setEditable(false);

                    final FileChooser openFileDialog = new FileChooser();
                    ExtensionFilter selectPgnFiles = new ExtensionFilter(
                            I18nProxy.getString(I18nApplicationKey.PGN_FILES), "*.pgn");
                    openFileDialog.getExtensionFilters().add(selectPgnFiles);
                    openFileDialog.setSelectedExtensionFilter(selectPgnFiles);
                    openFileDialog.setTitle(I18nProxy.getString(I18nApplicationKey.GAMES_OPEN));

                    final Button fileDialogOpenerButton = new Button(
                            I18nProxy.getString(I18nApplicationKey.SELECT_FILE));
                    fileDialogOpenerButton.setOnAction(event -> {
                        event.consume();
                        final File openFile = openFileDialog.showOpenDialog(Chess.getApplicationStage());
                        if (openFile != null) {
                            fileInput.textProperty().setValue(openFile.getAbsolutePath());
                            try {
                                events.clear();
                                events.addAll(PgnResourceHelper.readPgnLocalFile(openFile));
                                openGamesList();
                            } catch (Exception ex) {
                                LOG.error("File read error ocured", ex);
                                ErrorMessagePopup popup = new ErrorMessagePopup();
                                popup.setMessage(I18nApplicationKey.LOAD_FILE_UNABLE);
                                popup.display(true);
                            }
                        }
                    });
                    resourceOptions.add(fileInput, 0, 0);
                    resourceOptions.add(fileDialogOpenerButton, 0, 1);
                    break;
            }
        }
    }

    /**
     * Creates the checkbox for enabling demo camera view.
     *
     * @return The checkbox.
     */
    private CheckBox demoCameraCheckBox() {
        CheckBox demoCamera = new CheckBox();
        this.playerGameOptions.demoMode().bind(demoCamera.selectedProperty());
        demoCamera.setText(I18nProxy.getString(I18nApplicationKey.NEW_GAME_OPTIONS_ENABLE_DEMO_VIEW));
        demoCamera.setTooltip(new Tooltip(
                I18nProxy.getString(I18nApplicationKey.NEW_GAME_OPTIONS_ENABLE_DEMO_VIEW_TOOLTIP)
        ));
        return demoCamera;
    }

    /**
     * Creates the playlist button.
     *
     * @return The playlist button.
     */
    private Button playListbutton() {
        final Button openPlayList = new Button(I18nProxy.getString(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_PLAYER_PLAYLIST_OPTIONS));
        openPlayList.setOnAction((event) -> openGamesList());
        openPlayList.disableProperty().bind(Bindings.isEmpty(events));
        return openPlayList;
    }

    /**
     * Opens a popup with a list of games to choose from.
     *
     * @param gamesList List of games to show in the popup.
     */
    private void openGamesList() {
        if (!Objects.isNull(events) && !events.isEmpty()) {
            GamesListPopup pop = new GamesListPopup(events);
            if (events.size() == 1 && events.get(0).isSingleGame()) {
                this.playerGameOptions.games().clear();
                this.playerGameOptions.games().add(events.get(0).getRounds().get(0).getGames().get(0));
            } else {
                pop.setOnAction(() -> {
                    this.playerGameOptions.games().clear();
                    this.playerGameOptions.games().setAll(pop.getSelectedGames());
                });
                pop.display(true);
            }
        } else {
            showErrorMessage(I18nApplicationKey.LOAD_PGN_NO_GAMES);
        }
    }

    /**
     * Shows an error message with the given message.
     *
     * @param messageKey The message.
     */
    private void showErrorMessage(final I18nApplicationKey messageKey) {
        ErrorMessagePopup message = new ErrorMessagePopup();
        message.setMessage(messageKey);
        message.display(true);
    }

}
