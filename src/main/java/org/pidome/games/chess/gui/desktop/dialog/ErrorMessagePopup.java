/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Label;
import org.pidome.games.chess.resources.i18n.I18nKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;

/**
 * Popup used to display messages.
 *
 * @author johns
 */
public class ErrorMessagePopup extends GamePopup {

    public ErrorMessagePopup() {
        super(new SimpleStringProperty("Message"));
        this.setCenter(new Label("An error occurred"));
        this.setOnAction(GamePopup.PopupAction.EMPTY);
    }

    /**
     * Sets a message.
     *
     * @param fixedErrorMessage The key of the error message.
     */
    public final void setMessage(final I18nKey fixedErrorMessage) {
        this.setCenter(new Label(I18nProxy.getString(fixedErrorMessage)));
    }

    /**
     * Sets a message.
     *
     * @param errorMessage The error message.
     */
    public final void setMessage(final String errorMessage) {
        this.setCenter(new Label(errorMessage));
    }

}
