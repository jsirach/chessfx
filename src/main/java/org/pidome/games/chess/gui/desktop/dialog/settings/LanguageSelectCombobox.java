/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.settings;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import org.pidome.games.chess.resources.GameLanguages;
import org.pidome.games.chess.resources.i18n.I18nLanguageKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nReplaceKey;
import org.pidome.games.chess.resources.i18n.I18nReplacePair;

/**
 * Language select combobox.
 *
 * @author johns
 */
public class LanguageSelectCombobox extends ComboBox<String> {

    /**
     * Contains the language mapping in the correct language.
     */
    private final TreeMap<String, String> translatedLanguages;

    /**
     * Constructor.
     */
    public LanguageSelectCombobox() {
        this.setViewFactories();
        translatedLanguages = GameLanguages.getSupportedLocales().entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> {
                            return I18nProxy.getInstance().get(new I18nLanguageKey(entry.getKey()),
                                    new I18nReplacePair(I18nReplaceKey.languageEnglish, GameLanguages.getSupportedLocales().get(entry.getKey())))
                                    .getValue();
                        },
                        (a, b) -> b, TreeMap::new)
                );

        this.setItems(FXCollections.observableArrayList(translatedLanguages.keySet()));
        this.setValue(I18nProxy.getInstance().getLocale().get().toLanguageTag());
    }

    /**
     * Sets the view factory.
     */
    private void setViewFactories() {
        setCellFactory((final ListView<String> param) -> {
            return new ListCell<String>() {
                @Override
                public void updateItem(final String item, final boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(translatedLanguages.get(item));
                    } else {
                        setText(null);
                    }
                }
            };
        });
        this.setConverter(new StringConverter<String>() {
            @Override
            public String toString(final String item) {
                return item == null ? null : translatedLanguages.get(item);
            }

            @Override
            public String fromString(final String string) {
                if (string == null) {
                    return null;
                }
                return translatedLanguages.entrySet().stream()
                        .filter(entry -> entry.getValue().equals(string))
                        .findAny()
                        .map(entry -> entry.getKey())
                        .orElse(GameLanguages.DEFAULT_LOCALE_KEY);
            }

        });
    }

}
