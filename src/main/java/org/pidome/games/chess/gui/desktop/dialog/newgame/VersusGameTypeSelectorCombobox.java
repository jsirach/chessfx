/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.engines.AbstractGameEngine;
import org.pidome.games.chess.model.options.GameOptions.GamePlayMode;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.ThreadTools;

/**
 * A scene selector.
 *
 * @author johns
 * @param <T> The type.
 */
public class VersusGameTypeSelectorCombobox<T> extends ComboBox<Class<AbstractGameEngine>> {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(VersusGameTypeSelectorCombobox.class);

    /**
     * The play mode for the combobox.
     */
    private final GamePlayMode playMode;

    /**
     * Property for the description of the selected value.
     */
    private final StringProperty description = new SimpleStringProperty();

    /**
     * Constructor.
     *
     * @param playMode The play mode this combobox must support.
     */
    public VersusGameTypeSelectorCombobox(final GamePlayMode playMode) {
        this.playMode = playMode;
        this.setViewFactories();
    }

    /**
     * Returns the description property for the selected value.
     *
     * @return String property.
     */
    protected StringProperty descriptionProperty() {
        return this.description;
    }

    /**
     * Sets the view factory.
     */
    private void setViewFactories() {
        setCellFactory((ListView<Class<AbstractGameEngine>> param) -> {
            return new ListCell<Class<AbstractGameEngine>>() {
                @Override
                public void updateItem(final Class<AbstractGameEngine> item, final boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null) {
                        setText(GameResourcesHelper.getEngineName(item));
                    } else {
                        setText(null);
                    }
                }
            };
        });
        this.setConverter(new StringConverter<Class<AbstractGameEngine>>() {
            @Override
            public String toString(final Class<AbstractGameEngine> item) {
                return GameResourcesHelper.getEngineName(item);
            }

            @Override
            public Class<AbstractGameEngine> fromString(final String string) {
                for (Class<AbstractGameEngine> engine : VersusGameTypeSelectorCombobox.this.getItems()) {
                    if (GameResourcesHelper.getEngineName(engine).equals(string)) {
                        return engine;
                    }
                }
                return null;
            }
        });
        this.valueProperty().addListener((observable, oldValue, newValue) -> {
            this.description.setValue(
                    I18nProxy.getInstance().get(
                            I18nApplicationKey.valueOf(
                                    GameResourcesHelper.getEngineAnnotation(newValue).i18nName().name() + "_DESCRIPTION"
                            )
                    ).get()
            );
        });
    }

    /**
     * Collects the items.
     */
    protected void buildResourceList() {
        ThreadTools.threadExecuted(() -> {
            this.getItems().addAll(GameResourcesHelper.getPlaymodeTypeGameEngines(this.playMode));
            FXUtils.runOnFXThread(() -> {
                this.setValue(this.getItems().get(0));
            });
        });
    }

}
