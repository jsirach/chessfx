/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import org.pidome.games.chess.engines.AbstractGameEngine;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.controls.FieldSet;

/**
 * Pane for game type selection.
 *
 * @author johns
 */
public class VersusGameTypeSelectorFieldSet extends FieldSet {

    /**
     * Select box for the game mode selection.
     */
    private final VersusGameTypeSelectorCombobox<AbstractGameEngine> selectorCombobox;

    /**
     * Label for the description of the selection made.
     */
    private final Label selectionDescription = new Label();

    /**
     * The content border pane.
     */
    private final BorderPane content = new BorderPane();

    /**
     * Constructor.
     *
     * @param options The game options.
     */
    public VersusGameTypeSelectorFieldSet(final AbstractInteractiveGameOptions options) {
        super(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_SELECT_VERSUS_ENGINE);
        selectorCombobox = new VersusGameTypeSelectorCombobox(options.gameReadOnlyPlayModeProperty().get());
        selectionDescription.textProperty().bind(selectorCombobox.descriptionProperty());
        final Label labelforSelector = new Label(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_SELECT_VERSUS_ENGINE).get());
        labelforSelector.setLabelFor(selectorCombobox);
        content.setLeft(labelforSelector);
        BorderPane.setMargin(selectorCombobox, new Insets(0, 0, 0, 10));
        BorderPane.setMargin(selectionDescription, new Insets(5, 0, 5, 0));
        BorderPane.setAlignment(selectorCombobox, Pos.CENTER_LEFT);
        BorderPane.setAlignment(labelforSelector, Pos.CENTER_LEFT);
        selectorCombobox.valueProperty().addListener((observableValue, oldEngine, newEngine) -> {
            options.gameEngineProperty().setValue(newEngine);
        });
        selectorCombobox.buildResourceList();
        content.setCenter(selectorCombobox);
        content.setBottom(selectionDescription);
        this.setContent(content);
    }

    /**
     * Called when a game starts.
     */
    @Override
    public void onAction() {
        /// not used.
    }

    /**
     * Called when destroyed.
     */
    @Override
    public void dispose() {
        // not used.
    }

}
