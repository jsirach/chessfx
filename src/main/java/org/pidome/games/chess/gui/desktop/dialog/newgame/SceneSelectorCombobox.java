/*
 * Copyright (C) 2020 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.games.chess.resources.GameResourcesHelper;
import org.pidome.games.chess.resources.GameResourcesHelper.ResourceLocation;
import org.pidome.games.chess.resources.SceneResource;
import org.pidome.games.chess.utils.FXUtils;
import org.pidome.games.chess.utils.ThreadTools;

/**
 * A scene selector.
 *
 * @author johns
 */
public class SceneSelectorCombobox extends ComboBox<SceneResource> {

    /**
     * Logger.
     */
    private static final Logger LOG = LogManager.getLogger(SceneSelectorCombobox.class);

    /**
     * Scene preview image.
     */
    private final ImageView scenePreviewImage = new ImageView();

    /**
     * Constructor.
     */
    public SceneSelectorCombobox() {
        this.scenePreviewImage.setPreserveRatio(true);
        this.setViewFactories();
        this.setChangeListener();
    }

    /**
     * Sets the view factory.
     */
    private void setViewFactories() {
        setCellFactory((ListView<SceneResource> param) -> {
            return new ListCell<SceneResource>() {
                @Override
                public void updateItem(final SceneResource scene, final boolean empty) {
                    super.updateItem(scene, empty);
                    if (scene != null) {
                        setText(scene.getName());
                    } else {
                        setText(null);
                    }
                }
            };
        });
        this.setConverter(new StringConverter<SceneResource>() {
            @Override
            public String toString(final SceneResource scene) {
                if (scene != null) {
                    return scene.getName();
                } else {
                    return null;
                }
            }

            @Override
            public SceneResource fromString(final String sceneName) {
                if (sceneName != null) {
                    for (SceneResource scene : SceneSelectorCombobox.this.getItems()) {
                        if (scene.getName().equals(sceneName)) {
                            return scene;
                        }
                    }
                }
                return null;
            }

        });
    }

    /**
     * Returns the preview image holder for the scene preview image.
     *
     * @return Scene image view holder.
     */
    public final ImageView getPreviewImage() {
        return this.scenePreviewImage;
    }

    /**
     * The local change listener.
     */
    private void setChangeListener() {
        valueProperty().addListener((ov, oldV, newV) -> {
            try {
                scenePreviewImage.setImage(GameResourcesHelper.getScenePreview(newV));
            } catch (Exception ex) {
                LOG.error("Could not load preview", ex);
                scenePreviewImage.setImage(null);
            }
        });
    }

    /**
     * Collects the items.
     */
    public void buildResourceList() {
        buildResourceList(null);
    }

    /**
     * Collects the items.
     *
     * @param sceneResource The default scene resource to set, keep null for no
     * default, but first ocurrence in the list.
     */
    public void buildResourceList(final SceneResource sceneResource) {
        ThreadTools.threadExecuted(() -> {
            this.setItems(
                    FXCollections.observableArrayList(
                            Stream.of(GameResourcesHelper.getAvailableGameScenes(ResourceLocation.INTERNAL),
                                    GameResourcesHelper.getAvailableGameScenes(ResourceLocation.COMMUNITY))
                                    .flatMap(Collection::stream)
                                    .collect(Collectors.toList())
                    )
            );
            FXUtils.runOnFXThread(() -> {
                if (!this.getItems().isEmpty()) {
                    if (!Objects.isNull(sceneResource) && this.getItems().contains(sceneResource)) {
                        this.setValue(sceneResource);
                    } else {
                        this.setValue(this.getItems().get(0));
                    }
                }
            });
        });
    }

    public void buildSelectiveResourceList(final ResourceLocation resourceLocation) {
        ThreadTools.threadExecuted(() -> {
            this.setItems(
                    FXCollections.observableArrayList(GameResourcesHelper.getAvailableGameScenes(resourceLocation))
            );
            FXUtils.runOnFXThread(() -> {
                if (!this.getItems().isEmpty()) {
                    this.setValue(this.getItems().get(0));
                }
            });
        });
    }

}
