/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.settings;

import java.util.Locale;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.settings.GenericSettings;
import org.pidome.games.chess.settings.Settings;
import org.pidome.games.chess.utils.controls.FieldSet;
import org.pidome.games.chess.utils.controls.GameTab;

/**
 * Tab for generic UI settings in the application.
 *
 * @author johns
 */
public final class GenericSettingsTab extends GameTab {

    /**
     * Tab content.
     */
    private final GridPane contentPane = new GridPane();

    /**
     * field set for the language.
     */
    private final FieldSet fieldSetLanguage = new FieldSet(I18nApplicationKey.SETTINGS_APPLICATION_LANGUAGE_LABEL);

    /**
     * Graphics label.
     */
    private final Label labelForLanguageSelect = new Label();

    /**
     * The original locale in case of canceling.
     */
    final Locale originalLocale = I18nProxy.getInstance().getLocale().get();

    /**
     * Language selection.
     */
    private final LanguageSelectCombobox languageSelectioncombobox = new LanguageSelectCombobox();

    /**
     * The generic settings.
     */
    private final GenericSettings settings;

    /**
     * Constructor.
     */
    public GenericSettingsTab() {
        super(I18nApplicationKey.SETTINGS_APPLICATION_DEFAULT_UI_TAB);
        settings = Settings.get(GenericSettings.class);
        this.contentPane.setHgap(10);
        this.contentPane.setVgap(10);
        buildContent();
        this.setContent(contentPane);
    }

    /**
     * Builds the content to show.
     */
    private void buildContent() {
        this.contentPane.add(buildLanguageFieldSet(), 0, 0);
    }

    /**
     * Builds the language fieldset.
     *
     * @return FieldSet.
     */
    private FieldSet buildLanguageFieldSet() {
        final GridPane languagePane = new GridPane();
        languagePane.setHgap(10);
        languagePane.setVgap(10);

        FieldSet.setSettingsLabelProperties(labelForLanguageSelect, I18nApplicationKey.SETTINGS_APPLICATION_LANGUAGE_LABEL);

        languageSelectioncombobox.setMinWidth(150);
        languageSelectioncombobox.setMaxWidth(150);

        languageSelectioncombobox.valueProperty().bindBidirectional(settings.locale());
        languagePane.add(labelForLanguageSelect, 0, 0);
        languagePane.add(languageSelectioncombobox, 1, 0);
        fieldSetLanguage.setContent(languagePane);

        return fieldSetLanguage;
    }

    /**
     * Perform an action.
     */
    @Override
    public void onAction() {
        // Not needed.
    }

    /**
     * Dispose of any bindings.
     */
    @Override
    public void dispose() {
        labelForLanguageSelect.textProperty().unbind();
        languageSelectioncombobox.valueProperty().unbindBidirectional(settings.locale()
        );
    }

}
