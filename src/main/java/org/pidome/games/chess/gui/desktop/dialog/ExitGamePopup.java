/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog;

import javafx.scene.control.Label;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.gui.desktop.dialog.GamePopup;

/**
 * Class used to ask the user to exit the game or not.
 *
 * @author johns
 */
public class ExitGamePopup extends GamePopup {

    /**
     * Exit question label.
     */
    Label exitQuestion = new Label();

    /**
     * Constructor.
     */
    public ExitGamePopup() {
        super(I18nProxy.getInstance().get(I18nApplicationKey.QUIT_CURRENT_GAME_ARE_YOU_SURE_TITLE));
        construct();
    }

    /**
     * Create the view.
     */
    private void construct() {
        exitQuestion.textProperty().bind(I18nProxy.getInstance().get(I18nApplicationKey.QUIT_CURRENT_GAME_ARE_YOU_SURE));
        this.setCenter(exitQuestion);

        this.setOnAction(() -> {
            System.exit(0);
        });

        this.setOnCancel(() -> {
        });

    }

    /**
     * Close the popup.
     */
    @Override
    public void close() {
        super.close();
        exitQuestion.textProperty().unbind();
    }

}
