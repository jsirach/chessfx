/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import org.pidome.games.chess.Chess;
import org.pidome.games.chess.resources.i18n.I18nInterfaceKey;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.utils.FXUtils;

/**
 * a default style for a popup.
 *
 * @author John
 */
public abstract class GamePopup extends BasePopup {

    /**
     * Specific popup action types to be used with onAction or onCancel
     */
    public enum PopupAction {
        /**
         * No action to register.
         */
        EMPTY,
        /**
         * The default action.
         *
         * This should normally not be used, but only to reset an EMPTY.
         */
        DEFAULT;
    }

    /**
     * Confirmation button.
     */
    private final Button confirmButton;

    /**
     * Cancel button.
     */
    private final Button cancelButton;

    /**
     * Run on confirming.
     */
    private Runnable runOnAction;

    /**
     * Run on canceling.
     */
    private Runnable runOnCancel;

    /**
     * The popup title.
     */
    private final StringProperty title;

    /**
     * The popup sub-title.
     */
    private final StringProperty subTitle;

    /**
     * Title label.
     */
    private final Label titleLabel = new Label();

    /**
     * Title label.
     */
    private final Label subTitleLabel = new Label();

    /**
     * What to do when an onAction is triggered.
     */
    private PopupAction popupOnActionAction = PopupAction.DEFAULT;

    /**
     * What to do when an onCancel is triggered.
     */
    private PopupAction popupOnCancelAction = PopupAction.DEFAULT;

    /**
     * Constructor.
     *
     * Using this constructor will block the whole application as the owner
     * window will be guaranteed the application window.
     *
     * @param title The popup title.
     */
    public GamePopup(final StringProperty title) {
        this(title, null);
    }

    /**
     * Constructor.Using this constructor will block the whole application as
     * the owner window will be guaranteed the application window.
     *
     *
     * @param title The popup title.
     * @param subTitle The subtitle to show if not null.
     */
    public GamePopup(final StringProperty title, final StringProperty subTitle) {
        this(Chess.getApplicationStage(), title, subTitle);
    }

    /**
     * Constructor.
     *
     * When a subtitle is given (not null) the popup will be shown as
     * <pre>title - subtitle</pre>
     *
     * The window owner can be a windows literally the window owning the popup,
     * or use <code>Chess.getApplicationStage()</code> to make the window owned
     * by the application's primary window.
     *
     * @param owner The window owner.
     * @param title The popup title.
     * @param subTitle The subtitle.
     */
    public GamePopup(final Stage owner, final StringProperty title, final StringProperty subTitle) {
        super(owner);
        this.title = title;
        this.subTitle = subTitle;
        this.confirmButton = new Button();
        this.cancelButton = new Button();
        this.confirmButton.textProperty().bind(I18nProxy.getInstance().get(I18nInterfaceKey.BUTTON_OK));
        this.cancelButton.textProperty().bind(I18nProxy.getInstance().get(I18nInterfaceKey.BUTTON_CANCEL));
        this.confirmButton.setOnAction(this::performOnAction);
        this.cancelButton.setOnAction(this::performOnAction);
    }

    /**
     * Displays the popup.
     *
     * This method is shorthand for <code>display(true)</code>
     */
    @Override
    public final void display() {
        super.display(true);
    }

    /**
     * Builds the skeleton for the interface.
     */
    @Override
    protected void buildSkeleton() {
        if (this.title != null && this.title.isNotNull().get()) {
            final HBox topBox = new HBox();
            titleLabel.textProperty().bind(this.title);
            topBox.getChildren().add(titleLabel);
            topBox.setPadding(new Insets(5, 0, 0, 0));
            if (this.subTitle != null && this.subTitle.isNotNull().get()) {
                subTitleLabel.textProperty().bind(subTitle);
                topBox.getChildren().addAll(new Label(" - "), this.subTitleLabel);
                HBox.setHgrow(subTitleLabel, Priority.ALWAYS);
            } else {
                HBox.setHgrow(titleLabel, Priority.ALWAYS);
            }
            this.setTop(topBox);
        }

        if (this.runOnAction != null || this.runOnCancel != null) {
            final HBox bottomBox = new HBox(5);
            bottomBox.setAlignment(Pos.CENTER_RIGHT);
            if (this.runOnCancel != null) {
                bottomBox.getChildren().add(this.cancelButton);
            }
            if (this.runOnAction != null) {
                bottomBox.getChildren().add(this.confirmButton);
            }
            this.setBottom(bottomBox);
        }

    }

    /**
     * What to do when confirming.
     *
     * @param runnable The runnable to execute.
     */
    public void setOnAction(final Runnable runnable) {
        this.runOnAction = runnable;
        enableEnterKey();
    }

    /**
     * Set the onAction type.
     *
     * Use this method to register an onAction without implying a runnable.
     *
     * @param popupOnAction The onaction type to set.
     */
    public void setOnAction(final PopupAction popupOnAction) {
        this.popupOnActionAction = popupOnAction;
        this.setOnAction(() -> {
        });
    }

    /**
     * What to do when canceling.
     *
     * @param runnable The runnable to execute.
     */
    public final void setOnCancel(final Runnable runnable) {
        this.runOnCancel = runnable;
        enableEscapeKey();
    }

    /**
     * Set the onCancel type.
     *
     * Use this method to register an onCancel without implying a runnable.
     *
     * @param popupOnAction The onaction type to set.
     */
    public void setOnCancel(final PopupAction popupOnAction) {
        this.popupOnCancelAction = popupOnAction;
        this.setOnCancel(() -> {
        });
    }

    /**
     * Perform the chosen action.
     *
     * @param performRunOnAction True for the onAction false for the onCancel.
     */
    private void performOnAction(final ActionEvent event) {
        if (event.getSource().equals(this.confirmButton) && popupOnActionAction.equals(PopupAction.DEFAULT) && this.runOnAction != null) {
            runOnActionAction();
        } else if (popupOnCancelAction.equals(PopupAction.DEFAULT) && this.runOnCancel != null) {
            runOnCancelAction();
        }
        close();
    }

    /**
     * Enables pressing the escape key to close a window and perform the cancel
     * action.
     */
    private void enableEscapeKey() {
        this.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            event.consume();
            if (event.getCode().equals(KeyCode.ESCAPE)) {
                runOnCancelAction();
                this.close();
            }
        });
    }

    /**
     * Pressing enter confirms
     */
    private void enableEnterKey() {
        this.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            event.consume();
            if (event.getCode().equals(KeyCode.ENTER)) {
                runOnActionAction();
                close();
            }
        });
    }

    /**
     * Runs the action registered as the onAction action.
     */
    protected void runOnActionAction() {
        if (this.runOnAction != null) {
            new Thread(this.runOnAction).start();
        }
    }

    /**
     * Runs the action registered as the action to cancel.
     */
    @Override
    protected void runOnCancelAction() {
        if (this.runOnCancel != null) {
            new Thread(this.runOnCancel).start();
        }
    }

    /**
     * Clean any held pointers.
     */
    @Override
    public void close() {
        titleLabel.textProperty().unbind();
        this.confirmButton.textProperty().unbind();
        this.cancelButton.textProperty().unbind();
        FXUtils.runOnFXThread(() -> {
            super.close();
            this.runOnAction = null;
            this.runOnCancel = null;
        });
    }

    /**
     * Returns the confirm button disable property.
     *
     * @return Boolean property.
     */
    public BooleanProperty confirmButtonDisableProperty() {
        return this.confirmButton.disableProperty();
    }

}
