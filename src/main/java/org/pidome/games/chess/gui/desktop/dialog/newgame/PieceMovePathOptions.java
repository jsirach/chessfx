/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineCap;
import javafx.util.StringConverter;
import org.pidome.games.chess.model.options.boards.PathTravelOptions;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.controls.DualColorPicker;
import org.pidome.games.chess.utils.controls.FieldSet;
import org.pidome.games.chess.utils.controls.colorselector.ColorPickerFeature;

/**
 * Options for pieces paths.
 *
 * @author johns
 */
public class PieceMovePathOptions extends DualColorPicker {

    /**
     * Path travel options.
     */
    private final PathTravelOptions pathTravelOptions;

    /**
     * The white line example.
     */
    private final Line whiteLine = new Line();

    /**
     * The white line example.
     */
    private final Line blackLine = new Line();

    /**
     * Slider used to set the thickness of the travel path line.
     */
    private final Slider lineThicknessSlider = new Slider();

    /**
     * Constructor.
     *
     * @param pathTravelOptions The path travel options.
     */
    public PieceMovePathOptions(final PathTravelOptions pathTravelOptions) {
        super(ColorPickerFeature.SELECT_OPACITY_DUAL_SINGLE);
        this.pathTravelOptions = pathTravelOptions;

        /**
         * Set line rounding in start and beginning, need to set value first. *
         */
        this.lineThicknessSlider.setMin(1.0);
        this.lineThicknessSlider.setMax(11.25);

        bindLeftColorPicker(this.pathTravelOptions.whitePathColorProperty());
        setLeftPanetitle(I18nApplicationKey.SELECT_COLOR_PIECE_PATH_WHITE);

        bindRightColorPicker(this.pathTravelOptions.blackPathColorProperty());
        setRightPanetitle(I18nApplicationKey.SELECT_COLOR_PIECE_PATH_BLACK);

        bindSingleOpacity(this.pathTravelOptions.pathOpacityProperty());

        this.getContentPane().add(new Label(I18nProxy.getInstance().get(I18nApplicationKey.THICKNESS).get()), 0, 4, 2, 1);
        this.formatLineThicknessSlider();
        lineThicknessSlider.valueProperty().bindBidirectional(this.pathTravelOptions.lineThicknessProperty());

        this.getContentPane().add(this.lineThicknessSlider, 0, 5, 2, 1);

        this.getContentPane().add(exampleCheckerField(), 0, 6, 2, 1);
    }

    /**
     * Sets the value label formatter for the slider.
     */
    private void formatLineThicknessSlider() {
        this.lineThicknessSlider.setLabelFormatter(new StringConverter<Double>() {

            /**
             * Convert to percentage.
             *
             * @param doubleValue the double value to convert.
             * @return String in percentage.
             */
            @Override
            public String toString(final Double doubleValue) {
                if (doubleValue != null) {
                    if (doubleValue == lineThicknessSlider.getMin()) {
                        return I18nProxy.getInstance().get(I18nApplicationKey.THIN).get();
                    } else if (doubleValue == lineThicknessSlider.getMax()) {
                        return I18nProxy.getInstance().get(I18nApplicationKey.THICK).get();
                    }
                }
                return null;
            }

            /**
             * Convert percentage String to double.
             *
             * @param stringValue the string value to convert.
             * @return double.
             */
            @Override
            public Double fromString(final String stringValue) {
                if (stringValue != null) {
                    if (stringValue.equals(I18nProxy.getInstance().get(I18nApplicationKey.THICK).get())) {
                        return lineThicknessSlider.getMin();
                    } else if (stringValue.equals(I18nProxy.getInstance().get(I18nApplicationKey.THICK).get())) {
                        return lineThicknessSlider.getMax();
                    }
                }
                return null;
            }
        });
        this.lineThicknessSlider.setShowTickLabels(true);
    }

    /**
     * Constructs an example chess field.
     *
     * @return Gridpane with example chess field.
     */
    private FieldSet exampleCheckerField() {
        final FieldSet content = new FieldSet(I18nApplicationKey.EXAMPLE);
        final StackPane exampleHolder = new StackPane();
        content.setPrefHeight(100);
        final GridPane examplePane = new GridPane();
        for (int j = 0; j < 8; j++) {
            examplePane.getColumnConstraints().add(new ColumnConstraints() {
                {
                    setPercentWidth(12.5);
                }
            });
        }
        for (int i = 0; i < 2; i++) {
            examplePane.getRowConstraints().add(new RowConstraints() {
                {
                    setPercentHeight(50);
                }
            });
            for (int j = 0; j < 8; j++) {
                final Rectangle rect = new Rectangle();
                rect.setFill((i % 2) == 0 ? ((j % 2) == 0 ? Color.WHITE : Color.BLACK) : ((j % 2) == 0 ? Color.BLACK : Color.WHITE));
                rect.widthProperty().bind(examplePane.widthProperty().divide(8));
                rect.heightProperty().bind(examplePane.widthProperty().divide(8));
                examplePane.add(rect, j, i);
            }
        }
        whiteLine.strokeWidthProperty().bind(this.pathTravelOptions.lineThicknessProperty());
        whiteLine.setStrokeLineCap(StrokeLineCap.ROUND);
        whiteLine.startXProperty().bind(examplePane.widthProperty().divide(16));
        whiteLine.startYProperty().bind(examplePane.heightProperty().divide(4));
        whiteLine.endXProperty().bind(examplePane.widthProperty().subtract(examplePane.widthProperty().divide(16)));
        whiteLine.endYProperty().bind(examplePane.heightProperty().subtract(examplePane.heightProperty().divide(4)));

        whiteLine.strokeProperty().bind(this.pathTravelOptions.whitePathColorProperty());
        whiteLine.opacityProperty().bind(this.pathTravelOptions.pathOpacityProperty());

        blackLine.strokeWidthProperty().bind(this.pathTravelOptions.lineThicknessProperty());
        blackLine.setStrokeLineCap(StrokeLineCap.ROUND);
        blackLine.startXProperty().bind(examplePane.widthProperty().divide(16));
        blackLine.startYProperty().bind(examplePane.heightProperty().subtract(examplePane.heightProperty().divide(4)));
        blackLine.endXProperty().bind(examplePane.widthProperty().subtract(examplePane.widthProperty().divide(16)));
        blackLine.endYProperty().bind(examplePane.heightProperty().divide(4));

        blackLine.strokeProperty().bind(this.pathTravelOptions.blackPathColorProperty());
        blackLine.opacityProperty().bind(this.pathTravelOptions.pathOpacityProperty());

        exampleHolder.getChildren().addAll(examplePane, whiteLine, blackLine);
        content.setContent(exampleHolder);
        return content;
    }

    /**
     * Closes the popup and unbinds.
     */
    @Override
    public final void close() {
        super.close();
        whiteLine.strokeProperty().unbind();
        whiteLine.opacityProperty().unbind();
        whiteLine.strokeWidthProperty().unbind();
        blackLine.strokeProperty().unbind();
        blackLine.opacityProperty().unbind();
        blackLine.strokeWidthProperty().unbind();
        lineThicknessSlider.valueProperty().unbindBidirectional(this.pathTravelOptions.lineThicknessProperty());
    }

}
