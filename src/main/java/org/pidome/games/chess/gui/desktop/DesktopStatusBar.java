/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * The status bar for the game.
 *
 * @author John
 */
public final class DesktopStatusBar extends HBox {

    /**
     * Property holding fps.
     */
    private DoubleProperty FPS = new SimpleDoubleProperty();

    /**
     * Label for showing FPS (todo).
     */
    private Label fpsLabel = new Label();

    /**
     * Status bar constructor.
     */
    public DesktopStatusBar() {
        super(10);
        this.getStyleClass().add("status-bar");
        this.setPadding(new Insets(10));
        fpsLabel.textProperty().bind(Bindings.format("FPS: %.0f", FPS.getValue()));
        this.getChildren().addAll(new Label("Chess development instance"), fpsLabel);
    }

}
