/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import java.util.Objects;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import org.pidome.games.chess.world.config.PerspectiveSceneLightSourceConfig;
import org.pidome.games.chess.world.views.PerspectiveSceneView;

/**
 * Point light axis.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class PointLightAxis extends MoveableObject {

    private static final double ILLUMINATION_VALUE = 0.6;

    final PhongMaterial sphereMaterial = new PhongMaterial();

    private ChangeListener<? super Number> hueListener = this::updateHue;

    private ChangeListener<? super Number> saturationListener = this::updateSaturation;

    private ChangeListener<? super Number> brightnessListener = this::updateBrightness;

    private final PerspectiveSceneView sceneView;

    private static final double AXIS_SIZE = 3.0;
    private static final double AXIS_LENGTH = 160.0;
    private static final double LIGHTPOINT_SIZE = 15;

    private ObjectProperty<PerspectiveSceneLightSourceConfig> lightItemProperty = new SimpleObjectProperty<>();
    private final ChangeListener<PerspectiveSceneLightSourceConfig> configListener = this::updateLightSource;

    final Cylinder xAxis = new Cylinder(AXIS_SIZE, AXIS_LENGTH);
    final Cylinder yAxis = new Cylinder(AXIS_SIZE, AXIS_LENGTH);
    final Cylinder zAxis = new Cylinder(AXIS_SIZE, AXIS_LENGTH);

    protected PointLightAxis(final PerspectiveSceneView sceneView) {
        super(sceneView);
        this.sceneView = sceneView;
        final PhongMaterial redMaterial = new PhongMaterial();
        redMaterial.setDiffuseColor(Color.RED);
        redMaterial.setSelfIlluminationMap(generateImage(ILLUMINATION_VALUE, 0d, 0d, 1d));

        final PhongMaterial greenMaterial = new PhongMaterial();
        greenMaterial.setDiffuseColor(Color.GREEN);
        greenMaterial.setSelfIlluminationMap(generateImage(0d, ILLUMINATION_VALUE, 0d, 1d));

        final PhongMaterial blueMaterial = new PhongMaterial();
        blueMaterial.setDiffuseColor(Color.BLUE);
        blueMaterial.setSelfIlluminationMap(generateImage(0d, 0d, ILLUMINATION_VALUE, 1d));

        Rotate rotateX = new Rotate(90, 0, 0, 0, Rotate.Z_AXIS);
        xAxis.getTransforms().add(rotateX);

        Rotate rotateY = new Rotate(90, 0, 0, 0, Rotate.Y_AXIS);
        yAxis.getTransforms().add(rotateY);

        Rotate rotateZ = new Rotate(90, 0, 0, 0, Rotate.X_AXIS);
        zAxis.getTransforms().add(rotateZ);

        final Sphere sphere = new Sphere(LIGHTPOINT_SIZE);

        xAxis.setMaterial(redMaterial);
        yAxis.setMaterial(greenMaterial);
        zAxis.setMaterial(blueMaterial);
        sphere.setMaterial(sphereMaterial);

        getChildren().addAll(xAxis, yAxis, zAxis, sphere);

        lightItemProperty.addListener(configListener);

    }

    protected final void bindToLightConfiguration(final PerspectiveSceneLightSourceConfig lightItem) {
        this.lightItemProperty.setValue(lightItem);
    }

    private void updateLightSource(final ObservableValue obs, PerspectiveSceneLightSourceConfig oldVal, PerspectiveSceneLightSourceConfig newVal) {
        if (!Objects.isNull(oldVal)) {
            translateXProperty().unbindBidirectional(oldVal.getLocation().xProperty());
            translateYProperty().unbindBidirectional(oldVal.getLocation().yProperty());
            translateZProperty().unbindBidirectional(oldVal.getLocation().zProperty());
            oldVal.getColor().hueProperty().removeListener(hueListener);
            oldVal.getColor().saturationProperty().removeListener(saturationListener);
            oldVal.getColor().brightnessProperty().removeListener(brightnessListener);
        }
        if (!Objects.isNull(newVal)) {

            translateXProperty().setValue(newVal.getLocation().xProperty().doubleValue());
            translateYProperty().setValue(newVal.getLocation().yProperty().doubleValue());
            translateZProperty().setValue(newVal.getLocation().zProperty().doubleValue());

            translateXProperty().bindBidirectional(newVal.getLocation().xProperty());
            translateYProperty().bindBidirectional(newVal.getLocation().yProperty());
            translateZProperty().bindBidirectional(newVal.getLocation().zProperty());

            updateHue(null, null, newVal.getColor().hueProperty().doubleValue());
            updateSaturation(null, null, newVal.getColor().saturationProperty().doubleValue());
            updateBrightness(null, null, newVal.getColor().brightnessProperty().doubleValue());
            newVal.getColor().hueProperty().addListener(hueListener);
            newVal.getColor().saturationProperty().addListener(saturationListener);
            newVal.getColor().brightnessProperty().addListener(brightnessListener);

        }
    }

    private void updateHue(final Observable obs, final Number oldVal, final Number newVal) {
        final double saturation = lightItemProperty.get().getColor().getSaturation();
        final double brightness = lightItemProperty.get().getColor().getBrightness();

        double[] rgb = colorToRgb(newVal.doubleValue(), saturation, brightness);
        sphereMaterial.setDiffuseColor(Color.hsb(newVal.doubleValue(), saturation, brightness));
        sphereMaterial.setSelfIlluminationMap(generateImage(rgb[0], rgb[1], rgb[2], 1d));
    }

    private void updateSaturation(final Observable obs, final Number oldVal, final Number newVal) {
        final double hue = lightItemProperty.get().getColor().getHue();
        final double brightness = lightItemProperty.get().getColor().getBrightness();

        double[] rgb = colorToRgb(hue, newVal.doubleValue(), brightness);
        sphereMaterial.setDiffuseColor(Color.hsb(hue, newVal.doubleValue(), brightness));
        sphereMaterial.setSelfIlluminationMap(generateImage(rgb[0], rgb[1], rgb[2], 1d));
    }

    private void updateBrightness(final Observable obs, final Number oldVal, final Number newVal) {
        final double hue = lightItemProperty.get().getColor().getHue();
        final double saturation = lightItemProperty.get().getColor().getSaturation();

        double[] rgb = colorToRgb(hue, saturation, newVal.doubleValue());
        sphereMaterial.setDiffuseColor(Color.hsb(hue, saturation, newVal.doubleValue()));
        sphereMaterial.setSelfIlluminationMap(generateImage(rgb[0], rgb[1], rgb[2], 1d));
    }

    private double[] colorToRgb(final double hue, final double saturation, final double brightness) {
        Color color = Color.hsb(hue, saturation, brightness);
        double[] rgb = new double[3];
        rgb[0] = color.getRed();
        rgb[1] = color.getGreen();
        rgb[2] = color.getBlue();
        return rgb;
    }

    public Image generateImage(double red, double green, double blue, double opacity) {
        WritableImage img = new WritableImage(1, 1);
        PixelWriter pw = img.getPixelWriter();

        Color color = Color.color(red, green, blue, opacity);
        pw.setColor(0, 0, color);
        return img;
    }

}
