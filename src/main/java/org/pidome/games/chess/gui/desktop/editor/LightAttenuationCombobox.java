/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.editor;

import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.StringConverter;
import org.pidome.games.chess.world.config.PerspectiveSceneLightAttenuationConfig;

/**
 * Combobox for light attenuation.
 *
 * @author John Sirach <john.sirach@pidome.org>
 */
public class LightAttenuationCombobox extends ComboBox<PerspectiveSceneLightAttenuationConfig.Type> {

    public LightAttenuationCombobox() {
        createFactories();
        this.getItems().addAll(PerspectiveSceneLightAttenuationConfig.Type.values());
    }

    private void createFactories() {
        setCellFactory((ListView<PerspectiveSceneLightAttenuationConfig.Type> param) -> {
            return new ListCell<PerspectiveSceneLightAttenuationConfig.Type>() {
                @Override
                public void updateItem(final PerspectiveSceneLightAttenuationConfig.Type value, final boolean empty) {
                    super.updateItem(value, empty);
                    if (value == null) {
                        setText(null);
                    } else {
                        setText(value.getLabel());
                    }
                }
            };
        });
        this.setConverter(new StringConverter<PerspectiveSceneLightAttenuationConfig.Type>() {

            @Override
            public String toString(final PerspectiveSceneLightAttenuationConfig.Type value) {
                return value == null ? null : value.getLabel();
            }

            @Override
            public PerspectiveSceneLightAttenuationConfig.Type fromString(final String string) {
                return string == null ? null : PerspectiveSceneLightAttenuationConfig.Type.valueOf(string);
            }
        });
    }

}
