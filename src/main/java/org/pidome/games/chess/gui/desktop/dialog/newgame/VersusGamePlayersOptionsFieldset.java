/*
 * Copyright (C) 2021 John Sirach <john.sirach@pidome.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.pidome.games.chess.gui.desktop.dialog.newgame;

import com.github.bhlangonijr.chesslib.Side;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.pidome.games.chess.model.game.ChessGamePlayerModel;
import org.pidome.games.chess.model.options.GameOptions.GamePlayMode;
import static org.pidome.games.chess.model.options.GameOptions.GamePlayMode.VERSUS;
import org.pidome.games.chess.model.options.AbstractInteractiveGameOptions;
import org.pidome.games.chess.resources.i18n.I18nProxy;
import org.pidome.games.chess.resources.i18n.I18nApplicationKey;
import org.pidome.games.chess.utils.controls.FieldSet;

/**
 * Fieldset for displaying player settings.
 *
 * @author johns
 */
public class VersusGamePlayersOptionsFieldset extends FieldSet {

    /**
     * The game options.
     */
    private AbstractInteractiveGameOptions options;

    /**
     * Label of player one.
     */
    private final Label playerWhiteLabel = new Label(I18nProxy.getInstance().get(I18nApplicationKey.PLAYER_WHITE).get());

    /**
     * Label of player two.
     */
    private final Label playerBlackLabel = new Label(I18nProxy.getInstance().get(I18nApplicationKey.PLAYER_BLACK).get());

    /**
     * Player one's name text field.
     */
    private final TextField playerWhite = new TextField();

    /**
     * Player two's text field.
     */
    private final TextField playerBlack = new TextField();

    /**
     * Select auto selection for color.
     */
    private final CheckBox selectColorRandom = new CheckBox(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_PLAYER_COLOR_RANDOM).get());

    /**
     * Property to control the disabling of controls.
     */
    private final BooleanProperty controlsDisabledProperty = new SimpleBooleanProperty(Boolean.FALSE);

    /**
     * Pane to put the controls on.
     */
    private final GridPane playersOptionsPane = new GridPane();

    /**
     * Constructor.
     *
     * @param options The options applicable for the players.
     */
    public VersusGamePlayersOptionsFieldset(final AbstractInteractiveGameOptions options) {
        super(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_PLAYERS_TITLE);
        this.options = options;
        this.playerWhite.disableProperty().bind(controlsDisabledProperty);
        this.options.currentGameProperty().get().getPlayerWhite().sideProperty().setValue(Side.WHITE);
        this.options.currentGameProperty().get().getPlayerBlack().sideProperty().setValue(Side.BLACK);
        this.playerBlack.disableProperty().bind(controlsDisabledProperty);
        this.selectColorRandom.disableProperty().bind(this.controlsDisabledProperty);
        constructContent();
        validProperty().bind(Bindings.and(
                playerWhite.textProperty().isNotEmpty(),
                playerWhite.textProperty().isNotEmpty()));
    }

    /**
     * Constructs the content.
     */
    private void constructContent() {
        this.playersOptionsPane.setVgap(10);
        this.playersOptionsPane.setHgap(10);

        composePlayersFields();

        this.setContent(this.playersOptionsPane);

        adjustToGameEngineOptions(null, null, this.options.gamePlayModeProperty().get());
        this.options.gamePlayModeProperty().addListener(this::adjustToGameEngineOptions);
    }

    /**
     * Displays fields as such it corresponds to engine supported options.
     *
     * @param observable The observable set.
     * @param oldMode The previous mode.
     * @param newMode The newly set mode.
     */
    private void adjustToGameEngineOptions(final Observable observable, final GamePlayMode oldMode, final GamePlayMode newMode) {
        switch (newMode) {
            case VERSUS:
            case BROADCASTER:
                playerWhite.textProperty().setValue("Player 1");
                playerBlack.textProperty().setValue("Player 2");
                break;
            case SPECTATOR:
            case PLAYER:
                playerWhite.textProperty().setValue("Player white");
                playerBlack.textProperty().setValue("Player black");
                break;
        }
    }

    /**
     * Creates the players fields and binds the values.
     */
    private void composePlayersFields() {
        playerWhite.textProperty().bindBidirectional(this.options.currentGameProperty().get().getPlayerWhite().nameProperty());
        playerBlack.textProperty().bindBidirectional(this.options.currentGameProperty().get().getPlayerBlack().nameProperty());

        final Label title = new Label(I18nProxy.getInstance().get(I18nApplicationKey.NEW_GAME_OPTIONS_CATEGORY_GAME_ENGINE_VERSUS_PLAYERS_NAMES).get());
        title.getStyleClass().add("text-underline");
        this.playersOptionsPane.add(title, 0, 0, 2, 1);
        this.playersOptionsPane.add(playerWhiteLabel, 0, 1);
        this.playersOptionsPane.add(playerWhite, 1, 1);
        this.playersOptionsPane.add(playerBlackLabel, 0, 2);
        this.playersOptionsPane.add(playerBlack, 1, 2);
        this.playersOptionsPane.add(selectColorRandom, 1, 3);
    }

    /**
     * The white side player.
     *
     * @return Player of white side.
     */
    public TextField playerWhiteName() {
        return this.playerWhite;
    }

    /**
     * The black side player.
     *
     * @return Player of black side.
     */
    public TextField playerBlackName() {
        return this.playerBlack;
    }

    /**
     * Sets the newly toggle.
     *
     * @param observableValue The observable value, may be null.
     * @param previousToggle The previously selected toggle, may be null.
     * @param newToggle The toggle to select.
     */
    private void setColorSelection() {
        if (selectColorRandom.selectedProperty().get() == true) {
            ChessGamePlayerModel whitePlayer = this.options.currentGameProperty().get().getPlayerWhite();
            ChessGamePlayerModel blackPlayer = this.options.currentGameProperty().get().getPlayerBlack();
            if (Math.random() > .5) {
                this.options.currentGameProperty().get().setPlayerWhite(blackPlayer);
                this.options.currentGameProperty().get().setPlayerBlack(whitePlayer);
            }
        }
    }

    /**
     * Performed when the user has clicked ok in the game start options screen.
     */
    @Override
    public final void onAction() {
    }

    /**
     * Called when the fieldset is closed.
     */
    @Override
    public void dispose() {
        playerWhite.textProperty().unbindBidirectional(this.options.currentGameProperty().get().getPlayerWhite().nameProperty());
        playerBlack.textProperty().unbindBidirectional(this.options.currentGameProperty().get().getPlayerBlack().nameProperty());
        setColorSelection();
        clearGrid();
        this.options.gamePlayModeProperty().removeListener(this::adjustToGameEngineOptions);
        this.options = null;
        validProperty().unbind();
    }

    /**
     * Sets the input controls disabled.
     *
     * @param disabled true to disable, false to enable.
     */
    public final void setControlsDisabled(boolean disabled) {
        this.controlsDisabledProperty.setValue(disabled);
    }

    /**
     * Clears the grid.
     */
    private void clearGrid() {
        this.playersOptionsPane.getChildren().clear();
    }

}
